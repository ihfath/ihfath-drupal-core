<?php

// Install script for Ihfath Messaging
//
// Prefix (For autocomplete): ihfathmsg_

function ihfathmsg_schema() {
  $schema = array();
  
  $schema["ihfath_msg"] = array(
    "description" => "{ihfath_msg} holds message information",
    "fields"      => array(
      "mid" => array(
        "description" => "Message ID",
        "type"        => "serial",
        "not null"    => TRUE,
        "unsigned"    => TRUE
      ),
      "mrid" => array(
        "description" => "ID of message that this is a response to",
        "type"        => "int",
        "unsigned"    => TRUE
      ),
      "userrole" => array(
        "description" => "Ihfath user role",
        "type"        => "int",
        "not null"    => TRUE,
        "unsigned"    => TRUE,
        "size"        => "small"
      ),
      "suid" => array(
        "description" => "UID of the sender",
        "type"        => "int",
        "not null"    => TRUE,
        "unsigned"    => TRUE
      ),
      "ruid" => array(
        "description" => "UID of the reciever",
        "type"        => "int",
        "not null"    => FALSE,
        "unsigned"    => TRUE
      ),
      "category" => array(
        "description" => "Lesson category, see Ihfathmsg/CATEGORY",
        "type"        => "int",
        "not null"    => TRUE,
        "unsigned"    => TRUE,
        "size"        => "small",
        "default"     => 0
      ),
      "viewed" => array(
        "description" => "0:Not Viewed or 1:Viewed - Whether a message has been viewed",
        "type"        => "int",
        "not null"    => TRUE,
        "unsigned"    => TRUE,
        "size"        => "tiny",
        "default"     => 0
      ),
      "subject" => array(
        "description" => "Subject of the message",
        "type"        => "varchar",
        "length"      => 255,
        "not null"    => TRUE
      ),
      "body" => array(
        "description" => "Body of the message",
        "type"        => "text",
        "not null"    => TRUE,
        "size"        => "big"
      ),
      "timestamp" => array(
        "description" => "Time the message was sent",
        "type"        => "int",
        "not null"    => TRUE,
        "unsigned"    => TRUE
      ),
      "attachments" => array(
        "description" => "Number of file attachments",
        "type"        => "int",
        "not null"    => TRUE,
        "unsigned"    => TRUE,
        "default"     => 0
      )
    ),
    "primary key" => array("mid"),
    "indexes"     => array(
      "received" => array("viewed", "category", "userrole", "ruid", "timestamp"),
      "sent"     => array("viewed", "category", "userrole", "suid", "timestamp"),
    )
  );
  
  $schema["ihfath_msg_addin"] = array(
    "description" => "{ihfath_msg_addin} holds arbitrary lesson metadata",
    "fields"      => array(
      "maid" => array(
        "description" => "Message addin ID",
        "type"        => "serial",
        "not null"    => TRUE,
        "unsigned"    => TRUE,
      ),
      "mid" => array(
        "description" => "Message ID",
        "type"        => "int",
        "not null"    => TRUE,
        "unsigned"    => TRUE,
      ),
      "category" => array(
        "description" => "Lesson category, see Ihfathmsg/CATEGORY",
        "type"        => "int",
        "not null"    => TRUE,
        "unsigned"    => TRUE,
        "size"        => "small",
      ),
      "baseint" => array(
        "description" => "Indexed addin numerical value",
        "type"        => "int",
        "not null"    => TRUE,
        "unsigned"    => TRUE,
        "default"     => 0,
      ),
      "secondint" => array(
        "description" => "Second indexed addin numerical value",
        "type"        => "int",
        "not null"    => TRUE,
        "unsigned"    => TRUE,
        "default"     => 0,
      ),
      "txtval" => array(
        "description" => "Addin textual value",
        "type"        => "varchar",
        "not null"    => TRUE,
        "length"      => 255,
      ),
    ),
    "primary key" => array("maid"),
    "indexes"     => array(
      "generic" => array("mid", "category"),
      "filter"  => array("category", "baseint", "secondint"),
    ),
  );
  
  $schema["ihfath_msg_attach"] = array(
    "description" => "{ihfath_msg_attach} holds message attachments",
    "fields"      => array(
      "aid" => array(
        "description" => "Attachment ID",
        "type"        => "serial",
        "not null"    => TRUE,
        "unsigned"    => TRUE
      ),
      "proxy" => array(
        "description" => "Proxy attachment ID",
        "type"        => "int",
        "not null"    => TRUE,
        "unsigned"    => TRUE,
        "default"     => 0
      ),
      "mid" => array(
        "description" => "Message ID",
        "type"        => "int",
        "not null"    => TRUE,
        "unsigned"    => TRUE
      ),
      "suid" => array(
        "description" => "UID of the sender",
        "type"        => "int",
        "not null"    => TRUE,
        "unsigned"    => TRUE
      ),
      "file_hash" => array(
        "description" => "File hash",
        "type"        => "varchar",
        "not null"    => TRUE,
        "length"      => 255
      ),
      "file_name" => array(
        "description" => "File name",
        "type"        => "varchar",
        "not null"    => TRUE,
        "length"      => 255
      ),
      "file_size" => array(
        "description" => "Size of the file in bytes",
        "type"        => "int",
        "not null"    => TRUE,
        "unsigned"    => TRUE,
        "default"     => 0
      )
    ),
    "primary key" => array("aid"),
    "indexes"     => array(
      "msg_attached"  => array("mid"),
      "attach_sender" => array("suid"),
      "file_hash"     => array("file_hash"),
    )
  );
  
  return $schema;
}
