<?php
global $base_url;

$list_role    = $form["#list_role"];
$student_view = $list_role === 3;
$users        = ihfathmsg_admin_get_users($list_role);

?>
<!-- Mod Background -->
<?php
$user_view_role = $list_role === 2 ? "Professors'" : "Students'";

// Set page title
drupal_set_title(t("{$user_view_role} Messages"));

$navblock = module_invoke("mod_background_page", "block_view", "mod_block_background");
echo render($navblock);

$unread_prof_count = Ihfathmsg\Get::count(FALSE, array(
  Ihfathmsg\Category::Report,
  Ihfathmsg\Category::LessonNote,
  Ihfathmsg\Category::Basic,
), 2);

$unread_stud_count = Ihfathmsg\Get::count(FALSE, array(
  Ihfathmsg\Category::Report,
  Ihfathmsg\Category::LessonNote,
  Ihfathmsg\Category::Basic,
), 3);
?>

<!-- User list -->
<div class="section ihfath-small-section">
  <div class="container ihfath-wrap">
    <!-- Professor/Student Tabs -->
    <div class="ihfathmsg-list-tabs">
      <i>
        <a href="<?php echo $base_url . "/ihfath/messages" ?>" <?php echo $student_view ? "" : 'class="main-bg"'; ?>>
          <div class="ihfath-msg-tab-contents">
            <?php echo t("Professors"); ?>
            <?php
            if ($unread_prof_count) { ?>

              <div class="ihfathmsg-unread-count ihfathmsg-full-staff-count" data-staff-count="<?php echo $unread_prof_count; ?>">
                <span><?php echo $unread_prof_count; ?></span>
              </div>

            <?php
            } ?>
          </div>
        </a><!--
     --><a href="<?php echo $base_url . "/ihfath/messages/students" ?>" <?php echo $student_view ? 'class="main-bg"' : ""; ?>>
          <div class="ihfath-msg-tab-contents">
            <?php echo t("Students"); ?>
            <?php
            if ($unread_stud_count) { ?>

              <div class="ihfathmsg-unread-count ihfathmsg-full-stud-count" data-staff-count="<?php echo $unread_stud_count; ?>">
                <span><?php echo $unread_stud_count; ?></span>
              </div>

            <?php
            } ?>
          </div>
        </a>
      </i>
    </div>

    <hr>

    <div id="ihfathmsg-admin-staff-cards" class="ihfathmsg-admin-view">
      <?php
      foreach ($users as $uid => $user) {
        // Get staff count
        $user_count = $user->msg_count;
        $user_nil   = "";

        if (!$user_count) {
          $user_count = 0;
          $user_nil   = " ihfathmsg-staff-count-nil";
        } ?>

        <a href="<?php echo $base_url; ?>/ihfath/messages/<?php echo $uid; ?>" class="ihfathmsg-admin-staff-card-anchor">
          <div class="ihfathmsg-admin-staff-card">
            <div class="ihfathmsg-pretty-image"></div>
            <div class="ihfathmsg-staff-name">
              <?php echo $user->name; ?>
            </div>
            <div class="ihfathmsg-unread-count ihfathmsg-staff-count-<?php echo $uid . $user_nil ?>" data-staff-count="<?php echo $user_count; ?>">
              <span><?php echo $user_count; ?></span>
            </div>
          </div>
        </a>

      <?php
      } ?>
    </div>

    <div class="ihfath-pager">
      <?php echo theme("pager"); ?>
    </div>
  </div>
</div>

<style media="screen">
  .ihfathmsg-list-tabs {
    text-align: center;
    margin: 20px 0px;
  }

  .ihfath-msg-tab-contents {
    display: inline-block;
  }

  .container.ihfath-wrap .ihfathmsg-list-tabs i {
    position: relative;
    z-index: 10;
    display: inline-block;
    vertical-align: middle;
    padding: 0px 0px;
    width: auto;
  }

  .ihfathmsg-list-tabs i a:not(.main-bg) {
    background: #F2F2F2;
  }

  .ihfathmsg-list-tabs i a {
    position: relative;
    display: inline-block;
    vertical-align: top;
    padding: 0px 15px;
    height: 45px;
    width: 160px;

    font-style: normal;
    text-align: center;
    line-height: 45px;
  }

  .ihfathmsg-list-tabs i a:first-child {
    border-top-left-radius: 45px;
    border-bottom-left-radius: 45px;
  }

  .ihfathmsg-list-tabs i a:last-child {
    border-top-right-radius: 45px;
    border-bottom-right-radius: 45px;
  }

  .ihfathmsg-list-tabs i a.main-bg {
    font-weight: bold;
  }

  /* Section */
  .ihfath-small-section {
    padding: 50px 0px;
  }
</style>
