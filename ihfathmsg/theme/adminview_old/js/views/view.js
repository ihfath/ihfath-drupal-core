function View() {
  lces.types.component.call(this);
  var that = this;
  
  this.main = jSh.d(".ihfath-msg-view");
  this.variants = {};
  
  // State
  this.setState("variant", null);
  this.addStateListener("variant", function(variant) {
    var prevVariant = this.oldStateStatus ? that.variants[this.oldStateStatus] : null;
    var nextVariant;
    
    if (!(nextVariant = this.variants[variant])) {
      throw new ReferenceError("Ihfathmsg: No such view variant `" + variant + "`");
    }
    
    if (prevVariant) {
      // Hide the previous variant
      prevVariant.element.classList.add("ihfath-mv-hidden");
    }
    
    // Show next variant
    nextVariant.element.classList.remove("ihfath-mv-hidden");
  });
}

View.prototype.addVariant = function(name, element) {
  element.classList.add(".ihfath-msg-view-variant");
  element.classList.add(".ihfath-mv-hidden");
  
  this.variants[name] = {
    name: name,
    element: element
  };
  
  // Add to main
  this.main.appendChild(element);
};

jSh.inherit(View, lces.types.component);
module.exports = View;
