// Ihfath Professor Current Schedule Implementation with Cellidi
var Modal = require("./modal");

function IhfathCurrentProfSchedule(args) {
  var programMap = {};

  // Map program PIDs to index numbers
  args.programs.forEach(function(program, i_p) {
    programMap[program.pid] = i_p;
  });

  // Create wrapper
  var wrapper = jSh("#ihfath-checkout-schedule-root");
  var options = {
    element: wrapper,

    dimensions: args.dimensions,

    offsetX: 0,
    offsetY: 0,

    canSelect: false,
    history: true,

    headers: {
      top: scheduleOnNewDayHeaderCell,
      left: scheduleOnNewHourHeaderCell
    },

    newCell: scheduleOnNewCell,
    onCellStateChange: scheduleOnStateChange,

    emptyState: function(renew, old) {
      if (renew) {
        return {
          dur: old.dur,
          pid: old.pid,
          hour: old.hour
        };
      } else {
        return {
          dur: 0,
          pid: 0,
          hour: null
        };
      }
    },

    diffStates: function(a, b) {
      return a.dur !== b.dur || a.pid !== b.pid;
    }
  };

  // Clear wrapper
  wrapper.innerHTML = "";

  // Create CellDisplay
  var CellDisplay = Cellidi.CellDisplay;
  var schedule    = Cellidi(options, CellDisplay.mapEachCell(args.data.table, function(data) {
    return {
      dur: 0,
      pid: 0,
      hour: data
    };
  }));

  // Cellidi Callbacks
  function scheduleOnNewCell(x, y, virtualX, virtualY, data, cellDisplay) {
    // Make cell time in 12hour format
    var yMap         = virtualY === 0 ? 12 : virtualY;
    var cellTime     = virtualY < 12 ? [yMap, "AM"] : [(virtualY === 12 ? 12 : yMap - 12), "PM"];
    var cellTimeFull = cellTime.join("");
    var cell;
    var checker = y % 2;

    if (data.hour !== 1 /* Vacant */) {
      var bookedMap = {
        2: ".ihfath-booked-cell",      // Booked
        4: ".ihfath-user-booked-cell", // Booked by the same user in the cart
      };
      var bookedHour = bookedMap[data.hour] || "";

      // It's a weekend cell, return dead cell
      cell = jSh.d(bookedHour + (checker ? ".ihfath-cell-checker" : ""), null, [
        jSh.c("span", ".ihfath-time", cellTime[0]),
        jSh.c("span", ".ihfath-time-meta", cellTime[1])
      ]);

      if (data.hour === 4) {
        cell.title = args.locale.bookedByUser;
      }

      return {
        dom: cell,
        deadCell: true
      };
    }

    var content;
    var extraClass = "";
    var cellLink   = "javascript:void(0)";
    var element    = "div";

    if (isNaN(data.hour)) {
      // It's data with lessons
      element = "a";

      content = [

      ];

      extraClass = ".ihfath-full-cell";
    } else {
      // Nothing on this day
      var lessonDetails = jSh.c("span", ".ihfath-lesson-details");
      content = [
        jSh.c("span", ".ihfath-time", cellTime[0]),
        jSh.c("span", ".ihfath-time-meta", cellTime[1]),
        lessonDetails,
        jSh.d("ihfath-fill-indicator"),
        jSh.d("ihfath-selection-indicator")
      ];

      extraClass = ".ihfath-empty-cell";
    }

    cell = jSh.c(element, {
      prop: {
        href: cellLink,
        title: args.time + " " + cellTimeFull + ", " + args.dayMap[virtualX]
      },

      sel: extraClass + (checker ? ".ihfath-cell-checker" : ""),
      child: content
    });

    return {
      dom: cell,
      domLesson: lessonDetails
    };
  }

  function scheduleOnNewDayHeaderCell(x, virtualX, orientation, cellDisplay) {
    return jSh.d(null, args.dayMap[virtualX]);
  }

  function scheduleOnNewHourHeaderCell(y, virtualY, orientation, cellDisplay) {
    var yMap     = virtualY === 0 ? 12 : virtualY;
    var cellTime = virtualY < 12 ? [yMap, "AM"] : [(virtualY === 12 ? 12 : yMap - 12), "PM"];

    var checker = y % 2;

    return jSh.d(".main-color" + (checker ? ".ihfath-cell-checker" : ""), null, [
      jSh.t(cellTime[0]),
      jSh.c("span", null, cellTime[1])
    ]);
  }

  function scheduleOnStateChange(state, cellModels) {
    for (var i=0; i<state.length; i++) {
      var newState = state[i];
      var model    = cellModels[i];
      var dom      = model.dom;

      dom.classList.remove("ihfath-full");
      dom.classList.remove("ihfath-half-full");

      if (newState.pid !== 0) {
        switch (newState.dur) {
          case 0.5:
            dom.classList.add("ihfath-half-full");
            break;
          case 1:
          case 1.5:
          case 2:
            dom.classList.add("ihfath-full");
            break;
        }

        dom.classList.remove("ihfath-empty-cell");
        if (newState.pid === -1) {
          model.domLesson.innerHTML = "";
        } else {
          const hours = Math.floor(newState.dur);
          const minutes = Math.floor((newState.dur - hours) * 60) + "";

          model.domLesson.innerHTML = "";
          model.domLesson.appendChild([
            jSh.t(args.programs[programMap[newState.pid]].data.en.name),
            jSh.c("span", ".main-color", " " + hours + ":" + minutes.padStart(2, "0"))
          ]);
        }
      } else {
        dom.classList.add("ihfath-empty-cell");
      }
    }
  }

  // Create modal
  var modalPrograms = args.programs;
  var modalHours = args.hours;
  var modal = schedule.modal = new Modal(schedule, args.locale, args.curProgramId, args.hours);

  // State
  var curCell   = null;
  var fullCells = schedule.mapEachCell(schedule._cellMap, function(cell) {
    if (cell.deadCell) {
      return null;
    } else {
      return false;
    }
  });

  // Update `fullCells` on rebuild
  schedule.on("rebuild", function() {
    fullCells = schedule.mapEachCell(schedule._cellMap, function(cell) {
      if (cell.deadCell) {
        return null;
      } else {
        return false;
      }
    });
  });

  // Create events
  schedule.on("activeinput", function(e) {
    var model;

    if (model = e.cell) {
      // Prevent default browser text selection behaviour
      e.event.preventDefault();

      // Update UI
      var dom      = model.dom;
      var coords   = model.coords;
      var state    = schedule.getCellState(model);
      var nextCell = false;

      if (fullCells[coords[0]][coords[1]] === true) {
        // Get upper cell
        model    = schedule._cellMap[coords[0]][coords[1] - 1];
        dom      = model.dom;
        state    = schedule.getCellState(model);
        nextCell = true;
      } else if (fullCells[coords[0]][coords[1] + 1] !== null && fullCells[coords[0]][coords[1] + 1] !== undefined) {
        // This cell has a free cell beneath it
        nextCell = true;
      }

      // Only update modal if we've truly selected a different cell
      if (curCell !== model) {
        var curCellClass = "ihfath-cell-selected";

        // Unhighlight the previously selected cell
        if (curCell) {
          curCell.dom.classList.remove(curCellClass);
        }

        // Highlight the newly selected cell
        curCell = model;
        curCell.dom.classList.add(curCellClass);

        // Update modal
        modal.set(state.dur === 0 ? -1 : state.pid, state.dur, dom, nextCell);
      }
    }
  });

  var apiSetting = false;
  modal.onChange(function(programPid, hours) {
    if (hours === 0 || programPid === -1) {
      // Guard
      apiSetting = true;

      // Clear cell
      clearCell();

      apiSetting = false;
    } else {
      // Update cell
      updateCell(programPid, hours);
    }
  });

  // Remove highlight from curCell when modal is closed
  modal._state.addStateListener("visible", function(visible) {
    if (!visible) {
      var curCellClass = "ihfath-cell-selected";

      // Unhighlight the previously selected cell
      if (curCell) {
        curCell.dom.classList.remove(curCellClass);
      }

      curCell = null;
    }
  });

  function clearCell() {
    var cellState = schedule.getCellState(curCell);
    var editCells = [[curCell.coords[0], curCell.coords[1]]];

    if (cellState.dur > 1) {
      editCells.push([curCell.coords[0], curCell.coords[1] + 1]);
      fullCells[curCell.coords[0]][curCell.coords[1] + 1] = false;
    }

    schedule.changeSubset(editCells, function(x, y, data) {
      return {
        pid: 0,
        dur: 0,
        hour: data.hour
      };
    });
  };

  function updateCell(programPid, hours) {
    var dur = hours;
    var pid = programPid;

    var x = curCell.coords[0];
    var y = curCell.coords[1];

    var cellState = schedule.getCellState(curCell);
    var height    = schedule.options.dimensions[1];
    var editCells = [];

    // Cancel if no program in cell
    // if (cellState.pid === 0) {
    //   return;
    // }

    // Set new cell states
    for (var i=0; i<height; i++) {
      editCells.push([x, i]);

      switch (i) {
        case y + 1:
          if (dur > 1) {
            fullCells[x][i] = true;
          } else {
            // Mark this cell as available if it's not a dead cell
            if (fullCells[x][i] !== null) {
              fullCells[x][i] = false;
            }
          }
          break;
        default:
          // Mark this cell as available if it's not a dead cell
          if (fullCells[x][i] !== null) {
            fullCells[x][i] = false;
          }
      }
    }

    schedule.changeSubset(editCells, function(cellX, cellY, data) {
      if (dur === 1 || dur === 0.5) {
        if (y === cellY) {
          return {
            dur: dur,
            pid: programPid,
            hour: data.hour
          };
        } else {
          return {
            dur: 0,
            pid: 0,
            hour: data.hour
          };
        }
      } else {
        switch (cellY) {
          case y:
            return {
              dur: dur,
              pid: programPid,
              hour: data.hour
            };
            break;
          case y + 1:
            return {
              dur: dur - 1,
              pid: -1,
              hour: data.hour
            };
            break;
          default:
            return {
              dur: 0,
              pid: 0,
              hour: data.hour
            };
            break;
        }
      }
    });
  }

  // Data change updates
  schedule.on("newstate", function() {
    updateHiddenLessonsInput();

    if (!apiSetting) {
      updateModal();
    }
  });

  function updateModal(newCell, pid, hour) {
    var cell = newCell || curCell;

    var newPid  = pid;
    var newHour = hour;

    if (cell) {
      var cellState     = schedule.getCellState(cell);
      var coords        = cell.coords;
      var nextCell      = false;
      var nextCellValue = fullCells[coords[0]][coords[1] + 1];

      if (pid === undefined) newPid = cellState.pid;
      if (hour === undefined) newHour = cellState.dur;

      var dom = cell.dom;

      if (nextCellValue !== null && nextCellValue !== undefined) {
        nextCell = true;
      }

      // Update modal
      modal.set(newPid, newHour, dom, nextCell);
      return cell;
    } else {
      return false;
    }
  }

  // Schedule informing outside world of updates
  schedule.addEvent("update");
  function updateHiddenLessonsInput() {
    var table  = schedule.state;
    var width  = schedule.options.dimensions[0];
    var height = schedule.options.dimensions[1];

    var lessons = [];

    for (var x=0; x<width; x++) {
      for (var y=0; y<height; y++) {
        var cell = table[x][y];

        if (cell.pid > 0) {
          var hour = y;

          if (hour !== 0) {
            hour *= 10;
          }

          // Add this lesson to the list
          lessons.push({
            weektime: ((x + 1) * 1000) + hour,
            dur: cell.dur * 60,
            pid: cell.pid
          });

          if (cell.dur > 1) {
            // The next cell is also occupied with this class, skip it
            y++;
          }
        }
      }
    }

    schedule.triggerEvent("update", {
      lessons: lessons
    });
  }

  return {
    schedule: schedule,
    hideModal: function() {
      modal._state.visible = false;
    }
  };
}

module.exports = IhfathCurrentProfSchedule;
