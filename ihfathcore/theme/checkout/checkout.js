(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
var Schedule = require("./schedule");
var Stages = require("./stages");

function main(config) {
  var schedule = Schedule(config.schedule);
  
  // Start schedules
  Stages(config, schedule);
}

window.IhfathCheckout = main;

},{"./schedule":3,"./stages":4}],2:[function(require,module,exports){
function Modal(cd, locale, curProgramId, hours) {
  var state = this._state = lces.new();
  var that = this;

  // Set initial state values
  state.setState("program", -1);
  state.setState("hours", 1);
  state.setState("visible", false);
  state.setState("cell", null);
  state.potentialSize = false;
  state.curProgramId = curProgramId;
  state.apiSetting = false;
  state.hourList = null;

  // Example:
  // this.setProgramList([
  //   ["Nahw", 3],
  //   ["Sarf", 4],
  //   ["Balaagha", 5]
  // ]);
  this.setProgramList([]);

  // Create dom
  var dom = this._dom = this._makeDom(locale);

  // Add program list
  dom.programList.appendChild(dom.programListArr = this._makeDomProgramList(state.programList));

  // Add state bindings
  state.addStateListener("program", function(program) {
    that._renderProgramList(this.oldStateStatus, program, this);

    // Update API-set listeners
    if (!state.apiSetting && that._onChangeCb) {
      that._onChangeCb(state.program, state.hours);
    }
  });

  // Prevent setting the program getting ignored when
  // hours is zero
  state.addStateCondition("program", function(program) {
    if (!state.apiSetting) {
      if (state.hours === 0) {
        state.states.hours.stateStatus = 1;
        that._updateHourUI(1);
      }
    }

    return true;
  });

  state.addStateListener("hours", function(hours) {
    if (hours === 0) {
      // Implicitly reset `program` state
      var programObj = state.states.program;
      var newProgram = -1;
      var oldProgram = state.program;

      programObj.oldStateStatus = oldProgram;
      programObj.stateStatus = newProgram;

      console.log("HOURS: ", hours);
      that._renderProgramList(oldProgram, newProgram, this);
    }

    that._updateHourUI(hours);

    // Update API-set listeners
    if (!state.apiSetting && that._onChangeCb) {
      that._onChangeCb(state.program, state.hours);
    }
  });

  state.addStateListener("cell", function(cell) {
    // Center modal
    var modal = dom.main;

    that._centerModal(modal, cell);
    state.states.visible.oldStateStatus = state.visible;
    state.states.visible.stateStatus = true;
  });

  state.addStateListener("visible", function(visible) {
    var modal = dom.main;

    if (visible) {
      that._centerModal(modal, state.cell);
    } else {
      modal.style.left = dom.invisibleOffset + "px";
      modal.style.top  = dom.invisibleOffset + "px";
    }
  });

  // Set possible hours
  // TODO: Get this from... Somewhere
  this.setHourList(hours);
}

// Setters
Modal.prototype.setHourList = function(list) {
  this._state.hourList = this._serializeHourList(list);
  this._state.hours = this._hourChange(0, true);
};

Modal.prototype.setProgramList = function(list) {
  var state = this._state;

  state.programList = list;
  state.programListMap = {};
  state.programList.forEach(function(program, i) {
    state.programListMap[program[1]] = i;
  });

  // Rerender DOM list
  if (this._dom) {
    this._dom.programList.innerHTML = "";
    this._dom.programList.appendChild(this._dom.programListArr = this._makeDomProgramList(state.programList));
  }
};

// Dom events
Modal.prototype._renderProgramList = function(oldProgram, program, stateObj) {
  var state = this._state;
  var programList = this._dom.programListArr;
  var className = "ihfath-cm-selected";
  // var oldProgramIndex = stateObj.stateStatus;

  // Clear old
  if (oldProgram !== -1) {
    var oldIndex = state.programListMap[oldProgram];
    programList[oldIndex !== undefined ? oldIndex : 0].classList.remove(className);
  }

  // Set new
  if (program !== -1) {
    var newIndex = state.programListMap[program];
    programList[newIndex !== undefined ? newIndex : 0].classList.add(className);
  }
};

// Dom building
Modal.prototype._makeDom = function(locale) {
  var state = this._state;
  var that = this;

  // Create programs widget
  var programList = jSh.d({
    sel: ".ihfath-cm-program-list.ihfath-cm-column",
    events: {
      click: function(e) {
        var target = e.target;

        if ("ihfathProgramPid" in target) {
          // Set pid
          state.program = target.ihfathProgramPid;
        }
      }
    }
  });

  // Create hour widget
  var hourInput = jSh.c("input", {
    sel: ".ihfath-hours-input",
    events: {
      change: function() {
        var hours = that._hourChange(null, null, parseFloat(this.value));
        this.value = hours;
      }
    },
    prop: {
      type: "text"
    }
  });
  var hourButtons = jSh.d(".ihfath-cm-hour-btns", null, [
    jSh.d({
      sel: ".ihfath-cm-hour-btn.ihfath-cm-hour-up.main-bg",
      child: jSh.c("i", ".fa.fa-angle-up"),
      events: {
        click: function() {
          state.hours = that._hourChange(1);
        }
      }
    }),

    jSh.d({
      sel: ".ihfath-cm-hour-btn.ihfath-cm-hour-down.main-bg",
      child: jSh.c("i", ".fa.fa-angle-down"),
      events: {
        click: function() {
          state.hours = that._hourChange(-1);
        }
      }
    }),
  ]);

  // Clear button
  var clearButton = jSh.d({
    sel: ".ihfath-cm-clear-btn",
    text: locale.clearCell,
    events: {
      click: function() {
        // Clear state
        state.hours = 0;
      }
    }
  });

  // Close button
  var closeButton = jSh.d({
    sel: ".ihfath-cm-close-btn",
    text: "×",
    events: {
      click: function() {
        state.visible = false;
      }
    }
  });

  var hourWrap = jSh.d(".ihfath-cm-hour.ihfath-cm-column", null, [
    jSh.d(".ihfath-cm-hour-top", null, [
      hourInput,
      jSh.d(".ihfath-cm-hour-caption", null, [
        jSh.c("span", null, locale.hours)
      ]),
      hourButtons
    ]),
    jSh.d(".ihfath-cm-hour-bottom", null, [
      clearButton,
      closeButton
    ])
  ]);

  // Create main
  var main = jSh.d(".ihfath-checkout-modal", null, [
    programList,
    hourWrap
  ]);

  // Add to document
  document.body.appendChild(main);

  // Set "invisible" initially
  var invisibleOffset = -10000;
  main.style.left = invisibleOffset + "px";
  main.style.top  = invisibleOffset + "px";

  return {
    main: main,
    invisibleOffset: invisibleOffset,
    programList: programList,
    hourInput: hourInput
  };
};

Modal.prototype._updateHourUI = function(hoursFull) {
  const hours = Math.floor(hoursFull);
  const minutes = Math.floor((hoursFull - hours) * 60) + "";

  this._dom.hourInput.value = hours + ":" + minutes.padStart(2, "0");
};

Modal.prototype._makeDomProgramList = function(list) {
  var dom = list.map(function(program, i) {
    return jSh.d({
      sel: ".ihfath-cm-program-item.main-color", //  + (i > 0 ? "" : ".ihfath-cm-selected"),
      text: program[0],
      prop: {
        ihfathProgramPid: program[1]
      }
    });
  });

  return dom;
};

Modal.prototype._centerModal = function(modal, cell) {
  var cellRect = cell && cell.getBoundingClientRect() || {
    // Kinda "center" the modal when no cell is selected
    top: innerHeight / 2,
    left: 0,
    right: innerWidth,
    bottom: innerHeight
  };
  var modalRect = modal.getBoundingClientRect();

  // Update position
  var modalHeight = modalRect.bottom - modalRect.top;
  var modalWidth  = modalRect.right - modalRect.left;
  var cellWidth   = cellRect.right - cellRect.left;

  var borderWidth = 0; // Turns out border was just an outline, thus zero for now
  modal.style.left = (cellRect.left + scrollX + (cellWidth / 2) - (modalWidth / 2)) + "px";
  modal.style.top = ((cellRect.top + scrollY) - modalHeight - borderWidth) + "px";
};

Modal.prototype._serializeHourList = function(list) {
  var mapping = {};
  var serialized = {
    list: list,
    mapping: mapping
  };

  for (var i=0; i<list.length; i++) {
    mapping[list[i]] = i;
  }

  return serialized;
};

// Filter and process the user inputted hour
Modal.prototype._hourChange = function(indexDirection, absolute, directMap) {
  var state = this._state;
  var hours = state.hourList;
  var oldHourIndex = hours.mapping[state.hours];
  var newHourIndex = 0;
  var value = null;

  if (directMap) {
    var index = hours.mapping[directMap];

    // Return a direct map
    value = hours.list[index === undefined ? oldHourIndex : index];
  } else if (!absolute) {
    // Get value via relative distance
    newHourIndex = hours.mapping[state.hours] + indexDirection;
    value = hours.list[Math.max(Math.min(newHourIndex, hours.list.length - 1), 0)];
  }

  if (value) {
    state.setState("program", state.curProgramId);
  }

  console.log("VALUE:", value, "POTENTIAL:", state.potentialSize);
  return state.potentialSize ? value : Math.min(value, 1);
};

Modal.prototype.onChange = function(cb) {
  this._onChangeCb = cb; // cb(this._state.program, this._state.hours);
};

Modal.prototype.set = function(program, hours, dom, potentialSize) {
  // Add setting guard
  this._state.apiSetting = true;

  this._state.program = program;
  this._state.hours = hours;

  // Remove setting guard
  this._state.apiSetting = false;

  // Set potentialSize
  console.log("POTENTIAL:", potentialSize);
  this._state.potentialSize = potentialSize;

  // Center and display modal
  this._state.cell = dom;
  this._state.visible = true;
};

module.exports = Modal;

},{}],3:[function(require,module,exports){
// Ihfath Professor Current Schedule Implementation with Cellidi
var Modal = require("./modal");

function IhfathCurrentProfSchedule(args) {
  var programMap = {};

  // Map program PIDs to index numbers
  args.programs.forEach(function(program, i_p) {
    programMap[program.pid] = i_p;
  });

  // Create wrapper
  var wrapper = jSh("#ihfath-checkout-schedule-root");
  var options = {
    element: wrapper,

    dimensions: args.dimensions,

    offsetX: 0,
    offsetY: 0,

    canSelect: false,
    history: true,

    headers: {
      top: scheduleOnNewDayHeaderCell,
      left: scheduleOnNewHourHeaderCell
    },

    newCell: scheduleOnNewCell,
    onCellStateChange: scheduleOnStateChange,

    emptyState: function(renew, old) {
      if (renew) {
        return {
          dur: old.dur,
          pid: old.pid,
          hour: old.hour
        };
      } else {
        return {
          dur: 0,
          pid: 0,
          hour: null
        };
      }
    },

    diffStates: function(a, b) {
      return a.dur !== b.dur || a.pid !== b.pid;
    }
  };

  // Clear wrapper
  wrapper.innerHTML = "";

  // Create CellDisplay
  var CellDisplay = Cellidi.CellDisplay;
  var schedule    = Cellidi(options, CellDisplay.mapEachCell(args.data.table, function(data) {
    return {
      dur: 0,
      pid: 0,
      hour: data
    };
  }));

  // Cellidi Callbacks
  function scheduleOnNewCell(x, y, virtualX, virtualY, data, cellDisplay) {
    // Make cell time in 12hour format
    var yMap         = virtualY === 0 ? 12 : virtualY;
    var cellTime     = virtualY < 12 ? [yMap, "AM"] : [(virtualY === 12 ? 12 : yMap - 12), "PM"];
    var cellTimeFull = cellTime.join("");
    var cell;
    var checker = y % 2;

    if (data.hour !== 1 /* Vacant */) {
      var bookedMap = {
        2: ".ihfath-booked-cell",      // Booked
        4: ".ihfath-user-booked-cell", // Booked by the same user in the cart
      };
      var bookedHour = bookedMap[data.hour] || "";

      // It's a weekend cell, return dead cell
      cell = jSh.d(bookedHour + (checker ? ".ihfath-cell-checker" : ""), null, [
        jSh.c("span", ".ihfath-time", cellTime[0]),
        jSh.c("span", ".ihfath-time-meta", cellTime[1])
      ]);

      if (data.hour === 4) {
        cell.title = args.locale.bookedByUser;
      }

      return {
        dom: cell,
        deadCell: true
      };
    }

    var content;
    var extraClass = "";
    var cellLink   = "javascript:void(0)";
    var element    = "div";

    if (isNaN(data.hour)) {
      // It's data with lessons
      element = "a";

      content = [

      ];

      extraClass = ".ihfath-full-cell";
    } else {
      // Nothing on this day
      var lessonDetails = jSh.c("span", ".ihfath-lesson-details");
      content = [
        jSh.c("span", ".ihfath-time", cellTime[0]),
        jSh.c("span", ".ihfath-time-meta", cellTime[1]),
        lessonDetails,
        jSh.d("ihfath-fill-indicator"),
        jSh.d("ihfath-selection-indicator")
      ];

      extraClass = ".ihfath-empty-cell";
    }

    cell = jSh.c(element, {
      prop: {
        href: cellLink,
        title: args.time + " " + cellTimeFull + ", " + args.dayMap[virtualX]
      },

      sel: extraClass + (checker ? ".ihfath-cell-checker" : ""),
      child: content
    });

    return {
      dom: cell,
      domLesson: lessonDetails
    };
  }

  function scheduleOnNewDayHeaderCell(x, virtualX, orientation, cellDisplay) {
    return jSh.d(null, args.dayMap[virtualX]);
  }

  function scheduleOnNewHourHeaderCell(y, virtualY, orientation, cellDisplay) {
    var yMap     = virtualY === 0 ? 12 : virtualY;
    var cellTime = virtualY < 12 ? [yMap, "AM"] : [(virtualY === 12 ? 12 : yMap - 12), "PM"];

    var checker = y % 2;

    return jSh.d(".main-color" + (checker ? ".ihfath-cell-checker" : ""), null, [
      jSh.t(cellTime[0]),
      jSh.c("span", null, cellTime[1])
    ]);
  }

  function scheduleOnStateChange(state, cellModels) {
    for (var i=0; i<state.length; i++) {
      var newState = state[i];
      var model    = cellModels[i];
      var dom      = model.dom;

      dom.classList.remove("ihfath-full");
      dom.classList.remove("ihfath-half-full");

      if (newState.pid !== 0) {
        switch (newState.dur) {
          case 0.5:
            dom.classList.add("ihfath-half-full");
            break;
          case 1:
          case 1.5:
          case 2:
            dom.classList.add("ihfath-full");
            break;
        }

        dom.classList.remove("ihfath-empty-cell");
        if (newState.pid === -1) {
          model.domLesson.innerHTML = "";
        } else {
          const hours = Math.floor(newState.dur);
          const minutes = Math.floor((newState.dur - hours) * 60) + "";

          model.domLesson.innerHTML = "";
          model.domLesson.appendChild([
            jSh.t(args.programs[programMap[newState.pid]].data.en.name),
            jSh.c("span", ".main-color", " " + hours + ":" + minutes.padStart(2, "0"))
          ]);
        }
      } else {
        dom.classList.add("ihfath-empty-cell");
      }
    }
  }

  // Create modal
  var modalPrograms = args.programs;
  var modalHours = args.hours;
  var modal = schedule.modal = new Modal(schedule, args.locale, args.curProgramId, args.hours);

  // State
  var curCell   = null;
  var fullCells = schedule.mapEachCell(schedule._cellMap, function(cell) {
    if (cell.deadCell) {
      return null;
    } else {
      return false;
    }
  });

  // Update `fullCells` on rebuild
  schedule.on("rebuild", function() {
    fullCells = schedule.mapEachCell(schedule._cellMap, function(cell) {
      if (cell.deadCell) {
        return null;
      } else {
        return false;
      }
    });
  });

  // Create events
  schedule.on("activeinput", function(e) {
    var model;

    if (model = e.cell) {
      // Prevent default browser text selection behaviour
      e.event.preventDefault();

      // Update UI
      var dom      = model.dom;
      var coords   = model.coords;
      var state    = schedule.getCellState(model);
      var nextCell = false;

      if (fullCells[coords[0]][coords[1]] === true) {
        // Get upper cell
        model    = schedule._cellMap[coords[0]][coords[1] - 1];
        dom      = model.dom;
        state    = schedule.getCellState(model);
        nextCell = true;
      } else if (fullCells[coords[0]][coords[1] + 1] !== null && fullCells[coords[0]][coords[1] + 1] !== undefined) {
        // This cell has a free cell beneath it
        nextCell = true;
      }

      // Only update modal if we've truly selected a different cell
      if (curCell !== model) {
        var curCellClass = "ihfath-cell-selected";

        // Unhighlight the previously selected cell
        if (curCell) {
          curCell.dom.classList.remove(curCellClass);
        }

        // Highlight the newly selected cell
        curCell = model;
        curCell.dom.classList.add(curCellClass);

        // Update modal
        modal.set(state.dur === 0 ? -1 : state.pid, state.dur, dom, nextCell);
      }
    }
  });

  var apiSetting = false;
  modal.onChange(function(programPid, hours) {
    if (hours === 0 || programPid === -1) {
      // Guard
      apiSetting = true;

      // Clear cell
      clearCell();

      apiSetting = false;
    } else {
      // Update cell
      updateCell(programPid, hours);
    }
  });

  // Remove highlight from curCell when modal is closed
  modal._state.addStateListener("visible", function(visible) {
    if (!visible) {
      var curCellClass = "ihfath-cell-selected";

      // Unhighlight the previously selected cell
      if (curCell) {
        curCell.dom.classList.remove(curCellClass);
      }

      curCell = null;
    }
  });

  function clearCell() {
    var cellState = schedule.getCellState(curCell);
    var editCells = [[curCell.coords[0], curCell.coords[1]]];

    if (cellState.dur > 1) {
      editCells.push([curCell.coords[0], curCell.coords[1] + 1]);
      fullCells[curCell.coords[0]][curCell.coords[1] + 1] = false;
    }

    schedule.changeSubset(editCells, function(x, y, data) {
      return {
        pid: 0,
        dur: 0,
        hour: data.hour
      };
    });
  };

  function updateCell(programPid, hours) {
    var dur = hours;
    var pid = programPid;

    var x = curCell.coords[0];
    var y = curCell.coords[1];

    var cellState = schedule.getCellState(curCell);
    var height    = schedule.options.dimensions[1];
    var editCells = [];

    // Cancel if no program in cell
    // if (cellState.pid === 0) {
    //   return;
    // }

    // Set new cell states
    for (var i=0; i<height; i++) {
      editCells.push([x, i]);

      switch (i) {
        case y + 1:
          if (dur > 1) {
            fullCells[x][i] = true;
          } else {
            // Mark this cell as available if it's not a dead cell
            if (fullCells[x][i] !== null) {
              fullCells[x][i] = false;
            }
          }
          break;
        default:
          // Mark this cell as available if it's not a dead cell
          if (fullCells[x][i] !== null) {
            fullCells[x][i] = false;
          }
      }
    }

    schedule.changeSubset(editCells, function(cellX, cellY, data) {
      if (dur === 1 || dur === 0.5) {
        if (y === cellY) {
          return {
            dur: dur,
            pid: programPid,
            hour: data.hour
          };
        } else {
          return {
            dur: 0,
            pid: 0,
            hour: data.hour
          };
        }
      } else {
        switch (cellY) {
          case y:
            return {
              dur: dur,
              pid: programPid,
              hour: data.hour
            };
            break;
          case y + 1:
            return {
              dur: dur - 1,
              pid: -1,
              hour: data.hour
            };
            break;
          default:
            return {
              dur: 0,
              pid: 0,
              hour: data.hour
            };
            break;
        }
      }
    });
  }

  // Data change updates
  schedule.on("newstate", function() {
    updateHiddenLessonsInput();

    if (!apiSetting) {
      updateModal();
    }
  });

  function updateModal(newCell, pid, hour) {
    var cell = newCell || curCell;

    var newPid  = pid;
    var newHour = hour;

    if (cell) {
      var cellState     = schedule.getCellState(cell);
      var coords        = cell.coords;
      var nextCell      = false;
      var nextCellValue = fullCells[coords[0]][coords[1] + 1];

      if (pid === undefined) newPid = cellState.pid;
      if (hour === undefined) newHour = cellState.dur;

      var dom = cell.dom;

      if (nextCellValue !== null && nextCellValue !== undefined) {
        nextCell = true;
      }

      // Update modal
      modal.set(newPid, newHour, dom, nextCell);
      return cell;
    } else {
      return false;
    }
  }

  // Schedule informing outside world of updates
  schedule.addEvent("update");
  function updateHiddenLessonsInput() {
    var table  = schedule.state;
    var width  = schedule.options.dimensions[0];
    var height = schedule.options.dimensions[1];

    var lessons = [];

    for (var x=0; x<width; x++) {
      for (var y=0; y<height; y++) {
        var cell = table[x][y];

        if (cell.pid > 0) {
          var hour = y;

          if (hour !== 0) {
            hour *= 10;
          }

          // Add this lesson to the list
          lessons.push({
            weektime: ((x + 1) * 1000) + hour,
            dur: cell.dur * 60,
            pid: cell.pid
          });

          if (cell.dur > 1) {
            // The next cell is also occupied with this class, skip it
            y++;
          }
        }
      }
    }

    schedule.triggerEvent("update", {
      lessons: lessons
    });
  }

  return {
    schedule: schedule,
    hideModal: function() {
      modal._state.visible = false;
    }
  };
}

module.exports = IhfathCurrentProfSchedule;

},{"./modal":2}],4:[function(require,module,exports){
function Stage(config, dom, main) {
  lces.types.component.call(this);
  var that = this;
  
  this.dom  = dom;
  this.main = main;
  this.name = config.name;
  this.setState("visible", false);
  this.setState("enabled", false);
  this.addEvent("stagestatechange");
  
  // Run setup
  config.setup(this, this.main);
  
  this.addStateListener("visible", function visibleCb(visible) {
    if (visible) {
      that.enabled = true;
    }
  });
  
  // Show the header when enabled
  this.addStateListener("enabled", function(enabled) {
    if (enabled) {
      dom.header.classList.remove("ihfath-disabled");
    } else {
      dom.header.classList.add("ihfath-disabled");
    }
  });
  
  // Change to this stage when
  dom.header.addEventListener("click", function() {
    if (that.enabled) {
      // Clear any loading stage (if any)
      main.clearLoadingStage();
      
      // Show this stage
      main.curStage = that.name;
    }
  });
}

jSh.inherit(Stage, lces.types.component);

Stage.prototype.setStage = function(stage) {
  if (stage in this.main._model.stages) {
    // Set the next stage
    this.main.curStage = stage;
  } else {
    throw new ReferenceError("IhfathCheckout.setStage: Stage \"" + stage + "\" doesn't exist");
  }
};

Stage.prototype.setStageState = function(stage, state) {
  if (stage in this.main._model.stages) {
    this.main.setStageState(stage, state);
  } else {
    throw new ReferenceError("IhfathCheckout.setStageState: Stage \"" + stage + "\" doesn't exist");
  }
};

Stage.prototype.getStageState = function(stage) {
  if (stage in this.main._model.stages) {
    return this.main.getStateStatus(stage);
  } else {
    throw new ReferenceError("IhfathCheckout.getStageState: Stage \"" + stage + "\" doesn't exist");
  }
};

Stage.prototype.loadStage = function(stage) {
  if (stage in this.main._model.stages) {
    this.main.loadStage(stage);
  } else {
    throw new ReferenceError("IhfathCheckout.loadStage: Stage \"" + stage + "\" doesn't exist");
  }
};

Stage.prototype.stageLoaded = function() {
  this.main.stageLoaded();
};

Stage.prototype.getNextStage = function() {
  return this.main.getNextStage(this.name);
};

Stage.prototype.getPrevStage = function() {
  return this.main.getPrevStage(this.name);
};

Stage.prototype.setValue = function(stage, value) {
  if (stage in this.main._model.stages) {
    this.main.setState(stage, value);
  } else {
    throw new ReferenceError("IhfathCheckout.setValue: Stage \"" + stage + "\" doesn't exist");
  }
};

Stage.prototype.onChange = function(cb) {
  this.main.addStateListener(this.name, cb);
};

Stage.prototype.onStageChange = function(cb) {
  this.main.addStateListener("curStage", cb);
};

// Main init
function stages(args, schedule) {
  var stagesConfig = args.stages;
  var mainState = lces.new();
  var model = mainState._model = {
    names: [],
    stages: {},
    _stages: []
  };
  
  // Make schedule accessible to stages' setup
  mainState.scheduleCtrl = schedule;
  
  mainState.setState("curStage", null);
  mainState.addStateListener("curStage", function(curStage) {
    var oldStage = this.oldStateStatus;
    
    // Hide old stage and show new one
    if (oldStage) {
      model.stages[oldStage].visible = false;
      setDomState(false, getStageDom(oldStage));
    }
    
    if (curStage) {
      setDomState(true, getStageDom(curStage));
      model.stages[curStage].visible = true;
    }
  });
  
  mainState._getStage = function(stage, offset) {
    return model.names[model.stages[stage]._index + offset];
  };
  
  mainState.setStageState = function(stage, state) {
    model.stages[stage].enabled = !!state;
  };
  
  mainState.getStageState = function(stage) {
    return model._stages[stage].enabled;
  };
  
  mainState.getNextStage = function(stage) {
    return this._getStage(stage, 1);
  };
  
  mainState.getPrevStage = function(stage) {
    return this._getStage(stage, -1);
  };
  
  mainState._loadingStage = null;
  mainState.loadStage = function(stage) {
    this._loadingStage = stage;
    var loaderDom = getStageDom("loader");
    
    // Set title text
    loaderDom.body.jSh(".ihfath-prof-name")[0].textContent = this._loadingStage;
    
    // Hide current stage and show loader
    setDomState(false, getStageDom(this.curStage), true);
    setDomState(true, loaderDom, true);
  };
  
  mainState.stageLoaded = function(aborted) {
    if (this._loadingStage) {
      // Hide loader and (show if aborted) old stage
      setDomState(false, getStageDom("loader"), true);
      setDomState(aborted, getStageDom(this.curStage), true);
      
      if (!aborted) {
        // Alert everyone of change and show new stage
        this.curStage = this._loadingStage;
      }
      
      // Clear stage from loading "queue"
      this._loadingStage = null;
    }
  };
  
  mainState.clearLoadingStage = function() {
    if (this._loadingStage) {
      // Hide loader and show old stage
      setDomState(false, getStageDom("loader"), true);
      setDomState(false, getStageDom(this.curStage));
      
      // Alert everyone of change and show new stage
      this.curStage = this._loadingStage;
      
      // Clear stage from loading "queue"
      this._loadingStage = null;
    }
  };
  
  mainState.serialize = function() {
    var values = {};
    model.names.forEach(function(stage) {
      values[stage] = mainState[stage];
    });
    
    return values;
  };
  
  mainState.addEvent("stagestatechange");
  mainState.on("stagestatechange", function(e) {
    model._stages.forEach(function(stage) {
      stage.triggerEvent("stagestatechange", e);
    });
  });
  
  // getStateDom()
  {
    var _stageDomCache = {};
    function getStageDom(name) {
      if (name in _stageDomCache) {
        // Cache hit
        return _stageDomCache[name];
      } else {
        var stageHeader = jSh(".ihfath-stage-header-" + name)[0];
        var stageBody = jSh(".ihfath-stage-" + name)[0];
        
        return _stageDomCache[name] = {
          header: stageHeader,
          body: stageBody
        };
      }
    }
  }
  
  // setDomState()
  function setDomState(state, dom, ignoreHeader) {
    if (state) {
      if (!ignoreHeader) {
        dom.header.classList.add("active");
      }
      
      dom.body.classList.remove("ihfath-hidden");
    } else {
      if (!ignoreHeader) {
        dom.header.classList.remove("active");
      }
      
      dom.body.classList.add("ihfath-hidden");
    }
  }
  
  stagesConfig.forEach(function(stage) {
    // Add to mainstate
    mainState.setState(stage.name, "default" in stage ? stage.default : null);
    mainState.addStateListener(stage.name, function(value) {
      mainState.triggerEvent("stagestatechange", {
        stage: stage.name,
        oldValue: this.oldStateStatus,
        value: value
      });
    });
    
    // Push to model
    model.names.push(stage.name);
    
    // Create new Stage instance
    var stageInst = new Stage(stage, getStageDom(stage.name), mainState);
    model.stages[stage.name] = stageInst;
    stageInst._index = model.names.length - 1;
    
    model._stages.push(stageInst);
  });
  
  // Set the first stage as the visible stage
  mainState.curStage = model.names[0];
  
  // Stage setup
  if (typeof args.stageSetup === "function") {
    args.stageSetup(mainState);
  }
}

module.exports = stages;

},{}]},{},[1]);
