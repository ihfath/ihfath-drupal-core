function Modal(cd, locale, curProgramId, hours) {
  var state = this._state = lces.new();
  var that = this;

  // Set initial state values
  state.setState("program", -1);
  state.setState("hours", 1);
  state.setState("visible", false);
  state.setState("cell", null);
  state.potentialSize = false;
  state.curProgramId = curProgramId;
  state.apiSetting = false;
  state.hourList = null;

  // Example:
  // this.setProgramList([
  //   ["Nahw", 3],
  //   ["Sarf", 4],
  //   ["Balaagha", 5]
  // ]);
  this.setProgramList([]);

  // Create dom
  var dom = this._dom = this._makeDom(locale);

  // Add program list
  dom.programList.appendChild(dom.programListArr = this._makeDomProgramList(state.programList));

  // Add state bindings
  state.addStateListener("program", function(program) {
    that._renderProgramList(this.oldStateStatus, program, this);

    // Update API-set listeners
    if (!state.apiSetting && that._onChangeCb) {
      that._onChangeCb(state.program, state.hours);
    }
  });

  // Prevent setting the program getting ignored when
  // hours is zero
  state.addStateCondition("program", function(program) {
    if (!state.apiSetting) {
      if (state.hours === 0) {
        state.states.hours.stateStatus = 1;
        that._updateHourUI(1);
      }
    }

    return true;
  });

  state.addStateListener("hours", function(hours) {
    if (hours === 0) {
      // Implicitly reset `program` state
      var programObj = state.states.program;
      var newProgram = -1;
      var oldProgram = state.program;

      programObj.oldStateStatus = oldProgram;
      programObj.stateStatus = newProgram;

      console.log("HOURS: ", hours);
      that._renderProgramList(oldProgram, newProgram, this);
    }

    that._updateHourUI(hours);

    // Update API-set listeners
    if (!state.apiSetting && that._onChangeCb) {
      that._onChangeCb(state.program, state.hours);
    }
  });

  state.addStateListener("cell", function(cell) {
    // Center modal
    var modal = dom.main;

    that._centerModal(modal, cell);
    state.states.visible.oldStateStatus = state.visible;
    state.states.visible.stateStatus = true;
  });

  state.addStateListener("visible", function(visible) {
    var modal = dom.main;

    if (visible) {
      that._centerModal(modal, state.cell);
    } else {
      modal.style.left = dom.invisibleOffset + "px";
      modal.style.top  = dom.invisibleOffset + "px";
    }
  });

  // Set possible hours
  // TODO: Get this from... Somewhere
  this.setHourList(hours);
}

// Setters
Modal.prototype.setHourList = function(list) {
  this._state.hourList = this._serializeHourList(list);
  this._state.hours = this._hourChange(0, true);
};

Modal.prototype.setProgramList = function(list) {
  var state = this._state;

  state.programList = list;
  state.programListMap = {};
  state.programList.forEach(function(program, i) {
    state.programListMap[program[1]] = i;
  });

  // Rerender DOM list
  if (this._dom) {
    this._dom.programList.innerHTML = "";
    this._dom.programList.appendChild(this._dom.programListArr = this._makeDomProgramList(state.programList));
  }
};

// Dom events
Modal.prototype._renderProgramList = function(oldProgram, program, stateObj) {
  var state = this._state;
  var programList = this._dom.programListArr;
  var className = "ihfath-cm-selected";
  // var oldProgramIndex = stateObj.stateStatus;

  // Clear old
  if (oldProgram !== -1) {
    var oldIndex = state.programListMap[oldProgram];
    programList[oldIndex !== undefined ? oldIndex : 0].classList.remove(className);
  }

  // Set new
  if (program !== -1) {
    var newIndex = state.programListMap[program];
    programList[newIndex !== undefined ? newIndex : 0].classList.add(className);
  }
};

// Dom building
Modal.prototype._makeDom = function(locale) {
  var state = this._state;
  var that = this;

  // Create programs widget
  var programList = jSh.d({
    sel: ".ihfath-cm-program-list.ihfath-cm-column",
    events: {
      click: function(e) {
        var target = e.target;

        if ("ihfathProgramPid" in target) {
          // Set pid
          state.program = target.ihfathProgramPid;
        }
      }
    }
  });

  // Create hour widget
  var hourInput = jSh.c("input", {
    sel: ".ihfath-hours-input",
    events: {
      change: function() {
        var hours = that._hourChange(null, null, parseFloat(this.value));
        this.value = hours;
      }
    },
    prop: {
      type: "text"
    }
  });
  var hourButtons = jSh.d(".ihfath-cm-hour-btns", null, [
    jSh.d({
      sel: ".ihfath-cm-hour-btn.ihfath-cm-hour-up.main-bg",
      child: jSh.c("i", ".fa.fa-angle-up"),
      events: {
        click: function() {
          state.hours = that._hourChange(1);
        }
      }
    }),

    jSh.d({
      sel: ".ihfath-cm-hour-btn.ihfath-cm-hour-down.main-bg",
      child: jSh.c("i", ".fa.fa-angle-down"),
      events: {
        click: function() {
          state.hours = that._hourChange(-1);
        }
      }
    }),
  ]);

  // Clear button
  var clearButton = jSh.d({
    sel: ".ihfath-cm-clear-btn",
    text: locale.clearCell,
    events: {
      click: function() {
        // Clear state
        state.hours = 0;
      }
    }
  });

  // Close button
  var closeButton = jSh.d({
    sel: ".ihfath-cm-close-btn",
    text: "×",
    events: {
      click: function() {
        state.visible = false;
      }
    }
  });

  var hourWrap = jSh.d(".ihfath-cm-hour.ihfath-cm-column", null, [
    jSh.d(".ihfath-cm-hour-top", null, [
      hourInput,
      jSh.d(".ihfath-cm-hour-caption", null, [
        jSh.c("span", null, locale.hours)
      ]),
      hourButtons
    ]),
    jSh.d(".ihfath-cm-hour-bottom", null, [
      clearButton,
      closeButton
    ])
  ]);

  // Create main
  var main = jSh.d(".ihfath-checkout-modal", null, [
    programList,
    hourWrap
  ]);

  // Add to document
  document.body.appendChild(main);

  // Set "invisible" initially
  var invisibleOffset = -10000;
  main.style.left = invisibleOffset + "px";
  main.style.top  = invisibleOffset + "px";

  return {
    main: main,
    invisibleOffset: invisibleOffset,
    programList: programList,
    hourInput: hourInput
  };
};

Modal.prototype._updateHourUI = function(hoursFull) {
  const hours = Math.floor(hoursFull);
  const minutes = Math.floor((hoursFull - hours) * 60) + "";

  this._dom.hourInput.value = hours + ":" + minutes.padStart(2, "0");
};

Modal.prototype._makeDomProgramList = function(list) {
  var dom = list.map(function(program, i) {
    return jSh.d({
      sel: ".ihfath-cm-program-item.main-color", //  + (i > 0 ? "" : ".ihfath-cm-selected"),
      text: program[0],
      prop: {
        ihfathProgramPid: program[1]
      }
    });
  });

  return dom;
};

Modal.prototype._centerModal = function(modal, cell) {
  var cellRect = cell && cell.getBoundingClientRect() || {
    // Kinda "center" the modal when no cell is selected
    top: innerHeight / 2,
    left: 0,
    right: innerWidth,
    bottom: innerHeight
  };
  var modalRect = modal.getBoundingClientRect();

  // Update position
  var modalHeight = modalRect.bottom - modalRect.top;
  var modalWidth  = modalRect.right - modalRect.left;
  var cellWidth   = cellRect.right - cellRect.left;

  var borderWidth = 0; // Turns out border was just an outline, thus zero for now
  modal.style.left = (cellRect.left + scrollX + (cellWidth / 2) - (modalWidth / 2)) + "px";
  modal.style.top = ((cellRect.top + scrollY) - modalHeight - borderWidth) + "px";
};

Modal.prototype._serializeHourList = function(list) {
  var mapping = {};
  var serialized = {
    list: list,
    mapping: mapping
  };

  for (var i=0; i<list.length; i++) {
    mapping[list[i]] = i;
  }

  return serialized;
};

// Filter and process the user inputted hour
Modal.prototype._hourChange = function(indexDirection, absolute, directMap) {
  var state = this._state;
  var hours = state.hourList;
  var oldHourIndex = hours.mapping[state.hours];
  var newHourIndex = 0;
  var value = null;

  if (directMap) {
    var index = hours.mapping[directMap];

    // Return a direct map
    value = hours.list[index === undefined ? oldHourIndex : index];
  } else if (!absolute) {
    // Get value via relative distance
    newHourIndex = hours.mapping[state.hours] + indexDirection;
    value = hours.list[Math.max(Math.min(newHourIndex, hours.list.length - 1), 0)];
  }

  if (value) {
    state.setState("program", state.curProgramId);
  }

  console.log("VALUE:", value, "POTENTIAL:", state.potentialSize);
  return state.potentialSize ? value : Math.min(value, 1);
};

Modal.prototype.onChange = function(cb) {
  this._onChangeCb = cb; // cb(this._state.program, this._state.hours);
};

Modal.prototype.set = function(program, hours, dom, potentialSize) {
  // Add setting guard
  this._state.apiSetting = true;

  this._state.program = program;
  this._state.hours = hours;

  // Remove setting guard
  this._state.apiSetting = false;

  // Set potentialSize
  console.log("POTENTIAL:", potentialSize);
  this._state.potentialSize = potentialSize;

  // Center and display modal
  this._state.cell = dom;
  this._state.visible = true;
};

module.exports = Modal;
