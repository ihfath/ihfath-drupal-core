function Stage(config, dom, main) {
  lces.types.component.call(this);
  var that = this;
  
  this.dom  = dom;
  this.main = main;
  this.name = config.name;
  this.setState("visible", false);
  this.setState("enabled", false);
  this.addEvent("stagestatechange");
  
  // Run setup
  config.setup(this, this.main);
  
  this.addStateListener("visible", function visibleCb(visible) {
    if (visible) {
      that.enabled = true;
    }
  });
  
  // Show the header when enabled
  this.addStateListener("enabled", function(enabled) {
    if (enabled) {
      dom.header.classList.remove("ihfath-disabled");
    } else {
      dom.header.classList.add("ihfath-disabled");
    }
  });
  
  // Change to this stage when
  dom.header.addEventListener("click", function() {
    if (that.enabled) {
      // Clear any loading stage (if any)
      main.clearLoadingStage();
      
      // Show this stage
      main.curStage = that.name;
    }
  });
}

jSh.inherit(Stage, lces.types.component);

Stage.prototype.setStage = function(stage) {
  if (stage in this.main._model.stages) {
    // Set the next stage
    this.main.curStage = stage;
  } else {
    throw new ReferenceError("IhfathCheckout.setStage: Stage \"" + stage + "\" doesn't exist");
  }
};

Stage.prototype.setStageState = function(stage, state) {
  if (stage in this.main._model.stages) {
    this.main.setStageState(stage, state);
  } else {
    throw new ReferenceError("IhfathCheckout.setStageState: Stage \"" + stage + "\" doesn't exist");
  }
};

Stage.prototype.getStageState = function(stage) {
  if (stage in this.main._model.stages) {
    return this.main.getStateStatus(stage);
  } else {
    throw new ReferenceError("IhfathCheckout.getStageState: Stage \"" + stage + "\" doesn't exist");
  }
};

Stage.prototype.loadStage = function(stage) {
  if (stage in this.main._model.stages) {
    this.main.loadStage(stage);
  } else {
    throw new ReferenceError("IhfathCheckout.loadStage: Stage \"" + stage + "\" doesn't exist");
  }
};

Stage.prototype.stageLoaded = function() {
  this.main.stageLoaded();
};

Stage.prototype.getNextStage = function() {
  return this.main.getNextStage(this.name);
};

Stage.prototype.getPrevStage = function() {
  return this.main.getPrevStage(this.name);
};

Stage.prototype.setValue = function(stage, value) {
  if (stage in this.main._model.stages) {
    this.main.setState(stage, value);
  } else {
    throw new ReferenceError("IhfathCheckout.setValue: Stage \"" + stage + "\" doesn't exist");
  }
};

Stage.prototype.onChange = function(cb) {
  this.main.addStateListener(this.name, cb);
};

Stage.prototype.onStageChange = function(cb) {
  this.main.addStateListener("curStage", cb);
};

// Main init
function stages(args, schedule) {
  var stagesConfig = args.stages;
  var mainState = lces.new();
  var model = mainState._model = {
    names: [],
    stages: {},
    _stages: []
  };
  
  // Make schedule accessible to stages' setup
  mainState.scheduleCtrl = schedule;
  
  mainState.setState("curStage", null);
  mainState.addStateListener("curStage", function(curStage) {
    var oldStage = this.oldStateStatus;
    
    // Hide old stage and show new one
    if (oldStage) {
      model.stages[oldStage].visible = false;
      setDomState(false, getStageDom(oldStage));
    }
    
    if (curStage) {
      setDomState(true, getStageDom(curStage));
      model.stages[curStage].visible = true;
    }
  });
  
  mainState._getStage = function(stage, offset) {
    return model.names[model.stages[stage]._index + offset];
  };
  
  mainState.setStageState = function(stage, state) {
    model.stages[stage].enabled = !!state;
  };
  
  mainState.getStageState = function(stage) {
    return model._stages[stage].enabled;
  };
  
  mainState.getNextStage = function(stage) {
    return this._getStage(stage, 1);
  };
  
  mainState.getPrevStage = function(stage) {
    return this._getStage(stage, -1);
  };
  
  mainState._loadingStage = null;
  mainState.loadStage = function(stage) {
    this._loadingStage = stage;
    var loaderDom = getStageDom("loader");
    
    // Set title text
    loaderDom.body.jSh(".ihfath-prof-name")[0].textContent = this._loadingStage;
    
    // Hide current stage and show loader
    setDomState(false, getStageDom(this.curStage), true);
    setDomState(true, loaderDom, true);
  };
  
  mainState.stageLoaded = function(aborted) {
    if (this._loadingStage) {
      // Hide loader and (show if aborted) old stage
      setDomState(false, getStageDom("loader"), true);
      setDomState(aborted, getStageDom(this.curStage), true);
      
      if (!aborted) {
        // Alert everyone of change and show new stage
        this.curStage = this._loadingStage;
      }
      
      // Clear stage from loading "queue"
      this._loadingStage = null;
    }
  };
  
  mainState.clearLoadingStage = function() {
    if (this._loadingStage) {
      // Hide loader and show old stage
      setDomState(false, getStageDom("loader"), true);
      setDomState(false, getStageDom(this.curStage));
      
      // Alert everyone of change and show new stage
      this.curStage = this._loadingStage;
      
      // Clear stage from loading "queue"
      this._loadingStage = null;
    }
  };
  
  mainState.serialize = function() {
    var values = {};
    model.names.forEach(function(stage) {
      values[stage] = mainState[stage];
    });
    
    return values;
  };
  
  mainState.addEvent("stagestatechange");
  mainState.on("stagestatechange", function(e) {
    model._stages.forEach(function(stage) {
      stage.triggerEvent("stagestatechange", e);
    });
  });
  
  // getStateDom()
  {
    var _stageDomCache = {};
    function getStageDom(name) {
      if (name in _stageDomCache) {
        // Cache hit
        return _stageDomCache[name];
      } else {
        var stageHeader = jSh(".ihfath-stage-header-" + name)[0];
        var stageBody = jSh(".ihfath-stage-" + name)[0];
        
        return _stageDomCache[name] = {
          header: stageHeader,
          body: stageBody
        };
      }
    }
  }
  
  // setDomState()
  function setDomState(state, dom, ignoreHeader) {
    if (state) {
      if (!ignoreHeader) {
        dom.header.classList.add("active");
      }
      
      dom.body.classList.remove("ihfath-hidden");
    } else {
      if (!ignoreHeader) {
        dom.header.classList.remove("active");
      }
      
      dom.body.classList.add("ihfath-hidden");
    }
  }
  
  stagesConfig.forEach(function(stage) {
    // Add to mainstate
    mainState.setState(stage.name, "default" in stage ? stage.default : null);
    mainState.addStateListener(stage.name, function(value) {
      mainState.triggerEvent("stagestatechange", {
        stage: stage.name,
        oldValue: this.oldStateStatus,
        value: value
      });
    });
    
    // Push to model
    model.names.push(stage.name);
    
    // Create new Stage instance
    var stageInst = new Stage(stage, getStageDom(stage.name), mainState);
    model.stages[stage.name] = stageInst;
    stageInst._index = model.names.length - 1;
    
    model._stages.push(stageInst);
  });
  
  // Set the first stage as the visible stage
  mainState.curStage = model.names[0];
  
  // Stage setup
  if (typeof args.stageSetup === "function") {
    args.stageSetup(mainState);
  }
}

module.exports = stages;
