var Schedule = require("./schedule");
var Stages = require("./stages");

function main(config) {
  var schedule = Schedule(config.schedule);
  
  // Start schedules
  Stages(config, schedule);
}

window.IhfathCheckout = main;
