<?php

class IhfathCoreBaseAjax {
  const ACCESS_DENIED = '{"error":true,"cause":"Access denied","data":null}';
  const INVALID_DIRECTIVE = '{"error":true,"cause":"Invalid directive","data":null}';
  const INVALID_REQUEST = '{"error":true,"cause":"Invalid request","data":null}';

  // Helper callbacks
  const cb_proxy = "ihfathcore_callback_proxy_namespace";
  const cb_form_get_proxy = "ihfathcore_form_get_callback_proxy_namespace";
  const cb_form_submit_proxy = "ihfathcore_form_submit_callback_proxy_namespace";

  /**
   *  [
   *    IHF_ROLE_ID => DRUPAL_ROLE_ID,
   *    ...
   *  ]
   */
  static public $roles = NULL;

  // Authorized Ihfath roles that can invoke these directives
  const AUTHORIZED_ROLES = array(
    1,
  );

  static protected function response($data, $set_json_header = TRUE) {
    if ($set_json_header) {
      header("Content-Type: text/json");
    }

    return json_encode(array(
      "error" => FALSE,
      "data" => $data,
    ));
  }

  static protected function error($cause, $set_json_header = TRUE) {
    if ($set_json_header) {
      header("Content-Type: text/json");
    }

    return json_encode(array(
      "error" => TRUE,
      "cause" => $cause,
      "data" => NULL,
    ));
  }

  static protected function getField($field, $column = "value") {
    if (isset($field[LANGUAGE_NONE][0][$column])) {
      return $field[LANGUAGE_NONE][0][$column];
    } else {
      return NULL;
    }
  }

  static protected function setFields($entity_obj, array $fields, $column = "value") {
    foreach ($fields as $field_name => $field_val) {
      $entity_obj->{$field_name}[LANGUAGE_NONE][0][$column] = $field_val;
      // FIXME: Add other lang fields
    }
  }

  /**
   * Get a mapping of [KOA role ID => Drupal role ID]
   */
  static protected function getRoles() {
    if (isset(self::$roles)) {
      return self::$roles;
    }

    $roles = db_select("role", "r")
               ->fields("r", array("rid", "name"))
               ->condition("r.name", array("admin", "student", "professor"))
               ->execute()->fetchAllAssoc("rid");

    $roles_formatted = array();
    $map = array(
      "admin" => 1,
      "professor" => 2,
      "student" => 3,
    );

    foreach ($roles as $rid => $role) {
      if (isset($map[$role->name])) {
        $roles_formatted[$map[$role->name]] = $role;
      }
    }

    return self::$roles = $roles_formatted;
  }

  // Getters
  static function ajax($directive = NULL) {
    header("Content-Type: text/json");

    $user_role = ihfathcore_get_role();
    $data = json_decode(file_get_contents("php://input"), FALSE);

    if (!count(static::AUTHORIZED_ROLES)
        || !in_array($user_role, static::AUTHORIZED_ROLES)) {
      echo static::ACCESS_DENIED;
      die();
    }

    $pass = (object) array();
    $output;

    if (isset($directive)
        && trim($directive)
        && is_callable(array(static::class, $directive))) {
      $output = static::$directive($data, $pass, $user_role);
    } else {
      $output = static::INVALID_DIRECTIVE;
    }

    // Maybe keep Drupal execution running here
    if ($output === $pass) {
      if (isset($output->output)) {
        echo $output->output;
      }

      // Pass
    } else {
      echo $output;
      die();
    }
  }
}

class IhfathCoreAjaxStatic extends IhfathCoreBaseAjax {
  static function getRoles() {
    return parent::getRoles();
  }
}

class IhfathCoreDevAjax extends IhfathCoreBaseAjax {
  static function dummyLessons($data) {
    if (!(isset($data->suid)
          && isset($data->puid)
          && isset($data->weekCount))) {
      return self::INVALID_REQUEST;
    }

    // Lesson count per week
    $lessons_per_week = rand(4, 8);
    $lessons_week = array(
      // array(
      //   WEEKTIME,
      //   DUR_MIN,
      // )
    );
    $weeks = $data->weekCount;

    $max_weektime_hr = 24 * 7;
    $last_weektime_hr = 0;
    $base_weektime = 1000;
    $possible_hours = array(
      60, 90, 120,
    );

    $parsed_weektimes = array(
      // WEEKTIME => PARSED_WEEKTIME_ARRAY,
    );

    for ($i=0; $i<$lessons_per_week; $i++) {
      $max_weektime_hr_range = $max_weektime_hr / $lessons_per_week;
      $max_weektime_hr_range_start = $i * $max_weektime_hr_range;
      $weektime_hr = rand($max_weektime_hr_range_start, $max_weektime_hr_range_start + $max_weektime_hr_range - 1);

      $new_weektime = IhfathCoreWeektime::shiftWeektime($base_weektime, 0, $weektime_hr);
      $lessons_week[] = array(
        $new_weektime,
        $possible_hours[rand(0, 2)],
      );

      $parsed_weektimes[$new_weektime] = IhfathCoreWeektime::parseWeektime($new_weektime);
    }

    $student = user_load($data->suid);

    $start = gmmktime(0, 0, 0, gmdate("n"), (gmdate("j") - gmdate("N") + 1) - (7 * $weeks), gmdate("Y"));
    $quarter_hour_secs = 60 * 15;
    $hour_secs = 60 * 60;
    $day_secs = 60 * 60 * 24;
    $week_secs = 60 * 60 * 24 * 7;

    // Create series
    // ===============================================
    // ...
    $series_id = db_insert("ihfath_lsn_series")
                   ->fields(array(
                     "suid" => $data->suid,
                     "order_id" => 0,
                   ))
                   ->execute();

    // Create weektimes
    // ===============================================
    $weektime_ids = array(
      // WEEKTIME => WEEKTIME_ID,
    );

    foreach ($lessons_week as $lesson_weektime) {
      $weektime = $parsed_weektimes[$lesson_weektime[0]];
      $start_date = $start
                    + ($day_secs * ($weektime[0] - 1))
                    + ($hour_secs * $weektime[1])
                    + ($quarter_hour_secs * $weektime[2]);
      $end_date = $start
                  + (($weeks - 1) * $week_secs)
                  + ($day_secs * ($weektime[0] - 1))
                  + ($hour_secs * $weektime[1])
                  + ($quarter_hour_secs * $weektime[2])
                  + $lesson_weektime[1] * 60; // Add duration to end_date

      $wktime_id = db_insert("ihfath_lsn_series_weektime")
                     ->fields(array(
                       "sid" => $series_id,
                       "weektime" => $lesson_weektime[0],
                       "start_date" => $start_date,
                       "end_date" => $end_date,
                       "duration" => $lesson_weektime[1],
                     ))
                     ->execute();

      $weektime_ids[$lesson_weektime[0]] = $wktime_id;
    }

    // Get default course
    $course = IhfathCourse::loadCourse(1);

    // Create lessons
    // ===============================================
    $lessons = array();
    $cur_week = 0;

    $lesson_insert_query = db_insert("ihfath_lsn");
    $lesson_insert_query->fields(array(
      "type",
      "program",
      "status",
      "series_weektime_id",
      "series",
      "puid",
      "suid",
      "order_id",

      "start_date",
      "paid_duration",
      "prof_start",
      "prof_dur",
      "prof_last",
      "student_start",
      "student_dur",
      "student_last",

      "cancelled",
    ));

    while ($cur_week < $weeks) {
      foreach ($lessons_week as $lesson) {
        $cancel = rand(0, 4) === 0 ? 15 : 0;
        $begin_randomness = rand(0, 5);
        $end_randomness = rand(0, 10);
        $weektime = $parsed_weektimes[$lesson[0]];
        $start_date = $start
                      + ($cur_week * $week_secs)
                      + ($day_secs * ($weektime[0] - 1))
                      + ($hour_secs * $weektime[1])
                      + ($quarter_hour_secs * $weektime[2]);

        // Create lessons
        $lesson_full = array(
          "type" => $course->type,
          "program" => $course->pid,
          "status" => $course->levels[0]->id,
          "series_weektime_id" => $weektime_ids[$lesson[0]],
          "series" => $series_id,
          "puid" => $data->puid,
          "suid" => $data->suid,
          "order_id" => 0,

          "start_date" => $start_date,
          "paid_duration" => $lesson[1],
          // "week" => $cur_week,

          "cancelled" => (int) ((bool) $cancel),

          "prof_start" => $start_date + ($cancel * 60) + ($begin_randomness * 60),
          "prof_dur" => ($lesson[1] * 60) - (($cancel * 60) + ($begin_randomness * 60) + $end_randomness),
          "prof_last" => $start_date + ($lesson[1] * 60) - (($cancel * 60) + ($begin_randomness * 60) + $end_randomness),
          "student_start" => $start_date,
          "student_dur" => ($lesson[1] * 60) - ($end_randomness * 60),
          "student_last" => $start_date + ($lesson[1] * 60) - ($end_randomness * 60),
        );

        $lessons[] = $lesson_full;
        $lesson_insert_query->values($lesson_full);
      }

      $cur_week++;
    }

    $lesson_insert_query->execute();

    return self::response(array(
      "lessons_week" => $lessons_week,
      "lessons" => $lessons,
      "start" => $start,
      "success" => TRUE,
    ));
  }

  static function wipeAllLessons($data) {
    // Wipe all lessons
    db_truncate("ihfath_lsn")->execute();
    db_truncate("ihfath_lsn_series")->execute();
    db_truncate("ihfath_lsn_series_weektime")->execute();

    return self::response(array(
      "success" => TRUE,
    ));
  }

  static function getPages() {
    return self::response(IhfathUIPageLayout::getPage(1));
  }
}
