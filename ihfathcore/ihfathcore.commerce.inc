<?php

//
// Ihfathcore Commerce logic
//

namespace Ihfathcore;

class Commerce {
  const hourField = "field_ihfath_lesson_hours";
  const productBundle = "ihfath_lesson_hours_product";
  static $currency = "EUR";
  static $_hasProductType = NULL;
  static $_productType = NULL;

  static function hasProductType() {
    if (!isset(self::$_hasProductType)) {
      self::$_hasProductType = !!commerce_product_type_load(self::productBundle);
    }

    return self::$_hasProductType;
  }

  static function getProductType() {
    if (!isset(self::$_productType)) {
      self::$_productType = commerce_product_type_load(self::productBundle);
    }

    return self::$_productType;
  }

  static function _createBaseProductTypes() {
    $lesson_hours_product_base = commerce_product_ui_product_type_new();

    $lesson_hours_product = array(
      "type" => self::productBundle,
      "name" => "Ihfath Lesson Hours Product",
      "description" => t("Complete hours of Ihfath Lessons"),
      "is_new" => TRUE
    );

    $lesson_hours_product += $lesson_hours_product_base;

    // Create Ihfath Lesson Hours product type
    commerce_product_ui_product_type_save($lesson_hours_product);

    // Create Ihfath Lesson Hour field
    $hour_field = array(
      "field_name" => self::hourField,
      "label" => "Ihfath Lesson Hours",
      "cardinality" => 1,
      "locked" => FALSE,
      "type" => "number_decimal",
      "settings" => array(
        "label" => "Ihfath Lesson Hours"
      )
    );

    field_create_field($hour_field);

    // Create field instances

    // Create hour field instance
    $hour_field_instance = array(
      "field_name" => self::hourField,
      "entity_type" => "commerce_product",
      "bundle" => self::productBundle,
      "required" => TRUE
    );

    field_create_instance($hour_field_instance);

    // Create on sale flag field instance
    $price_field_instance = array(
      "field_name" => "field_commerce_saleprice_on_sale",
      "entity_type" => "commerce_product",
      "bundle" => self::productBundle,

      // Copy pasted from the module's instance code
      'commerce_cart_settings' => array(
        'attribute_field' => 0,
        'attribute_widget' => 'select',
      ),
      'display' => array(
        'default' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => '1',
        ),
        'line_item' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
        'node_teaser' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
      ),
      'label' => 'On sale',
      'required' => 0,
      'settings' => array(
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'options',
        'settings' => array(
          'display_label' => 1,
        ),
        'type' => 'options_onoff',
        'weight' => '4',
      ),
    );

    field_create_instance($price_field_instance);

    // Create sales price field instance
    $price_field_instance = array(
      "field_name" => "field_commerce_saleprice",
      "entity_type" => "commerce_product",
      "bundle" => self::productBundle,

      // Copy pasted from the module's instance code
      'description' => 'This price will be used if the <em>On sale</em> checkbox is checked.',
      'display' => array(
        'default' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => '2',
        ),
        'line_item' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
        'node_teaser' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
      ),
      'label' => 'Sale price',
      'required' => 0,
      'settings' => array(
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'commerce_price',
        'settings' => array(
          'currency_code' => 'default',
        ),
        'type' => 'commerce_price_simple',
        'weight' => '5',
      ),
    );

    field_create_instance($price_field_instance);
  }

  // FIXME: Swap the $hour_prices mapping, hours are unique not prices
  static function createProducts($hour_prices = array(4000 => 1), $skip_check = FALSE) {
    // Only make products if the product type exists
    if ($skip_check || !self::hasProductType()) {
      // Build the product type
      self::_createBaseProductTypes();

      // Create the products
      foreach ($hour_prices as $price => $hour) {
        $product = commerce_product_new(self::productBundle);
        $product->sku = "ihfath-lesson-hours-" . $hour;
        $product->title = "Ihfath {$hour} Lesson Hours";
        $product->uid = 1; // Created by the admin

        $product->{self::hourField}[LANGUAGE_NONE][0]["value"] = $hour;
        $product->commerce_price[LANGUAGE_NONE][0]["amount"] = $price;
        $product->commerce_price[LANGUAGE_NONE][0]["currency_code"] = self::$currency;

        commerce_product_save($product);
      }
    }
  }

  static function updateProducts($hour_prices, $parse = FALSE) {
    if ($parse) {
      // Parse
      $hour_prices = self::parseProductList($hour_prices);

      if (!isset($hour_prices)) {
        return NULL;
      }
    }

    // Only update when $hour_prices is valid
    if (!is_array($hour_prices)) {
      throw new \Exception("::updateProducts(): \$hour_prices must be an array");
    }

    // Ony update products if the product type exists
    if (self::hasProductType()) {
      // Search for the products
      $matched_products = commerce_product_load_multiple(array(), array(
        "type" => self::productBundle
      ));

      $hour_map = array_flip($hour_prices);

      // Manipulate the products
      foreach ($matched_products as $product) {
        $hour = $product->{self::hourField}[LANGUAGE_NONE][0]["value"];

        if (isset($hour_map[$hour])) {
          // Set price and save i there's a difference
          $price = ((float) $hour_map[$hour]) * 100;

          if ($product->commerce_price[LANGUAGE_NONE][0]["amount"] != $price || !((int) $product->status)) {
            $product->commerce_price[LANGUAGE_NONE][0]["amount"] = $price;
            $product->commerce_price[LANGUAGE_NONE][0]["currency_code"] = self::$currency;
            $product->status = 1;

            // Save product
            commerce_product_save($product);
          }

          // Mark this hour completed
          $hour_map[$hour] = TRUE;
        } else {
          // Disable product
          $product->status = 0;
          commerce_product_save($product);
        }
      }

      // Create new products for hours that have no products
      foreach ($hour_map as $hour => $hour_status) {
        if ($hour_status !== TRUE) {
          $hour = (float) $hour;

          $product = commerce_product_new(self::productBundle);
          $product->sku = "ihfath-lesson-hours-" . $hour;
          $product->title = "Ihfath {$hour} Lesson Hours";
          $product->uid = 1; // Created by the admin

          $price = ((float) $hour_status) * 100;

          $product->{self::hourField}[LANGUAGE_NONE][0]["value"] = $hour;
          $product->commerce_price[LANGUAGE_NONE][0]["amount"] = $price;
          $product->commerce_price[LANGUAGE_NONE][0]["currency_code"] = self::$currency;

          commerce_product_save($product);
        }
      }
    } else {
      self::createProducts($hour_prices, TRUE);
    }

    return $hour_prices;
  }

  static function parseProductList($list) {
    $map = explode("\n", $list);
    $remapped = array();

    foreach ($map as $piece) {
      $piece = trim($piece);

      if ($piece !== "") {
        if (preg_match('/^(\d+(?:\.\d{1,2})?)[ \t]*\:[ \t]*(\d+(?:\.\d+)?)$/', $piece, $matches)) {
          $remapped[(string) $matches[1]] = (string) $matches[2];
        } else {
          return NULL;
        }
      }
    }

    return $remapped;
  }

  static function getProductList($omit_inactive = TRUE) {
    if (self::hasProductType()) {
      $matched_products = commerce_product_load_multiple(array(), array(
        "type" => self::productBundle
      ));

      $output = "";

      if ($omit_inactive) {
        foreach ($matched_products as $product) {
          if ((int) $product->status) {
            $output .= ($product->commerce_price[LANGUAGE_NONE][0]["amount"] / 100) . ":" . $product->{self::hourField}[LANGUAGE_NONE][0]["value"] . "\n";
          }
        }
      } else {
        foreach ($matched_products as $product) {
          $output .= ($product->commerce_price[LANGUAGE_NONE][0]["amount"] / 100) . ":" . $product->{self::hourField}[LANGUAGE_NONE][0]["value"] . "\n";
        }
      }

      return $output;
    } else {
      throw new \Exception("::getProductList(): There are no Ihfath Lesson Hour Products");
    }
  }

  /**
   * Returns assoc array of products mapped by the hour field
   */
  static function getProducts($omit_inactive = TRUE) {
    if (self::hasProductType()) {
      $matched_products = commerce_product_load_multiple(array(), array(
        "type" => self::productBundle
      ));

      $products = array();

      if ($omit_inactive) {
        foreach ($matched_products as $product) {
          if ((int) $product->status) {
            $products[$product->product_id] = $product;
          }
        }
      } else {
        foreach ($matched_products as $product) {
          $products[$product->product_id] = $product;
        }
      }

      return $products;
    } else {
      throw new \Exception("::getProducts(): There are no Ihfath Lesson Hour Products");
    }
  }

  static function processOrder($order) {
    if ((int) $order->uid === 0) {
      // We've nothing to do with anonymous users14253410
      return;
    }

    $users = array();

    foreach ($order->commerce_line_items['und'] as $line) {
      $line_item = commerce_line_item_load($line['line_item_id']);
      $line_item_id = $line_item->line_item_id;

      // Load the product to determine the product type for this lineitem
      $product = commerce_product_load($line_item->commerce_product["und"][0]["product_id"]);
      $product_type = $product->type;

      if ($product_type === self::productBundle) {
        // Attempt to fulfill the product checkout
        $series = db_select("ihfath_checkout_lsn", "icl")
                    ->fields("icl")
                    ->condition("icl.lineitem_id", $line_item_id, "=")
                    ->execute();

        // Get the uid
        $uid = $order->uid;

        while ($lesson = $series->fetchAssoc()) {
          $metadata = unserialize($lesson["lesson_data"]);

          // Update the user's timezone if not done already
          if (!isset($users[$uid])) {
            // Get the user object
            $user = user_load($uid);

            // Get the _approximate_ timezone name
            $tz_name = ihfathcore_tz_offset_to_name($metadata["timezone_offset"]);
            $user->timezone = $tz_name;

            // Set the gender
            $user->field_ihfath_gender["und"][0]["value"] = $metadata["gender"];

            // FIXME: Why isn't this working
            // watchdog("IHFATHCORE", "Ihfathcore\Commerce: Update @user with order data, timezone: @timezone, gender: @gender", array(
            //   "@user" => $user->name,
            //   "@timezone" => $tz_name,
            //   "@gender" => $metadata["gender"],
            // ), WATCHDOG_DEBUG, "user/{$user->uid}/edit");

            // Save user
            user_save($user);
            $users[$uid] = TRUE;
          }

          // Add order id
          foreach ($metadata["series"] as &$cur_series) {
            $cur_series["order_id"] = $order->order_id;
          }

          // Create the lessons
          ihfathlsn_auto_add_series($uid, $metadata["start"], $metadata["hours"], $metadata["series"]);
        }

        // If all goes well, mark this lineitem mapping as fulfilled
        db_update("ihfath_checkout_lsn")
          ->fields(array(
            "fulfilled" => 1
          ))
          ->condition("lineitem_id", $line_item_id, "=")
          ->execute();
      } else {
        // Unconcerned with this product
      }
    }
  }

  static function loadCartOrder() {
    global $user;

    $cart = array();
    $order = commerce_cart_order_load($user->uid);

    if ($order && isset($order->commerce_line_items['und'])) {
      foreach ($order->commerce_line_items['und'] as $line) {
        $line_item = commerce_line_item_load($line['line_item_id']);
        $line_item_id = $line_item->line_item_id;

        // Load the product to determine the product type for this lineitem
        $product = commerce_product_load($line_item->commerce_product["und"][0]["product_id"]);
        $product_type = $product->type;

        if ($product_type === self::productBundle) {
          // Attempt to fulfill the product checkout
          $series = db_select("ihfath_checkout_lsn", "icl")
                      ->fields("icl")
                      ->condition("icl.lineitem_id", $line_item_id, "=")
                      ->execute();

          while ($lesson = $series->fetchAssoc()) {
            $lesson["lesson_data"] = unserialize($lesson["lesson_data"]);
            $cart[] = $lesson;
          }
        } else {
          // Unconcerned with this product
        }
      }
    } else {
      $cart = FALSE;
    }

    return $cart;
  }

  static function destroyBaseProducts() {

  }
}
