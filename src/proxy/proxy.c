/**
 * 
 * Ghostscript proxy for Imagemagick, uses modern dynamic linker and libs
 *
 * Compile with MUSL
 *
 */

#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#define LD_PATH "/home/ihfath13/public_html/sites/all/modules/contrib/ihfath/lib64/ld-linux.so.2"
#define EXEC_PATH "/home/ihfath13/public_html/sites/all/modules/contrib/ihfath/bin/gs-main"
#define LIB_PATH "/home/ihfath13/public_html/sites/all/modules/contrib/ihfath/lib64"
// #define EXEC_PATH "/usr/bin/gs"

int main(int argc, char *argv[]) {
  setenv("LD_LIBRARY_PATH", LIB_PATH, 1);
  
  char **args = malloc(sizeof(char *) * (argc + 2));
  
  args[0] = "ld-linux.so.2";
  args[1] = EXEC_PATH;
  memcpy(args + 2, argv + 1, sizeof(char *) * (argc));

  execv(LD_PATH, (char* const*) args);
  
  return 0;
}

