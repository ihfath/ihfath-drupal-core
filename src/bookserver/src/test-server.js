"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const net_1 = require("net");
const fs_1 = require("fs");
const path_1 = require("path");
const server_1 = require("./server");
const IS_DEBUG = process.argv[2] === "-d";
const SOCK_PATH = IS_DEBUG ? "/tmp/koa_book.sock" : path_1.join(__dirname, "../../../tmp/book.sock");
const TIMEOUT_MS = 2000;
const START = new Date().getTime();
if (!fs_1.existsSync(SOCK_PATH)) {
    console.log("Socket unavailable");
    process.exit();
}
// Create dummy connection
const conn = net_1.createConnection(SOCK_PATH);
// Test fails if connection times out
const timeout = setTimeout(() => {
    conn.end();
    console.log("Failed to connect to server");
}, TIMEOUT_MS);
conn.on("error", (e) => {
    console.log("Connection failed: " + e.name);
    conn.destroy();
});
let deserialized = null;
conn.on("data", (data) => {
    deserialized = server_1.deserialize(data, deserialized);
    const { buffers } = deserialized;
    for (const buffer of buffers) {
        const decodedBuffer = server_1.decodeBuffer(buffer);
        if (decodedBuffer.type === "ping" /* PING */) {
            const now = new Date().getTime();
            // Success
            console.log(`Server pong ${now - START}ms`);
            conn.end();
            clearTimeout(timeout);
            break;
        }
    }
});
// Send "ping"
conn.write(server_1.serialize({
    type: "ping" /* PING */,
    payload: undefined,
}));
