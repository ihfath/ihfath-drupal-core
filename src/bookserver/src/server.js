"use strict";
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) if (e.indexOf(p[i]) < 0)
            t[p[i]] = s[p[i]];
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
const net_1 = require("net");
const fs_1 = require("fs");
const util_1 = require("util");
const path_1 = require("path");
const crypto_1 = require("crypto");
const child_process_1 = require("child_process");
const perf_hooks_1 = require("perf_hooks");
// Debug flag to just use system's /tmp
const IS_DEBUG = process.argv[2] === "-d";
;
const NOTIFY_INTERVAL_MS = 1000;
const PAGES_PER_BATCH = 5;
const SOCK_PATH = IS_DEBUG ? "/tmp/koa_book.sock" : path_1.join(__dirname, "../../../tmp/book.sock");
const PID_PATH = IS_DEBUG ? "/tmp/koa_book.pid" : path_1.join(__dirname, "../../../tmp/book.pid");
const IHFATH_BASE_DIR = path_1.join(__dirname, "../../..");
// Utils
const rename = util_1.promisify(fs_1.rename);
const sha256 = function (data) {
    const hash = crypto_1.createHash("sha256");
    hash.update(data);
    return hash.digest("hex");
};
;
var IPing;
(function (IPing) {
    IPing[IPing["PING"] = 0] = "PING";
    IPing[IPing["PONG"] = 1] = "PONG";
})(IPing = exports.IPing || (exports.IPing = {}));
;
// On client data event
function processClientBuffer(client, data, connData) {
    const req = decodeBuffer(data);
    switch (req.type) {
        // New connection
        case "connect" /* CONNECT */: {
            const data = req.payload;
            connData.set(client, Object.assign({ timeout: null, started: false, finished: false, newPages: {} }, data));
            // FIXME: Remove this
            console.log("Client CONNECT", {
                data,
            });
            start(client, connData);
            break;
        }
        // New job dispatched
        case "job" /* JOB */: {
            const data = req.payload;
            const origData = connData.get(client);
            connData.set(client, Object.assign(origData, data, 
            // Reset server state props
            {
                started: false,
                finished: false,
                newPages: {},
            }));
            // FIXME: Remove this
            console.log("Client JOB", {
                data,
            });
            break;
        }
        case "ping" /* PING */: {
            client.write(serialize({
                type: "ping" /* PING */,
            }));
            break;
        }
    }
}
// Serialize
function serialize(data) {
    return Buffer.from(JSON.stringify(data)).toString("base64") + "\u001B" /* DELIM */;
}
exports.serialize = serialize;
function deserialize(data, partialBufferWrap) {
    const buffers = [];
    let currentPayload = Buffer.from(data).toString();
    if (!partialBufferWrap) {
        partialBufferWrap = { buffers: [], partialBuffer: "" };
    }
    let { partialBuffer } = partialBufferWrap;
    let escIndex = 0;
    while ((escIndex = currentPayload.indexOf("\u001B" /* DELIM */)) !== -1) {
        const nextBuffer = partialBuffer + currentPayload.slice(0, escIndex);
        currentPayload = currentPayload.slice(escIndex + 1);
        // Save this buffer
        buffers.push(nextBuffer);
        // Clear partial buffer
        partialBuffer = "";
    }
    // There's a partial message that's not received yet
    if (currentPayload) {
        partialBuffer = currentPayload;
    }
    return {
        buffers,
        partialBuffer,
    };
}
exports.deserialize = deserialize;
function decodeBuffer(buffer) {
    return JSON.parse(Buffer.from(buffer, "base64").toString());
}
exports.decodeBuffer = decodeBuffer;
// Communicate with clients
function start(client, connData) {
    const data = connData.get(client);
    if (data.file) {
        processPdf(data);
    }
    // Periodically update client with status of jobs (and their completion)
    data.timeout = setInterval(() => {
        const { timeout } = data, 
        // started,
        // finished,
        copy = __rest(data, ["timeout"]);
        const payload = serialize(copy);
        client.write(payload);
        // console.log("UPDATE CLIENT", copy);
        // New job
        if (!data.started) {
            processPdf(data);
        }
    }, NOTIFY_INTERVAL_MS);
}
async function processPdf(pdf) {
    const start = pdf.progress;
    const end = Math.min(start + (PAGES_PER_BATCH - 1), pdf.pages - 1);
    const { renderDir, pdfDir } = pdf;
    pdf.started = true;
    const cmd = (!IS_DEBUG
        ? `${IHFATH_BASE_DIR}/bin/env64 ${IHFATH_BASE_DIR}/bin/`
        : "") + `convert -density 120 "${pdfDir}/${pdf.file}[${start}-${end}]" -background "#FFFFFF" -alpha remove -sharpen 0x0.25 "${renderDir}/${pdf.dir}/p%05d.jpg"`;
    const job = child_process_1.exec(cmd, {
        // Reset Dynamic linker search path for native server binary
        env: !IS_DEBUG ? {
            MAGICK_CODER_MODULE_PATH: "/home/ihfath13/.config/ImageMagick/coders",
        } : {},
    });
    console.log("Client job exec: " + cmd);
    job.on("exit", async (code) => {
        if (code === 0) {
            // Rename pages
            const newNames = {};
            const renameJobs = [];
            for (let i = start; i <= end; i++) {
                const num = (i + "").padStart(5, "0");
                const fileName = `p${num}.jpg`;
                const newFileName = num + sha256(fileName + pdf.file + perf_hooks_1.performance.now()).slice(2, 10);
                newNames[i] = pdf.dir + "/" + newFileName;
                // Set new file name
                renameJobs.push(rename(path_1.join(renderDir, pdf.dir, fileName), path_1.join(renderDir, pdf.dir, newFileName)));
            }
            // Wait till all renaming I/O finishes
            await Promise.all(renameJobs);
            // Add new page file names
            pdf.newPages = newNames;
            pdf.progress = end + 1; // Point to next pic
            if (end + 1 === pdf.pages) {
                // Finished
                pdf.finished = true;
            }
            else {
                // Do next batch of pages
                processPdf(pdf);
            }
        }
        else {
            console.error("Client job failed: " + code + "\nJob exec: " + child_process_1.exec);
        }
    });
}
function main() {
    // Connection data
    const connData = new WeakMap();
    // Start server
    const server = net_1.createServer((socket) => {
        let deserialized = null;
        socket.on("data", (data) => {
            deserialized = deserialize(data, deserialized);
            const { buffers } = deserialized;
            // Process all buffers
            for (const buffer of buffers) {
                processClientBuffer(socket, buffer, connData);
            }
        });
        socket.on("close", () => {
            const data = connData.get(socket);
            clearInterval(data && data.timeout);
        });
        socket.on("error", (e) => {
            console.log("CLIENT ERROR", e);
        });
        console.log("New client " + socket.address().toString());
    });
    // Listen on sock
    server.listen(SOCK_PATH, () => {
        console.log("Server bound on " + SOCK_PATH);
    });
    // Save PID
    fs_1.writeFileSync(PID_PATH, process.pid + "", "utf8");
    // Make socket open to others
    fs_1.chmodSync(SOCK_PATH, 0o777);
    // Catch errors
    server.on("error", (e) => {
        console.error("Error: ", e);
    });
    // Shutdown server
    process.on("SIGINT", () => {
        server.close(() => {
            console.log("\nServer shutdown");
        });
    });
    // Persist when receiving SIGHUP
    process.on("SIGHUP", () => {
        // Nothing to do...
    });
}
if (require.main === module) {
    main();
}
