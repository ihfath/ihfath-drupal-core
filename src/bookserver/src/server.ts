import { createServer } from "net";
import { rename as rn, unlinkSync, writeFileSync, chmodSync, stat } from "fs";
import { promisify } from "util";
import { join, basename, dirname } from "path";
import { createHash } from "crypto";
import { exec } from "child_process";
import { performance } from "perf_hooks";

// Debug flag to just use system's /tmp
const IS_DEBUG = process.argv[2] === "-d";

const enum CODE {
  DELIM = "\u001B",
};

const NOTIFY_INTERVAL_MS = 1000;
const PAGES_PER_BATCH = 5;
const SOCK_PATH = IS_DEBUG ? "/tmp/koa_book.sock" : join(__dirname, "../../../tmp/book.sock");
const PID_PATH = IS_DEBUG ? "/tmp/koa_book.pid" : join(__dirname, "../../../tmp/book.pid");
const IHFATH_BASE_DIR = join(__dirname, "../../..");

// Utils
const rename = promisify(rn);

const sha256 = function(data: string) {
  const hash = createHash("sha256");
  hash.update(data);
  return hash.digest("hex");
};

// Shapes
interface IConnData {
  timeout: NodeJS.Timer;
  started: boolean;
  finished: boolean;

  // PHP consumed props
  newPages: {
    [page: number]: string;
  };

  // PHP: Connection based props
  renderDir: string;
  pdfDir: string;

  // PHP: Job based props
  dir: string;
  file: string;
  progress: number;
  pages: number;
  docId: string;
}

export const enum EReqType {
  CONNECT = "connect",
  JOB = "job",
  PING = "ping",
};

interface IConnReq {
  type: EReqType;
  payload: any;
}

export interface IReqConn extends IConnReq {
  type: EReqType.CONNECT;
  payload: {
    // ...
  };
}

export interface IReqJob extends IConnReq {
  type: EReqType.JOB;
  payload: IConnData;
}

export enum IPing {
  PING,
  PONG,
};

export interface IReqPing extends IConnReq {
  type: EReqType.PING;
  payload: undefined;
}

export type IConnReqAny = IReqConn | IReqJob | IReqPing;
type TConnData = WeakMap<NodeJS.Socket, IConnReq | {}>;

// On client data event
function processClientBuffer(client: NodeJS.Socket, data: string, connData: TConnData) {
  const req: IConnReqAny = decodeBuffer(data);

  switch (req.type) {
    // New connection
    case EReqType.CONNECT: {
      const data = req.payload;
      connData.set(client, {
        timeout: null,
        started: false,
        finished: false,
        newPages: {},
        ...data,
      });

      // FIXME: Remove this
      console.log("Client CONNECT", {
        data,
      });

      start(client, connData);
      break;
    }

    // New job dispatched
    case EReqType.JOB: {
      const data = req.payload;
      const origData = connData.get(client);
      connData.set(client, Object.assign(
        origData,
        data,
        // Reset server state props
        {
          started: false,
          finished: false,
          newPages: {},
        }));

      // FIXME: Remove this
      console.log("Client JOB", {
        data,
      });

      break;
    }

    case EReqType.PING: {
      client.write(serialize({
        type: EReqType.PING,
      }));

      break;
    }
  }
}

// Serialize
export function serialize(data: any) {
  return Buffer.from(JSON.stringify(data)).toString("base64") + CODE.DELIM;
}

export interface IDeserialized {
  buffers: string[];
  partialBuffer: string;
}

export function deserialize(data: Buffer, partialBufferWrap?: IDeserialized): IDeserialized {
  const buffers: string[] = [];
  let currentPayload = Buffer.from(data).toString();

  if (!partialBufferWrap) {
    partialBufferWrap = { buffers: [], partialBuffer: "" };
  }

  let { partialBuffer } = partialBufferWrap;
  let escIndex = 0;

  while ((escIndex = currentPayload.indexOf(CODE.DELIM)) !== -1) {
    const nextBuffer = partialBuffer + currentPayload.slice(0, escIndex);
    currentPayload = currentPayload.slice(escIndex + 1);

    // Save this buffer
    buffers.push(nextBuffer);

    // Clear partial buffer
    partialBuffer = "";
  }

  // There's a partial message that's not received yet
  if (currentPayload) {
    partialBuffer = currentPayload;
  }

  return {
    buffers,
    partialBuffer,
  };
}

export function decodeBuffer<T = any>(buffer: string): T {
  return <T> JSON.parse(Buffer.from(buffer, "base64").toString());
}

// Communicate with clients
function start(client: NodeJS.Socket, connData: TConnData) {
  const data = <IConnData> connData.get(client);

  if (data.file) {
    processPdf(data);
  }

  // Periodically update client with status of jobs (and their completion)
  data.timeout = setInterval(() => {
    const {
      timeout,
      // started,
      // finished,
      ...copy
    } = data;

    const payload = serialize(copy);
    client.write(payload);

    // console.log("UPDATE CLIENT", copy);

    // New job
    if (!data.started) {
      processPdf(data);
    }
  }, NOTIFY_INTERVAL_MS);
}

async function processPdf(pdf: IConnData) {
  const start = pdf.progress;
  const end = Math.min(start + (PAGES_PER_BATCH - 1), pdf.pages - 1);
  const { renderDir, pdfDir } = pdf;

  pdf.started = true;

  const cmd = (
    !IS_DEBUG
      ? `${ IHFATH_BASE_DIR }/bin/env64 ${ IHFATH_BASE_DIR }/bin/`
      : ""
  ) + `convert -density 120 "${ pdfDir }/${ pdf.file }[${ start }-${ end }]" -background "#FFFFFF" -alpha remove -sharpen 0x0.25 "${ renderDir }/${ pdf.dir }/p%05d.jpg"`;

  const job = exec(cmd, {
    // Reset Dynamic linker search path for native server binary
    env: !IS_DEBUG ? {
      MAGICK_CODER_MODULE_PATH: "/home/ihfath13/.config/ImageMagick/coders",
    } : {},
  });

  console.log("Client job exec: " + cmd);

  job.on("exit", async (code) => {
    if (code === 0) {
      // Rename pages
      const newNames: {
        [page: number]: string;
      } = {};
      const renameJobs: Promise<any>[] = [];

      for (let i=start; i<=end; i++) {
        const num = (i + "").padStart(5, "0");
        const fileName = `p${ num }.jpg`;
        const newFileName = num + sha256(fileName + pdf.file + performance.now()).slice(2, 10);
        newNames[i] = pdf.dir + "/" + newFileName;

        // Set new file name
        renameJobs.push(rename(
          join(renderDir, pdf.dir, fileName),
          join(renderDir, pdf.dir, newFileName)
        ));
      }

      // Wait till all renaming I/O finishes
      await Promise.all(renameJobs);

      // Add new page file names
      pdf.newPages = newNames;
      pdf.progress = end + 1; // Point to next pic

      if (end + 1 === pdf.pages) {
        // Finished
        pdf.finished = true;
      } else {
        // Do next batch of pages
        processPdf(pdf);
      }
    } else {
      console.error("Client job failed: " + code + "\nJob exec: " + exec);
    }
  });
}

function main() {
  // Connection data
  const connData = new WeakMap<NodeJS.Socket, IConnReq | {}>();

  // Start server
  const server = createServer((socket) => {
    let deserialized: IDeserialized = null;

    socket.on("data", (data) => {
      deserialized = deserialize(data, deserialized);
      const { buffers } = deserialized;

      // Process all buffers
      for (const buffer of buffers) {
        processClientBuffer(socket, buffer, connData);
      }
    });

    socket.on("close", () => {
      const data = <IConnData> connData.get(socket);
      clearInterval(data && data.timeout);
    });

    socket.on("error", (e) => {
      console.log("CLIENT ERROR", e);
    });

    console.log("New client " + socket.address().toString());
  });

  // Listen on sock
  server.listen(SOCK_PATH, () => {
    console.log("Server bound on " + SOCK_PATH);
  });

  // Save PID
  writeFileSync(PID_PATH, process.pid + "", "utf8");

  // Make socket open to others
  chmodSync(SOCK_PATH, 0o777);

  // Catch errors
  server.on("error", (e) => {
    console.error("Error: ", e);
  });

  // Shutdown server
  process.on("SIGINT", () => {
    server.close(() => {
      console.log("\nServer shutdown");
    });
  });

  // Persist when receiving SIGHUP
  process.on("SIGHUP", () => {
    // Nothing to do...
  });
}

if (require.main === module) {
  main();
}
