import { createConnection } from "net";
import { existsSync } from "fs";
import { join } from "path";
import {
  serialize,
  deserialize,
  decodeBuffer,
  IDeserialized,
  EReqType,
  IReqPing,
  IConnReqAny,
} from "./server";

const IS_DEBUG = process.argv[2] === "-d";
const SOCK_PATH = IS_DEBUG ? "/tmp/koa_book.sock" : join(__dirname, "../../../tmp/book.sock");
const TIMEOUT_MS = 2000;
const START = new Date().getTime();

if (!existsSync(SOCK_PATH)) {
  console.log("Socket unavailable");
  process.exit();
}

// Create dummy connection
const conn = createConnection(SOCK_PATH);

// Test fails if connection times out
const timeout = setTimeout(() => {
  conn.end();
  console.log("Failed to connect to server");
}, TIMEOUT_MS);

conn.on("error", (e) => {
  console.log("Connection failed: " + e.name);
  conn.destroy();
});

let deserialized: IDeserialized = null;
conn.on("data", (data) => {
  deserialized = deserialize(data, deserialized);
  const { buffers } = deserialized;

  for (const buffer of buffers) {
    const decodedBuffer = decodeBuffer<IConnReqAny>(buffer);

    if (decodedBuffer.type === EReqType.PING) {
      const now = new Date().getTime();
      // Success
      console.log(`Server pong ${ now - START }ms`);
      conn.end();

      clearTimeout(timeout);
      break;
    }
  }
});

// Send "ping"
conn.write(serialize(<IReqPing> {
  type: EReqType.PING,
  payload: undefined,
}));
