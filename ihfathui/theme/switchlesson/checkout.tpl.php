<?php
global $language;
global $base_url;

$ihfath = $form["#ihfath"];
$professors = $ihfath["professors"];
$cur_lang = $language->language;

$types = $ihfath["types"];
$programs = $ihfath["programs"];
$schedule = $ihfath["schedule"];
$course = $ihfath["course"];

function _get_ihfath_prof_lesson_types($prof, $types, $programs, $course) {
  $prof_programs = isset($prof->field_ihfathlsn_prof_programs[LANGUAGE_NONE]) ? unserialize($prof->field_ihfathlsn_prof_programs[LANGUAGE_NONE][0]["value"]) : array();
  $prof_types_map = array(
    // ...
  );

  $found_course = FALSE;

  foreach ($prof_programs as $program) {
    // FIXME: Legacy code
    // Professor teaches an unavailable program
    if (!isset($programs[$program])) {
      continue;
    }

    $prog_obj = $programs[$program];

    if ($prog_obj->pid == $course->pid) {
      $found_course = TRUE;
    }

    if (!isset($prof_types_map[$prog_obj->type]) && isset($types[$prog_obj->type])) {
      $prof_types_map[$prog_obj->type] = $types[$prog_obj->type];
    }
  }

  return $found_course ? $prof_types_map : NULL;
}

// Utility
function jst(...$args) {
  return json_encode(t(...$args));
}

function jse(...$args) {
  return json_encode(...$args);
}

?>
<div class="ihfath-wrap">
  <!-- Main Input -->
  <?php
  echo drupal_render($form["checkout"]); ?>

  <div class="ihfath-checkout-main">
    <div class="ihfath-checkout-progress">
      <div class="tabs-style-ballon">
        <ul class="nav nav-tabs ihfath ihfath-center">
          <li class="ihfath-stage-header-profile active">
            <a href="javascript:0[0]"><span><span class="ihfath-progress-number">1</span><?php echo t("Choose a gender"); ?></span><div class="ihfath-arrow"></div></a>
          </li>
          <li class="ihfath-stage-header-prof ihfath-disabled">
            <a href="javascript:0[0]"><span><span class="ihfath-progress-number">2</span><?php echo t("Choose a professor"); ?></span><div class="ihfath-arrow"></div></a>
          </li>
          <li class="ihfath-stage-header-schedule ihfath-disabled">
            <a href="javascript:0[0]"><span><span class="ihfath-progress-number">3</span><?php echo t("Set your schedule"); ?></span><div class="ihfath-arrow"></div></a>
          </li>
          <li class="ihfath-stage-header-confirm ihfath-disabled">
            <a href="javascript:0[0]"><span><i class="fa fa-check"></i><?php echo t("Confirm your order"); ?></span></a>
          </li>
        </ul>
      </div>

      <div class="ihfath-stage-header-loader ihfath-hidden"></div>
    </div>
    <div class="section ihfath-checkout-section">
      <div class="container ihfath-stages">
        <div class="ihfath-hidden ihfath-stage ihfath-stage-profile ihfath-stage-selected">
          <div class="ihfath-c-horizontal-buttons">
            <div class="ihfath-inline-block ihfath-valign-middle">
              <h3>
                <?php echo t("Choose your gender") ?>
                <div class="ihfath-note-text">
                  <?php echo t("We don't allow gender mixing in our classes") ?>
                </div>
                <hr>
              </h3>

              <a href="javascript:0[0]" class="ihfath-val-male ihfath-align-left ihfath-btn-min-width-mid ihfath-btn-flat-right btn btn-xxl btn-3d btn-green btn-outlined btn-round with-icon">
                <i class="fa fa-male"></i> <?php echo t("Male") ?>
              </a>

              <a href="javascript:0[0]" class="ihfath-val-female ihfath-align-right ihfath-btn-min-width-mid ihfath-btn-flat-left btn btn-xxl btn-3d btn-green btn-outlined btn-round with-icon">
                <?php echo t("Female") ?> <i class="fa fa-female"></i>
              </a>
            </div>
          </div>
        </div>
        <div class="ihfath-hidden ihfath-stage ihfath-stage-prof">
          <div class="ihfath-center">
            <h3>
              <br>
              <br>
              <?php echo t("Choose a professor") ?>
              <hr>
            </h3>
          </div>

          <?php
          $prof_mapping = array();
          $prof_index_data = array(
            "male" => array(),
            "female" => array()
          );

          $i = 0;
          foreach ($professors as $prof) {
            $prof_lesson_types = _get_ihfath_prof_lesson_types($prof, $types, $programs, $course);

            // Check the professor teaches this course
            if (!$prof_lesson_types) {
              continue;
            }

            $prof_card_badge_width = count($prof_lesson_types) ? 100 / count($prof_lesson_types) : 100;

            $prof_first_name = isset($prof->field_ihfath_firstname[LANGUAGE_NONE]) ? $prof->field_ihfath_firstname[LANGUAGE_NONE][0]["value"] : "";
            $prof_last_name = isset($prof->field_ihfath_lastname[LANGUAGE_NONE]) ? $prof->field_ihfath_lastname[LANGUAGE_NONE][0]["value"] : "";
            $prof_real_name = $prof_first_name . " " . $prof_last_name;

            // Add to index
            $prof_index = count($prof_mapping);
            $prof_mapping[$prof->uid] = TRUE;

            $gender_index = &$prof_index_data[$prof->field_ihfath_gender["und"][0]["value"]];
            foreach ($prof_lesson_types as $tid => $prof_type) {
              if (!isset($gender_index[$tid])) {
                $gender_index[$tid] = array();
              }

              $gender_index[$tid][] = $prof->uid;
            }

            ?>

              <div class="ihfath-professor-card-wrap" data-value="<?php echo $prof->uid ?>">
                <div class="ihfath-professor-card" data-value="<?php echo $prof->uid ?>">
                  <div class="ihfath-professor-card-header">
                    <i class="fa fa-group main-color"></i>
                    Professor <span class="main-color"><?php echo $prof_real_name ?></span>

                    <div class="ihfath-float-right">
                      <i class="fa fa-globe"></i>
                      Egyptian
                    </div>
                  </div>
                  <div style="clear: both;"></div>
                  <div class="ihfath-professor-card-badges">

                    <?php
                    foreach ($prof_lesson_types as $tid => $prof_type) { ?>

                      <div class="ihfath-card-badge" style="width: <?php echo $prof_card_badge_width ?>%">
                        <i class="fa fa-graduation-cap main-color"></i>
                        <?php echo $prof_type->data["current"]->name ?>
                      </div>

                    <?php
                    } ?>

                  </div>
                </div>
              </div>

          <?php
          $i++;
          } ?>

          <script type="text/javascript">
            window.addEventListener("load", function() {
              /**
               * Professor filtering functionality
               */
              const program = <?= jse(+$course->pid) ?>;
              var profState = <?= jse($prof_mapping) ?>;
              var baseProfState = {};
              var profIndex = <?= jse($prof_index_data) ?>;
              var profIdMap = {};
              var profIds = [];

              // Get prof id-card mapping
              jSh(".ihfath-professor-card-wrap").forEach(function(profCard) {
                var profId = profCard.dataset.value;

                profIdMap[profId] = profCard;
                baseProfState[profId] = false;
                profIds.push(profId);
              });

              function updateProfessors(profile) {
                var newState = jSh.extendObj({}, baseProfState);

                (profIndex[profile][program] || []).forEach(function(profId) {
                  newState[profId] = true;
                });

                profIds.forEach(function(profId) {
                  var newProfState = newState[profId];
                  var profCard = profIdMap[profId];

                  if (newProfState !== profState[profId]) {
                    if (newProfState) {
                      profCard.classList.remove("ihfath-hidden");
                    } else {
                      profCard.classList.add("ihfath-hidden");
                    }
                  }
                });

                profState = newState;
              }

              window.ihfathUpdateProfessors = updateProfessors;
            });
          </script>
        </div>
        <div class="ihfath-hidden ihfath-stage ihfath-stage-schedule">
          <div class="ihfath-center">
            <h3>
              <br>
              <br>
              <?php echo t("Set your schedule") ?>
            </h3>
          </div>

          <div class="ihfath-checkout-schedule">
            <div id="ihfath-checkout-schedule-root">
              <!-- JS generated schedule here -->
            </div>
          </div>

          <hr>
          <!-- Back button -->
          <a href="javascript:0[0]" class="ihfath-back-to-prof ihfath-align-left ihfath-btn-flat-right btn btn-lg btn-3d btn-green btn-outlined btn-round with-icon">
            <i class="fa fa-arrow-left"></i> <?php echo t("Back to professors list") ?>
          </a>

          <!-- Next button -->
          <a href="javascript:0[0]" disabled="disabled" class="ihfath-continue-checkout ihfath-float-right ihfath-align-right ihfath-btn-flat-left btn btn-lg btn-3d btn-green btn-outlined btn-round with-icon">
            <?php echo t("Continue to checkout") ?> <i class="fa fa-arrow-right"></i>
          </a>
        </div>

        <div class="ihfath-hidden ihfath-stage ihfath-stage-confirm">
          <div class="ihfath-c-horizontal-buttons">
            <div class="ihfath-inline-block ihfath-valign-middle">
              <h3>
                <?php echo t("Add to cart and...") ?>
                <hr>
              </h3>

              <?php
              $submit_more_btn = &$form["submitmore"];
              $submit_more_btn["#attributes"]["class"] = array(
                "btn",
                "btn-xxl",
                "btn-3d",
                "btn-green",
                "btn-outlined",
                "btn-round",
                "ihfath-btn-min-width-mid",
                "ihfath-btn-flat-right"
              );

              $submit_btn = &$form["submit"];
              $submit_btn["#attributes"]["class"] = array(
                "btn",
                "btn-xxl",
                "btn-3d",
                "btn-green",
                "btn-outlined",
                "btn-round",
                "ihfath-btn-min-width-mid",
                "ihfath-btn-flat-left"
              );

              // Remove "main-bg" class via somewhat crude means...
              echo str_replace(array(
                "main-bg",
                "shape",
                "btn-lg"
              ), array(
                "",
                "",
                ""
              ), drupal_render($submit_more_btn)); ?>

              <span class="ihfath-note-text ihfath-confirm-separator"><?php echo t("- or -") ?></span>

              <?php
              echo str_replace(array(
                "main-bg",
                "shape",
                "btn-lg"
              ), array(
                "",
                "",
                ""
              ), drupal_render($submit_btn)); ?>
            </div>
          </div>
        </div>
        <div class="ihfath-hidden ihfath-stage ihfath-stage-loader">
          <div class="ihfath-c-horizontal-buttons">
            <div class="ihfath-inline-block ihfath-valign-middle">
              <div class="ihfath-note-text">
                <?php echo str_replace(array(
                  "@prof"
                ), array(
                  '<span class="ihfath-prof-name"></span>'
                ), t("Loading @prof...")); ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="ihfath-stage-navigation">
      <div class="">
        <!-- FIXME: Should this be removed -->
      </div>
    </div>

    <?php
    $programs_json = array();

    foreach ($programs as $program) {
      if (!isset($programs_json[$program->type])) {
        $programs_json[$program->type] = array();
      }

      $programs_json[$program->type][] = array(
        $program->data["current"]->name,
        (int) $program->pid,
      );
    }
     ?>

    <!-- Schedule loading logic -->
    <script type="text/javascript">
      window.addEventListener("load", function() {
        var scheduleData = <?= jse($schedule ? $schedule : NULL) ?>;
        var programData  = <?= jse($programs_json) ?>;

        // Schedule cell state mappings
        var mapStateNames = [
          <?= jst("Unavailable") ?>,
          <?= jst("Vacant") ?>,
          <?= jst("Booked") ?>,
          <?= jst("Weekend") ?>,
        ];
        var dayMap = [
          <?= jst("Monday") ?>,
          <?= jst("Tuesday") ?>,
          <?= jst("Wednesday") ?>,
          <?= jst("Thursday") ?>,
          <?= jst("Friday") ?>,
          <?= jst("Saturday") ?>,
          <?= jst("Sunday") ?>,
        ];

        IhfathCheckout({
          schedule: {
            data: scheduleData,
            dimensions: [
              7, 24
            ],

            types: <?= jse($types) ?>,
            programs: <?= jse(array_values($programs)) ?>,
            curProgramId: <?= jse((int) $course->pid) ?>,
            hours: [
              1, 1.5, 2
            ],

            time: <?= jse(t("Time") . ":") ?>,
            locale: {
              clearCell: <?= jst("Clear cell") ?>,
              hours: <?= jst("Hours") ?>,
              bookedByUser: <?= jst("You've already booked this in your cart") ?>,
            },
            dayMap: dayMap,
            mapStateNames: mapStateNames,
          },

          <?php
          // Stages are bound to "header" and a "body" elements with the classnames
          // "ihfath-stage-header-STAGE" and "ihfath-stage-STAGE" respectively ?>
          stages: [{
            /**
             * PROFILE STAGE
             */
            name: "profile",
            setup: function(ctrl, main) {
              var btnMap = {};

              ["male", "female"].forEach(function(profile) {
                var btn = ctrl.dom.body.jSh(".ihfath-val-" + profile)[0];
                btnMap[profile] = btn;

                btn.addEventListener("click", function() {
                  ctrl.setValue(ctrl.name, profile);
                  ctrl.setStage(ctrl.getNextStage());
                });
              });

              // Highlight buttons when they change
              ctrl.onChange(function(profile) {
                var oldProfile = this.oldStateStatus;

                // oldProfile could be null
                if (oldProfile) {
                  btnMap[oldProfile].classList.remove("ihfath-selected");
                }

                btnMap[profile].classList.add("ihfath-selected");
              });
            }
          }, {
            /**
             * PROFESSOR STAGE
             */
            name: "prof",
            setup: function(ctrl, main) {
              var cards = jSh.toArr(ctrl.dom.body.jSh(".ihfath-professor-card"));
              var cardMap = {};

              cards.forEach(function(card) {
                var cardValue = +card.dataset.value;
                cardMap[cardValue] = card;

                card.addEventListener("click", function() {
                  ctrl.setValue(ctrl.name, cardValue);
                  ctrl.loadStage(ctrl.getNextStage());
                });
              });

              // Highlight cards when they change
              ctrl.onChange(function(prof) {
                var oldProf = this.oldStateStatus;

                // oldProfile could be null
                if (oldProf) {
                  cardMap[oldProf].classList.remove("ihfath-selected");
                }

                cardMap[prof].classList.add("ihfath-selected");
              });
            }
          }, {
            /**
             * SCHEDULE STAGE
             */
            name: "schedule",
            setup: function(ctrl, main) {
              var timezone = -1 * ((new Date().getTimezoneOffset()) / 60);
              var schedule = main.scheduleCtrl.schedule;
              var scheduleCache = {
                // "profId": {profSchedule}
              };
              var modal = schedule.modal;
              modal.setProgramList(<?= jse(array_values($programs_json)[0]) ?>);

              ctrl.on("stagestatechange", function(e) {
                if (e.stage === "prof") {
                  var newProf = e.value;

                  if (!scheduleCache[newProf]) {
                    var req = new lcRequest({
                      uri: "<?= $base_url ?>/ihfath/ajax/professor-schedule/" + newProf + "/" + timezone,
                      success: function() {
                        var newTable = JSON.parse(this.responseText);
                        scheduleCache[newProf] = schedule.genStateTable(newTable.table);

                        // Render new table data
                        schedule.rebuild(schedule.mapEachCell(newTable.table, function(data) {
                          return {
                            dur: 0,
                            pid: 0,
                            hour: data,
                          };
                        }));

                        // Load next stage
                        ctrl.stageLoaded();
                      }
                    });

                    req.send();
                  } else {
                    setTimeout(function() {
                      // Render new table data
                      schedule.rebuild(schedule.mapEachCell(scheduleCache[newProf], function(data) {
                        return {
                          dur: 0,
                          pid: 0,
                          hour: data,
                        };
                      }));

                      // Load schedule stage
                      ctrl.stageLoaded();
                    }, 10);
                  }
                }
              });

              ctrl.addStateListener("visible", function(visible) {
                if (!visible) {
                  // Hide modal if user navigates to another stage
                  main.scheduleCtrl.hideModal();
                }
              });

              // Add schedule event listener
              schedule.on("update", function(data) {
                ctrl.setValue(ctrl.name, data.lessons);

                // Disable/enable button
                if (data.lessons.length) {
                  next.removeAttribute("disabled");
                } else {
                  next.setAttribute("disabled", "disabled");

                  // Disable next stage
                  ctrl.setStageState(ctrl.getNextStage(), false);
                }
              });

              // Add btn event listeners
              var back = ctrl.dom.body.jSh(".ihfath-back-to-prof")[0];
              var next = ctrl.dom.body.jSh(".ihfath-continue-checkout")[0];

              back.addEventListener("click", function() {
                ctrl.setStage(ctrl.getPrevStage());
              });

              next.addEventListener("click", function() {
                ctrl.setStage(ctrl.getNextStage());

                // Go to the top of the page
                window.scrollTo(screenX, 0);
              });
            }
          }, {
            /**
             * CONFIRMATION STAGE
             */
            name: "confirm",
            setup: function(ctrl, main) {
              ctrl.on("stagestatechange", function(e) {
                console.log("CHANGE", e);
              });
            }
          }, {
            /**
             * LOADER PSEUDO-STAGE
             */
            name: "loader",
            setup: function(ctrl, main) {
              // Nothing here
            }
          }],

          stageSetup: function(main) {
            var input = jSh("#ihfath-checkout-data");
            var timezone = -1 * ((new Date().getTimezoneOffset()) / 60);

            // Save the stage values on change and update professor cards
            main.on("stagestatechange", function(e) {
              var stageData = main.serialize();
              stageData.timezoneOffset = timezone;

              input.value = JSON.stringify(stageData);

              // Update professor cards
              ihfathUpdateProfessors(main.profile || "male");
            });
          }
        });
      });
    </script>
  </div>
</div>

<div class="ihfath-hidden">
  <?php
  // Meta elements
  echo drupal_render_children($form); ?>
</div>

<style media="screen">
  .ihfath-center {
    text-align: center;
  }

  .ihfath-float-right {
    float: right;
  }

  .ihfath-inline-block {
    display: inline-block;
    line-height: 1.5;
  }

  .ihfath-valign-middle {
    vertical-align: middle;
  }

  .ihfath-hidden {
    display: none !important;
  }

  .section.ihfath-checkout-section {
    padding-top: 0px;
  }

  .ihfath-stages {
    min-height: 640px;
  }

  .ihfath-stage {
    width: 100%;
    height: 100%;
  }

  .ihfath-stage .row {
    margin-bottom: 30px;
  }

  .ihfath-small-hr {
    width: 200px;
  }

  .ihfath-note-text {
    font-style: italic;
    font-size: 13px;
    font-weight: normal;
    color: #aaa;
  }

  .ihfath-c-horizontal-buttons {
    position: absolute;
    top: 0px;
    right: 0px;
    bottom: 0px;
    left: 0px;
    width: 100%;
    height: 250px;
    margin: auto;

    line-height: 350px;
    text-align: center;
  }

  .ihfath-stages .ihfath-stage .btn-green:hover,
  .ihfath-stages .ihfath-stage .btn-green.ihfath-selected {
    color: #fff;
    background: #6dab3c;
    border-color: #6dab3c;
  }

  .btn.ihfath-btn-min-width-mid {
    min-width: 225px;
  }

  .btn.ihfath-btn-min-width-small {
    min-width: 150px;
  }

  .ihfath-align-left {
    text-align: left;
  }

  .ihfath-align-right {
    text-align: right;
  }

  .btn.ihfath-btn-flat-left {
    border-top-left-radius: 0px;
    border-bottom-left-radius: 0px;
  }

  .btn.ihfath-btn-flat-right {
    border-top-right-radius: 0px;
    border-bottom-right-radius: 0px;
  }

  .ihfath-checkout-progress .nav-tabs {
    font-size: 0px;
  }

  .ihfath-checkout-progress .nav-tabs > li {
    position: relative;
    display: inline-block;
    margin-bottom: 0px !important;
    float: none;

    font-size: 15px;
  }

  .ihfath-checkout-progress .nav-tabs > li.ihfath-disabled {
    opacity: 0.6;
    cursor: default;
  }

  .ihfath-checkout-progress .nav-tabs > li.ihfath-disabled a {
    cursor: default;
  }

  .ihfath-checkout-progress .nav-tabs > li a {
    padding-left: 30px;
    padding-right: 15px;
    border: 0px !important;
  }

  .ihfath-checkout-progress .nav-tabs > li:first-child a {
    padding-left: 20px;
  }

  .ihfath-checkout-progress .nav-tabs > li:last-child a {
    padding-right: 20px;
  }

  .ihfath-progress-number {
    position: relative;
    display: inline-block;
    vertical-align: middle;
    height: 18px;
    width: 18px;
    margin-right: 10px;

    font-size: 13px;
    font-family: Arial;
    font-weight: bold;
    line-height: 19px;
    text-align: center;
  }

  .ihfath-progress-number::after {
    content: "";
    position: absolute;
    top: 50%;
    left: 50%;
    width: 24px;
    height: 24px;
    /*box-sizing: border-box;*/

    border: 1px solid #444;
    border-radius: 100%;

    transform: translate(-50%, -50%);
    pointer-events: none;
  }

  li.active .ihfath-progress-number::after {
    border-color: #fff;
    box-shadow: 1px 1px 1px rgba(0,0,0,.15);
  }

  li:not(.active):hover .ihfath-progress-number::after {
    border-color: #a9bf04;
  }

  .ihfath-arrow {
    position: absolute;
    display: inline-block;
    z-index: 100;
    top: 0px;
    right: -14px;
    height: 100%;
    width: 26px;
    overflow: hidden;
  }

  .ihfath-arrow::after,
  .ihfath-arrow::before {
    content: "";
    position: absolute;
    z-index: 100;
    right: 0px;
    width: 500px;
    height: 500px;

    background: #EAEAEA;
    border-right: 2px solid #C7C7C7;
    transition: background 0.2s, color 0.2s;
  }

  .ihfath-arrow::before {
    bottom: 50%;
    transform: rotate(-25deg);
    transform-origin: bottom right;
  }

  .ihfath-arrow::after {
    top: 50%;
    transform: rotate(25deg);
    transform-origin: top right;
  }

  li.active .ihfath-arrow::after,
  li.active .ihfath-arrow::before {
    background: #a9bf04;
  }

  /* Professor Cards */
  .ihfath-stage-prof {
    position: relative;
    font-size: 0px;
  }

  .ihfath-professor-card-wrap {
    display: inline-block;
    width: 50%;
    padding: 0px 15px 30px;
  }

  .ihfath-professor-card {
    border: 1px solid #e1e1e1;
    border-radius: 4px;

    cursor: pointer;
  }

  .ihfath-professor-card-header {
    height: 50px;
    line-height: 50px;

    border-bottom: inherit;
    font-weight: bold;
    font-size: 14px;
  }

  .ihfath-professor-card-header > .fa {
    display: inline-block;
    width: 50px;
    height: 50px;
    vertical-align: middle;

    text-align: center;
    line-height: 50px;
  }

  .ihfath-professor-card-header .ihfath-float-right {
    margin-right: 20px;

    font-weight: normal;
  }

  .ihfath-professor-card-badges {
    position: relative;
    min-height: 4px;

    line-height: 40px;
    font-size: 0px;
    background: rgba(0, 0, 0, 0.03);
  }

  .ihfath-card-badge {
    display: inline-block;
    vertical-align: middle;

    text-align: center;
    font-size: 13px;
  }

  /* Click */
  .ihfath-professor-card:hover,
  .ihfath-professor-card.ihfath-selected {
    border-color: #a9bf04;
    background: #a9bf04;
    box-shadow: 0px 2px 3px rgba(0, 0, 0, 0.225);
    text-shadow: 1px 1px 1px rgba(0, 0, 0, 0.15);
  }

  .ihfath-professor-card:hover *,
  .ihfath-professor-card.ihfath-selected * {
    color: #fff !important;
  }

  .ihfath-professor-card:hover .ihfath-professor-card-header,
  .ihfath-professor-card.ihfath-selected .ihfath-professor-card-header {
    border-color: rgba(255, 255, 255, 0.25);
  }

  /* Schedule stage */
  .ihfath-stage .btn[disabled] {
    pointer-events: none;
  }

  /* Confirmation stage */
  .ihfath-stage-confirm .ihfath-confirm-separator {
    display: inline-block;
    vertical-align: middle;
    height: 50px;
    margin: 0px 20px;

    line-height: 50px;
    font-size: 25px;
  }
</style>
