<?php
// Exposed:
//   - $lesson    Current lesson being switched _from_
//   - $program   Program for the current lesson
//   - $prof      Current professor for the switched lesson
//   - $schedule  Schedule for the professor

// Utility
function jst(...$args) {
  return json_encode(t(...$args));
}

function jse(...$args) {
  return json_encode(...$args);
}
?>
<div class="ihfath-wrap">
  <h3 class="ihfath-center">
    <br>
    <?= t("Switch your class") ?>
    <br>
  </h3>
  <div class="ihfath-schedule">
    <!-- Schedule -->
  </div>
  <div class="ihfath-actions">
    <!-- Back button -->
    <a
      href="<?= url('<front>') ?>"
      class="ihfath-back-to-dash ihfath-align-left ihfath-btn-flat-right btn btn-lg btn-3d btn-green btn-outlined btn-round with-icon">
      <i class="fa fa-arrow-left"></i> <?= t("Back to dashboard") ?>
    </a>

    <!-- Next button -->
    <a href="javascript:0[0]" disabled="disabled" class="ihfath-continue-switch-lesson ihfath-float-right ihfath-align-right ihfath-btn-flat-left btn btn-lg btn-3d btn-green btn-outlined btn-round with-icon">
      <?= t("Switch lesson") ?> <i class="fa fa-arrow-right"></i>
    </a>
  </div>
</div>

<style>
  .pageContent > div.section {
    padding-top: 0;
  }
</style>

<script type="text/javascript">
  window.addEventListener("load", function() {
    const profId = <?= jse($prof->uid) ?>;
    const schedule = <?= jse($schedule) ?>;
    const duration = <?= jse($lesson->paid_duration / 60) ?>;
    const programName = <?= jse($program->data["current"]->name) ?>;
    const lid = <?= jse($lesson->lid) ?>;
    const switchApi = <?= jse(url("ihfath/ajax/student/switchLesson")) ?>;
    const frontPage = <?= jse(url("<front>")) ?>;

    // Locale
    var dayMap = [
      <?= jst("Monday") ?>,
      <?= jst("Tuesday") ?>,
      <?= jst("Wednesday") ?>,
      <?= jst("Thursday") ?>,
      <?= jst("Friday") ?>,
      <?= jst("Saturday") ?>,
      <?= jst("Sunday") ?>,
    ];

    IhfathSwitchLesson({
      root: document.querySelector(".ihfath-schedule"),
      switchApi,
      frontPage,
      switchLessonBtn: document.querySelector(".ihfath-continue-switch-lesson"),
      data: schedule,
      duration,
      programName,
      lid,
      dimensions: [
        7, 24
      ],

      dayMap,

      locale: {
        bookedByUser: <?= jst("You've already booked this in your cart") ?>,
        time: <?= jse(t("Time") . ":") ?>,
      },
    });
  });
</script>
