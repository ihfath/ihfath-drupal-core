(function () {
	'use strict';

	function noop() {}

	function assign(tar, src) {
		for (var k in src) tar[k] = src[k];
		return tar;
	}

	function assignTrue(tar, src) {
		for (var k in src) tar[k] = 1;
		return tar;
	}

	function addLoc(element, file, line, column, char) {
		element.__svelte_meta = {
			loc: { file, line, column, char }
		};
	}

	function append(target, node) {
		target.appendChild(node);
	}

	function insert(target, node, anchor) {
		target.insertBefore(node, anchor);
	}

	function detachNode(node) {
		node.parentNode.removeChild(node);
	}

	function destroyEach(iterations, detach) {
		for (var i = 0; i < iterations.length; i += 1) {
			if (iterations[i]) iterations[i].d(detach);
		}
	}

	function createElement(name) {
		return document.createElement(name);
	}

	function createText(data) {
		return document.createTextNode(data);
	}

	function createComment() {
		return document.createComment('');
	}

	function addListener(node, event, handler, options) {
		node.addEventListener(event, handler, options);
	}

	function removeListener(node, event, handler, options) {
		node.removeEventListener(event, handler, options);
	}

	function setAttribute(node, attribute, value) {
		if (value == null) node.removeAttribute(attribute);
		else node.setAttribute(attribute, value);
	}

	function setData(text, data) {
		text.data = '' + data;
	}

	function toggleClass(element, name, toggle) {
		element.classList[toggle ? 'add' : 'remove'](name);
	}

	function blankObject() {
		return Object.create(null);
	}

	function destroy(detach) {
		this.destroy = noop;
		this.fire('destroy');
		this.set = noop;

		this._fragment.d(detach !== false);
		this._fragment = null;
		this._state = {};
	}

	function destroyDev(detach) {
		destroy.call(this, detach);
		this.destroy = function() {
			console.warn('Component was already destroyed');
		};
	}

	function _differs(a, b) {
		return a != a ? b == b : a !== b || ((a && typeof a === 'object') || typeof a === 'function');
	}

	function _differsImmutable(a, b) {
		return a != a ? b == b : a !== b;
	}

	function fire(eventName, data) {
		var handlers =
			eventName in this._handlers && this._handlers[eventName].slice();
		if (!handlers) return;

		for (var i = 0; i < handlers.length; i += 1) {
			var handler = handlers[i];

			if (!handler.__calling) {
				try {
					handler.__calling = true;
					handler.call(this, data);
				} finally {
					handler.__calling = false;
				}
			}
		}
	}

	function flush(component) {
		component._lock = true;
		callAll(component._beforecreate);
		callAll(component._oncreate);
		callAll(component._aftercreate);
		component._lock = false;
	}

	function get() {
		return this._state;
	}

	function init(component, options) {
		component._handlers = blankObject();
		component._slots = blankObject();
		component._bind = options._bind;
		component._staged = {};

		component.options = options;
		component.root = options.root || component;
		component.store = options.store || component.root.store;

		if (!options.root) {
			component._beforecreate = [];
			component._oncreate = [];
			component._aftercreate = [];
		}
	}

	function on(eventName, handler) {
		var handlers = this._handlers[eventName] || (this._handlers[eventName] = []);
		handlers.push(handler);

		return {
			cancel: function() {
				var index = handlers.indexOf(handler);
				if (~index) handlers.splice(index, 1);
			}
		};
	}

	function set(newState) {
		this._set(assign({}, newState));
		if (this.root._lock) return;
		flush(this.root);
	}

	function _set(newState) {
		var oldState = this._state,
			changed = {},
			dirty = false;

		newState = assign(this._staged, newState);
		this._staged = {};

		for (var key in newState) {
			if (this._differs(newState[key], oldState[key])) changed[key] = dirty = true;
		}
		if (!dirty) return;

		this._state = assign(assign({}, oldState), newState);
		this._recompute(changed, this._state);
		if (this._bind) this._bind(changed, this._state);

		if (this._fragment) {
			this.fire("state", { changed: changed, current: this._state, previous: oldState });
			this._fragment.p(changed, this._state);
			this.fire("update", { changed: changed, current: this._state, previous: oldState });
		}
	}

	function _stage(newState) {
		assign(this._staged, newState);
	}

	function setDev(newState) {
		if (typeof newState !== 'object') {
			throw new Error(
				this._debugName + '.set was called without an object of data key-values to update.'
			);
		}

		this._checkReadOnly(newState);
		set.call(this, newState);
	}

	function callAll(fns) {
		while (fns && fns.length) fns.shift()();
	}

	function _mount(target, anchor) {
		this._fragment[this._fragment.i ? 'i' : 'm'](target, anchor || null);
	}

	function removeFromStore() {
		this.store._remove(this);
	}

	var protoDev = {
		destroy: destroyDev,
		get,
		fire,
		on,
		set: setDev,
		_recompute: noop,
		_set,
		_stage,
		_mount,
		_differs
	};

	function Store(state, options) {
		this._handlers = {};
		this._dependents = [];

		this._computed = blankObject();
		this._sortedComputedProperties = [];

		this._state = assign({}, state);
		this._differs = options && options.immutable ? _differsImmutable : _differs;
	}

	assign(Store.prototype, {
		_add(component, props) {
			this._dependents.push({
				component: component,
				props: props
			});
		},

		_init(props) {
			const state = {};
			for (let i = 0; i < props.length; i += 1) {
				const prop = props[i];
				state['$' + prop] = this._state[prop];
			}
			return state;
		},

		_remove(component) {
			let i = this._dependents.length;
			while (i--) {
				if (this._dependents[i].component === component) {
					this._dependents.splice(i, 1);
					return;
				}
			}
		},

		_set(newState, changed) {
			const previous = this._state;
			this._state = assign(assign({}, previous), newState);

			for (let i = 0; i < this._sortedComputedProperties.length; i += 1) {
				this._sortedComputedProperties[i].update(this._state, changed);
			}

			this.fire('state', {
				changed,
				previous,
				current: this._state
			});

			this._dependents
				.filter(dependent => {
					const componentState = {};
					let dirty = false;

					for (let j = 0; j < dependent.props.length; j += 1) {
						const prop = dependent.props[j];
						if (prop in changed) {
							componentState['$' + prop] = this._state[prop];
							dirty = true;
						}
					}

					if (dirty) {
						dependent.component._stage(componentState);
						return true;
					}
				})
				.forEach(dependent => {
					dependent.component.set({});
				});

			this.fire('update', {
				changed,
				previous,
				current: this._state
			});
		},

		_sortComputedProperties() {
			const computed = this._computed;
			const sorted = this._sortedComputedProperties = [];
			const visited = blankObject();
			let currentKey;

			function visit(key) {
				const c = computed[key];

				if (c) {
					c.deps.forEach(dep => {
						if (dep === currentKey) {
							throw new Error(`Cyclical dependency detected between ${dep} <-> ${key}`);
						}

						visit(dep);
					});

					if (!visited[key]) {
						visited[key] = true;
						sorted.push(c);
					}
				}
			}

			for (const key in this._computed) {
				visit(currentKey = key);
			}
		},

		compute(key, deps, fn) {
			let value;

			const c = {
				deps,
				update: (state, changed, dirty) => {
					const values = deps.map(dep => {
						if (dep in changed) dirty = true;
						return state[dep];
					});

					if (dirty) {
						const newValue = fn.apply(null, values);
						if (this._differs(newValue, value)) {
							value = newValue;
							changed[key] = true;
							state[key] = value;
						}
					}
				}
			};

			this._computed[key] = c;
			this._sortComputedProperties();

			const state = assign({}, this._state);
			const changed = {};
			c.update(state, changed, true);
			this._set(state, changed);
		},

		fire,

		get,

		on,

		set(newState) {
			const oldState = this._state;
			const changed = this._changed = {};
			let dirty = false;

			for (const key in newState) {
				if (this._computed[key]) throw new Error(`'${key}' is a read-only computed property`);
				if (this._differs(newState[key], oldState[key])) changed[key] = dirty = true;
			}
			if (!dirty) return;

			this._set(newState, changed);
		}
	});

	var UserRole;
	(function (UserRole) {
	    UserRole["Professor"] = "2";
	    UserRole["Student"] = "3";
	})(UserRole || (UserRole = {}));
	// Side effects to keep in Rollup
	const _ = () => {
	    document.createComment("");
	};

	/* src/components/Dropdown/Dropdown.html generated by Svelte v2.16.0 */

	function _index({ value, list }) {
	  let index = 0;

	  for (let i=0; i<list.length; i++) {
	    const item = list[i];

	    if (item[0] === value) {
	      index = i;
	      break;
	    }
	  }

	  return index;
	}

	function data() {
	  return {
	    value: null,
	    list: [],
	    visible: false,
	    disabled: false,
	    right: false,
	  }
	}
	var methods = {
	  change(value) {
	    this.set({
	      value,
	      visible: false,
	    });
	    this.fire("change", {
	      value,
	    });
	  }
	};

	function oncreate() {
	  const listener = (e) => {
	    let target = e.target;

	    while (target !== document.body) {
	      if (target === this.refs.dropdown) {
	        return;
	      }

	      target = target.parentNode;
	    }

	    this.set({
	      visible: false,
	    });
	  };

	  this.on("state", ({ current }) => {
	    if (current.visible) {
	      window.addEventListener("click", listener);
	    } else {
	      window.removeEventListener("click", listener);
	    }
	  });

	  this.on("destroy", () => {
	    window.removeEventListener("click", listener);
	  });
	}
	const file = "src/components/Dropdown/Dropdown.html";

	function click_handler(event) {
		const { component, ctx } = this._svelte;

		component.change(ctx.item[0]);
	}

	function get_each_context(ctx, list, i) {
		const child_ctx = Object.create(ctx);
		child_ctx.item = list[i];
		return child_ctx;
	}

	function create_main_fragment(component, ctx) {
		var div3, div0, button, span0, text0_value = (ctx.list[ctx._index] || "  ")[1], text0, text1, span1, i, text2, div2, div1;

		function click_handler(event) {
			component.set({visible: !ctx.disabled});
		}

		var each_value = ctx.list;

		var each_blocks = [];

		for (var i_1 = 0; i_1 < each_value.length; i_1 += 1) {
			each_blocks[i_1] = create_each_block(component, get_each_context(ctx, each_value, i_1));
		}

		return {
			c: function create() {
				div3 = createElement("div");
				div0 = createElement("div");
				button = createElement("button");
				span0 = createElement("span");
				text0 = createText(text0_value);
				text1 = createText("\n\n      ");
				span1 = createElement("span");
				i = createElement("i");
				text2 = createText("\n  ");
				div2 = createElement("div");
				div1 = createElement("div");

				for (var i_1 = 0; i_1 < each_blocks.length; i_1 += 1) {
					each_blocks[i_1].c();
				}
				addLoc(span0, file, 14, 6, 312);
				i.className = "fa fa-angle-down";
				addLoc(i, file, 17, 8, 399);
				span1.className = "icon is-small";
				addLoc(span1, file, 16, 6, 362);
				addListener(button, "click", click_handler);
				button.className = "button";
				button.type = "button";
				addLoc(button, file, 8, 4, 182);
				div0.className = "dropdown-trigger svelte-d6sc07";
				addLoc(div0, file, 7, 2, 147);
				div1.className = "dropdown-content svelte-d6sc07";
				addLoc(div1, file, 22, 4, 503);
				div2.className = "dropdown-menu svelte-d6sc07";
				addLoc(div2, file, 21, 2, 471);
				div3.className = "dropdown svelte-d6sc07";
				div3.tabIndex = "0";
				toggleClass(div3, "dropdown-disabled", ctx.disabled);
				toggleClass(div3, "is-active", ctx.visible);
				toggleClass(div3, "is-right", ctx.right);
				addLoc(div3, file, 0, 0, 0);
			},

			m: function mount(target, anchor) {
				insert(target, div3, anchor);
				append(div3, div0);
				append(div0, button);
				append(button, span0);
				append(span0, text0);
				append(button, text1);
				append(button, span1);
				append(span1, i);
				append(div3, text2);
				append(div3, div2);
				append(div2, div1);

				for (var i_1 = 0; i_1 < each_blocks.length; i_1 += 1) {
					each_blocks[i_1].m(div1, null);
				}

				component.refs.dropdown = div3;
			},

			p: function update(changed, _ctx) {
				ctx = _ctx;
				if ((changed.list || changed._index) && text0_value !== (text0_value = (ctx.list[ctx._index] || "  ")[1])) {
					setData(text0, text0_value);
				}

				if (changed.list) {
					each_value = ctx.list;

					for (var i_1 = 0; i_1 < each_value.length; i_1 += 1) {
						const child_ctx = get_each_context(ctx, each_value, i_1);

						if (each_blocks[i_1]) {
							each_blocks[i_1].p(changed, child_ctx);
						} else {
							each_blocks[i_1] = create_each_block(component, child_ctx);
							each_blocks[i_1].c();
							each_blocks[i_1].m(div1, null);
						}
					}

					for (; i_1 < each_blocks.length; i_1 += 1) {
						each_blocks[i_1].d(1);
					}
					each_blocks.length = each_value.length;
				}

				if (changed.disabled) {
					toggleClass(div3, "dropdown-disabled", ctx.disabled);
				}

				if (changed.visible) {
					toggleClass(div3, "is-active", ctx.visible);
				}

				if (changed.right) {
					toggleClass(div3, "is-right", ctx.right);
				}
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(div3);
				}

				removeListener(button, "click", click_handler);

				destroyEach(each_blocks, detach);

				if (component.refs.dropdown === div3) component.refs.dropdown = null;
			}
		};
	}

	// (27:8) {:else}
	function create_else_block(component, ctx) {
		var a, text0_value = ctx.item[1], text0, text1;

		return {
			c: function create() {
				a = createElement("a");
				text0 = createText(text0_value);
				text1 = createText("\n          ");
				a._svelte = { component, ctx };

				addListener(a, "click", click_handler);
				a.href = "javascript:0[0]";
				a.className = "dropdown-item svelte-d6sc07";
				addLoc(a, file, 27, 10, 652);
			},

			m: function mount(target, anchor) {
				insert(target, a, anchor);
				append(a, text0);
				append(a, text1);
			},

			p: function update(changed, _ctx) {
				ctx = _ctx;
				if ((changed.list) && text0_value !== (text0_value = ctx.item[1])) {
					setData(text0, text0_value);
				}

				a._svelte.ctx = ctx;
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(a);
				}

				removeListener(a, "click", click_handler);
			}
		};
	}

	// (25:8) {#if item === 0}
	function create_if_block(component, ctx) {
		var hr;

		return {
			c: function create() {
				hr = createElement("hr");
				hr.className = "dropdown-divider svelte-d6sc07";
				addLoc(hr, file, 25, 10, 596);
			},

			m: function mount(target, anchor) {
				insert(target, hr, anchor);
			},

			p: noop,

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(hr);
				}
			}
		};
	}

	// (24:6) {#each list as item}
	function create_each_block(component, ctx) {
		var if_block_anchor;

		function select_block_type(ctx) {
			if (ctx.item === 0) return create_if_block;
			return create_else_block;
		}

		var current_block_type = select_block_type(ctx);
		var if_block = current_block_type(component, ctx);

		return {
			c: function create() {
				if_block.c();
				if_block_anchor = createComment();
			},

			m: function mount(target, anchor) {
				if_block.m(target, anchor);
				insert(target, if_block_anchor, anchor);
			},

			p: function update(changed, ctx) {
				if (current_block_type === (current_block_type = select_block_type(ctx)) && if_block) {
					if_block.p(changed, ctx);
				} else {
					if_block.d(1);
					if_block = current_block_type(component, ctx);
					if_block.c();
					if_block.m(if_block_anchor.parentNode, if_block_anchor);
				}
			},

			d: function destroy$$1(detach) {
				if_block.d(detach);
				if (detach) {
					detachNode(if_block_anchor);
				}
			}
		};
	}

	function Dropdown(options) {
		this._debugName = '<Dropdown>';
		if (!options || (!options.target && !options.root)) {
			throw new Error("'target' is a required option");
		}

		init(this, options);
		this.refs = {};
		this._state = assign(data(), options.data);

		this._recompute({ value: 1, list: 1 }, this._state);
		if (!('value' in this._state)) console.warn("<Dropdown> was created without expected data property 'value'");
		if (!('list' in this._state)) console.warn("<Dropdown> was created without expected data property 'list'");
		if (!('disabled' in this._state)) console.warn("<Dropdown> was created without expected data property 'disabled'");
		if (!('visible' in this._state)) console.warn("<Dropdown> was created without expected data property 'visible'");
		if (!('right' in this._state)) console.warn("<Dropdown> was created without expected data property 'right'");
		this._intro = true;

		this._fragment = create_main_fragment(this, this._state);

		this.root._oncreate.push(() => {
			oncreate.call(this);
			this.fire("update", { changed: assignTrue({}, this._state), current: this._state });
		});

		if (options.target) {
			if (options.hydrate) throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
			this._fragment.c();
			this._mount(options.target, options.anchor);

			flush(this);
		}
	}

	assign(Dropdown.prototype, protoDev);
	assign(Dropdown.prototype, methods);

	Dropdown.prototype._checkReadOnly = function _checkReadOnly(newState) {
		if ('_index' in newState && !this._updatingReadonlyProperty) throw new Error("<Dropdown>: Cannot set read-only property '_index'");
	};

	Dropdown.prototype._recompute = function _recompute(changed, state) {
		if (changed.value || changed.list) {
			if (this._differs(state._index, (state._index = _index(state)))) changed._index = true;
		}
	};

	// import { Store } from "svelte/store";
	var Role;
	(function (Role) {
	    Role["Both"] = "0";
	    Role["Students"] = "2";
	    Role["Teachers"] = "3";
	})(Role || (Role = {}));

	function oncreate$1() {
	    // Reload on form update
	    let timeout = null;
	    this.on("formUpdate", (delay = true) => {
	        const { reloadOnUpdate } = this.get();
	        if (reloadOnUpdate) {
	            clearTimeout(timeout);
	            timeout = setTimeout(() => {
	                this.search();
	            }, delay ? 500 : 0);
	        }
	    });
	    // Display search results
	    this.store.on("searchresults", (data) => {
	        const { page } = this.get();
	        const pager = pagerLinks(page, data.pager);
	        this.set({
	            list: data.list,
	            courses: data.courses,
	            users: data.users,
	            pager: pager,
	            page: Math.min(page, pager.total)
	        });
	    });
	}
	function pagerLinks(page, pager) {
	    // Change that to alter the max page numbers displayed (including start/end)
	    // Must be an odd number, will otherwise be coerced into one
	    let maxLinks = 7;
	    // ...
	    maxLinks = maxLinks % 2 === 0 ? maxLinks + 1 : maxLinks;
	    const maxIntermediate = maxLinks - 2;
	    const { total } = pager;
	    // const last = Math.max(total - 1, 1); // FIXME: Something weird happening here
	    const last = total;
	    pager.last = last;
	    // No intermediate pages to show
	    if (last < 3) {
	        pager.intermediatePages = [];
	        pager.startSep = false;
	        pager.endSep = false;
	        return pager;
	    }
	    const half = (maxIntermediate - 1) / 2;
	    const endOverflow = (last - page);
	    const intermediateStart = Math.max(page - (half + Math.max((half + 1) - endOverflow, 0)), 2);
	    const intermediateEnd = Math.min(intermediateStart + maxIntermediate, last);
	    const intermediatePages = new Array(intermediateEnd - intermediateStart)
	        .fill(0)
	        .map((_, i) => intermediateStart + i);
	    if (intermediateStart - 1 > 1) {
	        pager.startSep = true;
	    }
	    if (intermediateEnd < last) {
	        pager.endSep = true;
	    }
	    pager.intermediatePages = intermediatePages;
	    return pager;
	}
	const methods$1 = {
	    reset() {
	        this.set({
	            fname: "",
	            lname: "",
	            email: "",
	            phone: "",
	            role: Role.Both,
	            studentsWithoutLessons: false,
	        });
	        this.search();
	    },
	    search(keepPage = false) {
	        const { fname, lname, email, phone, role, studentsWithoutLessons, teachersWithoutStudents, page, } = this.get();
	        if (!keepPage) {
	            this.set({
	                page: 1,
	            });
	        }
	        this.store.fire("search", {
	            fname,
	            lname,
	            email,
	            phone,
	            role,
	            studentsWithoutLessons,
	            teachersWithoutStudents,
	            page: keepPage ? page - 1 : 0,
	        });
	    },
	    showDetails(uid) {
	        const { users } = this.get();
	        const user = users[uid];
	        if (user) {
	            user.showDetails = !user.showDetails;
	            this.set({
	                users,
	            });
	        }
	    },
	    navigatePage(page) {
	        this.set({
	            page,
	        });
	        this.search(true);
	    },
	    relativePage(dir) {
	        const { page, pager } = this.get();
	        const newPage = Math.min(Math.max(page + dir, 1), pager.last);
	        if (newPage !== page) {
	            this.navigatePage(newPage);
	        }
	    }
	};

	/* src/components/app/App.html generated by Svelte v2.16.0 */



	function userList({ list, users }) {
		return list.map((uid) => users[uid]);
	}

	function data$1() {
	  return {
	    fname: "",
	    lname: "",
	    email: "",
	    phone: "",
	    role: "0",
	    studentsWithoutLessons: false,
	    teachersWithoutStudents: true,
	    users: {},
	    // @ts-ignore
	    list: [],
	    courses: {},
	    reloadOnUpdate: true,

	    page: 1,
	    pager: {
	      last: 1,
	      total: 1,
	      startSep: false,
	      endSep: false,
	      // @ts-ignore
	      intermediatePages: [],
	    },

	    columns: 8,
	    roleMap: [
	      null,
	      null,
	      "Teacher",
	      "Student",
	    ],
	    roleList: [
	      ["0", "Students and teachers"],
	      ["2", "Teachers"],
	      ["3", "Students"],
	    ],
	  }
	}
	var methods_1 = {
	  reset: methods$1.reset,
	  search: methods$1.search,
	  showDetails: methods$1.showDetails,
	  navigatePage: methods$1.navigatePage,
	  relativePage: methods$1.relativePage,
	};

	const file$1 = "src/components/app/App.html";

	function click_handler_1(event) {
		const { component, ctx } = this._svelte;

		component.navigatePage(ctx.intermediatePage);
	}

	function get_each1_context(ctx, list, i) {
		const child_ctx = Object.create(ctx);
		child_ctx.intermediatePage = list[i];
		return child_ctx;
	}

	function get_each_context_2(ctx, list, i) {
		const child_ctx = Object.create(ctx);
		child_ctx.course = list[i];
		return child_ctx;
	}

	function get_each_context_1(ctx, list, i) {
		const child_ctx = Object.create(ctx);
		child_ctx.puid = list[i];
		return child_ctx;
	}

	function get_each_context$1(ctx, list, i) {
		const child_ctx = Object.create(ctx);
		child_ctx.suid = list[i];
		return child_ctx;
	}

	function click_handler$1(event) {
		const { component, ctx } = this._svelte;

		component.showDetails(ctx.user.id);
	}

	function get_each0_context(ctx, list, i) {
		const child_ctx = Object.create(ctx);
		child_ctx.user = list[i];
		return child_ctx;
	}

	function create_main_fragment$1(component, ctx) {
		var div9, div6, div2, div0, h40, text1, input0, input0_updating = false, text2, div1, h41, text4, input1, input1_updating = false, text5, div5, div3, h42, text7, input2, input2_updating = false, text8, div4, h43, text10, input3, input3_updating = false, text11, div8, div7, label0, input4, text12, span0, text14, label1, input5, text15, span1, text17, label2, input6, text18, span2, text20, dropdown_updating = {}, text21, div15, div14, div10, text22, div13, div11, button0, text24, div12, button1, text26, div16, table, tbody, tr, th0, strong0, text28, th1, strong1, text30, th2, strong2, text32, th3, strong3, text34, th4, strong4, text36, th5, strong5, text38, th6, strong6, text40, th7, strong7, text42, text43, nav, a0, text45, a1, text47, ul, li, a2, text49, text50, text51, text52;

		function input0_input_handler() {
			input0_updating = true;
			component.set({ fname: input0.value });
			input0_updating = false;
		}

		function input_handler(event) {
			component.fire('formUpdate', true);
		}

		function input1_input_handler() {
			input1_updating = true;
			component.set({ lname: input1.value });
			input1_updating = false;
		}

		function input_handler_1(event) {
			component.fire('formUpdate', true);
		}

		function input2_input_handler() {
			input2_updating = true;
			component.set({ email: input2.value });
			input2_updating = false;
		}

		function input_handler_2(event) {
			component.fire('formUpdate', true);
		}

		function input3_input_handler() {
			input3_updating = true;
			component.set({ phone: input3.value });
			input3_updating = false;
		}

		function input_handler_3(event) {
			component.fire('formUpdate', true);
		}

		function input4_change_handler() {
			component.set({ studentsWithoutLessons: input4.checked });
		}

		function change_handler(event) {
			component.fire('formUpdate', false);
		}

		function input5_change_handler() {
			component.set({ teachersWithoutStudents: input5.checked });
		}

		function change_handler_1(event) {
			component.fire('formUpdate', false);
		}

		function input6_change_handler() {
			component.set({ reloadOnUpdate: input6.checked });
		}

		var dropdown_initial_data = { list: ctx.roleList };
		if (ctx.role !== void 0) {
			dropdown_initial_data.value = ctx.role;
			dropdown_updating.value = true;
		}
		var dropdown = new Dropdown({
			root: component.root,
			store: component.store,
			data: dropdown_initial_data,
			_bind(changed, childState) {
				var newState = {};
				if (!dropdown_updating.value && changed.value) {
					newState.role = childState.value;
				}
				component._set(newState);
				dropdown_updating = {};
			}
		});

		component.root._beforecreate.push(() => {
			dropdown._bind({ value: 1 }, dropdown.get());
		});

		dropdown.on("change", function(event) {
			component.fire('formUpdate', true);
		});

		function click_handler(event) {
			component.reset();
		}

		function click_handler_1(event) {
			component.search();
		}

		var each0_value = ctx.userList;

		var each0_blocks = [];

		for (var i = 0; i < each0_value.length; i += 1) {
			each0_blocks[i] = create_each_block_1(component, get_each0_context(ctx, each0_value, i));
		}

		var each0_else = null;

		if (!each0_value.length) {
			each0_else = create_else_block_3(component, ctx);
			each0_else.c();
		}

		function click_handler_2(event) {
			component.relativePage(-1);
		}

		function click_handler_3(event) {
			component.relativePage(1);
		}

		function click_handler_4(event) {
			component.navigatePage(1);
		}

		var if_block0 = (ctx.pager.startSep) && create_if_block_2(component, ctx);

		var each1_value = ctx.pager.intermediatePages;

		var each1_blocks = [];

		for (var i = 0; i < each1_value.length; i += 1) {
			each1_blocks[i] = create_each_block$1(component, get_each1_context(ctx, each1_value, i));
		}

		var if_block1 = (ctx.pager.endSep) && create_if_block_1(component, ctx);

		var if_block2 = (ctx.pager.last !== 1) && create_if_block$1(component, ctx);

		return {
			c: function create() {
				div9 = createElement("div");
				div6 = createElement("div");
				div2 = createElement("div");
				div0 = createElement("div");
				h40 = createElement("h4");
				h40.textContent = "First name";
				text1 = createText("\n        ");
				input0 = createElement("input");
				text2 = createText("\n      ");
				div1 = createElement("div");
				h41 = createElement("h4");
				h41.textContent = "Last name";
				text4 = createText("\n        ");
				input1 = createElement("input");
				text5 = createText("\n    ");
				div5 = createElement("div");
				div3 = createElement("div");
				h42 = createElement("h4");
				h42.textContent = "Email";
				text7 = createText("\n        ");
				input2 = createElement("input");
				text8 = createText("\n      ");
				div4 = createElement("div");
				h43 = createElement("h4");
				h43.textContent = "Phone number (with country code)";
				text10 = createText("\n        ");
				input3 = createElement("input");
				text11 = createText("\n  ");
				div8 = createElement("div");
				div7 = createElement("div");
				label0 = createElement("label");
				input4 = createElement("input");
				text12 = createText("\n        ");
				span0 = createElement("span");
				span0.textContent = "Students without lessons";
				text14 = createText("\n      ");
				label1 = createElement("label");
				input5 = createElement("input");
				text15 = createText("\n        ");
				span1 = createElement("span");
				span1.textContent = "Teachers without students";
				text17 = createText("\n      ");
				label2 = createElement("label");
				input6 = createElement("input");
				text18 = createText("\n        ");
				span2 = createElement("span");
				span2.textContent = "Auto update";
				text20 = createText("\n    ");
				dropdown._fragment.c();
				text21 = createText("\n\n\n");
				div15 = createElement("div");
				div14 = createElement("div");
				div10 = createElement("div");
				text22 = createText("\n    ");
				div13 = createElement("div");
				div11 = createElement("div");
				button0 = createElement("button");
				button0.textContent = "Reset";
				text24 = createText("\n      ");
				div12 = createElement("div");
				button1 = createElement("button");
				button1.textContent = "Search";
				text26 = createText("\n\n\n");
				div16 = createElement("div");
				table = createElement("table");
				tbody = createElement("tbody");
				tr = createElement("tr");
				th0 = createElement("th");
				strong0 = createElement("strong");
				strong0.textContent = "First name";
				text28 = createText("\n        ");
				th1 = createElement("th");
				strong1 = createElement("strong");
				strong1.textContent = "Last name";
				text30 = createText("\n        ");
				th2 = createElement("th");
				strong2 = createElement("strong");
				strong2.textContent = "Email";
				text32 = createText("\n        ");
				th3 = createElement("th");
				strong3 = createElement("strong");
				strong3.textContent = "Address";
				text34 = createText("\n        ");
				th4 = createElement("th");
				strong4 = createElement("strong");
				strong4.textContent = "Phone";
				text36 = createText("\n        ");
				th5 = createElement("th");
				strong5 = createElement("strong");
				strong5.textContent = "Role";
				text38 = createText("\n        ");
				th6 = createElement("th");
				strong6 = createElement("strong");
				strong6.textContent = "Joined";
				text40 = createText("\n        ");
				th7 = createElement("th");
				strong7 = createElement("strong");
				strong7.textContent = "Actions";
				text42 = createText("\n      ");

				for (var i = 0; i < each0_blocks.length; i += 1) {
					each0_blocks[i].c();
				}

				text43 = createText("\n\n\n");
				nav = createElement("nav");
				a0 = createElement("a");
				a0.textContent = "Previous";
				text45 = createText("\n  ");
				a1 = createElement("a");
				a1.textContent = "Next";
				text47 = createText("\n  ");
				ul = createElement("ul");
				li = createElement("li");
				a2 = createElement("a");
				a2.textContent = "1";
				text49 = createText("\n    ");
				if (if_block0) if_block0.c();
				text50 = createText("\n    ");

				for (var i = 0; i < each1_blocks.length; i += 1) {
					each1_blocks[i].c();
				}

				text51 = createText("\n    ");
				if (if_block1) if_block1.c();
				text52 = createText("\n    ");
				if (if_block2) if_block2.c();
				h40.className = "subtitle";
				addLoc(h40, file$1, 4, 8, 152);
				addListener(input0, "input", input0_input_handler);
				addListener(input0, "input", input_handler);
				setAttribute(input0, "type", "text");
				input0.className = "input";
				input0.placeholder = "First name...";
				addLoc(input0, file$1, 5, 8, 197);
				div0.className = "column";
				addLoc(div0, file$1, 3, 6, 123);
				h41.className = "subtitle";
				addLoc(h41, file$1, 13, 8, 410);
				addListener(input1, "input", input1_input_handler);
				addListener(input1, "input", input_handler_1);
				setAttribute(input1, "type", "text");
				input1.className = "input";
				input1.placeholder = "Last name...";
				addLoc(input1, file$1, 14, 8, 454);
				div1.className = "column";
				addLoc(div1, file$1, 12, 6, 381);
				div2.className = "columns input-data svelte-1r4c1d4";
				addLoc(div2, file$1, 2, 4, 84);
				h42.className = "subtitle";
				addLoc(h42, file$1, 24, 8, 714);
				addListener(input2, "input", input2_input_handler);
				addListener(input2, "input", input_handler_2);
				setAttribute(input2, "type", "text");
				input2.className = "input";
				input2.placeholder = "Email...";
				addLoc(input2, file$1, 25, 8, 754);
				div3.className = "column";
				addLoc(div3, file$1, 23, 6, 685);
				h43.className = "subtitle";
				addLoc(h43, file$1, 33, 8, 962);
				addListener(input3, "input", input3_input_handler);
				addListener(input3, "input", input_handler_3);
				setAttribute(input3, "type", "text");
				input3.className = "input";
				input3.placeholder = "Phone number...";
				addLoc(input3, file$1, 34, 8, 1029);
				div4.className = "column";
				addLoc(div4, file$1, 32, 6, 933);
				div5.className = "columns input-data svelte-1r4c1d4";
				addLoc(div5, file$1, 22, 4, 646);
				div6.className = "user-search-form-control svelte-1r4c1d4";
				addLoc(div6, file$1, 1, 2, 41);
				addListener(input4, "change", input4_change_handler);
				addListener(input4, "change", change_handler);
				setAttribute(input4, "type", "checkbox");
				addLoc(input4, file$1, 46, 8, 1318);
				addLoc(span0, file$1, 50, 8, 1454);
				addLoc(label0, file$1, 45, 6, 1302);
				addListener(input5, "change", input5_change_handler);
				addListener(input5, "change", change_handler_1);
				setAttribute(input5, "type", "checkbox");
				addLoc(input5, file$1, 53, 8, 1529);
				addLoc(span1, file$1, 57, 8, 1666);
				addLoc(label1, file$1, 52, 6, 1513);
				addListener(input6, "change", input6_change_handler);
				setAttribute(input6, "type", "checkbox");
				addLoc(input6, file$1, 60, 8, 1742);
				addLoc(span2, file$1, 63, 8, 1822);
				addLoc(label2, file$1, 59, 6, 1726);
				div7.className = "content";
				addLoc(div7, file$1, 44, 4, 1274);
				div8.className = "user-search-form-options svelte-1r4c1d4";
				addLoc(div8, file$1, 43, 2, 1231);
				div9.className = "content user-search-form svelte-1r4c1d4";
				addLoc(div9, file$1, 0, 0, 0);
				div10.className = "level-left";
				addLoc(div10, file$1, 76, 4, 2074);
				addListener(button0, "click", click_handler);
				button0.className = "button is-link is-normal";
				addLoc(button0, file$1, 81, 8, 2198);
				div11.className = "level-item";
				addLoc(div11, file$1, 80, 6, 2165);
				addListener(button1, "click", click_handler_1);
				button1.className = "button is-link is-normal";
				addLoc(button1, file$1, 88, 8, 2365);
				div12.className = "level-item";
				addLoc(div12, file$1, 87, 6, 2332);
				div13.className = "level-right";
				addLoc(div13, file$1, 79, 4, 2133);
				div14.className = "level";
				addLoc(div14, file$1, 75, 2, 2050);
				div15.className = "content";
				addLoc(div15, file$1, 74, 0, 2026);
				addLoc(strong0, file$1, 104, 12, 2711);
				addLoc(th0, file$1, 104, 8, 2707);
				addLoc(strong1, file$1, 105, 12, 2756);
				addLoc(th1, file$1, 105, 8, 2752);
				addLoc(strong2, file$1, 106, 12, 2800);
				addLoc(th2, file$1, 106, 8, 2796);
				addLoc(strong3, file$1, 107, 12, 2840);
				addLoc(th3, file$1, 107, 8, 2836);
				addLoc(strong4, file$1, 108, 12, 2882);
				addLoc(th4, file$1, 108, 8, 2878);
				addLoc(strong5, file$1, 109, 12, 2922);
				addLoc(th5, file$1, 109, 8, 2918);
				addLoc(strong6, file$1, 110, 12, 2961);
				addLoc(th6, file$1, 110, 8, 2957);
				addLoc(strong7, file$1, 111, 28, 3018);
				th7.className = "actions svelte-1r4c1d4";
				addLoc(th7, file$1, 111, 8, 2998);
				tr.className = "svelte-1r4c1d4";
				addLoc(tr, file$1, 102, 6, 2642);
				addLoc(tbody, file$1, 101, 4, 2628);
				table.className = "user-list table is-striped svelte-1r4c1d4";
				addLoc(table, file$1, 100, 2, 2581);
				div16.className = "user-list-wrap content svelte-1r4c1d4";
				addLoc(div16, file$1, 99, 0, 2542);
				addListener(a0, "click", click_handler_2);
				a0.href = "javascript:0[0]";
				a0.className = "pagination-previous";
				addLoc(a0, file$1, 199, 2, 6377);
				addListener(a1, "click", click_handler_3);
				a1.href = "javascript:0[0]";
				a1.className = "pagination-next";
				addLoc(a1, file$1, 205, 2, 6494);
				addListener(a2, "click", click_handler_4);
				a2.href = "javascript:0[0]";
				a2.className = "pagination-link";
				toggleClass(a2, "is-current", ctx.page === 1);
				addLoc(a2, file$1, 213, 6, 6646);
				addLoc(li, file$1, 212, 4, 6635);
				ul.className = "pagination-list";
				addLoc(ul, file$1, 211, 2, 6602);
				nav.className = "pagination is-centered";
				addLoc(nav, file$1, 198, 0, 6338);
			},

			m: function mount(target, anchor) {
				insert(target, div9, anchor);
				append(div9, div6);
				append(div6, div2);
				append(div2, div0);
				append(div0, h40);
				append(div0, text1);
				append(div0, input0);

				input0.value = ctx.fname;

				append(div2, text2);
				append(div2, div1);
				append(div1, h41);
				append(div1, text4);
				append(div1, input1);

				input1.value = ctx.lname;

				append(div6, text5);
				append(div6, div5);
				append(div5, div3);
				append(div3, h42);
				append(div3, text7);
				append(div3, input2);

				input2.value = ctx.email;

				append(div5, text8);
				append(div5, div4);
				append(div4, h43);
				append(div4, text10);
				append(div4, input3);

				input3.value = ctx.phone;

				append(div9, text11);
				append(div9, div8);
				append(div8, div7);
				append(div7, label0);
				append(label0, input4);

				input4.checked = ctx.studentsWithoutLessons;

				append(label0, text12);
				append(label0, span0);
				append(div7, text14);
				append(div7, label1);
				append(label1, input5);

				input5.checked = ctx.teachersWithoutStudents;

				append(label1, text15);
				append(label1, span1);
				append(div7, text17);
				append(div7, label2);
				append(label2, input6);

				input6.checked = ctx.reloadOnUpdate;

				append(label2, text18);
				append(label2, span2);
				append(div8, text20);
				dropdown._mount(div8, null);
				insert(target, text21, anchor);
				insert(target, div15, anchor);
				append(div15, div14);
				append(div14, div10);
				append(div14, text22);
				append(div14, div13);
				append(div13, div11);
				append(div11, button0);
				append(div13, text24);
				append(div13, div12);
				append(div12, button1);
				insert(target, text26, anchor);
				insert(target, div16, anchor);
				append(div16, table);
				append(table, tbody);
				append(tbody, tr);
				append(tr, th0);
				append(th0, strong0);
				append(tr, text28);
				append(tr, th1);
				append(th1, strong1);
				append(tr, text30);
				append(tr, th2);
				append(th2, strong2);
				append(tr, text32);
				append(tr, th3);
				append(th3, strong3);
				append(tr, text34);
				append(tr, th4);
				append(th4, strong4);
				append(tr, text36);
				append(tr, th5);
				append(th5, strong5);
				append(tr, text38);
				append(tr, th6);
				append(th6, strong6);
				append(tr, text40);
				append(tr, th7);
				append(th7, strong7);
				append(tbody, text42);

				for (var i = 0; i < each0_blocks.length; i += 1) {
					each0_blocks[i].m(tbody, null);
				}

				if (each0_else) {
					each0_else.m(tbody, null);
				}

				insert(target, text43, anchor);
				insert(target, nav, anchor);
				append(nav, a0);
				append(nav, text45);
				append(nav, a1);
				append(nav, text47);
				append(nav, ul);
				append(ul, li);
				append(li, a2);
				append(ul, text49);
				if (if_block0) if_block0.m(ul, null);
				append(ul, text50);

				for (var i = 0; i < each1_blocks.length; i += 1) {
					each1_blocks[i].m(ul, null);
				}

				append(ul, text51);
				if (if_block1) if_block1.m(ul, null);
				append(ul, text52);
				if (if_block2) if_block2.m(ul, null);
			},

			p: function update(changed, _ctx) {
				ctx = _ctx;
				if (!input0_updating && changed.fname) input0.value = ctx.fname;
				if (!input1_updating && changed.lname) input1.value = ctx.lname;
				if (!input2_updating && changed.email) input2.value = ctx.email;
				if (!input3_updating && changed.phone) input3.value = ctx.phone;
				if (changed.studentsWithoutLessons) input4.checked = ctx.studentsWithoutLessons;
				if (changed.teachersWithoutStudents) input5.checked = ctx.teachersWithoutStudents;
				if (changed.reloadOnUpdate) input6.checked = ctx.reloadOnUpdate;

				var dropdown_changes = {};
				if (changed.roleList) dropdown_changes.list = ctx.roleList;
				if (!dropdown_updating.value && changed.role) {
					dropdown_changes.value = ctx.role;
					dropdown_updating.value = ctx.role !== void 0;
				}
				dropdown._set(dropdown_changes);
				dropdown_updating = {};

				if (changed.userList || changed.columns || changed.users || changed.$baseUrl || changed.courses || changed.Date || changed.roleMap) {
					each0_value = ctx.userList;

					for (var i = 0; i < each0_value.length; i += 1) {
						const child_ctx = get_each0_context(ctx, each0_value, i);

						if (each0_blocks[i]) {
							each0_blocks[i].p(changed, child_ctx);
						} else {
							each0_blocks[i] = create_each_block_1(component, child_ctx);
							each0_blocks[i].c();
							each0_blocks[i].m(tbody, null);
						}
					}

					for (; i < each0_blocks.length; i += 1) {
						each0_blocks[i].d(1);
					}
					each0_blocks.length = each0_value.length;
				}

				if (!each0_value.length && each0_else) {
					each0_else.p(changed, ctx);
				} else if (!each0_value.length) {
					each0_else = create_else_block_3(component, ctx);
					each0_else.c();
					each0_else.m(tbody, null);
				} else if (each0_else) {
					each0_else.d(1);
					each0_else = null;
				}

				if (changed.page) {
					toggleClass(a2, "is-current", ctx.page === 1);
				}

				if (ctx.pager.startSep) {
					if (!if_block0) {
						if_block0 = create_if_block_2(component, ctx);
						if_block0.c();
						if_block0.m(ul, text50);
					}
				} else if (if_block0) {
					if_block0.d(1);
					if_block0 = null;
				}

				if (changed.pager || changed.page) {
					each1_value = ctx.pager.intermediatePages;

					for (var i = 0; i < each1_value.length; i += 1) {
						const child_ctx = get_each1_context(ctx, each1_value, i);

						if (each1_blocks[i]) {
							each1_blocks[i].p(changed, child_ctx);
						} else {
							each1_blocks[i] = create_each_block$1(component, child_ctx);
							each1_blocks[i].c();
							each1_blocks[i].m(ul, text51);
						}
					}

					for (; i < each1_blocks.length; i += 1) {
						each1_blocks[i].d(1);
					}
					each1_blocks.length = each1_value.length;
				}

				if (ctx.pager.endSep) {
					if (!if_block1) {
						if_block1 = create_if_block_1(component, ctx);
						if_block1.c();
						if_block1.m(ul, text52);
					}
				} else if (if_block1) {
					if_block1.d(1);
					if_block1 = null;
				}

				if (ctx.pager.last !== 1) {
					if (if_block2) {
						if_block2.p(changed, ctx);
					} else {
						if_block2 = create_if_block$1(component, ctx);
						if_block2.c();
						if_block2.m(ul, null);
					}
				} else if (if_block2) {
					if_block2.d(1);
					if_block2 = null;
				}
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(div9);
				}

				removeListener(input0, "input", input0_input_handler);
				removeListener(input0, "input", input_handler);
				removeListener(input1, "input", input1_input_handler);
				removeListener(input1, "input", input_handler_1);
				removeListener(input2, "input", input2_input_handler);
				removeListener(input2, "input", input_handler_2);
				removeListener(input3, "input", input3_input_handler);
				removeListener(input3, "input", input_handler_3);
				removeListener(input4, "change", input4_change_handler);
				removeListener(input4, "change", change_handler);
				removeListener(input5, "change", input5_change_handler);
				removeListener(input5, "change", change_handler_1);
				removeListener(input6, "change", input6_change_handler);
				dropdown.destroy();
				if (detach) {
					detachNode(text21);
					detachNode(div15);
				}

				removeListener(button0, "click", click_handler);
				removeListener(button1, "click", click_handler_1);
				if (detach) {
					detachNode(text26);
					detachNode(div16);
				}

				destroyEach(each0_blocks, detach);

				if (each0_else) each0_else.d();

				if (detach) {
					detachNode(text43);
					detachNode(nav);
				}

				removeListener(a0, "click", click_handler_2);
				removeListener(a1, "click", click_handler_3);
				removeListener(a2, "click", click_handler_4);
				if (if_block0) if_block0.d();

				destroyEach(each1_blocks, detach);

				if (if_block1) if_block1.d();
				if (if_block2) if_block2.d();
			}
		};
	}

	// (189:6) {:else}
	function create_else_block_3(component, ctx) {
		var tr, td, text;

		return {
			c: function create() {
				tr = createElement("tr");
				td = createElement("td");
				text = createText("No users found");
				td.className = "user-list-no-users svelte-1r4c1d4";
				td.colSpan = ctx.columns;
				addLoc(td, file$1, 190, 10, 6189);
				tr.className = "svelte-1r4c1d4";
				addLoc(tr, file$1, 189, 8, 6174);
			},

			m: function mount(target, anchor) {
				insert(target, tr, anchor);
				append(tr, td);
				append(td, text);
			},

			p: function update(changed, ctx) {
				if (changed.columns) {
					td.colSpan = ctx.columns;
				}
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(tr);
				}
			}
		};
	}

	// (148:8) {#if user.showDetails}
	function create_if_block_3(component, ctx) {
		var tr, td;

		function select_block_type(ctx) {
			if (ctx.user.role === "2") return create_if_block_4;
			return create_else_block_1;
		}

		var current_block_type = select_block_type(ctx);
		var if_block = current_block_type(component, ctx);

		return {
			c: function create() {
				tr = createElement("tr");
				td = createElement("td");
				if_block.c();
				td.className = "ihf-user-relations svelte-1r4c1d4";
				td.colSpan = ctx.columns;
				addLoc(td, file$1, 149, 12, 4426);
				tr.className = "has-background-grey-dark svelte-1r4c1d4";
				addLoc(tr, file$1, 148, 10, 4376);
			},

			m: function mount(target, anchor) {
				insert(target, tr, anchor);
				append(tr, td);
				if_block.m(td, null);
			},

			p: function update(changed, ctx) {
				if (current_block_type === (current_block_type = select_block_type(ctx)) && if_block) {
					if_block.p(changed, ctx);
				} else {
					if_block.d(1);
					if_block = current_block_type(component, ctx);
					if_block.c();
					if_block.m(td, null);
				}

				if (changed.columns) {
					td.colSpan = ctx.columns;
				}
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(tr);
				}

				if_block.d();
			}
		};
	}

	// (164:14) {:else}
	function create_else_block_1(component, ctx) {
		var div1, h2, text_1, div0;

		var each_value_1 = ctx.user.professors;

		var each_blocks = [];

		for (var i = 0; i < each_value_1.length; i += 1) {
			each_blocks[i] = create_each_block_3(component, get_each_context_1(ctx, each_value_1, i));
		}

		var each_else_1 = null;

		if (!each_value_1.length) {
			each_else_1 = create_else_block_2(component, ctx);
			each_else_1.c();
		}

		return {
			c: function create() {
				div1 = createElement("div");
				h2 = createElement("h2");
				h2.textContent = "Professors";
				text_1 = createText("\n                  ");
				div0 = createElement("div");

				for (var i = 0; i < each_blocks.length; i += 1) {
					each_blocks[i].c();
				}
				h2.className = "svelte-1r4c1d4";
				addLoc(h2, file$1, 165, 18, 5116);
				div0.className = "user-rel-list has-text-white svelte-1r4c1d4";
				addLoc(div0, file$1, 166, 18, 5154);
				div1.className = "user-rel-list-wrap svelte-1r4c1d4";
				addLoc(div1, file$1, 164, 16, 5065);
			},

			m: function mount(target, anchor) {
				insert(target, div1, anchor);
				append(div1, h2);
				append(div1, text_1);
				append(div1, div0);

				for (var i = 0; i < each_blocks.length; i += 1) {
					each_blocks[i].m(div0, null);
				}

				if (each_else_1) {
					each_else_1.m(div0, null);
				}
			},

			p: function update(changed, ctx) {
				if (changed.users || changed.userList || changed.$baseUrl || changed.courses) {
					each_value_1 = ctx.user.professors;

					for (var i = 0; i < each_value_1.length; i += 1) {
						const child_ctx = get_each_context_1(ctx, each_value_1, i);

						if (each_blocks[i]) {
							each_blocks[i].p(changed, child_ctx);
						} else {
							each_blocks[i] = create_each_block_3(component, child_ctx);
							each_blocks[i].c();
							each_blocks[i].m(div0, null);
						}
					}

					for (; i < each_blocks.length; i += 1) {
						each_blocks[i].d(1);
					}
					each_blocks.length = each_value_1.length;
				}

				if (each_value_1.length) {
					if (each_else_1) {
						each_else_1.d(1);
						each_else_1 = null;
					}
				} else if (!each_else_1) {
					each_else_1 = create_else_block_2(component, ctx);
					each_else_1.c();
					each_else_1.m(div0, null);
				}
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(div1);
				}

				destroyEach(each_blocks, detach);

				if (each_else_1) each_else_1.d();
			}
		};
	}

	// (151:14) {#if user.role === "2"}
	function create_if_block_4(component, ctx) {
		var div1, h2, text_1, div0;

		var each_value = ctx.user.students;

		var each_blocks = [];

		for (var i = 0; i < each_value.length; i += 1) {
			each_blocks[i] = create_each_block_2(component, get_each_context$1(ctx, each_value, i));
		}

		var each_else = null;

		if (!each_value.length) {
			each_else = create_else_block$1(component, ctx);
			each_else.c();
		}

		return {
			c: function create() {
				div1 = createElement("div");
				h2 = createElement("h2");
				h2.textContent = "Students";
				text_1 = createText("\n                  ");
				div0 = createElement("div");

				for (var i = 0; i < each_blocks.length; i += 1) {
					each_blocks[i].c();
				}
				h2.className = "svelte-1r4c1d4";
				addLoc(h2, file$1, 152, 18, 4581);
				div0.className = "user-rel-list has-text-white svelte-1r4c1d4";
				addLoc(div0, file$1, 153, 18, 4617);
				div1.className = "user-rel-list-wrap svelte-1r4c1d4";
				addLoc(div1, file$1, 151, 16, 4530);
			},

			m: function mount(target, anchor) {
				insert(target, div1, anchor);
				append(div1, h2);
				append(div1, text_1);
				append(div1, div0);

				for (var i = 0; i < each_blocks.length; i += 1) {
					each_blocks[i].m(div0, null);
				}

				if (each_else) {
					each_else.m(div0, null);
				}
			},

			p: function update(changed, ctx) {
				if (changed.users || changed.userList) {
					each_value = ctx.user.students;

					for (var i = 0; i < each_value.length; i += 1) {
						const child_ctx = get_each_context$1(ctx, each_value, i);

						if (each_blocks[i]) {
							each_blocks[i].p(changed, child_ctx);
						} else {
							each_blocks[i] = create_each_block_2(component, child_ctx);
							each_blocks[i].c();
							each_blocks[i].m(div0, null);
						}
					}

					for (; i < each_blocks.length; i += 1) {
						each_blocks[i].d(1);
					}
					each_blocks.length = each_value.length;
				}

				if (each_value.length) {
					if (each_else) {
						each_else.d(1);
						each_else = null;
					}
				} else if (!each_else) {
					each_else = create_else_block$1(component, ctx);
					each_else.c();
					each_else.m(div0, null);
				}
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(div1);
				}

				destroyEach(each_blocks, detach);

				if (each_else) each_else.d();
			}
		};
	}

	// (180:20) {:else}
	function create_else_block_2(component, ctx) {
		var text;

		return {
			c: function create() {
				text = createText("No professors to show");
			},

			m: function mount(target, anchor) {
				insert(target, text, anchor);
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(text);
				}
			}
		};
	}

	// (173:24) {#each users[puid].courses as course}
	function create_each_block_4(component, ctx) {
		var a, i, text0, span, text1_value = ctx.courses[ctx.course].name, text1, a_href_value;

		return {
			c: function create() {
				a = createElement("a");
				i = createElement("i");
				text0 = createText("\n                            ");
				span = createElement("span");
				text1 = createText(text1_value);
				i.className = "fa fa-calendar";
				addLoc(i, file$1, 174, 28, 5749);
				addLoc(span, file$1, 175, 28, 5808);
				a.href = a_href_value = ctx.$baseUrl + 'admin/ihfath/lessons/' + ctx.puid + '/' + ctx.course;
				a.className = "button is-link rel-button-schedule svelte-1r4c1d4";
				addLoc(a, file$1, 173, 26, 5608);
			},

			m: function mount(target, anchor) {
				insert(target, a, anchor);
				append(a, i);
				append(a, text0);
				append(a, span);
				append(span, text1);
			},

			p: function update(changed, ctx) {
				if ((changed.courses || changed.users || changed.userList) && text1_value !== (text1_value = ctx.courses[ctx.course].name)) {
					setData(text1, text1_value);
				}

				if ((changed.$baseUrl || changed.userList || changed.users) && a_href_value !== (a_href_value = ctx.$baseUrl + 'admin/ihfath/lessons/' + ctx.puid + '/' + ctx.course)) {
					a.href = a_href_value;
				}
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(a);
				}
			}
		};
	}

	// (168:20) {#each user.professors as puid}
	function create_each_block_3(component, ctx) {
		var div, a, text0_value = ctx.users[ctx.puid].firstname + " " + ctx.users[ctx.puid].lastname, text0, a_href_value, text1;

		var each_value_2 = ctx.users[ctx.puid].courses;

		var each_blocks = [];

		for (var i = 0; i < each_value_2.length; i += 1) {
			each_blocks[i] = create_each_block_4(component, get_each_context_2(ctx, each_value_2, i));
		}

		return {
			c: function create() {
				div = createElement("div");
				a = createElement("a");
				text0 = createText(text0_value);
				text1 = createText("\n                        ");

				for (var i = 0; i < each_blocks.length; i += 1) {
					each_blocks[i].c();
				}
				a.href = a_href_value = `${ ctx.$baseUrl }user/${ ctx.puid }/edit`;
				a.className = "button is-dark rel-button-name svelte-1r4c1d4";
				addLoc(a, file$1, 169, 24, 5325);
				div.className = "rel-button-list svelte-1r4c1d4";
				addLoc(div, file$1, 168, 22, 5271);
			},

			m: function mount(target, anchor) {
				insert(target, div, anchor);
				append(div, a);
				append(a, text0);
				append(div, text1);

				for (var i = 0; i < each_blocks.length; i += 1) {
					each_blocks[i].m(div, null);
				}
			},

			p: function update(changed, ctx) {
				if ((changed.users || changed.userList) && text0_value !== (text0_value = ctx.users[ctx.puid].firstname + " " + ctx.users[ctx.puid].lastname)) {
					setData(text0, text0_value);
				}

				if ((changed.$baseUrl || changed.userList) && a_href_value !== (a_href_value = `${ ctx.$baseUrl }user/${ ctx.puid }/edit`)) {
					a.href = a_href_value;
				}

				if (changed.$baseUrl || changed.userList || changed.users || changed.courses) {
					each_value_2 = ctx.users[ctx.puid].courses;

					for (var i = 0; i < each_value_2.length; i += 1) {
						const child_ctx = get_each_context_2(ctx, each_value_2, i);

						if (each_blocks[i]) {
							each_blocks[i].p(changed, child_ctx);
						} else {
							each_blocks[i] = create_each_block_4(component, child_ctx);
							each_blocks[i].c();
							each_blocks[i].m(div, null);
						}
					}

					for (; i < each_blocks.length; i += 1) {
						each_blocks[i].d(1);
					}
					each_blocks.length = each_value_2.length;
				}
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(div);
				}

				destroyEach(each_blocks, detach);
			}
		};
	}

	// (159:20) {:else}
	function create_else_block$1(component, ctx) {
		var text;

		return {
			c: function create() {
				text = createText("No students to show");
			},

			m: function mount(target, anchor) {
				insert(target, text, anchor);
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(text);
				}
			}
		};
	}

	// (155:20) {#each user.students as suid}
	function create_each_block_2(component, ctx) {
		var a, text_value = ctx.users[ctx.suid].firstname + " " + ctx.users[ctx.suid].lastname, text;

		return {
			c: function create() {
				a = createElement("a");
				text = createText(text_value);
				a.className = "button is-dark rel-button-single svelte-1r4c1d4";
				addLoc(a, file$1, 155, 22, 4732);
			},

			m: function mount(target, anchor) {
				insert(target, a, anchor);
				append(a, text);
			},

			p: function update(changed, ctx) {
				if ((changed.users || changed.userList) && text_value !== (text_value = ctx.users[ctx.suid].firstname + " " + ctx.users[ctx.suid].lastname)) {
					setData(text, text_value);
				}
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(a);
				}
			}
		};
	}

	// (114:6) {#each userList as user}
	function create_each_block_1(component, ctx) {
		var tr, td0, text0_value = ctx.user.firstname, text0, text1, td1, text2_value = ctx.user.lastname, text2, text3, td2, text4_value = ctx.user.email, text4, text5, td3, text6_value = ctx.user.address, text6, text7, td4, text8_value = ctx.user.phone, text8, text9, td5, text10_value = ctx.roleMap[ctx.user.role], text10, text11, td6, text12_value = new ctx.Date(ctx.user.created).toDateString(), text12, text13, td7, a0, i0, a0_href_value, text14, a1, i1, a1_href_value, text15, a2, i2, a2_href_value, text16, a3, i3, a3_href_value, text17, a4, i4, text18, if_block_anchor;

		var if_block = (ctx.user.showDetails) && create_if_block_3(component, ctx);

		return {
			c: function create() {
				tr = createElement("tr");
				td0 = createElement("td");
				text0 = createText(text0_value);
				text1 = createText("\n          ");
				td1 = createElement("td");
				text2 = createText(text2_value);
				text3 = createText("\n          ");
				td2 = createElement("td");
				text4 = createText(text4_value);
				text5 = createText("\n          ");
				td3 = createElement("td");
				text6 = createText(text6_value);
				text7 = createText("\n          ");
				td4 = createElement("td");
				text8 = createText(text8_value);
				text9 = createText("\n          ");
				td5 = createElement("td");
				text10 = createText(text10_value);
				text11 = createText("\n          ");
				td6 = createElement("td");
				text12 = createText(text12_value);
				text13 = createText("\n          ");
				td7 = createElement("td");
				a0 = createElement("a");
				i0 = createElement("i");
				text14 = createText("\n            ");
				a1 = createElement("a");
				i1 = createElement("i");
				text15 = createText("\n            ");
				a2 = createElement("a");
				i2 = createElement("i");
				text16 = createText("\n            ");
				a3 = createElement("a");
				i3 = createElement("i");
				text17 = createText("\n            ");
				a4 = createElement("a");
				i4 = createElement("i");
				text18 = createText("\n        \n        ");
				if (if_block) if_block.c();
				if_block_anchor = createComment();
				td0.className = "svelte-1r4c1d4";
				addLoc(td0, file$1, 116, 10, 3176);
				td1.className = "svelte-1r4c1d4";
				addLoc(td1, file$1, 117, 10, 3212);
				td2.className = "svelte-1r4c1d4";
				addLoc(td2, file$1, 118, 10, 3247);
				td3.className = "svelte-1r4c1d4";
				addLoc(td3, file$1, 119, 10, 3279);
				td4.className = "svelte-1r4c1d4";
				addLoc(td4, file$1, 120, 10, 3313);
				td5.className = "svelte-1r4c1d4";
				addLoc(td5, file$1, 121, 10, 3345);
				td6.className = "svelte-1r4c1d4";
				addLoc(td6, file$1, 122, 10, 3385);
				i0.className = "fa fa-calendar svelte-1r4c1d4";
				addLoc(i0, file$1, 125, 14, 3567);
				a0.href = a0_href_value = `${ ctx.$baseUrl }admin/ihfath/lessons/${ ctx.user.id }`;
				addLoc(a0, file$1, 124, 12, 3491);
				i1.className = "fa fa-envelope-o svelte-1r4c1d4";
				addLoc(i1, file$1, 128, 14, 3698);
				a1.href = a1_href_value = `${ ctx.$baseUrl }ihfath/messages/${ ctx.user.id }`;
				addLoc(a1, file$1, 127, 12, 3627);
				i2.className = "fa fa-shopping-cart svelte-1r4c1d4";
				addLoc(i2, file$1, 133, 14, 3893);
				a2.href = a2_href_value = `${ ctx.$baseUrl }user/${ ctx.user.id }/orders`;
				a2.className = "svelte-1r4c1d4";
				toggleClass(a2, "is-disabled", ctx.user.role === '2');
				addLoc(a2, file$1, 130, 12, 3760);
				i3.className = "fa fa-edit svelte-1r4c1d4";
				addLoc(i3, file$1, 136, 14, 4023);
				a3.href = a3_href_value = `${ ctx.$baseUrl }user/${ ctx.user.id }/edit`;
				addLoc(a3, file$1, 135, 12, 3958);
				i4.className = "fa fa-bars svelte-1r4c1d4";
				addLoc(i4, file$1, 142, 14, 4216);

				a4._svelte = { component, ctx };

				addListener(a4, "click", click_handler$1);
				a4.href = "javascript:0[0]";
				a4.className = "has-text-info";
				addLoc(a4, file$1, 138, 12, 4079);
				td7.className = "actions has-text-link svelte-1r4c1d4";
				addLoc(td7, file$1, 123, 10, 3444);
				tr.className = "user-list-row svelte-1r4c1d4";
				addLoc(tr, file$1, 114, 8, 3099);
			},

			m: function mount(target, anchor) {
				insert(target, tr, anchor);
				append(tr, td0);
				append(td0, text0);
				append(tr, text1);
				append(tr, td1);
				append(td1, text2);
				append(tr, text3);
				append(tr, td2);
				append(td2, text4);
				append(tr, text5);
				append(tr, td3);
				append(td3, text6);
				append(tr, text7);
				append(tr, td4);
				append(td4, text8);
				append(tr, text9);
				append(tr, td5);
				append(td5, text10);
				append(tr, text11);
				append(tr, td6);
				append(td6, text12);
				append(tr, text13);
				append(tr, td7);
				append(td7, a0);
				append(a0, i0);
				append(td7, text14);
				append(td7, a1);
				append(a1, i1);
				append(td7, text15);
				append(td7, a2);
				append(a2, i2);
				append(td7, text16);
				append(td7, a3);
				append(a3, i3);
				append(td7, text17);
				append(td7, a4);
				append(a4, i4);
				insert(target, text18, anchor);
				if (if_block) if_block.m(target, anchor);
				insert(target, if_block_anchor, anchor);
			},

			p: function update(changed, _ctx) {
				ctx = _ctx;
				if ((changed.userList) && text0_value !== (text0_value = ctx.user.firstname)) {
					setData(text0, text0_value);
				}

				if ((changed.userList) && text2_value !== (text2_value = ctx.user.lastname)) {
					setData(text2, text2_value);
				}

				if ((changed.userList) && text4_value !== (text4_value = ctx.user.email)) {
					setData(text4, text4_value);
				}

				if ((changed.userList) && text6_value !== (text6_value = ctx.user.address)) {
					setData(text6, text6_value);
				}

				if ((changed.userList) && text8_value !== (text8_value = ctx.user.phone)) {
					setData(text8, text8_value);
				}

				if ((changed.roleMap || changed.userList) && text10_value !== (text10_value = ctx.roleMap[ctx.user.role])) {
					setData(text10, text10_value);
				}

				if ((changed.Date || changed.userList) && text12_value !== (text12_value = new ctx.Date(ctx.user.created).toDateString())) {
					setData(text12, text12_value);
				}

				if ((changed.$baseUrl || changed.userList) && a0_href_value !== (a0_href_value = `${ ctx.$baseUrl }admin/ihfath/lessons/${ ctx.user.id }`)) {
					a0.href = a0_href_value;
				}

				if ((changed.$baseUrl || changed.userList) && a1_href_value !== (a1_href_value = `${ ctx.$baseUrl }ihfath/messages/${ ctx.user.id }`)) {
					a1.href = a1_href_value;
				}

				if ((changed.$baseUrl || changed.userList) && a2_href_value !== (a2_href_value = `${ ctx.$baseUrl }user/${ ctx.user.id }/orders`)) {
					a2.href = a2_href_value;
				}

				if (changed.userList) {
					toggleClass(a2, "is-disabled", ctx.user.role === '2');
				}

				if ((changed.$baseUrl || changed.userList) && a3_href_value !== (a3_href_value = `${ ctx.$baseUrl }user/${ ctx.user.id }/edit`)) {
					a3.href = a3_href_value;
				}

				a4._svelte.ctx = ctx;

				if (ctx.user.showDetails) {
					if (if_block) {
						if_block.p(changed, ctx);
					} else {
						if_block = create_if_block_3(component, ctx);
						if_block.c();
						if_block.m(if_block_anchor.parentNode, if_block_anchor);
					}
				} else if (if_block) {
					if_block.d(1);
					if_block = null;
				}
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(tr);
				}

				removeListener(a4, "click", click_handler$1);
				if (detach) {
					detachNode(text18);
				}

				if (if_block) if_block.d(detach);
				if (detach) {
					detachNode(if_block_anchor);
				}
			}
		};
	}

	// (222:4) {#if pager.startSep}
	function create_if_block_2(component, ctx) {
		var li, span;

		return {
			c: function create() {
				li = createElement("li");
				span = createElement("span");
				span.textContent = "…";
				span.className = "pagination-ellipsis";
				addLoc(span, file$1, 223, 8, 6861);
				addLoc(li, file$1, 222, 6, 6848);
			},

			m: function mount(target, anchor) {
				insert(target, li, anchor);
				append(li, span);
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(li);
				}
			}
		};
	}

	// (229:4) {#each pager.intermediatePages as intermediatePage}
	function create_each_block$1(component, ctx) {
		var li, a, text_value = ctx.intermediatePage, text;

		return {
			c: function create() {
				li = createElement("li");
				a = createElement("a");
				text = createText(text_value);
				a._svelte = { component, ctx };

				addListener(a, "click", click_handler_1);
				a.href = "javascript:0[0]";
				a.className = "pagination-link";
				toggleClass(a, "is-current", ctx.intermediatePage === ctx.page);
				addLoc(a, file$1, 230, 8, 7026);
				addLoc(li, file$1, 229, 6, 7013);
			},

			m: function mount(target, anchor) {
				insert(target, li, anchor);
				append(li, a);
				append(a, text);
			},

			p: function update(changed, _ctx) {
				ctx = _ctx;
				if ((changed.pager) && text_value !== (text_value = ctx.intermediatePage)) {
					setData(text, text_value);
				}

				a._svelte.ctx = ctx;
				if ((changed.pager || changed.page)) {
					toggleClass(a, "is-current", ctx.intermediatePage === ctx.page);
				}
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(li);
				}

				removeListener(a, "click", click_handler_1);
			}
		};
	}

	// (240:4) {#if pager.endSep}
	function create_if_block_1(component, ctx) {
		var li, span;

		return {
			c: function create() {
				li = createElement("li");
				span = createElement("span");
				span.textContent = "…";
				span.className = "pagination-ellipsis";
				addLoc(span, file$1, 241, 8, 7312);
				addLoc(li, file$1, 240, 6, 7299);
			},

			m: function mount(target, anchor) {
				insert(target, li, anchor);
				append(li, span);
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(li);
				}
			}
		};
	}

	// (247:4) {#if pager.last !== 1}
	function create_if_block$1(component, ctx) {
		var li, a, text_value = ctx.pager.last, text;

		function click_handler_2(event) {
			component.navigatePage(ctx.pager.last);
		}

		return {
			c: function create() {
				li = createElement("li");
				a = createElement("a");
				text = createText(text_value);
				addListener(a, "click", click_handler_2);
				a.href = "javascript:0[0]";
				a.className = "pagination-link";
				toggleClass(a, "is-current", ctx.page === ctx.pager.last);
				addLoc(a, file$1, 248, 8, 7448);
				addLoc(li, file$1, 247, 6, 7435);
			},

			m: function mount(target, anchor) {
				insert(target, li, anchor);
				append(li, a);
				append(a, text);
			},

			p: function update(changed, _ctx) {
				ctx = _ctx;
				if ((changed.pager) && text_value !== (text_value = ctx.pager.last)) {
					setData(text, text_value);
				}

				if ((changed.page || changed.pager)) {
					toggleClass(a, "is-current", ctx.page === ctx.pager.last);
				}
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(li);
				}

				removeListener(a, "click", click_handler_2);
			}
		};
	}

	function App$1(options) {
		this._debugName = '<App>';
		if (!options || (!options.target && !options.root)) {
			throw new Error("'target' is a required option");
		}
		if (!options.store) {
			throw new Error("<App> references store properties, but no store was provided");
		}

		init(this, options);
		this._state = assign(assign(assign({ Date : Date }, this.store._init(["baseUrl"])), data$1()), options.data);
		this.store._add(this, ["baseUrl"]);

		this._recompute({ list: 1, users: 1 }, this._state);
		if (!('list' in this._state)) console.warn("<App> was created without expected data property 'list'");
		if (!('users' in this._state)) console.warn("<App> was created without expected data property 'users'");
		if (!('fname' in this._state)) console.warn("<App> was created without expected data property 'fname'");
		if (!('lname' in this._state)) console.warn("<App> was created without expected data property 'lname'");
		if (!('email' in this._state)) console.warn("<App> was created without expected data property 'email'");
		if (!('phone' in this._state)) console.warn("<App> was created without expected data property 'phone'");
		if (!('studentsWithoutLessons' in this._state)) console.warn("<App> was created without expected data property 'studentsWithoutLessons'");
		if (!('teachersWithoutStudents' in this._state)) console.warn("<App> was created without expected data property 'teachersWithoutStudents'");
		if (!('reloadOnUpdate' in this._state)) console.warn("<App> was created without expected data property 'reloadOnUpdate'");
		if (!('roleList' in this._state)) console.warn("<App> was created without expected data property 'roleList'");
		if (!('role' in this._state)) console.warn("<App> was created without expected data property 'role'");

		if (!('roleMap' in this._state)) console.warn("<App> was created without expected data property 'roleMap'");

		if (!('$baseUrl' in this._state)) console.warn("<App> was created without expected data property '$baseUrl'");
		if (!('columns' in this._state)) console.warn("<App> was created without expected data property 'columns'");
		if (!('courses' in this._state)) console.warn("<App> was created without expected data property 'courses'");
		if (!('page' in this._state)) console.warn("<App> was created without expected data property 'page'");
		if (!('pager' in this._state)) console.warn("<App> was created without expected data property 'pager'");
		this._intro = true;

		this._handlers.destroy = [removeFromStore];

		this._fragment = create_main_fragment$1(this, this._state);

		this.root._oncreate.push(() => {
			oncreate$1.call(this);
			this.fire("update", { changed: assignTrue({}, this._state), current: this._state });
		});

		if (options.target) {
			if (options.hydrate) throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
			this._fragment.c();
			this._mount(options.target, options.anchor);

			flush(this);
		}
	}

	assign(App$1.prototype, protoDev);
	assign(App$1.prototype, methods_1);

	App$1.prototype._checkReadOnly = function _checkReadOnly(newState) {
		if ('userList' in newState && !this._updatingReadonlyProperty) throw new Error("<App>: Cannot set read-only property 'userList'");
	};

	App$1.prototype._recompute = function _recompute(changed, state) {
		if (changed.list || changed.users) {
			if (this._differs(state.userList, (state.userList = userList(state)))) changed.userList = true;
		}
	};

	/*! *****************************************************************************
	Copyright (c) Microsoft Corporation. All rights reserved.
	Licensed under the Apache License, Version 2.0 (the "License"); you may not use
	this file except in compliance with the License. You may obtain a copy of the
	License at http://www.apache.org/licenses/LICENSE-2.0

	THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
	KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
	WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
	MERCHANTABLITY OR NON-INFRINGEMENT.

	See the Apache Version 2.0 License for specific language governing permissions
	and limitations under the License.
	***************************************************************************** */

	function __awaiter(thisArg, _arguments, P, generator) {
	    return new (P || (P = Promise))(function (resolve, reject) {
	        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
	        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
	        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
	        step((generator = generator.apply(thisArg, _arguments || [])).next());
	    });
	}

	// Request lib
	// Drupal apiBase for requests
	let apiBase = "";
	// Utilities
	function ok(response, isAsync = true) {
	    return __awaiter(this, void 0, void 0, function* () {
	        if (isAsync && !response.ok) {
	            throw new Error("HTTP status: " + response.status);
	        }
	        let data;
	        if (isAsync) {
	            data = jSh.parseJSON(yield response.text());
	        }
	        else {
	            data = jSh.parseJSON(response);
	        }
	        if (data.error) {
	            throw new Error(data.error);
	        }
	        return data.data;
	    });
	}
	function string(thing) {
	    return JSON.stringify(thing);
	}
	// Request directives
	const request = {
	    setBase(url, _langList) {
	        apiBase = url;
	    },
	    search(fname = "", lname = "", email = "", phone = "", role = Role.Both, studentsWithoutLessons = false, teachersWithoutStudents = true, page = 0) {
	        return __awaiter(this, void 0, void 0, function* () {
	            const response = yield fetch(apiBase + "/search?page=" + page, {
	                method: "POST",
	                body: string({
	                    fname,
	                    lname,
	                    email,
	                    phone,
	                    role: +role,
	                    studentsWithoutLessons,
	                    teachersWithoutStudents,
	                }),
	            });
	            return yield ok(response);
	        });
	    },
	};

	function getField(obj, field, def = "") {
	    const data = obj[field];
	    return data && data.und ? data.und[0].value : def;
	}
	function userToJs(data) {
	    const user = {};
	    user.id = +data.uid;
	    user.name = data.name;
	    user.firstname = getField(data, "field_ihfath_firstname");
	    user.lastname = getField(data, "field_ihfath_lastname");
	    user.address = getField(data, "field_ihfath_address");
	    user.phone = getField(data, "field_ihfath_phone");
	    user.email = data.mail;
	    user.created = (+data.created) * 1000;
	    user.role = data.role;
	    if (user.role === "3") {
	        user.professors = data.professors ? data.professors.map((p) => +p) : [];
	    }
	    else {
	        user.students = data.students ? data.students.map((p) => +p) : [];
	        user.courses = data.courses ? data.courses.map((c) => +c) : [];
	    }
	    return user;
	}
	function courseToJs(data) {
	    const course = {};
	    course.id = +data.pid;
	    course.machineName = data.name;
	    course.name = data.data.current.name;
	    course.description = data.data.current.description;
	    return course;
	}

	function main(env) {
	    const store = new Store({
	        baseUrl: env.baseUrl,
	    });
	    const app = new App$1({
	        target: env.root,
	        store,
	    });
	    request.setBase(env.apiBase);
	    store.on("search", (data = {}) => {
	        request
	            .search(data.fname, data.lname, data.email, data.phone, data.role, data.studentsWithoutLessons, data.teachersWithoutStudents, data.page)
	            .then(data => {
	            const users = {};
	            const list = data.list.map((uid) => +uid);
	            for (const [uid, user] of Object.entries(data.users)) {
	                users[+uid] = userToJs(user);
	            }
	            const courses = {};
	            for (const [id, course] of Object.entries(data.courses)) {
	                courses[+id] = courseToJs(data.courses[id]);
	            }
	            store.fire("searchresults", {
	                courses,
	                list,
	                users,
	                pager: data.pager,
	            });
	        })
	            .catch(e => {
	            console.error("Failed to search users", e);
	        });
	    });
	    // Load first batch
	    store.fire("search");
	}
	window.IhfathUserSearch = main;
	// Side effects for Rollup
	_();

}());
//# sourceMappingURL=usersearch.js.map
