<?php
// ...
function jst(...$args) {
  return json_encode(t(...$args));
}

function jse(...$args) {
  return json_encode(...$args);
}
?>
<div class="app-root bulma-root"></div>

<script>
  IhfathUserSearch({
    apiBase: <?= jse(url("ihfath/ajax/usersearch")) ?>,
    root: document.querySelector(".app-root"),
    baseUrl: <?= jse(url("<front>")) ?>,

    locale: {

    },
  });
</script>
