import { User, UserRole, Course } from "./defaults"

function getField(obj: any, field: string, def = "") {
  const data = obj[field];
  return data && data.und ? data.und[0].value : def;
}

export function userToJs(data: any): User {
  const user = <User> {};

  user.id = +data.uid;
  user.name = data.name;
  user.firstname = getField(data, "field_ihfath_firstname");
  user.lastname = getField(data, "field_ihfath_lastname");
  user.address = getField(data, "field_ihfath_address");
  user.phone = getField(data, "field_ihfath_phone");
  user.email = data.mail;
  user.created = (+data.created) * 1000;
  user.role = data.role;

  if (user.role === "3") {
    user.professors = data.professors ? data.professors.map((p: any) => +p) : [];
  } else {
    user.students = data.students ? data.students.map((p: any) => +p) : [];
    user.courses = data.courses ? data.courses.map((c: any) => +c) : [];
  }

  return user;
}

export function courseToJs(data: any): Course {
  const course = <Course> {};

  course.id = +data.pid;
  course.machineName = data.name;
  course.name = data.data.current.name;
  course.description = data.data.current.description;

  return course;
}
