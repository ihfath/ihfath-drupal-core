import { Store } from "svelte/store"
import Base from "./base"
import { Store as AppStore, User, SearchQuery, CourseMap, _ } from "./defaults"
import App from "./components/app/App.html"
import { request } from "./request"
import { userToJs, courseToJs } from "./utils"

function main(env: any) {
  const store: AppStore = new Store({
    baseUrl: env.baseUrl,
  });

  const app = new App({
    target: env.root,
    store,
  });

  request.setBase(env.apiBase);

  store.on("search", (data = <SearchQuery> {}) => {
    request
      .search(
        data.fname,
        data.lname,
        data.email,
        data.phone,
        data.role,
        data.studentsWithoutLessons,
        data.teachersWithoutStudents,

        data.page,
      )
      .then(data => {
        type UserMap = {
          [user: number]: User;
        };

        const users = <UserMap> {};
        const list: number[] = data.list.map((uid: any) => +uid);

        for (const [uid, user] of Object.entries(data.users)) {
          users[+uid] = userToJs(user);
        }

        const courses: CourseMap = {};
        for (const [id, course] of Object.entries(data.courses)) {
          courses[+id] = courseToJs(data.courses[id]);
        }

        store.fire("searchresults", {
          courses,
          list,
          users,
          pager: data.pager,
        });
      })
      .catch(e => {
        console.error("Failed to search users", e);
      });
  });

  // Load first batch
  store.fire("search");
}

(window as any).IhfathUserSearch = main;

// Side effects for Rollup
_();
