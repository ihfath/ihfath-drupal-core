// Request lib

import { Role } from "./base";
import { AbsenceResults } from "./defaults";

// JShorts
declare const jSh: any;

// Drupal apiBase for requests
let apiBase = "";
let generalApiBase = "";

// Language list
let langList = {};

// Utilities
async function ok<Data = any>(response: Response, isAsync = true): Promise<Data> {
  if (isAsync && !response.ok) {
    throw new Error("HTTP status: " + response.status);
  }

  let data: {
    error: string;
    data: any;
  };

  if (isAsync) {
    data = jSh.parseJSON(await response.text());
  } else {
    data = jSh.parseJSON(response);
  }

  if (data.error) {
    throw new Error(data.error);
  }

  return data.data;
}

function string(thing: any) {
  return JSON.stringify(thing);
}

// Request directives
export const request = {
  setBase(url: string, _langList?: any) {
    apiBase = url;
    langList = _langList;
  },

  async search(
    fname: string = "",
    lname: string = "",
    email: string = "",
    phone: string = "",
    role: Role = Role.Both,
    studentsWithoutLessons: boolean = false,
    teachersWithoutStudents: boolean = true,

    page: number = 0,
  ) {
    const response = await fetch(apiBase + "/search?page=" + page, {
      method: "POST",
      body: string({
        fname,
        lname,
        email,
        phone,
        role: +role,
        studentsWithoutLessons,
        teachersWithoutStudents,
      }),
    });

    return await ok(response);
  },

  async absences(
    name: string,
    startDay: number,
    startMonth: number,
    startYear: number,
    endDay: number,
    endMonth: number,
    endYear: number,

    page: number = 0,
  ): Promise<AbsenceResults> {
    const response = await fetch(apiBase + "/getAbsences?page=" + page, {
      method: "POST",
      body: string({
        name,
        startDay,
        startMonth,
        startYear,
        endDay,
        endMonth,
        endYear,
      }),
    });

    return await ok(response);
  },
};
