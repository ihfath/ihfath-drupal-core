<?php
// Vars...
global $language;

$base_path = drupal_get_path("module", "ihfathui") . "/";
$base_la_path = $base_path . "theme/courseadmin";

function jst(...$args) {
  return json_encode(t(...$args));
}

function jse(...$args) {
  return json_encode(...$args);
}

function mapPricesJS($prices) {
  return array_values(array_map(function ($product_hour) {
    return array(
      "id" => (int) $product_hour->product_id,
      "hours" => (float) $product_hour->field_ihfath_lesson_hours[LANGUAGE_NONE][0]["value"],
      "price" => (float) $product_hour->commerce_price[LANGUAGE_NONE][0]["amount"],
      "currency" => $product_hour->commerce_price[LANGUAGE_NONE][0]["currency_code"],
    );
  }, array_filter($prices, function($product) {
    return (int) $product->status;
  })));
}

// TODO: DRY this into IhfathBooks class
function mapBooksToJS($books) {
  return array_map(function($book) {
    return (object) array(
      "id" => $book->bhid,
      "title" => $book->field_book_title_field[LANGUAGE_NONE][0]["value"],
      "pages" => $book->pages,
      "direction" => $book->direction,
      "previewPage" => $book->preview_page,
      "processed" => $book->processed,
    );
  }, array_values($books));
}

function mapPageLayoutToJS($fplayout) {
  return array_map(function($item) {
    return array(
      "id" => (int) $item->item_id,
      "name" => $item->name,
      "themeId" => $item->theme_id,
      "courseId" => (int) $item->course_id,
      "type" => $item->course_id == 0 ? "custom" : "course",
      "data" => $item->content,
    );
  }, array_values($fplayout));
}

?>
<div class="app-root bulma-root">
  <!-- Svelte-populated -->
</div>

<script type="text/javascript">
  window.addEventListener("load", () => {
    // Make viewer root node and put it outside the page for fullscreen effect
    const page = document.querySelector("#page");
    const viewerRoot = document.createElement("div");
    page.parentNode.appendChild(viewerRoot);

    ihfathCourseAdmin({
      apiBase: <?= jse(array(
        url("ihfath/ajax/courseadmin"),
        url("ihfath/ajax/general"),
        url("/"),
      )) ?>,
      fileBase: <?= jse(url("sites/default/files/ihfathusercontent")) ?>,
      pageBase: <?= jse(url("sites/default/files/ihfathbooks")) ?>,
      root: document.querySelector(".app-root"),
      viewerRoot,
      frontPageLayout: <?= jse(mapPageLayoutToJS($fplayout_items)) ?>,
      themes: <?= jse($themes) ?>,
      themeCanvasResources: <?= jse(array(
        "script" => url($base_la_path . "/dist/course-admin-theme-canvas.js"),
        "style" => url($base_la_path . "/dist/course-admin-theme-canvas.css"),
        // NOTE: Probably replace this
        "fastyle" => url("sites/all/libraries/fontawesome/css/font-awesome.css"),
      )) ?>,
      prices: <?= jse(mapPricesJS(Ihfathcore\Commerce::getProducts())) ?>,
      books: <?= jse(mapBooksToJS($books)) ?>,

      locale: {
        _langauge: <?= jst($language->language) ?>,
        _languageList: <?= jse($langs) ?>,
        editFrontpageLayout: <?= jst("Front Page Layout") ?>,
        addCategory: <?= jst("Add Category") ?>,
        addCourse: <?= jst("Create Course") ?>,
        courseList: <?= jst("Choose a course") ?>,

        // Available checkbox
        availableCheckboxLabel: <?= jst("Course available") ?>,

        // File input selector
        fisNofileselectedLabel: <?= jst("No file selected") ?>,
        fisSelectfileLabel: <?= jst("select file") ?>,
        fisUploadingStatus: <?= jst("Uploading...") ?>,

        // Tab titles
        courseLevels: <?= jst("Levels") ?>,
        courseFaq: <?= jst("FAQ") ?>,
        coursePage: <?= jst("Course page") ?>,
        frontPage: <?= jst("Front page") ?>,
        pricingPage: <?= jst("Pricing") ?>,

        selectOrNew: <?= jst("Select course or create a new one") ?>,

        done: <?= jst("Done") ?>,
        edit: <?= jst("Edit") ?>,
        save: <?= jst("Save") ?>,
        cancel: <?= jst("Cancel") ?>,

        newCategory: <?= jst("New category name...") ?>,
        newName: <?= jst("New course") ?>,
        newSlogan: <?= jst("New slogan") ?>,
        newDescription: <?= jst("Description...") ?>,
        slugInputLabel: <?= jst("Slug") ?>,

        // Course page
        description: <?= jst("Description") ?>,
        coursePageTitle: <?= jst("Course Page Title") ?>,
        coursePageDescription: <?= jst("Course Page Description") ?>,
        cpeFeatureNameLabel: <?= jst("Feature name") ?>,
        cpeFeatureValueLabel: <?= jst("Feature value") ?>,
        cpeFeatureDefaultNewFeature: <?= jst("New feature title") ?>,
        cpeFeatureDefaultNewDescription: <?= jst("Feature description") ?>,
        cpeAddFeatureButton: <?= jst("Add feature") ?>,
        coursePageFeatures: <?= jst("Course Page Features") ?>,
        cpImageField: <?= jst("Course Page Image") ?>,

        // FrontPage(Editor) tab
        fpeAddFieldButton: <?= jst("Add field") ?>,
        fpeFieldNameLabel: <?= jst("Field name") ?>,
        fpeFieldValueLabel: <?= jst("Field value") ?>,
        fpeFieldDefaultNewField: <?= jst("New field") ?>,
        fpeFieldDefaultNewValue: <?= jst("New value") ?>,
        fpeFieldTypeText: <?= jst("Text") ?>,
        fpeFieldTypeBigtext: <?= jst("Paragraph") ?>,
        fpeFieldTypeImage: <?= jst("Image") ?>,
        fpeFieldDeleteButton: <?= jst("Remove") ?>,
        fpeFieldCreateThemeFieldsButton: <?= jst("Create Fields") ?>,

        // ThemeEditor
        fpeFieldOpenThemeEditorButton: <?= jst("Configure theme") ?>,
        teNoThemeSelectedThemeNameHeader: <?= jst("No theme selected") ?>,
        teCanvasSelectTheme: <?= jst("Select a theme below") ?>,
        teCanvasResetField: <?= jst("Reset field") ?>,
        teCanvasNoFieldsMessage: <?= jst("No fields") ?>,
        teVariablesNoFieldSelected: <?= jst("No field") ?>,

        // Levels tab
        ltAddEntryButton: <?= jst("Add entry") ?>,
        ltTitleLabel: <?= jst("Title") ?>,
        ltDescriptionLabel: <?= jst("Description") ?>,
        ltTitleDefault: <?= jst("My title") ?>,
        ltDescriptionDefault: <?= jst("My description") ?>,
        ltAddBookButton: <?= jst("Add book") ?>,

        // Prices tab
        ptAddPriceButton: <?= jst("Add") ?>,
        ptNoAvailableHours: <?= jst("No available hours") ?>,
        ptFeaturedPriceButton: <?= jst("Featured") ?>,

        // Actions
        courseActionDelete: <?= jst("Delete") ?>,
        categoryActionDisable: <?= jst("Toggle availability") ?>,
      }
    });
  });
</script>
