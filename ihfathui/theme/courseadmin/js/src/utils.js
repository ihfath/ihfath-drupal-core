// Utils

export function machineName(name) {
  return name.trim().replace(/^[\d- ]+|[^a-z\d- ]|[- ]+$/ig, "").replace(/ +|-+/g, "-").toLowerCase();
}

export function courseToJs(courseRaw, langList) {
  const course = {
    id: +courseRaw.pid,
    category: +courseRaw.type,
    available: !!(+courseRaw.available),
    locale: {
      // ...
    },
  };

  // FIXME: Remove this probably
  const baseLocale = {
    name: "",
    slogan: "",
    slug: "",
    cpDescription: "",
    fpData: "",
  };

  const map = {
    name: "name",
    description: "slogan",

    field_ihfathlsn_c_cpdesc: "cpDescription",
    field_ihfathlsn_c_cpfeat: "cpFeatures",
    field_ihfathlsn_c_fpdata: "fpData",
    field_ihfathlsn_c_cpimage: "cpImage",
    field_ihfathlsn_c_prices: "prices",
    field_ihfathlsn_c_f_price: "featuredPrice",
    // field_ihfathlsn_c_cptitle: "name", // NOTE: Handled in Drupal saveCourse AJAX handler
  };

  const baseEntries = {
    name: 1,
    description: 1,
  };
  const fieldEntries = {
    field_ihfathlsn_c_cpdesc: 1,
    field_ihfathlsn_c_cpimage: 1,
    field_ihfathlsn_c_cpfeat: 1,
    field_ihfathlsn_c_fpdata: 1,
    field_ihfathlsn_c_prices: 1,
    field_ihfathlsn_c_f_price: 1,
  };
  const preprocess = {
    fpData(data) {
      const parsed = jSh.parseJSON(data);

      if (parsed.error) {
        throw new Error("Failed parsing FrontPage data", parsed);
      }

      return parsed;
    },
    cpFeatures(data) {
      const parsed = jSh.parseJSON(data);

      if (parsed.error) {
        throw new Error("Failed parsing CoursePage Feature data", parsed);
      }

      return parsed;
    },
    prices(data) {
      const parsed = jSh.parseJSON(data);

      if (parsed.error) {
        throw new Error("Failed parsing price data", parsed);
      }

      return parsed;
    },
  };

  // Map base data
  for (const [lang, data] of Object.entries(courseRaw.data)) {
    const langData = course.locale[lang] || (course.locale[lang] = {});

    for (let [entry, value] of Object.entries(data)) {
      if (baseEntries.hasOwnProperty(entry)) {
        const prep = preprocess[map[entry]];

        if (prep) {
          value = prep(value);
        }

        langData[map[entry]] = value;
      }
    }

    // Add slug
    langData.slug = courseRaw.name;
  }

  // Map fields
  for (const field of Object.keys(courseRaw)) {
    if (fieldEntries.hasOwnProperty(field)) {
      const fieldData = courseRaw[field];

      // Make sure this field isn't empty
      if (!Array.isArray(fieldData)) {
        const prep = preprocess[map[field]];

        for (let [lang, {0: {value}}] of Object.entries(fieldData)) {
          if (prep) {
            value = prep(value);
          }

          if (lang === "und") {
            lang = "en";
          }

          const langData = course.locale[lang] || (course.locale[lang] = {});
          langData[map[field]] = value;
        }
      }
    }
  }

  // Map info data (levels & FAQ) to JS
  const infoData = [
    "levels",
    "faq",
  ];

  const infoDataFieldMap = {
    levels: {
      title: "field_ihfathlsn_c_lvl_title",
      desc: "field_ihfathlsn_c_lvl_desc",
      books: "field_ihfathlsn_c_lvl_books",
    },
    faq: {
      title: "field_ihfathlsn_c_faq_title",
      desc: "field_ihfathlsn_c_faq_desc",
    },
  };
  const infoDataPreprocess = {
    books(data) {
      if (!data) {
        return [];
      }

      const parsed = jSh.parseJSON(data);

      if (parsed.error) {
        throw new Error("Failed parsing course level book data", parsed);
      }

      return parsed;
    },
  };

  for (const lang of Object.keys(langList)) {
    const locale = course.locale[lang] || (course.locale[lang] = {});

    for (const infoType of infoData) {
      const infoLocale = (locale[infoType] = []);

      for (const data of Object.values(courseRaw[infoType])) {
        const dataTitle = data[infoDataFieldMap[infoType].title][lang];
        const dataDesc = data[infoDataFieldMap[infoType].desc][lang];

        const infoDataDeserialized = {
          id: data.id,
          title: dataTitle ? dataTitle[0].value : "",
          desc: dataDesc ? dataDesc[0].value : "",
        };

        if (infoType === "levels") {
          const dataBooks = data[infoDataFieldMap[infoType].books][lang];
          infoDataDeserialized.books = infoDataPreprocess.books(dataBooks ? dataBooks[0].value : "[]");
        }

        infoLocale.push(infoDataDeserialized);
      }
    }
  }

  // Default data if course is older uninitialized with this data
  const defaultData = {
    cpFeatures: [],
  };

  for (const [key, defaultValue] of Object.entries(defaultData)) {
    for (const langData of Object.values(course.locale)) {
      if (!(key in langData)) {
        langData[key] = deepCopy(defaultValue);
      }
    }
  }

  console.log("COURSEJS", course);
  return course;
}

export function getThemeUserFields(themeId, store) {
  const { themeIdMap } = store.get();
  const theme = themeIdMap[themeId];

  const fields = [];
  const themeMap = {};
  const themeVariables = {};
  const themeVariableTypes = {};

  for (const [fieldId, field] of Object.entries(theme.regions)) {
    themeMap[fieldId] = fields.length;

    fields.push({
      // @ts-ignore
      name: field.name,
      value: "",
      // @ts-ignore
      type: field.type,
    });
  }

  for (const [varName, varMeta] of Object.entries(theme.variables)) {
    themeVariables[varName] = "";
    themeVariableTypes[varName] = {
      type: varMeta[0],
      defaultValue: varMeta[1],
    };
  }

  return {
    fields,
    themeMap,
    themeVariables,
    themeVariableTypes,
  };
}

export function type(value) {
    const base = typeof value;

    if (base === "object") {
        return value === null ? "null" : (Array.isArray(value) ? "array" : "object");
    } else {
        return base;
    }
}

export function deepCopy(obj, deepArray = true) {
  let copy = {};

  switch (obj.constructor) {
    case Date:
      copy = new Date(obj.getTime());
      break;
    case RegExp:
      copy = new RegExp(obj.source, obj.pattern);
      break;
    case Array:
      // `obj` is an array, deepcopy it
      return deepCopy({a: obj}).a;
  }

  for (const key in obj) {
    if (obj.hasOwnProperty(key)) {
      const value = obj[key];

      switch (type(value)) {
        case "object":
          copy[key] = deepCopy(value, deepArray);
          break;

        case "array":
          if (deepArray) {
            const backlog = [
              [value, copy[key] = new Array(value.length), 0],
            ];

            let currentMeta = backlog[0];
            let current = currentMeta[0];
            let copyArr = currentMeta[1];

            arrayLoop:
            while (true) {
              for (let i = currentMeta[2]; i < current.length; i++) {
                const item = current[i];

                switch (type(item)) {
                  case "object":
                    copyArr[i] = deepCopy(item, deepArray);
                    break;

                  case "array":
                    currentMeta[2] = i + 1;

                    const newArr = new Array(item.length);
                    backlog.push(currentMeta = [item, newArr, 0]);

                    copyArr[i] = newArr;
                    current = item;
                    copyArr = newArr;
                    continue arrayLoop;

                  default:
                    copyArr[i] = item;
                }
              }

              backlog.pop();

              if (backlog.length) {
                currentMeta = backlog[backlog.length - 1];
                current = currentMeta[0];
                copyArr = currentMeta[1];
              } else {
                break arrayLoop;
              }
            }
          } else {
            copy[key] = value.slice();
          }
          break;

        default:
          copy[key] = value;
      }
    }
  }

  return copy;
}
