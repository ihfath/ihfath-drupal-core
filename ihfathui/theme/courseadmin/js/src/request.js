// Request lib

import { courseToJs } from "./utils";

// Drupal apiBase for requests
let apiBase = "";
let generalApiBase = "";
let rootApiBase = "";

// Language list
let langList = {};

// Utilities
async function ok(response, isAsync = true) {
  if (isAsync && !response.ok) {
    throw new Error("HTTP status: " + response.status);
  }

  let data;
  if (isAsync) {
    data = jSh.parseJSON(await response.text());
  } else {
    data = jSh.parseJSON(response);
  }

  if (data.error) {
    throw new Error(data.error);
  }

  return data.data;
}

function string(thing) {
  return JSON.stringify(thing);
}

// Request directives
export const request = {
  setBase(url, _langList) {
    apiBase = url[0];
    generalApiBase = url[1];
    rootApiBase = url[2];
    langList = _langList;
  },

  async list() {
    const response = await fetch(apiBase + "/listCategories", {
      method: "POST",
    });

    return await ok(response);
  },

  async saveCategory(id, name, machineName, available) {
    const response = await fetch(apiBase + "/saveCategory", {
      method: "POST",
      body: string({
        id,
        name,
        machineName,
        available,
      }),
    });

    return (await ok(response)).id;
  },

  async saveCourse(
      id,
      category,
      machineName,
      available,
      data,
    ) {
    const response = await fetch(apiBase + "/saveCourse", {
      method: "POST",
      body: string({
        pid: id,
        type: category,
        machineName,
        available: Number(available),
        data,
      }),
    });

    return (await ok(response));
  },

  async deleteCourse(pid) {
    const response = await fetch(apiBase + "/deleteCourse", {
      method: "POST",
      body: string({
        pid,
      }),
    });

    return await ok(response);
  },

  async loadCourse(pid) {
    const response = await fetch(apiBase + "/loadCourse", {
      method: "POST",
      body: string({
        pid,
      }),
    });

    return courseToJs(await ok(response), langList);
  },

  async themeThumbs() {
    const response = await fetch(apiBase + "/themeThumbs", {
      method: "POST",
      body: string({}),
    });

    return await ok(response);
  },

  async themeCanvas(id) {
    const response = await fetch(apiBase + "/themeCanvas", {
      method: "POST",
      body: string({
        id,
      }),
    });

    return await ok(response);
  },

  async saveFrontPageLayout(items) {
    const response = await fetch(apiBase + "/saveFrontPageLayout", {
      method: "POST",
      body: string({
        items,
      }),
    });

    return await ok(response);
  },

  async loadBook(id) {
    const response = await fetch(generalApiBase + "/loadBook", {
      method: "POST",
      body: string({
        id,
      }),
    });

    return await ok(response);
  },

  // Book viewer
  async loadBookPage(page, id) {
    const response = await fetch(generalApiBase + "/loadBookPage", {
      method: "POST",
      body: string({
        page,
        id,
      }),
    });

    return await ok(response);
  },

  // Users
  async searchUsers(name) {
    const response = await fetch(apiBase + "/searchUsers", {
      method: "POST",
      body: string({
        name,
      }),
    });

    return await ok(response);
  },

  async userListNames(users) {
    const response = await fetch(apiBase + "/userListNames", {
      method: "POST",
      body: string({
        users,
      }),
    });

    return await ok(response);
  },

  // Files
  async getFileForm(curFileHash = null) {
    const response = await fetch(apiBase + "/getFileForm", {
      method: "POST",
      body: string({
        curFileHash,
      }),
    });

    return await ok(response);
  },

  uploadFile(form, callback = null) {
    const data = new FormData();
    const keyMap = {
      buildId: "form_build_id",
      token: "form_token",
      id: "form_id",
      file: "files[file1]",
    };

    for (const keyBase of Object.keys(form)) {
      if (keyBase !== "actual" && keyBase !== "old") {
        const key = keyMap.hasOwnProperty(keyBase) ? keyMap[keyBase] : keyBase;

        data.append(key, form[keyBase]);
      }
    }

    const xhr = new XMLHttpRequest();

    // Attach progress cb
    if (callback) {
      xhr.upload.addEventListener("progress", (e) => {
        callback(e.loaded, e.total);
      });
    }

    const promise = new Promise((resolve, reject) => {
      xhr.onreadystatechange = function() {
        if (xhr.readyState === 4) {
          if (xhr.status === 200) {
            resolve(ok(xhr.responseText, false));
          } else {
            reject(new Error("Failed with status code " + xhr.status));
          }
        }
      };

      xhr.open("POST", apiBase + "/uploadFile");
      xhr.send(data);
    });

    return promise;
  },
};
