<?php
global $language;
$lang = $language->language;
$theme_prefix = "ihfathui_frontpage_course_theme_";

$themes_rendered = array();

// Render courses
foreach ($fplayout_items as $item) {
  $course = NULL;
  $fpdata = $item->content;

  // Render course if item has one assigned
  if ($item->course_id != 0) {
    $course = $courses[$item->course_id];
    $fpdata_langs = $course->field_ihfathlsn_c_fpdata;

    if (isset($fpdata_langs[$lang])) {
      $fpdata_serialized = $fpdata_langs[$lang][0]["value"];
    } else {
      $fpdata_serialized = $fpdata_langs[LANGUAGE_NONE][0]["value"];
    }

    $fpdata = json_decode($fpdata_serialized, FALSE);
  } else {
    // Extract fpdata for this language
    $fpdata = $fpdata->{$lang};
  }

  // Only render course if it has a theme selected
  if (is_object($fpdata) && $fpdata->theme) {
    $data = array();
    $fields = $fpdata->fields;
    $settings = isset($fpdata->themeVariables) ? (array) $fpdata->themeVariables : array();

    $theme_meta = ihfathui_fp_course_theme_list()[$fpdata->theme];
    $themes_rendered[$fpdata->theme] = 1;

    foreach ($fpdata->themeMap as $region_id => $field) {
      if ($field === NULL) {
        $data[$region_id] = NULL;
      } else {
        $data[$region_id] = $fields[$field]->value;
      }
    }

    // Remap settings if necessary
    foreach ($settings as $sett => $value) {
      if (isset($theme_meta->variables[$sett])) {
        switch ($theme_meta->variables[$sett][0]) {
          case "field":
            if (isset($fields[$value])) {
              $settings[$sett] = $fields[$value]->value;
            } else {
              unset($settings[$sett]);
            }
        }
      }
    }

    echo theme($theme_prefix . $fpdata->theme, array(
      "state" => "public",
      "data" => $data,
      "settings" => (object) ($settings + ($course
                                           ? array(
                                               "course_link" => url("course/" . $course->name),
                                               "course_name" => $course->name,
                                               "course_register_link" => url("pricing/" . $course->name),
                                             )
                                           : array())),
      "style_policy" => 0,
    ));
  }
}

// Add styles
foreach (array_keys($themes_rendered) as $theme) {
  echo theme($theme_prefix . $theme, array(
    "style_policy" => 1,
  ));
}
