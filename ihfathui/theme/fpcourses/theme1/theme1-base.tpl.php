<?php

// Variables provided:
// $state = "thumb" | "canvas" | "public";
// $data = {region1: "Foo", region2: "Bar", region3: NULL, ...};
// $settings = {foo: "bar", ...};
// $style_policy = 0 | 1 | 2; // 0: No styles- only content, 1: Only styles- no content, 2: Both styles and content
// $theme_name = "...theme name...";
// $theme_meta = [ ... metadata ... ];
// $file_base = url("sites/default/files/ihfathusercontent");

// Include theme-required PHP:
$THEME_HEAD;

// Theme functions:
//  - $region_control(string $id, bool $setting)           // Get a region element's attribute for use by the theme editor
//  - $region(string $id, string $dummy_text)              // Get a region's (markdown rendered) value
//  - $setting(string $setting_key, mixed $default_value)  // Get a setting's value
//  - $link(string $setting_key, mixed $default_value)     // Get a link/url, defaulting to `javascript:void 0` in a `canvas` state

if ($style_policy === 0 || $style_policy === 2) { ?>

  <section
    class="fp-content course theme1-course style-1"
    <?= $setting('course_name') ? 'id="' . $setting('course_name') . '"' : '' ?>
    style="background-image: url('<?= $file_base . '/' . $setting('background', 'css/graphics/arabic-book-covers-2.png') ?>')">
    <div class="constrain">
      <div class="contents">
        <div class="focus" style="align-items: flex-start;">
          <h1 style="margin: 0;" <?= $region_control(0) ?>>
            <?= $region(0, 'Lorem ipsum dolor sit amet<br>consectetur adipiscing') ?>
          </h1>
          <h2 <?= $region_control(1) ?>>
            <?= $region(1, 'Duis aute irure dolor in reprehenderit') ?>
          </h2>
          <a href="<?= $link("course_link", "course.html") ?>" class="button shadow" -theme-editor-course-link>
            <?= t("Learn more") ?>
          </a>
        </div>
        <div class="good-points" <?= $region_control(2) ?>>
          <?= $region(2, '
          <ul>
            <li>Lorem ipsum dolor sit</li>
            <li>Consectetur adipiscing elit</li>
            <li>Sed do eiusmod tempor incididunt</li>
            <li>Excepteur sint occaecat</li>
          </ul>
          ') ?>
        </div>
      </div>
      <!-- Books -->
      <div class="book-wrap">
        <img src="<?= $file_base . "/" . $setting("picture", "css/graphics/arabic-book-covers-2.png") ?>" class="books">
      </div>
    </div>
    <?php /* print_r(array(
        $theme_meta,
        $settings,
        $data,
      )) */ ?>
  </section>

<?php
}

if ($style_policy === 1 || $style_policy === 2) { ?>

  <style>
  THEME_CSS{}
  </style>

<?php
} ?>
