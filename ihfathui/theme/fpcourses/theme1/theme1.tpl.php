<?php

// Variables provided:
// $state = "thumb" | "canvas" | "public";
// $data = {region1: "Foo", region2: "Bar", region3: NULL, ...};
// $settings = {foo: "bar", ...};
// $style_policy = 0 | 1 | 2; // 0: No styles- only content, 1: Only styles- no content, 2: Both styles and content
// $theme_name = "...theme name...";
// $theme_meta = [ ... metadata ... ];
// $file_base = url("sites/default/files/ihfathusercontent");

// Include theme-required PHP:

// Variables provided:
// $state = "thumb" | "canvas" | "public";
// $data = {region1: "Foo", region2: "Bar", region3: NULL, ...};
// $settings = {foo: "bar", ...};
// $style_policy = 0 | 1 | 2; // 0: No styles- only content, 1: Only styles- no content, 2: Both styles and content
$regions = [];
$theme_name = basename(__FILE__, ".tpl.php");
$theme_meta = ihfathui_fp_course_theme_list()[$theme_name];
$file_base = url("sites/default/files/ihfathusercontent");

$region_visible = function($region_id) use ($data) {
  return isset($data[$region_id]) && (is_string($data[$region_id])
                                  ? trim($data[$region_id])
                                  : TRUE);
};

$region_control = function($id, $setting = FALSE) use ($state) {
  if ($state === "canvas") {
    return 'data-theme-editor-region' . ($setting ? '-setting' : '') . '="' . $id . '"';
  }

  return "";
};

$region = function($region_id, $dummy_text) use ($state, $data, $region_visible) {
  $print = $dummy_text;

  switch ($state) {
    case "thumb":
      // Thumbnail, just print
      return $print;
    case "canvas":
      if ($region_visible($region_id)) {
        $print = $data[$region_id];
      }

      return $print;
    case "public":
      if ($region_visible($region_id)) {
        return check_markup($data[$region_id], "ihfathui_markdown");
      }

      return "";
  }
};

$region_raw = function($region_id, $dummy_text) use ($state, $data, $region_visible) {
  $print = $dummy_text;

  switch ($state) {
    case "thumb":
      // Thumbnail, just print
      return $print;
    case "canvas":
      if ($region_visible($region_id)) {
        $print = $data[$region_id];
      }

      return $print;
    case "public":
      if ($region_visible($region_id)) {
        return $data[$region_id];
      }

      return "";
  }
};

$setting = function($setting, $default = "") use ($state, $settings, $data, $theme_meta) {
  if ($state === "public") {
    if (isset($settings->{$setting})) {
      return $settings->{$setting};
    }
  }

  return $default;
};

$link = function($sett, $default) use ($state, $setting, $settings) {
  if ($state === "canvas") {
    return "javascript:0[0]";
  } else {
    return $setting($sett, $default);
  }
};

$userlist = function($user_list) {
  $uids = array_filter(explode(",", $user_list), function($item) {
    return is_numeric($item);
  });

  if (count($uids)) {
    return user_load_multiple($uids);
  }

  return array();
};
;

// Theme functions:
//  - $region_control(string $id, bool $setting)           // Get a region element's attribute for use by the theme editor
//  - $region(string $id, string $dummy_text)              // Get a region's (markdown rendered) value
//  - $setting(string $setting_key, mixed $default_value)  // Get a setting's value
//  - $link(string $setting_key, mixed $default_value)     // Get a link/url, defaulting to `javascript:void 0` in a `canvas` state

if ($style_policy === 0 || $style_policy === 2) { ?>

  <section
    class="fp-content course theme1-course style-1"
    <?= $setting('course_name') ? 'id="' . $setting('course_name') . '"' : '' ?>
    style="background-image: url('<?= $file_base . '/' . $setting('background', 'css/graphics/arabic-book-covers-2.png') ?>')">
    <div class="constrain">
      <div class="contents">
        <div class="focus" style="align-items: flex-start;">
          <h1 style="margin: 0;" <?= $region_control(0) ?>>
            <?= $region(0, 'Lorem ipsum dolor sit amet<br>consectetur adipiscing') ?>
          </h1>
          <h2 <?= $region_control(1) ?>>
            <?= $region(1, 'Duis aute irure dolor in reprehenderit') ?>
          </h2>
          <a href="<?= $link("course_link", "course.html") ?>" class="button shadow" -theme-editor-course-link>
            <?= t("Learn more") ?>
          </a>
        </div>
        <div class="good-points" <?= $region_control(2) ?>>
          <?= $region(2, '
          <ul>
            <li>Lorem ipsum dolor sit</li>
            <li>Consectetur adipiscing elit</li>
            <li>Sed do eiusmod tempor incididunt</li>
            <li>Excepteur sint occaecat</li>
          </ul>
          ') ?>
        </div>
      </div>
      <!-- Books -->
      <div class="book-wrap">
        <img src="<?= $file_base . "/" . $setting("picture", "css/graphics/arabic-book-covers-2.png") ?>" class="books">
      </div>
    </div>
    <?php /* print_r(array(
        $theme_meta,
        $settings,
        $data,
      )) */ ?>
  </section>

<?php
}

if ($style_policy === 1 || $style_policy === 2) { ?>

  <style>
  *.none {
  display: none;
}

.input,
.button {
  padding: 1rem 1.25rem;
  font-weight: 900;
  font-size: 1.2rem;
  background: rgba(0, 0, 0, 0.45);
  border-radius: 0.25rem;
  text-align: center;
  border: 0;
  color: #fff;
  text-decoration: none;
}

.input.shadow,
.button.shadow {
  box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.2), 0px 5px 20px rgba(0, 0, 0, 0.2);
  transition: box-shadow 250ms ease, transform 250ms ease;
}

.input.shadow:hover,
.button.shadow:hover {
  transform: scale3d(1.025, 1.025, 1.025) translate3d(0px, -3px, 0px);
  box-shadow: 0px 8px 6px rgba(0, 0, 0, 0.2), 0px 13px 30px rgba(0, 0, 0, 0.2);
}

.input {
  text-align: left;
  font-family: Lato;
  font-weight: 400;
}

.input.darker {
  background: rgba(0, 0, 0, 0.75);
}

.good-points ul,
.good-points p {
  font-size: 1.5rem;
  font-weight: 300;
}

.good-points p {
  max-width: 500px;
}

.good-points ul {
  padding: 0;
  max-width: 400px;
}

.good-points ul li {
  position: relative;
  list-style: none;
  margin: 1.5rem 0;
}

.good-points ul li::before {
  content: "";
  position: relative;
  display: inline-block;
  top: 4px;
  margin: auto 0;
  margin-right: 15px;
  margin-left: -1.89em;
  width: 30px;
  height: 30px;
  box-sizing: border-box;
  background: url(<?= url(drupal_get_path("module", "ihfathui") . "/theme/fpcourses/") ?>graphics/logo-square-white.svg) no-repeat;
  background-position: center;
  background-size: cover;
}

.main {
  display: flex;
  flex-flow: column nowrap;
  margin-top: 60px;
  align-items: center;
  flex: 1 0 auto;
  height: 100%;
}

h1 > p, h2 > p, h3 > p, h4 > p, h5 > p, h6 > p {
  margin: 0;
}

.fp-content {
  display: flex;
  flex-flow: row nowrap;
  justify-content: center;
  height: 400px;
  margin-bottom: 0rem;
  width: 100%;
}

.fp-content > .constrain {
  display: flex;
  flex-flow: row;
  width: 100%;
  max-width: 1024px;
  padding: 0;
}

.fp-content h1,
.fp-content h2,
.fp-content h3,
.fp-content h4 {
  display: flex;
  flex-flow: column;
  font-family: "Open Sans";
  font-weight: 300;
  font-size: 3.2rem;
}

.fp-content h1 .soon,
.fp-content h2 .soon,
.fp-content h3 .soon,
.fp-content h4 .soon {
  padding: 0.4rem;
  margin-left: -0.05rem;
  margin-right: auto;
  margin-top: 0.5rem;
  color: #fff;
  font-family: Roboto;
  font-weight: 900;
  font-size: 0.9rem;
  text-transform: uppercase;
  background: #000;
}

.fp-content.course {
  background: radial-gradient(135% 130% at 16% 0%, transparent 75%, rgba(0, 0, 0, 0.1) 110%), "";
  font-family: Roboto;
}

.fp-content.course .constrain {
  align-items: center;
}

.fp-content.course.pack-end {
  background: radial-gradient(135% 130% at 84% 105%, transparent 80%, rgba(0, 0, 0, 0.1) 110%);
}

.fp-content.course.pack-end .constrain {
  justify-content: end;
}

.fp-content.course.pack-end .constrain h1 {
  margin-left: auto;
}

.fp-content.course.style-1.black-style .good-points ul li::before {
  background-image: url(<?= url(drupal_get_path("module", "ihfathui") . "/theme/fpcourses/") ?>graphics/logo-square-black.svg);
  opacity: 0.85;
}

.fp-content.course.style-1 .focus {
  display: flex;
  justify-content: center;
  align-items: flex-start;
  flex-flow: column;
  min-width: 400px;
}

.fp-content.course.style-1 h1 {
  font-size: 2.5rem;
  margin-bottom: 1rem;
  margin-top: -0.25rem;
}

.fp-content.course.style-1 h2 {
  font-size: 1.5rem;
  margin-bottom: 1.75rem;
}

.fp-content.course.style-1 .button {
  align-self: stretch;
}

.theme1-course {
  position: relative;
  overflow: hidden;
  height: 800px;
  color: #fff;
  background-color: #529A2A;
  background-size: cover;
  background-position: center;
}

.theme1-course .contents {
  display: flex;
  width: 100%;
  flex-flow: row nowrap;
  justify-content: space-between;
}

.theme1-course .book-wrap {
  display: flex;
  align-items: center;
  justify-content: center;
  margin-top: 2rem;
  margin-bottom: -4rem;
}

.theme1-course .books {
  max-width: 750px;
}

.theme1-course .good-points {
  margin-bottom: -1.5rem;
}

.theme1-course .button {
  background: #fff;
  color: #529A2A;
}

.theme1-course .constrain {
  position: relative;
  flex-flow: column nowrap;
  justify-content: center;
  align-items: center;
}

  </style>

<?php
} ?>
