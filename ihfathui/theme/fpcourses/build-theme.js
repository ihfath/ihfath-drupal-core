#! /usr/bin/env node

const path = require("path");
const fs = require("fs");
const sass = require("node-sass");
const chokidar = require("chokidar");

let dir = "";
let watch = false;
let first = true;

for (const arg of process.argv.slice(2)) {
  if (arg === "-w"
      || arg === "--watch") {
    watch = true;
  } else {
    dir = arg;
  }
}

if (!dir) {
  console.log(`
USAGE: build-theme [-w|--watch] THEME_DIRECTORY
  `.trim());
  process.exit();
}

const name = path.basename(dir);
let includedFiles = [];

function render(done) {
  sass.render({
    file: dir + "/" + name + ".scss",
    outputStyle: "expanded",
  }, (err, result) => {
    if (err) {
      console.error("Error compiling SCSS", err);
      return;
    }

    // Set included files
    includedFiles = result.stats.includedFiles;

    // Inject PHP code to adjust relative url's to properly reference assets
    let resultCss = result.css.toString("utf8").replace(
                      /(url\(['"]?)(?!(?:https?:\/\/|\/))/g,
                      '$1<?= url(drupal_get_path("module", "ihfathui") . "/theme/fpcourses/") ?>'
                    );

    // Get head PHP
    let headPhpSrc = fs.readFileSync(dir + "/../theme-head.php", {encoding: "utf8"});
    // Exclude <?php prefix from header src
    headPhpSrc = headPhpSrc.slice(headPhpSrc.indexOf("\n") + 1);

    // Save output to file
    const phpSrc = fs.readFileSync(dir + "/" + name + "-base.tpl.php", {encoding: "utf8"});
    const out = phpSrc.replace(/@X.*X@/g, "").replace("$THEME_HEAD", headPhpSrc).replace("THEME_CSS{}", resultCss);
    const outFile = dir + "/" + name + ".tpl.php";
    fs.writeFileSync(outFile, out);

    if (watch && !first) {
      console.log("[" + new Date().toISOString() + "] Change detected, saved to: " + outFile);
    } else {
      console.log("Saved to: " + outFile);
    }

    // Change save message when watch flag is on
    first = false;

    // Call done cb
    if (done) {
      done();
    }
  });
}

render(() => {
  // Watch
  if (watch) {
    const absPhp = path.join(process.cwd(), dir + "/" + name + "-base.tpl.php");
    const absBasePhp = path.join(process.cwd(), dir + "/../theme-head.php");

    let counter = 0;
    chokidar.watch([
      absPhp,
      absBasePhp,
    ].concat(includedFiles)).on("change", (path, evt) => {
      let current = ++counter;

      setTimeout(() => {
        if (current === counter) {
          render();
        }
      }, 150);
    });
  }
});
