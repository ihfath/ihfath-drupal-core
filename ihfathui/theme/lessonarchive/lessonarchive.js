// Ihfath Professor Current Schedule Implementation with Cellidi

function IhfathCurrentProfSchedule(args) {
  var wrapper = jSh("#ihfath-prof-current-schedule-wrapper");
  var options = {
    element: wrapper,

    dimensions: args.dimensions,

    offsetX: 0,
    offsetY: 0,

    canSelect: false,
    history: false,

    headers: {
      top: scheduleOnNewDayHeaderCell,
      left: scheduleOnNewHourHeaderCell
    },

    newCell: scheduleOnNewCell
  };

  // Clear wrapper
  wrapper.innerHTML = "";

  // Get today's date
  var today = (new Date().getDay()) - 1;

  if (today === -1) {
    // Wrap around for compatibility with the IHFATH Weektime
    today = 6;
  }

  var todayOffset = today + (6 * args.weekOffset);

  // Create CellDisplay
  var CellDisplay = Cellidi.CellDisplay;
  var schedule    = Cellidi(options, CellDisplay.mapEachCell(args.data.table, function(data) {
    switch (typeof data) {
      case "number":
        return {
          link: false,
          hour: data
        };
      case "object":
        return {
          name: data.lesson.prof_name,
          link: args.baseUrl + "/koa/lesson/" + data.lesson.lid,
          hour: data.hour,
          lesson: data.lesson
        };
    }
  }));

  // Cellidi Callbacks
  function scheduleOnNewCell(x, y, virtualX, virtualY, data, cellDisplay) {
    // Make cell time in 12hour format
    var yMap         = virtualY === 0 ? 12 : virtualY;
    var cellTime     = virtualY < 12 ? [yMap, "AM"] : [(virtualY === 12 ? 12 : yMap - 12), "PM"];
    var cellTimeFull = cellTime.join("");
    var cell;
    var checker = y % 2 ? ".ihfath-cell-checker" : "";
    var pastDay = !args.weekOffset && x < today ? ".ihfath-cell-past" : "";

    if (data.hour === 3 || data.hour === 0) {
      // It's a weekend cell, return dead cell
      cell = jSh.d(checker + pastDay, null, [
        jSh.c("span", null, cellTime[0]),
        jSh.c("span", null, cellTime[1])
      ]);
      // cell.style.color = mapStateColors[data];
      // cell.style.background = mapStateColors[data];

      return {
        dom: cell,
        deadCell: true
      };
    }

    var content;
    var extraClass  = "";
    var cellLink    = "javascript:void(0)";
    var element     = "div";
    var bgSizeClass = "";
    var cellSpanY   = 1;

    if (data.link) {
      // It's data with lessons
      element  = "a";
      cellLink = data.link;

      if (data.lesson.paid_duration > 60) {
        // This lesson is longer than an hour, expand to cell below
        cellSpanY = 2;

        // Add the bg size classname
        if (+(data.lesson.paid_duration) === 90) {
          // FIXME: Premature assumptions about the paid_duration
          bgSizeClass = ".ihfath-cell-size-one-n-half";
        } else {
          // Two hour cell
          bgSizeClass = ".ihfath-cell-size-two";
        }
      } else {
        // TODO: Do this better
        bgSizeClass = " ";
      }

      content = jSh.d(".ihfath-cell-contents", null, [
        jSh.c("span", ".white-color.bold", data.name + ": "),
        // jSh.t(" "),
        jSh.c("span", ".white-color", data.lesson.program_data.data.en.name),
        jSh.c("span", ".white-color.bold", " (" + (data.lesson.paid_duration / 60) + "h)")
      ]);

      extraClass = ".ihfath-full-cell";
    } else {
      // Nothing on this day
      content = [
        jSh.t(cellTime[0]),
        jSh.c("span", null, cellTime[1])
      ];

      extraClass = ".ihfath-empty-cell";
    }

    cell = jSh.c(element, {
      prop: {
        href: cellLink,
        title: args.time + " " + args.dayMap[virtualX] + ", " + cellTimeFull
      },

      sel: extraClass + checker + pastDay + (bgSizeClass !== " " ? bgSizeClass : ""),
      child: bgSizeClass ? [
        jSh.d(".ihfath-cell-bg"),
        content
      ] : content
    });

    if (cellSpanY > 1) {
      cell.style.lineHeight = (cellDisplay.options.baseCellHeight * 2) + "px";
    }

    // cell.style.background = mapStateColors[data];
    // cell.style.color = mapStateColors[data];

    return {
      dom: cell,
      spanY: cellSpanY
    };
  }

  function scheduleOnNewDayHeaderCell(x, virtualX, orientation, cellDisplay) {
    return jSh.d(null, args.dayMap[virtualX], jSh.c("span", null, " - " + args.weekOffsetDays[x]));
  }

  function scheduleOnNewHourHeaderCell(y, virtualY, orientation, cellDisplay) {
    var yMap     = virtualY === 0 ? 12 : virtualY;
    var cellTime = virtualY < 12 ? [yMap, "AM"] : [(virtualY === 12 ? 12 : yMap - 12), "PM"];

    var checker = y % 2;

    return jSh.d(".main-color" + (checker ? ".ihfath-cell-checker" : ""), null, [
      jSh.t(cellTime[0]),
      jSh.c("span", null, cellTime[1])
    ]);
  }
}
