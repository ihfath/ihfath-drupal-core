<?php
global $user;
global $base_url;

$cur_path = current_path();
$role = $form["#role"];

if ($cur_path === "index.html") {
  $cur_path = "";
} else {
  $cur_path = "/" . $cur_path;
}

$page_url = $base_url . $cur_path;

$schedule          = $form["#ihfath"]["schedule"];
$week_offset       = $form["#ihfath"]["week_offset"];
$week_offset_array = $form["#ihfath"]["week_offset_array"];

$week_offset_array_days = array_map(function($i_d) {
  return (int) date("j", $i_d);
}, $week_offset_array);
?>
<!-- Professor/Student Nav -->
<?php
$navblock = NULL;
$title    = drupal_set_title();

switch ($role) {
  case 1:
    $navblock = module_invoke("mod_background_page", "block_view", "mod_block_background");
    break;
  case 2:
    $navblock = module_invoke("ihfathui", "block_view", "ihfathblk_prof_nav");
    break;
  case 3:
    $navblock = module_invoke("ihfathui", "block_view", "ihfathblk_studentnav");
    break;
}

if (isset($navblock)) {
  echo render($navblock);
}

// Preserve title
drupal_set_title(t("Lesson Archive"));
?>

<?php
// FIXME: Let admin see user's archive
if ($role === 1): ?>

  <div class="section">
    <div class="container ihfath-wrap">
      Admin has no archive
    </div>
  </div>

<?php
else: ?>

<!-- Archive page -->
<div class="section ihfath-small-section">
  <div class="container">
    <div class="head-7 main-border ihfath-archive-header">
      <h4 class="uppercase bold" style="text-align: left;">
        <span class="main-bg "><i class="fa fa-calendar"></i> <?php echo t("Weekly Schedule"); ?></span>
      </h4>

      <a class="ihfath-archive-switch main-bg" href="<?php echo "{$base_url}/koa/lesson/archive/list" ?>">
        <i class="fa fa-list"></i>
        <?php echo t("List") ?>
      </a>
    </div>

    <div class="ihfath-schedule-navigation">
      <div class="ihfath-schedule-date">
        <?php echo date("M j", $week_offset_array[0]) . " - " . date("M j", $week_offset_array[6]); ?>
      </div>

      <?php

      if ($week_offset === 1) {
        $week_arg = "";
      } else {
        $week_arg = "/?week=" . ($week_offset -  1);
      } ?>

      <a href="<?php echo $page_url . $week_arg; ?>" class="ihfath-previous-week main-bg">
        <i class="fa fa-arrow-left"></i> <?php echo t("Previous week"); ?>
      </a>

      <a href="<?php echo $page_url . "/?week=" . ($week_offset +  1); ?>" class="ihfath-next-week main-bg">
        <?php echo t("Next week"); ?> <i class="fa fa-arrow-right"></i>
      </a>
    </div>

    <br>

    <!-- Current week's schedule -->
    <div class="ihfath-prof-schedule-widget">
      <div id="ihfath-prof-current-schedule-wrapper">
        <!-- JS populates this -->
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  window.addEventListener("DOMContentLoaded", function() {
    var data = <?php echo $schedule ? json_encode($schedule) : "null"; ?>;
    var weekOffset = <?php echo $week_offset; ?>;

    // Cell state mappings
    var mapStateNames = [
      "<?php echo t("Unavailable"); ?>",
      "<?php echo t("Vacant"); ?>",
      "<?php echo t("Booked"); ?>",
      "<?php echo t("Weekend"); ?>"
    ];

    var possibleStates = [
      0, 1, 3
    ];

    var dayMap = [
      "<?php echo t("Monday"); ?>",
      "<?php echo t("Tuesday"); ?>",
      "<?php echo t("Wednesday"); ?>",
      "<?php echo t("Thursday"); ?>",
      "<?php echo t("Friday"); ?>",
      "<?php echo t("Saturday"); ?>",
      "<?php echo t("Sunday"); ?>"
    ];

    // Invoke schedule
    IhfathCurrentProfSchedule({
      data: data,
      weekOffset: weekOffset,
      weekOffsetDays: <?php echo json_encode($week_offset_array_days); ?>,
      dimensions: [
        7, 24
      ],

      time: "<?php echo t("Time") . ":"; ?>",
      dayMap: dayMap,
      mapStateNames: mapStateNames,
      baseUrl: <?php _ihf_eje($base_url); ?>,
    });
  });
</script>

<style media="screen">
  /* Switch button */
  .ihfath-archive-header {
    position: relative;
  }

  .ihfath-archive-switch {
    position: absolute;
    display: inline-block;
    right: 0px;
    top: 5px;
    padding: 5px 10px;

    border-radius: 3px;
  }

  /* Navigation button styles */
  .ihfath-schedule-navigation > a {
    position: relative;
    z-index: 1;
    display: inline-block;
    padding: 10px 15px;

    font-size: 14px;
    cursor: pointer;
    transition: none;
  }

  .ihfath-next-week {
    float: right;
  }

  .ihfath-schedule-navigation {
    /* prevent undesirable float behaviour */
    overflow: hidden;
    position: relative;
  }

  .ihfath-schedule-date {
    position: absolute;
    left: 0px;
    right: 0px;
    height: 100%;

    line-height: 30px;
    font-size: 20px;
    text-align: center;
  }

  /* Schedule styles */
  /* State styles */

  a.ihfath-cell-past {
    background: #a9bf04 !important;
    cursor: default;
  }

  .ihfath-cell-past::after {
    content: "";
    position: absolute;
    z-index: 10;
    left: -2px;
    top: 0px;
    bottom: 0px;
    right: 0px;
    background: rgba(0, 0, 0, 0.1);

    pointer-events: none;
  }

  .ihfath-cell-past:nth-child(2)::after {
    left: 0px;
  }

  .ihfath-full-cell {
    position: relative;
    padding: 0px !important;
    line-height: 40px;

    color: #fff !important;
    font-size: 16px;
    text-shadow: 1px 1px 1px rgba(0,0,0,.15);
  }

  .ihfath-full-cell.ihfath-cell-size-one-n-half {
    line-height: 60px !important;
  }

  .ihfath-full-cell * {
    position: relative;
    z-index: 10;
  }

  .ihfath-full-cell .ihfath-cell-bg {
    position: absolute;
    top: 0px;
    left: 0px;
    right: 0px;
    bottom: 0px;

    background-color: #a9bf04;
    transition: background 0.35s ease;
  }

  .ihfath-full-cell:hover .ihfath-cell-bg {
    background-color: #8ba100;
  }

  .ihfath-full-cell.ihfath-cell-size-one-n-half .ihfath-cell-bg {
    bottom: 25%;
  }

  .ihfath-cell-contents {
    display: inline-block;
    vertical-align: middle;
    padding: 0px 5px;

    line-height: 16px;
  }

  /* Base styles */
  #ihfath-prof-current-schedule-wrapper .cellidi-cell:not(.main-bg) {
    background: #fff;
  }

  #ihfath-prof-current-schedule-wrapper .cellidi-cell {
    position: relative;
    border-right: 2px solid #fff;
  }

  #ihfath-prof-current-schedule-wrapper .cellidi-cell.ihfath-empty-cell,
  #ihfath-prof-current-schedule-wrapper .cellidi-cell.ihfath-full-cell {
    font-weight: bold;
  }

  #ihfath-prof-current-schedule-wrapper .cellidi-cell.ihfath-empty-cell span,
  #ihfath-prof-current-schedule-wrapper .cellidi-cell.ihfath-full-cell span {
    font-weight: normal;
    font-size: 0.75em;
  }

  #ihfath-prof-current-schedule-wrapper .cellidi-cell.ihfath-empty-cell {
    color: #B3B3B3;
  }

  #ihfath-prof-current-schedule-wrapper .cellidi-dead-cell {
    /*opacity: 0.35;*/
    color: rgba(102, 102, 102, 0.35) !important;
  }

  #ihfath-prof-current-schedule-wrapper .cellidi-dead-cell span {
    opacity: 0.25;
  }


  #ihfath-prof-current-schedule-wrapper .cellidi-cell.ihfath-cell-checker:not(.main-bg) {
    background: #E6E6E6;
  }

  #ihfath-prof-current-schedule-wrapper .cellidi-dead-cell.cellidi-cell.ihfath-cell-checker:not(.main-bg) {
    background: rgba(230, 230, 230, 0.35);
  }

  #ihfath-prof-current-schedule-wrapper .cellidi-header-cell:not(.main-bg) {
    background: rgba(0, 0, 0, 0.025);
  }

  #ihfath-prof-current-schedule-wrapper .cellidi-header-cell {
    border-color: rgba(0, 0, 0, 0.035);
  }

  #ihfath-prof-current-schedule-wrapper .cellidi-header-cell span {
    font-weight: normal !important;
  }

  #ihfath-prof-current-schedule-wrapper .cellidi-header-cell.cellidi-corner-cell-top-left {
    background: rgba(0, 0, 0, 0.055);
    border-width: 0px;
    /*border-right-width: 1px;
    border-right-style: solid;*/
  }

  #ihfath-prof-current-schedule-wrapper .cellidi-header-cell-left {
    border-right: 2px solid #fff;
    background: #fff;
  }

  #ihfath-prof-current-schedule-wrapper .cellidi-header-cell-left.ihfath-cell-checker {
    background: #E6E6E6;
  }

  #ihfath-prof-current-schedule-wrapper .cellidi-header-cell-left span {
    font-weight: normal;
    font-size: 0.75em;
  }
</style>

<?php
endif; ?>
