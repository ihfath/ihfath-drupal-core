<?php
global $base_url;
$cur_month = (int) date("n");

function jst(...$args) {
  return json_encode(t(...$args));
}

function jse(...$args) {
  return json_encode(...$args);
}

?>
<div class="ihfath-admin-lessons">
  <div class="ihfath-header">
    <div class="ihfath-row">
      <div class="ihfath-user">
        <h2>
          <div class="ihfath-input-wrap">
            <input id="ihfath-adminlessons-user-input" autocomplete="off" type="text" value="" placeholder="Username...">

            <div class="ihfath-suggestions ihfath-hidden">
              <!-- Suggestions here -->
            </div>
            <div class="ihfath-actions">
              <!-- <a href="#" target="_blank"><i class="fa fa-gear"></i></a> -->
              <i class="ihfath-user-ready ihfath-hidden" title="<?= t('Ready') ?>"><i class="fa fa-check"></i></i>
              <a class="ihfath-messages ihfath-hidden" href="#" target="_blank" title="<?= t('View this user\'s messages') ?>"><i class="fa fa-envelope-square"></i></a>
              <a class="ihfath-edit-user ihfath-hidden" href="#" target="_blank" title="<?= t('Edit this user') ?>"><i class="fa fa-external-link-square"></i></a>
              <i class="ihfath-spinner ihfath-hidden" title="<?= t('Loading...') ?>"><i class="fa fa-circle-o-notch"></i></i>
            </div>
          </div>
          <button class="ihfath-change-btn ihfath-hidden"><i class="fa fa-pencil ihfath-btn-i"></i> <?= t("Change") ?></button>
        </h2>
      </div>
      <div class="ihfath-tabs">
        <div class="ihfath-tab ihfath-tab-schedule">
          Schedule
        </div>
        <div class="ihfath-tab ihfath-tab-userrel">
          Teachers
        </div>
      </div>
    </div>
    <div id="ihfath-user-summary" class="ihfath-user-summary ihfath-hidden">
      <!-- Summary here -->
    </div>
  </div>
  <div class="ihfath-tab-body">
    <div class="ihfath-body-schedule">
      <div class="ihfath-bs-header">
        <div class="ihfath-input">
          <h3><?= t("Month") ?></h3>
          <select class="ihfath-month-select" disabled>
            <?php

            $months = array(
              "January",
              "February",
              "March",
              "April",
              "May",
              "June",
              "July",
              "August",
              "September",
              "October",
              "November",
              "December",
            );

            $month_index = 1;
            foreach ($months as $month) {
              $selected = $month_index === $cur_month ? "selected" : ""; ?>

              <option value="<?= $month_index ?>" <?= $selected ?>><?= t($month) ?></option>

            <?php
            $month_index++;
            }

            ?>
          </select>
        </div>
        <div class="ihfath-input">
          <h3><?= t("Year") ?></h3>
          <input class="ihfath-year-input" type="text" value="<?= date("Y") ?>" disabled>
        </div>
        <div class="ihfath-input ihfath-input-nav">
          <h3><?= t("Navigation") ?></h3>
          <button class="input-btn ihfath-go-prev-month">
            <i class="fa fa-angle-left"></i>
            <span><?= t("Previous") ?></span>
          </button>
          <button class="input-btn ihfath-go-next-month">
            <span><?= t("Next") ?></span>
            <i class="fa fa-angle-right"></i>
          </button>
        </div>
        <div class="ihfath-input ihfath-input-nav">
            <h3><?= t("Courses") ?></h3>
          <select>
            <option value="0"><?= t("All") ?></option>

            <?php
            foreach ($courses as $course) {
            ?>

              <option value="<?= $course->pid ?>">
                <?= $course->data["current"]->name ?>
              </option>

            <?php
            }
             ?>
          </select>
        </div>
        <div class="ihfath-input ihfath-input-nav">
          <h3><?= t("Timezone") ?></h3>
          <select>
            <option value="user"><?= t("User") ?></option>
            <option value="admin"><?= t("Admin") ?></option>
          </select>
        </div>
      </div>
      <div class="ihfath-schedule">
        <div class="ihfath-calendar ihfath-month">
          <h3><?= t("Monthly calendar") ?></h3>
          <div class="ihfath-schedule-root">
            <!-- Schedule here... -->
          </div>
        </div>
        <div class="ihfath-calendar ihfath-week">
          <h3>
            <div class="ihfath-back-to-month">
              <i class="fa fa-chevron-left"></i> <?= t("Back to monthly calendar") ?>
            </div>
            <?= t("Weekly calendar") ?>
          </h3>
          <div class="ihfath-schedule-root">
            <!-- Schedule here... -->
          </div>
        </div>
        <!-- Sidebar -->
        <div class="ihfath-sidebar ihfath-hidden">
          <div class="ihfath-header-fill">
            <i class="ihfath-sidebar-close fa fa-times" title="<?= t("Close sidebar") ?>"></i>
          </div>
          <div class="ihfath-sidebar-shadow"></div>
          <div class="ihfath-sidebar-content">
            <div class="ihfath-sidebar-inner">

            </div>
          </div>
        </div>
        <!-- Unavailable Message -->
        <div class="ihfath-unavailable ihfath-hidden">
          <div class="ihfath-text">
            <span>
              <!-- Message here -->
            </span>
          </div>
        </div>
      </div>
    </div>
    <div class="ihfath-body-userrel">
      User relations tab
      <!-- User relations here... -->
    </div>
  </div>

  <script type="text/javascript">
    window.addEventListener("load", function() {
      InitLessonAdmin({
        baseUrl: <?= jst($base_url) ?>,
        apiUrl: <?= jst($base_url . "/ihfath/ajax/lessonadmin") ?>,
        userId: <?= jse($lesson_user) ?>,

        dom: {
          form: jSh("#ihfathui-lesson-admin-callback"),
          userInput: jSh("#ihfath-adminlessons-user-input"),
          userSummary: jSh("#ihfath-user-summary"),

          calendarWrap: jSh(".ihfath-schedule")[0],
          calendarInput: jSh(".ihfath-bs-header")[0],
          calendarMonth: jSh(".ihfath-schedule .ihfath-month")[0],
          calendarWeek: jSh(".ihfath-schedule .ihfath-week")[0],
          calendarUnavailable: jSh(".ihfath-schedule > .ihfath-unavailable")[0],
          calendarSideBar: jSh(".ihfath-schedule > .ihfath-sidebar")[0],

          tabs: {
            tabs: jSh(".ihfath-tabs")[0],
            body: jSh(".ihfath-tab-body")[0],
            items: ["schedule", "userrel"],
          },
        },

        locale: {
          noData: <?= jst("No data to show") ?>,

          // User fields
          userRealName: <?= jst("Real Name") ?>,
          userGender: <?= jst("Gender") ?>,
          userMail: <?= jst("E-mail") ?>,
          userLastLogin: <?= jst("Last Login") ?>,
          userCreated: <?= jst("Registered") ?>,
          userRole: <?= jst("Role") ?>,
          userTimezone: <?= jst("Timezone") ?>,

          // User roles
          roleAdmin: <?= jst("Administrator") ?>,
          roleProfessor: <?= jst("Professor") ?>,
          roleStudent: <?= jst("Student") ?>,

          // Tab names
          users: <?= jst("Users") ?>,
          students: <?= jst("Students") ?>,
          professors: <?= jst("Professors") ?>,

          // Days
          days: <?= jse(array(
            t("Monday"),
            t("Tuesday"),
            t("Wednesday"),
            t("Thursday"),
            t("Friday"),
            t("Saturday"),
            t("Sunday"),
          )) ?>,
          hours: <?= jst("Hours") ?>,

          // Sidebar
          sidebar: {
            actions: <?= jst("Actions") ?>,
            details: <?= jst("Details") ?>,
            participants: <?= jst("Participants") ?>,
            lessonDetails: <?= jst("Lesson details") ?>,
            lessonActivity: <?= jst("Activity") ?>,

            // Actions
            actionSingle: <?= jst("Lesson") ?>,
            actionSeries: <?= jst("Series") ?>,
            actionMoveSingle: <?= jst("Move this lesson") ?>,
            actionMoveSeries: <?= jst("Move whole series") ?>,
            actionDeleteSingle: <?= jst("Delete this lesson") ?>,
            actionDeleteSeries: <?= jst("Delete whole series") ?>,
            actionPostponeLesson: <?= jst("Postpone lesson") ?>,
            actionGoToLesson: <?= jst("Go to lesson") ?>,

            // Participants
            professor: <?= jst("Professor") ?>,
            student: <?= jst("Student") ?>,
            admin: <?= jst("Admin") ?>,
            startTime: <?= jst("Start time") ?>,

            // Lesson
            lessonId: <?= jst("Lesson ID") ?>,
            paidDuration: <?= jst("Paid duration") ?>,
            lessonType: <?= jst("Type") ?>,
            lessonProgram: <?= jst("Program") ?>,
            lessonSeriesId: <?= jst("Series ID") ?>,
            lessonOrderId: <?= jst("Order ID") ?>,
            hours: <?= jst("Hours") ?>,

            // Activity
            lastVisit: <?= jst("Last visit") ?>,
            notAvailableNA: <?= jst("N/A") ?>,
            duration: <?= jst("Duration") ?>,
          },

          // Calendar
          findAUser: <?= jst("Please [click]load a user[/click]") ?>,
          userNoCalendar: <?= jst("User @user doesn't have a calendar") ?>,
          errorGettingCalendar: <?= jst("Error loading calendar for @user") ?>,
        }
      });
    });
  </script>
</div>
