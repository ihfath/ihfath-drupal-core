import {LocaleStrings} from "../locale";
import {JshExtendedNode, JshDiv} from "jshorts";

export interface ConfigSchema {
  locale: LocaleStrings;
  apiUrl?: string;
  baseUrl?: string;
  userId: string;

  dom: {
    form: HTMLFormElement,
    userInput: HTMLInputElement;
    userSummary: HTMLDivElement;

    calendarWrap: JshDiv;
    calendarInput: JshDiv;
    calendarMonth: JshDiv;
    calendarWeek: JshDiv;
    calendarUnavailable: JshDiv;
    calendarSideBar: JshDiv;

    tabs: {
      tabs: JshDiv;
      body: JshDiv;

      items: string[];
    },

    days: string[],
  }

  [propName: string]: any;
}
