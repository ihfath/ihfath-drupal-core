import {
  Component,
  ComponentStateConfig
} from "lces";
import {state} from "../state";
import jSh from "jshorts";

interface UserFieldStateModel {
  value: string;
  suggestionsVisible: boolean;
};

interface UserFieldEventModel {
  suggestions: string[];
  change: string;
}

interface UserFieldActions {
  [p: string]: {
    setup?: (userAction: UserAction) => void;
    click?: (this: HTMLElement, evt?: MouseEvent) => void;
  }
}

export
class Userfield extends Component<UserFieldStateModel, UserFieldEventModel> {
  constructor(
    public dom: HTMLInputElement,
    public actions: UserFieldActions,
    public userActions: {[p: string]: UserAction} = {},
    public domSuggestions?: HTMLDivElement,
    public domActions?: HTMLDivElement,
    public domChangeBtn?: HTMLButtonElement
  ) {
    super();
    
    this.newState("value", null);
    this.newState("suggestionsVisible", true);
    this.newEvent("suggestions");
    this.newEvent("change");
    
    // Add dom
    this.addDom(dom);
    this.addActions(actions);
    
    // Disable input when a user activated
    state.onState("userActive", userActive => {
      if (userActive) {
        this.dom.disabled = true;
      } else {
        this.dom.disabled = false;
      }
    });
    
    // Initialize suggestionsVisible
    this.setState("suggestionsVisible", false);
  }
  
  private addDom(dom: HTMLInputElement) {
    const that = this;
    
    // Trigger value state when user changes textfield
    let delayTimeout: number = null;
    dom.addEventListener("input", e => {
      clearTimeout(delayTimeout);
      const name = dom.value.trim();
      
      delayTimeout = setTimeout(() => {
        this.setState("value", name);
        this.triggerEvent("change", name);
      }, 150);
    });
    
    // Reference dom nodes
    const parentNode = jSh<HTMLDivElement>(dom.parentNode.parentNode as HTMLDivElement);
    const suggestions = this.domSuggestions = parentNode.jSh(".ihfath-suggestions")[0];
    const actions = this.domActions = parentNode.jSh(".ihfath-actions")[0];
    const changeBtn = this.domChangeBtn = parentNode.jSh<HTMLButtonElement>(".ihfath-change-btn")[0];
    
    // Bind curUser state to the input
    state.onState("user", (user: string) => {
      this.dom.value = user;
    });
    
    // Render new username suggestions
    state.on("userSuggestions", suggList => {
      // Show suggestion box
      this.setState("suggestionsVisible", true);
      
      if (suggList) {
        suggestions.innerHTML = "";
        const frag = jSh.docFrag();
        
        if (suggList.length) {
          for (const suggestion of suggList) {
            frag.appendChild(jSh.d({
              sel: ".ihfath-user-suggestion",
              child: jSh.c("span", {
                text: suggestion
              }),
              prop: <any>{
                ihfathSuggestion: suggestion
              }
            }));
          }
          
          // Add suggestions
          suggestions.appendChild(frag);
        } else {
          this.setState("suggestionsVisible", false);
        }
      }
    });
    
    // Toggle suggestions box visibility
    this.onState("suggestionsVisible", (visible: boolean) => {
      if (visible) {
        this.domSuggestions.classList.remove("ihfath-hidden");
      } else {
        this.domSuggestions.classList.add("ihfath-hidden");
      }
    });
    
    // Hide suggestions when input is blurred
    dom.addEventListener("blur", () => {
      this.timeout("hideSuggestionsOnBlur", 1000).then(() => {
        this.setState("suggestionsVisible", false);
      });
    });
    
    // Cancel suggestion box hiding when refocused
    dom.addEventListener("focus", () => {
      this.clearTimeout("hideSuggestionsOnBlur");
    });
    
    // Set the user to the clicked suggestion
    suggestions.addEventListener("click", evt => {
      let target: any = evt.target;
      let suggestion: string = null;
      
      while (target !== suggestions) {
        if ("ihfathSuggestion" in target) {
          suggestion = target.ihfathSuggestion;
          break;
        }
        
        target = target.parentNode;
      }
      
      if (suggestion !== null) {
        state.setState("user", suggestion);
        
        // Hide suggestion box
        this.setState("suggestionsVisible", false);
      }
    });
    
    // Toggle change btn visibility
    state.onState("userActive", userActive => {
      if (userActive) {
        changeBtn.classList.remove("ihfath-hidden");
      } else {
        changeBtn.classList.add("ihfath-hidden");
      }
    });
    
    // Disable userActive state when changeBtn is clicked
    changeBtn.addEventListener("click", function(e) {
      state.setState("userActive", false);
      state.setState("userData", null);
      state.setState("user", "");
      
      that.dom.focus();
      e.preventDefault();
    });
  }
  
  private addActions(actions: UserFieldActions) {
    const actionWrap = jSh(this.domActions);
    
    for (const action of Object.keys(actions)) {
      const actionCallbacks = actions[action];
      const className = "ihfath-" + action.replace(/([A-Z])/g, "-$1").toLowerCase();
      
      const setup = actionCallbacks.setup;
      const callback = actionCallbacks.click;
      const actionDom = actionWrap.jSh("." + className)[0];
      const userActionInst = new UserAction(actionDom, false, actionCallbacks);
      
      if (setup) {
        setup(userActionInst);
      }
      
      if (callback) {
        actionDom.addEventListener("click", callback);
      }
      
      // Save to user actions
      this.userActions[action] = userActionInst;
    }
  }
}

interface UserActionStateModel {
  visible: boolean;
}

export
class UserAction extends Component<UserActionStateModel> {
  constructor(
    public dom: HTMLElement,
    visible: boolean = false,
    public callbacks: UserFieldActions[0],
  ) {
    super();
    
    this.newState("visible", visible);
    this.onState("visible", visible => {
      if (visible) {
        this.dom.classList.remove("ihfath-hidden");
      } else {
        this.dom.classList.add("ihfath-hidden");
      }
    });
  }
}
