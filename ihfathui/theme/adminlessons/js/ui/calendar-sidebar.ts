import {Component} from "lces";
import jSh, {JshDiv} from "jshorts";
import {User, Lesson, UserRoles} from "../model";
import {state} from "../state";
import {locale} from "../locale";
import {api} from "../misc/req";
import {humanTime} from "./utils";
import {notifis} from "./notifi";

interface SidebarSectionStateModel {
  collapsed: boolean;
  domHeader: JshDiv;
  domContent: JshDiv;
}

export
class CalendarSideBar extends Component {
  private domContent: JshDiv;
  private domActions: JshDiv[];
  private domClose: JshDiv;
  private sections: {[s: string]: Component<SidebarSectionStateModel>};
  private lesson: Lesson;

  constructor(
    public dom: JshDiv,
  ) {
    super();

    this.sections = {};
    this.domContent = this.dom.jSh(".ihfath-sidebar-inner")[0];
    this.domActions = [];
    this.domClose = this.dom.jSh(".ihfath-sidebar-close")[0];

    state.onState("focusedLesson", lesson => {
      this.lesson = lesson;
      this.render(lesson);
    });

    this.domClose.addEventListener("click", e => {
      state.setState("sideBarVisible", false);
    });

    // Setup
    this.buildActions();
  }

  private buildActions() {
    const that = this;
    const actions = this.domActions;
    const lessonLink: HTMLAnchorElement = jSh.c<HTMLAnchorElement>("a", {
      sel: ".ihfath-action.ihfath-half",
      child: [
        jSh.c("i", ".fa.fa-external-link-square"),
        jSh.c("span", null, " " + locale.sidebar.actionGoToLesson),
      ],
      prop: {
        href: "#",
        title: locale.sidebar.actionGoToLesson,
        target: "_blank",
      },
    });

    const delLessonSeries = jSh.d({
      sel: ".ihfath-action.ihfath-half",
      child: [
        jSh.c("i", ".fa.fa-trash-o"),
        jSh.c("span", null, " " + locale.sidebar.actionSeries),
      ],
      prop: {
        title: locale.sidebar.actionDeleteSeries,
      },
      events: {
        click() {
          notifis.delLesson.setState("lesson", that.lesson);
          notifis.delLesson.open();
          // notifis.delLesson.render(true);
        }
      }
    });

    // Set anchor to delLessonSeries button
    notifis.delLesson.setState("anchor", delLessonSeries);

    state.onState("focusedLesson", lesson => {
      lessonLink.href = api.base + "/koa/lesson/" + lesson.lid;
    });

    actions.push(this.buildSection(
      "Actions", ".ihfath-rows", [
        // Move lesson/series
        jSh.d(null, null, [
          jSh.d({
            sel: ".ihfath-action.ihfath-half-first",
            child: [
              jSh.c("i", ".fa.fa-arrows"),
              jSh.c("span", null, " " + locale.sidebar.actionSingle),
            ],
            prop: {
              title: locale.sidebar.actionMoveSingle
            },
            events: {
              click() {
                state.triggerEvent("moveLesson", that.lesson);
              }
            }
          }),

          jSh.d({
            sel: ".ihfath-action.ihfath-half",
            child: [
              jSh.c("i", ".fa.fa-arrows"),
              jSh.c("span", null, " " + locale.sidebar.actionSeries),
            ],
            prop: {
              title: locale.sidebar.actionMoveSeries
            },
            events: {
              click() {

              }
            }
          }),
        ]),

        // Delete lesson/series
        jSh.d(null, null, [
          jSh.d({
            sel: ".ihfath-action.ihfath-half-first",
            child: [
              jSh.c("i", ".fa.fa-trash-o"),
              jSh.c("span", null, " " + locale.sidebar.actionSingle),
            ],
            prop: {
              title: locale.sidebar.actionDeleteSingle
            },
            events: {
              click() {
                const confirm = notifis.confirm;

                confirm.setState("action", "delete");
                confirm.setState("thing", "this lesson");
                confirm.setState("callback", () => {
                  state.triggerEvent("deleteLesson", that.lesson);
                });

                confirm.open();
              }
            }
          }),

          delLessonSeries,
        ]),

        jSh.d(null, null, [
          // Postpone lesson
          jSh.d({
            sel: ".ihfath-action.ihfath-half-first",
            child: [
              jSh.c("i", ".fa.fa-clock-o"),
              jSh.c("span", null, " " + locale.sidebar.actionPostponeLesson),
            ],
            prop: {
              title: locale.sidebar.actionPostponeLesson,
            },
            events: {
              click() {
                state.triggerEvent("postponeLesson", that.lesson);
              }
            }
          }),

          // Lesson link
          lessonLink,
        ])
      ]
    ));
  }

  private infoStrip(label: string, value: string = null) {
    // Render plain label/value
    if (value !== null) {
      return jSh.d(".ihfath-info", null, [
        jSh.c("span", ".ihfath-label", label),
        jSh.c("span", ".ihfath-value", value),
      ]) as JshDiv;
    } else {
      // Render header
      return jSh.d({
        sel: ".ihfath-section-header",
        child: jSh.c("span", null, label + " "),
      });
    }
  }

  private buildSection(header: string, sel: string, content: Node[]): JshDiv {
    let sectionComponent = this.sections[header];

    if (!sectionComponent) {
      sectionComponent = this.sections[header] = new Component<SidebarSectionStateModel>();

      sectionComponent.newState("collapsed", false);
      sectionComponent.newState("domHeader", null);
      sectionComponent.newState("domContent", null);

      const headerClick = () => {
        sectionComponent.setState("collapsed", !sectionComponent.getState("collapsed"));
      };

      sectionComponent.onState("domHeader", function(header) {
        const oldDiv = this.oldValue;

        if (oldDiv) {
          oldDiv.removeEventListener("click", headerClick);
        }

        if (header) {
          header.addEventListener("click", headerClick);
        }
      });

      // Toggle visiblity of section content
      sectionComponent.onState("collapsed", collapsed => {
        const headerDom = sectionComponent.getState("domHeader");
        const contentDom = sectionComponent.getState("domContent");

        if (collapsed) {
          headerDom && headerDom.classList.add("ihfath-collapsed");
          contentDom && contentDom.classList.add("ihfath-hidden");
        } else {
          headerDom && headerDom.classList.remove("ihfath-collapsed");
          contentDom && contentDom.classList.remove("ihfath-hidden");
        }
      });
    }

    // FIXME: Possible memory leak? Direct ref from callback to dom and vice versa
    const sectionState = sectionComponent;

    const headerDom = sectionState.setState("domHeader", jSh.d({
      sel: ".ihfath-section-header",
      events: {
        mousedown(e) {
          e.preventDefault();
        },
       },
      child: [
        jSh.c("span", null, header + " "),
        jSh.c("i", {
          sel: ".fa.fa-caret-down",
        }),
      ],
    }));

    const contentDom = sectionState.setState("domContent", jSh.d({
      sel: ".ihfath-content" + (sel || ""),
      child: content,
    }));

    const base = jSh.d({
      sel: ".ihfath-sidebar-section",
      child: [
        headerDom,
        contentDom,
      ],
    });

    // Reapply state
    sectionState.setState("collapsed", sectionState.getState("collapsed"), true);

    return base;
  }

  render(lesson: Lesson) {
    const content = this.domContent;
    const profUser = state.getState("usersInvolved")[lesson.puid];
    const studUser = state.getState("usersInvolved")[lesson.suid];

    const tzOffset = (new Date().getTimezoneOffset()) * 60;
    const shiftTzDate = (date: number, offset: number) => new Date((date + (offset * 60 * 60) + tzOffset) * 1000);
    const profStart = shiftTzDate(lesson.startDate, profUser.timezoneOffset);
    const studStart = shiftTzDate(lesson.startDate, studUser.timezoneOffset);
    const adminStart = new Date(lesson.startDate * 1000);
    content.innerHTML = "";

    // Add actions
    content.appendChild(this.domActions);

    // Participants
    content.appendChild(this.buildSection(
      locale.sidebar.participants, ".ihfath-rows", [
        this.infoStrip(locale.sidebar.professor, profUser.realName || profUser.name),
        this.infoStrip(locale.sidebar.startTime, `${ profStart.toLocaleTimeString() } — ${ profStart.toLocaleDateString() }`),
        this.infoStrip(locale.sidebar.student, studUser.name),
        this.infoStrip(locale.sidebar.startTime, `${ studStart.toLocaleTimeString() } — ${ studStart.toLocaleDateString() }`),
        this.infoStrip(locale.sidebar.admin, `${ adminStart.toLocaleTimeString() } — ${ adminStart.toLocaleDateString() }`),
      ]
    ));


    const profVisit = lesson.profLast ? shiftTzDate(lesson.profLast, profUser.timezoneOffset).toLocaleTimeString() : locale.sidebar.notAvailableNA;
    const studentVisit = lesson.studentLast ? shiftTzDate(lesson.studentLast, profUser.timezoneOffset).toLocaleTimeString() : locale.sidebar.notAvailableNA;

    // Activity
    content.appendChild(this.buildSection(
      locale.sidebar.lessonActivity, ".ihfath-rows", [
        this.infoStrip(locale.sidebar.lastVisit),
        this.infoStrip(locale.sidebar.professor, profVisit),
        this.infoStrip(locale.sidebar.student, studentVisit),
        this.infoStrip(locale.sidebar.duration),
        this.infoStrip(locale.sidebar.professor, humanTime(lesson.profDur)),
        this.infoStrip(locale.sidebar.student, humanTime(lesson.studentDur)),
      ]
    ));

    // Lesson details
    content.appendChild(this.buildSection(
      locale.sidebar.lessonDetails, ".ihfath-rows", [
        this.infoStrip(locale.sidebar.lessonId, lesson.lid + ""),
        this.infoStrip(locale.sidebar.lessonSeriesId, lesson.series + ""),
        this.infoStrip(locale.sidebar.lessonOrderId, lesson.orderId + ""),
        this.infoStrip(locale.sidebar.paidDuration, (lesson.paidDuration / 60) + " " + locale.sidebar.hours),
        this.infoStrip(locale.sidebar.lessonProgram, lesson.programName),
        this.infoStrip(locale.sidebar.lessonType, lesson.typeName),
      ]
    ));
  }
}
