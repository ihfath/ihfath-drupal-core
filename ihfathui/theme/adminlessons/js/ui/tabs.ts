import {
  Component,
  ComponentStateCallback,
  ComponentState
} from "lces";
import {state} from "../state";
import jSh from "jshorts";

interface TabStateModel {
  selected: boolean;
}

export
class Tab extends Component<TabStateModel> {
  constructor(
    public name: string,
    public domTab: HTMLElement,
    public domBody: HTMLElement,
    public index: number,
    public manager: TabManager,
  ) {
    super();
    
    // Toggle body visibility and tab highlight
    this.newState("selected", null);
    this.onState("selected", selected => {
      if (selected) {
        this.domTab.classList.add("ihfath-selected");
        this.domBody.classList.remove("ihfath-hidden");
      } else {
        this.domTab.classList.remove("ihfath-selected");
        this.domBody.classList.add("ihfath-hidden");
      }
    });
    
    // Select when clicked
    this.domTab.addEventListener("click", () => {
      this.select();
    });
  }
  
  setText(text: string) {
    this.domTab.textContent = text;
  }
  
  select() {
    this.manager.selectTab(this.index);
  }
}

export
class TabManager extends Component {
  tabs: Tab[];
  tabMap: {
    [tab: string]: Tab;
  };
  clearedAll: boolean;
  
  constructor() {
    super();
    const that = this;
    
    // Initialize props
    this.tabs = [];
    this.tabMap = {};
    this.clearedAll = false;
    
    // Change tabs' selected state
    state.onState("tab", function(this: ComponentState<string>, tabName: string) {
      const tab = that.tabMap[tabName];
      const oldTab = that.tabMap[this.oldValue];
      
      if (oldTab) {
        oldTab.setState("selected", false);
      }
      
      tab.setState("selected", true);
    });
    
    // Hide all unrelated tabs the first time
    state.onState("tab", function clearAll(tabName) {
      for (const tab of that.tabs) {
        if (tab.name !== tabName) {
          tab.setState("selected", false);
        }
      }
      
      state.removeStateListener("tab", clearAll);
    });
  }
  
  addTab(name: string, tab: HTMLElement, body: HTMLElement): Tab {
    const newTab = new Tab(name, tab, body, this.tabs.length, this);
    
    // Create references for our new tab
    this.tabs.push(newTab);
    this.tabMap[name] = newTab;
    
    return newTab;
  }
  
  getTab(name: string) {
    return this.tabMap[name];
  }
  
  selectTab(index: number | string) {
    let tab = null;
    
    if (typeof index === "number") {
      tab = this.tabs[index];
    } else {
      tab = this.tabMap[index];
    }
    
    if (tab) {
      // Select the tab
      state.setState("tab", tab.name);
    }
  }
}
