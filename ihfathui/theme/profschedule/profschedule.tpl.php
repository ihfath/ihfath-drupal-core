<?php
global $user;
global $base_url;
$cur_path = current_path();

if ($cur_path === "index.html") {
  $cur_path = "";
} else {
  $cur_path = "/" . $cur_path;
}

$page_url = $base_url . $cur_path;
$ihfath = $form["#ihfath"];

$schedule          = $ihfath["schedule"];
$week_offset       = $ihfath["week_offset"];
$week_offset_array = $ihfath["week_offset_array"];

$week_offset_array_days = array_map(function($i_d) {
  return (int) date("j", $i_d);
}, $week_offset_array);

$lessons_completed = $ihfath["lessons_completed"];

// Utils
function jst(...$args) {
  return json_encode(t(...$args));
}

function jse(...$args) {
  return json_encode(...$args);
}
 ?>
<div class="section ihfath-small-section ihfath-professor-completed-status">
  <div class="container">
    <ul>
      <li>
        <b class="ihfath-label"><?= t("Completed") ?></b>
        <span>
          <?php
            $completed = $lessons_completed["completed"];
            $hours = floor($completed / 60);
            $minutes = $completed % 60;

            // TODO: Generalize this into a common function
            echo preg_replace(array(
              '/\[%\]/',
              '/\[\/%\]/',
            ), array(
              '<span class="main-color">',
              '</span>',
            ), t("[%]@hours[/%] hours and [%]@minutes[/%] minutes of lessons", array(
              "@hours" => $hours,
              "@minutes" => $minutes,
            )));
          ?>
        </span>
      </li>
      <li>
        <b class="ihfath-label"><?= t("Absent") ?></b>
        <span>
          <?php
          echo preg_replace(array(
            '/\[%\]/',
            '/\[\/%\]/',
          ), array(
            '<span class="main-color">',
            '</span>',
          ), t("[%]@lessons[/%] lessons", array(
            "@lessons" => $lessons_completed["cancelled"]
          ))); ?>
        </span>
      </li>
      <li>
        <b class="ihfath-label"><?= t("Since") ?></b>
        <span>
          <?= t("@DAY @DATE, @MONTH", array(
                "@DAY" => t(date("l", $lessons_completed["since"])),
                "@DATE" => t(date("j", $lessons_completed["since"])),
                "@MONTH" => t(date("F", $lessons_completed["since"])),
              )) ?>
        </span>
      </li>
    </ul>
  </div>
</div>

<div class="section ihfath-small-section">
  <div class="container">
    <div class="head-7 main-border">
      <h4 class="uppercase bold" style="text-align: right;">
        <span class="main-bg "><i class="fa fa-calendar"></i> <?php echo t("This Week's Schedule"); ?></span>
      </h4>
    </div>

    <div class="ihfath-schedule-navigation">
      <div class="ihfath-schedule-date">
        <?php echo date("M j", $week_offset_array[0]) . " - " . date("M j", $week_offset_array[6]); ?>
      </div>

      <?php
      if ($week_offset > 0) {
        if ($week_offset === 1) {
          $week_arg = "";
        } else {
          $week_arg = "/?week=" . ($week_offset -  1);
        } ?>

        <a href="<?php echo $page_url . $week_arg; ?>" class="ihfath-previous-week main-bg">
          <i class="fa fa-arrow-left"></i> <?php echo t("Previous week"); ?>
        </a>

      <?php
      } ?>

      <a href="<?php echo $page_url . "/?week=" . ($week_offset +  1); ?>" class="ihfath-next-week main-bg">
        <?php echo t("Next week"); ?> <i class="fa fa-arrow-right"></i>
      </a>
    </div>

    <br>

    <!-- Current week's schedule -->
    <div class="ihfath-prof-schedule-widget">
      <div id="ihfath-prof-current-schedule-wrapper">
        <!-- JS populates this -->
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  window.addEventListener("DOMContentLoaded", function() {
    var data = <?php echo $schedule ? json_encode($schedule) : "null"; ?>;
    var weekOffset = <?php echo $week_offset; ?>;

    // Cell state mappings
    var mapStateNames = [
      "<?php echo t("Unavailable"); ?>",
      "<?php echo t("Vacant"); ?>",
      "<?php echo t("Booked"); ?>",
      "<?php echo t("Weekend"); ?>"
    ];

    var possibleStates = [
      0, 1, 3
    ];

    var dayMap = [
      "<?php echo t("Monday"); ?>",
      "<?php echo t("Tuesday"); ?>",
      "<?php echo t("Wednesday"); ?>",
      "<?php echo t("Thursday"); ?>",
      "<?php echo t("Friday"); ?>",
      "<?php echo t("Saturday"); ?>",
      "<?php echo t("Sunday"); ?>"
    ];

    // Invoke schedule
    IhfathCurrentProfSchedule({
      baseLessonUrl: <?= jse(url("koa/lesson/")) ?>,
      data: data,
      weekOffset: weekOffset,
      weekOffsetDays: <?php echo json_encode($week_offset_array_days); ?>,
      dimensions: [
        7, 24
      ],

      time: "<?php echo t("Time") . ":"; ?>",
      dayMap: dayMap,
      mapStateNames: mapStateNames
    });
  });
</script>

<style media="screen">
  /* Professor completed status */
  div.section.ihfath-professor-completed-status {
    padding-bottom: 0;
  }

  .ihfath-professor-completed-status .container li > * {
    font-size: 2.5rem;
  }

  .ihfath-professor-completed-status .container b.ihfath-label {
    display: inline-block;
    vertical-align: center;
    min-width: 150px;
  }

  /* Navigation button styles */
  .ihfath-schedule-navigation > a {
    position: relative;
    z-index: 1;
    display: inline-block;
    padding: 10px 15px;

    font-size: 14px;
    cursor: pointer;
    transition: none;
  }

  .ihfath-next-week {
    float: right;
  }

  .ihfath-schedule-navigation {
    /* prevent undesirable float behaviour */
    overflow: hidden;
    position: relative;
  }

  .ihfath-schedule-date {
    position: absolute;
    left: 0px;
    right: 0px;
    height: 100%;

    line-height: 30px;
    font-size: 20px;
    text-align: center;
  }

  /* Schedule styles */
  /* State styles */

  a.ihfath-cell-past {
    background: #a9bf04 !important;
    cursor: default;
  }

  .ihfath-cell-past::after {
    content: "";
    position: absolute;
    z-index: 10;
    left: -2px;
    top: 0px;
    bottom: 0px;
    right: 0px;
    background: rgba(0, 0, 0, 0.1);

    pointer-events: none;
  }

  .ihfath-cell-past:nth-child(2)::after {
    left: 0px;
  }

  .ihfath-full-cell {
    position: relative;
    padding: 0px !important;
    line-height: 40px;

    color: #fff !important;
    font-size: 16px;
    text-shadow: 1px 1px 1px rgba(0,0,0,.15);
  }

  div.ihfath-full-cell.ihfath-unclickable {
    pointer-events: none;
  }

  .ihfath-full-cell.ihfath-cell-size-one-n-half {
    line-height: 60px !important;
  }

  .ihfath-full-cell * {
    position: relative;
    z-index: 10;
  }

  .ihfath-full-cell .ihfath-cell-bg {
    position: absolute;
    top: 0px;
    left: 0px;
    right: 0px;
    bottom: 0px;

    background-color: #a9bf04;
    transition: background 0.35s ease;
  }

  .ihfath-full-cell.ihfath-unclickable-before .ihfath-cell-bg {
    background: #A62451;
  }

  .ihfath-full-cell.ihfath-unclickable-after .ihfath-cell-bg {
    background: #04BF9D;
  }

  .ihfath-full-cell:hover .ihfath-cell-bg {
    background-color: #8ba100;
  }

  .ihfath-full-cell.ihfath-cell-size-one-n-half .ihfath-cell-bg {
    bottom: 25%;
  }

  .ihfath-cell-contents {
    display: inline-block;
    vertical-align: middle;
    padding: 0px 5px;

    line-height: 16px;
  }

  /* Base styles */
  #ihfath-prof-current-schedule-wrapper .cellidi-cell:not(.main-bg) {
    background: #fff;
  }

  #ihfath-prof-current-schedule-wrapper .cellidi-cell {
    position: relative;
    border-right: 2px solid #fff;
  }

  #ihfath-prof-current-schedule-wrapper .cellidi-cell.ihfath-empty-cell,
  #ihfath-prof-current-schedule-wrapper .cellidi-cell.ihfath-full-cell {
    font-weight: bold;
  }

  #ihfath-prof-current-schedule-wrapper .cellidi-cell.ihfath-empty-cell span,
  #ihfath-prof-current-schedule-wrapper .cellidi-cell.ihfath-full-cell span {
    font-weight: normal;
    font-size: 0.75em;
  }

  #ihfath-prof-current-schedule-wrapper .cellidi-cell.ihfath-empty-cell {
    color: #B3B3B3;
  }

  #ihfath-prof-current-schedule-wrapper .cellidi-dead-cell {
    /*opacity: 0.35;*/
    color: rgba(102, 102, 102, 0.35) !important;
  }

  #ihfath-prof-current-schedule-wrapper .cellidi-dead-cell span {
    opacity: 0.25;
  }


  #ihfath-prof-current-schedule-wrapper .cellidi-cell.ihfath-cell-checker:not(.main-bg) {
    background: #E6E6E6;
  }

  #ihfath-prof-current-schedule-wrapper .cellidi-dead-cell.cellidi-cell.ihfath-cell-checker:not(.main-bg) {
    background: rgba(230, 230, 230, 0.35);
  }

  #ihfath-prof-current-schedule-wrapper .cellidi-header-cell:not(.main-bg) {
    background: rgba(0, 0, 0, 0.025);
  }

  #ihfath-prof-current-schedule-wrapper .cellidi-header-cell {
    border-color: rgba(0, 0, 0, 0.035);
  }

  #ihfath-prof-current-schedule-wrapper .cellidi-header-cell span {
    font-weight: normal !important;
  }

  #ihfath-prof-current-schedule-wrapper .cellidi-header-cell.cellidi-corner-cell-top-left {
    background: rgba(0, 0, 0, 0.055);
    border-width: 0px;
    /*border-right-width: 1px;
    border-right-style: solid;*/
  }

  #ihfath-prof-current-schedule-wrapper .cellidi-header-cell-left {
    border-right: 2px solid #fff;
    background: #fff;
  }

  #ihfath-prof-current-schedule-wrapper .cellidi-header-cell-left.ihfath-cell-checker {
    background: #E6E6E6;
  }

  #ihfath-prof-current-schedule-wrapper .cellidi-header-cell-left span {
    font-weight: normal;
    font-size: 0.75em;
  }


</style>
