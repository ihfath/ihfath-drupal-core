import resolve from 'rollup-plugin-node-resolve';
import typescript from "rollup-plugin-typescript";
import svelte from 'rollup-plugin-svelte';
import scss from "rollup-plugin-scss";
import sass from '@nsivertsen/svelte-preprocess-sass';

export default {
  input: "js/main.js",
  output: {
    sourcemap: true,
    format: "iife",
    // name: "course_admin",
    file: "../lessondetails.js",
    globals: {
      jshorts: "jSh",
    },
  },
  external: [
    "jshorts",
  ],
  plugins: [
    resolve(),
    scss({
      output: "../lessondetails.css",
    }),
    typescript({
      // ...
    }),
    svelte({
      preprocess: {
        style: sass(),
      },

      dev: false,
      emitCss: true,
    }),
  ],
};
