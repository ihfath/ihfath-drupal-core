// import { Store } from "svelte/store";

export interface BaseEvents<State> {
  state: {
    changed: {
      [K in keyof State]: boolean;
    },
    current: State,
    previous: State,
  };
}

type SvelteStore = Store;
declare class BaseComponent<State = {}, Events = {}, Store = SvelteStore> {
  store?: Store;

  constructor(opts: {
    target: HTMLElement;
    store?: Store;
    data?: {
      [K in keyof State]?: State[K];
    };
  });

  get(): {
    [K in keyof State]: State[K];
  }

  set(partialState: {
    [K in keyof State]?: State[K];
  }): void;

  fire<K extends keyof Events>(event: K, data?: Events[K]): void;
  on<K extends keyof (Events & BaseEvents<State>)>(event: K, cb: (this: BaseComponent<State, Events, Store>, data?: (Events & BaseEvents<State>)[K]) => void): void;
}

export declare class Store<State = {}, Events = {}> {
  constructor(state: State);

  get(): {
    [K in keyof State]: State[K];
  }

  set(partialState: {
    [K in keyof State]?: State[K];
  }): void;

  fire<K extends keyof Events>(event: K, data?: Events[K]): void;
  on<K extends keyof (Events & BaseEvents<State>)>(event: K, cb: (this: Store<State, Events>, data?: (Events & BaseEvents<State>)[K]) => void): void;
}

export default BaseComponent;
