import { LessonNote } from "./lesson-notes";
import { Attachments } from "./file-attachments";
import { WhiteboardBooks } from "./whiteboard-books";
import { clockSync } from "./clock";
import "../css/lessondetails.scss";

window.IhfathClockSync = clockSync;
window.IhfathLessonNoteController = LessonNote;
window.IhfathAttachments = Attachments;
window.IhfathWhiteboardBooks = WhiteboardBooks;
