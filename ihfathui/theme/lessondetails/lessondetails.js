(function (jSh$1) {
  'use strict';

  jSh$1 = jSh$1 && jSh$1.hasOwnProperty('default') ? jSh$1['default'] : jSh$1;

  // @ts-check
  function LessonNote(options) {
    var ctrl = lces.new();
    const {
      submit,
      input,
      wrap,
      stars,
    } = options;

    // Setup ctrl
    {
      ctrl.setState("rating", 5);
      ctrl.setState("note", "");
      ctrl.setState("enabled", false);
      ctrl.setState("submitted", false);

      // Add events
      ctrl.addStateListener("enabled", function(enabled) {
        if (enabled) {
          submit.classList.add("main-bg");
        } else {
          submit.classList.remove("main-bg");
        }
      });

      input.addEventListener("input", function() {
        ctrl.enabled = enabled(this);
      });

      submit.addEventListener("click", function() {
        if (ctrl.enabled) {
          input.disabled = true;
          ctrl.submitted = true;
          wrap.classList.add("ihfath-ln-submitted");

          (new lcRequest({
            uri: options.baseURL + "/ihfath/ajax/general/lessonUpdate",
            method: "POST",
            formData: JSON.stringify({
              lid: options.lid,
              rating: ctrl.rating,
              note_body: input.value.trim()
            }),
            success: function() {
              console.log("UPDATE NOTES", this.responseText);
            }
          })).send();
        }
      });

      function enabled(input) {
        return input.value.trim().length >= options.minNoteChar;
      }

      // Check if enabled
      ctrl.enabled = enabled(input);
    }

    // Star logic
    var rating = 0;
    var visual = 5;

    stars.forEach(function(star, i) {
      var starValue = i + 1;

      star.addEventListener("mousedown", function() {
        if (ctrl.submitted) {
          return false;
        }

        rating = starValue;

        renderStars(rating);
      });

      star.addEventListener("mouseover", function() {
        if (rating !== 0) {
          return false;
        }

        visual = starValue;
        renderStars(starValue);
      });
    });

    ctrl.addStateListener("submitted", function(submitted) {
      if (submitted) {
        rating = visual;
        renderStars(rating);
      }
    });

    function renderStars(rating) {
      // Update controller
      ctrl.rating = rating--;

      // Render update to stars
      stars.forEach(function(star, i_s) {
        if (i_s > rating) {
          star.classList.remove("main-color");
        } else {
          star.classList.add("main-color");
        }
      });
    }

    return ctrl;
  }

  // @ts-check
  function Attachments(args) {
    const {
      attachBasePath,
      attachWrap,
      form,
      formSubmit,
      formWrap,
      baseUrl,
      lessonId,
      localeUploading,
    } = args;

    form.addEventListener("submit", function submitCallback(e) {
      e.preventDefault();

      var data = new FormData(form);

      formSubmit.value = localeUploading;
      formSubmit.disabled = true;

      // Request
      var req = new lcRequest({
        method: "POST",
        uri: window.location.pathname,
        formData: data,
        success: function() {
          var output = jSh.parseJSON(this.responseText);

          if (!output.error) {
            var newAttachments = attachments(output.attachments);

            attachWrap.innerHTML = "";
            attachWrap.appendChild(newAttachments);

            formWrap.innerHTML = output.form;
            IhfathmsgRenewFileInputs();

            form = formWrap.jSh(0);
            form.addEventListener("submit", submitCallback);

            // Fix submit button's classnames
            formSubmit = jSh("#edit-submit");
            formSubmit.className = "btn main-bg btn-md form-submit btn-round";
          }
        }
      });

      req.send();
    });

    function attachments(attachGroups) {
      var itemsMapped = [];

      attachGroups.forEach(function(group) {
        group.attachments.forEach(function(item) {
          itemsMapped.push(jSh.c("a", {
            sel: ".ihfathmsg-msg-file-anchor",
            prop: {
              title: item.file_name,
              href: attachBasePath + item.file_hash + "/" + item.file_name,
            },
            child: [
              jSh.c("span", ".ihfathmsg-msg-file-name", item.file_name),
              jSh.c("span", ".ihfathmsg-msg-file-size", item.file_size),
            ],
          }));
        });
      });

      return itemsMapped;
    }
  }

  // @ts-check

  function WhiteboardBooks(args) {
    const {
      mainNav,
      wboard,
      wboardFrame,
      viewerRoot,
      btnBooks,
      btnFullscreen,
      locale,
    } = args;
    const btnBooksIcon = btnBooks.querySelector(".fa");
    const btnFullscreenIcon = btnFullscreen.querySelector(".fa");
    const btnFullscreenMsg = btnFullscreen.querySelector("span");

    let wboardHeight = 0;
    let isFullscreen = false;
    let isBookvisible = false;

    // Create viewer
    const viewer = ihfathBookViewer({
      root: args.viewerRoot,
      pageBase: args.pageBase,
      useOverlay: false,
      headerHeight: 90,
      showClose: true,
    });

    viewer.onclose(() => {
      setBooksvisible(false);
    });

    viewer.onmove(({ moving }) => {
      if (moving) {
        wboardFrame.style.pointerEvents = "none";
      } else {
        wboardFrame.style.pointerEvents = "all";
      }
    });

    viewer.setBooklist(args.bookList);

    const books = {

    };
    function loadBook({ id }) {
      if (books[id]) {
        viewer.open(books[id]);
      } else {
        fetch(args.bookApi, {
          method: "POST",
          body: JSON.stringify({
            id,
          }),
        }).then(async (response) => {
          if (response.ok) {
            const data = jSh.parseJSON(await response.text());

            if (!data.error) {
              viewer.open(books[id] = data.data.book);
            }
          }
        });
      }
    }

    viewer.onrequest(loadBook);

    viewer.onpage((pageNum, bookId) => {
      fetch(args.bookPageApi, {
        method: "POST",
        body: JSON.stringify({
          id: bookId,
          page: pageNum
        }),
      }).then(async (response) => {
        if (response.ok) {
          const data = jSh.parseJSON(await response.text());

          if (!data.error) {
            viewer.openPage(pageNum, data.data.hash);
          }
        }
      });
    });
    // Load first book

    if (args.bookList.length) {
      loadBook({
        id: args.bookList[0].id,
      });
    }

    function styles(elm, styles) {
      // @ts-ignore
      Object.assign(elm.style, styles);
    }
    function classes(elm, classes, remove = false) {
      const names = classes.split(/\.+/g);

      if (remove) {
        for (const name of names) {
          if (name) {
            elm.classList.remove(name);
          }
        }
      } else {
        for (const name of names) {
          if (name) {
            elm.classList.add(name);
          }
        }
      }
    }

    function animateFullscreen(toggle, book = false, isFull = false) {
      const dur = 260;
      const rect = (toggle ? wboard : wboard.parentNode).getBoundingClientRect();
      const fullStyles = {
        left: "0px",
        right: book ? "33.333%" : "0px",
        top: "0px",
        bottom: "0px",
      };
      const smallStyles = {
        left: rect.left + "px",
        right: (innerWidth - rect.right) + "px",
        top: rect.top + "px",
        bottom: (innerHeight - rect.bottom) + "px",
      };

      console.log(smallStyles);

      // Start fixed
      if (toggle) {
        classes(wboard, ".ihf-whiteboard-fullscreen");
        styles(wboard, {
          width: "auto",
        });
      }

      // Get into start position
      styles(wboard, toggle && !isFull
                       ? smallStyles
                       : (isFull
                          ? smallStyles
                          : fullStyles));

      // Start animation
      requestAnimationFrame(() => {
        classes(wboard, ".ihf-whiteboard-animated");
        styles(wboard, toggle ? fullStyles : smallStyles);

        // End animation
        setTimeout(() => {
          classes(wboard, ".ihf-whiteboard-animated", true);

          // Unfix
          if (!toggle) {
            classes(wboard, ".ihf-whiteboard-fullscreen", true);
            styles(wboard, fullStyles);
            styles(wboard, {
              width: "100%",
            });
          }
        }, dur);
      });
    }

    function animateBook(toggle) {
      const dur = 260;
      const openStyle = {
        right: "0",
      };
      const closedStyle = {
        right: "-33.333%",
      };

      // Starter position
      styles(viewerRoot, toggle ? closedStyle : openStyle);

      if (toggle) {
        classes(viewerRoot, ".ihf-visible");
      }

      requestAnimationFrame(() => {
        classes(viewerRoot, ".ihf-book-animated");
        styles(viewerRoot, toggle ? openStyle : closedStyle);

        setTimeout(() => {
          classes(viewerRoot, ".ihf-book-animated", true);

          if (!toggle) {
            classes(viewerRoot, ".ihf-visible", true);
          }
        }, dur);
      });
    }

    function setFullscreen(toggle, books = null) {
      if (books === null) {
        books = isBookvisible;
      }

      if (toggle) {
        btnFullscreenIcon.className = "fa fa-compress";
        btnFullscreenMsg.textContent = locale.minimize;

        // Fill up empty space
        const rect = wboard.getBoundingClientRect();
        wboardHeight = rect.bottom - rect.top;
        wboard.parentNode.style.height = wboardHeight + "px";

        // Hide header/navigation
        mainNav.style.display = "none";

        // hide scrollbar
        document.body.style.overflow = "hidden";

        // Start animation
        animateFullscreen(toggle, books, isFullscreen);
      } else {
        setBooksvisible(false, true);
        btnFullscreenIcon.className = "fa fa-expand";
        btnFullscreenMsg.textContent = locale.fullscreen;

        // Show header/navigation
        mainNav.style.display = "block";

        // Show scrollbar
        document.body.style.overflow = "visible";

        // Start animation
        animateFullscreen(toggle, books);
      }

      isFullscreen = toggle;
    }

    function setBooksvisible(toggle, fullScreenToggled = false) {
      if (toggle) {
        setFullscreen(true, true);
        animateBook(toggle);
      } else {
        if (!fullScreenToggled) {
          setFullscreen(true, false);
        }

        animateBook(toggle);
      }

      isBookvisible = toggle;
    }

    btnBooks.addEventListener("click", function() {
      setBooksvisible(!isBookvisible);
    });

    btnFullscreen.addEventListener("click", function() {
      setFullscreen(!isFullscreen);
    });
  }

  function noop() {}

  function assign(tar, src) {
  	for (var k in src) tar[k] = src[k];
  	return tar;
  }

  function append(target, node) {
  	target.appendChild(node);
  }

  function insert(target, node, anchor) {
  	target.insertBefore(node, anchor);
  }

  function detachNode(node) {
  	node.parentNode.removeChild(node);
  }

  function createElement(name) {
  	return document.createElement(name);
  }

  function createText(data) {
  	return document.createTextNode(data);
  }

  function addListener(node, event, handler, options) {
  	node.addEventListener(event, handler, options);
  }

  function removeListener(node, event, handler, options) {
  	node.removeEventListener(event, handler, options);
  }

  function setAttribute(node, attribute, value) {
  	if (value == null) node.removeAttribute(attribute);
  	else node.setAttribute(attribute, value);
  }

  function setData(text, data) {
  	text.data = '' + data;
  }

  function toggleClass(element, name, toggle) {
  	element.classList[toggle ? 'add' : 'remove'](name);
  }

  function blankObject() {
  	return Object.create(null);
  }

  function destroy(detach) {
  	this.destroy = noop;
  	this.fire('destroy');
  	this.set = noop;

  	this._fragment.d(detach !== false);
  	this._fragment = null;
  	this._state = {};
  }

  function _differs(a, b) {
  	return a != a ? b == b : a !== b || ((a && typeof a === 'object') || typeof a === 'function');
  }

  function _differsImmutable(a, b) {
  	return a != a ? b == b : a !== b;
  }

  function fire(eventName, data) {
  	var handlers =
  		eventName in this._handlers && this._handlers[eventName].slice();
  	if (!handlers) return;

  	for (var i = 0; i < handlers.length; i += 1) {
  		var handler = handlers[i];

  		if (!handler.__calling) {
  			try {
  				handler.__calling = true;
  				handler.call(this, data);
  			} finally {
  				handler.__calling = false;
  			}
  		}
  	}
  }

  function flush(component) {
  	component._lock = true;
  	callAll(component._beforecreate);
  	callAll(component._oncreate);
  	callAll(component._aftercreate);
  	component._lock = false;
  }

  function get() {
  	return this._state;
  }

  function init(component, options) {
  	component._handlers = blankObject();
  	component._slots = blankObject();
  	component._bind = options._bind;
  	component._staged = {};

  	component.options = options;
  	component.root = options.root || component;
  	component.store = options.store || component.root.store;

  	if (!options.root) {
  		component._beforecreate = [];
  		component._oncreate = [];
  		component._aftercreate = [];
  	}
  }

  function on(eventName, handler) {
  	var handlers = this._handlers[eventName] || (this._handlers[eventName] = []);
  	handlers.push(handler);

  	return {
  		cancel: function() {
  			var index = handlers.indexOf(handler);
  			if (~index) handlers.splice(index, 1);
  		}
  	};
  }

  function set(newState) {
  	this._set(assign({}, newState));
  	if (this.root._lock) return;
  	flush(this.root);
  }

  function _set(newState) {
  	var oldState = this._state,
  		changed = {},
  		dirty = false;

  	newState = assign(this._staged, newState);
  	this._staged = {};

  	for (var key in newState) {
  		if (this._differs(newState[key], oldState[key])) changed[key] = dirty = true;
  	}
  	if (!dirty) return;

  	this._state = assign(assign({}, oldState), newState);
  	this._recompute(changed, this._state);
  	if (this._bind) this._bind(changed, this._state);

  	if (this._fragment) {
  		this.fire("state", { changed: changed, current: this._state, previous: oldState });
  		this._fragment.p(changed, this._state);
  		this.fire("update", { changed: changed, current: this._state, previous: oldState });
  	}
  }

  function _stage(newState) {
  	assign(this._staged, newState);
  }

  function callAll(fns) {
  	while (fns && fns.length) fns.shift()();
  }

  function _mount(target, anchor) {
  	this._fragment[this._fragment.i ? 'i' : 'm'](target, anchor || null);
  }

  function removeFromStore() {
  	this.store._remove(this);
  }

  var proto = {
  	destroy,
  	get,
  	fire,
  	on,
  	set,
  	_recompute: noop,
  	_set,
  	_stage,
  	_mount,
  	_differs
  };

  // import { Store } from "svelte/store";

  function submit(evt) {
      const form = evt.target;
  }
  function setActionUrl(evt) {
      const { rescheduleUrl, appendUrl } = this.get();
      const btn = evt.target;
      let actionUrl = "";
      if (btn.name === "reschedule") {
          actionUrl = rescheduleUrl;
      }
      else {
          actionUrl = appendUrl;
      }
      this.set({
          actionUrl,
      });
  }

  /* js/components/lesson-expired/LessonExpired.html generated by Svelte v2.16.0 */

  function data() {
    return {
      active: false,
      role: 3,
      backUrl: "",
      rescheduleUrl: "",
      appendUrl: "",

      // Form-specific state
      actionUrl: "",

      formBuildId: "",
      formToken: "",
      formId: "",
    };
  }
  var methods = {
    submit,
    setActionUrl,
  };

  function create_main_fragment(component, ctx) {
  	var div0, text, div1;

  	function select_block_type(ctx) {
  		if (ctx.role === 2) return create_if_block;
  		return create_else_block;
  	}

  	var current_block_type = select_block_type(ctx);
  	var if_block = current_block_type(component, ctx);

  	return {
  		c() {
  			div0 = createElement("div");
  			text = createText("\n");
  			div1 = createElement("div");
  			if_block.c();
  			div0.className = "ihfath-lesson-expired-overlay svelte-cnod2q";
  			toggleClass(div0, "ihfath-is-visible", ctx.active);
  			div1.className = "ihfath-lesson-expired-wrap svelte-cnod2q";
  			toggleClass(div1, "ihfath-is-visible", ctx.active);
  		},

  		m(target, anchor) {
  			insert(target, div0, anchor);
  			insert(target, text, anchor);
  			insert(target, div1, anchor);
  			if_block.m(div1, null);
  		},

  		p(changed, ctx) {
  			if (changed.active) {
  				toggleClass(div0, "ihfath-is-visible", ctx.active);
  			}

  			if (current_block_type === (current_block_type = select_block_type(ctx)) && if_block) {
  				if_block.p(changed, ctx);
  			} else {
  				if_block.d(1);
  				if_block = current_block_type(component, ctx);
  				if_block.c();
  				if_block.m(div1, null);
  			}

  			if (changed.active) {
  				toggleClass(div1, "ihfath-is-visible", ctx.active);
  			}
  		},

  		d(detach) {
  			if (detach) {
  				detachNode(div0);
  				detachNode(text);
  				detachNode(div1);
  			}

  			if_block.d();
  		}
  	};
  }

  // (19:2) {:else}
  function create_else_block(component, ctx) {
  	var h1, text0_value = ctx.$locale.lessonExpireMessageStudent, text0, text1, form, textarea, text2, div, button0, text3_value = ctx.$locale.lessonExpireRescheduleBtn, text3, text4, button1, text5_value = ctx.$locale.lessonExpireAppendBtn, text5, text6, input0, text7, input1, text8, input2;

  	function click_handler(event) {
  		component.setActionUrl(event);
  	}

  	function click_handler_1(event) {
  		component.setActionUrl(event);
  	}

  	function submit_handler(event) {
  		component.submit(event);
  	}

  	return {
  		c() {
  			h1 = createElement("h1");
  			text0 = createText(text0_value);
  			text1 = createText("\n\n    ");
  			form = createElement("form");
  			textarea = createElement("textarea");
  			text2 = createText("\n\n      ");
  			div = createElement("div");
  			button0 = createElement("button");
  			text3 = createText(text3_value);
  			text4 = createText("\n        ");
  			button1 = createElement("button");
  			text5 = createText(text5_value);
  			text6 = createText("\n\n      ");
  			input0 = createElement("input");
  			text7 = createText("\n      ");
  			input1 = createElement("input");
  			text8 = createText("\n      ");
  			input2 = createElement("input");
  			h1.className = "title svelte-cnod2q";
  			textarea.name = "message";
  			textarea.placeholder = "Leave a comment...";
  			textarea.className = "ihfath-lesson-expired-message svelte-cnod2q";
  			addListener(button0, "click", click_handler);
  			button0.type = "submit";
  			button0.name = "reschedule";
  			button0.className = "cws-button btn btn-xl btn-3d btn-round btn-default svelte-cnod2q";
  			setAttribute(button0, "href", ctx.rescheduleUrl);
  			addListener(button1, "click", click_handler_1);
  			button1.type = "submit";
  			button1.name = "append";
  			button1.className = "cws-button btn btn-xl btn-3d btn-round btn-default svelte-cnod2q";
  			setAttribute(button1, "href", ctx.appendUrl);
  			div.className = "ihfath-le-buttons svelte-cnod2q";
  			setAttribute(input0, "type", "hidden");
  			input0.name = "form_build_id";
  			input0.value = ctx.formBuildId;
  			setAttribute(input1, "type", "hidden");
  			input1.name = "form_token";
  			input1.value = ctx.formToken;
  			setAttribute(input2, "type", "hidden");
  			input2.name = "form_id";
  			input2.value = ctx.formId;
  			addListener(form, "submit", submit_handler);
  			form.action = ctx.actionUrl;
  			form.method = "POST";
  		},

  		m(target, anchor) {
  			insert(target, h1, anchor);
  			append(h1, text0);
  			insert(target, text1, anchor);
  			insert(target, form, anchor);
  			append(form, textarea);
  			append(form, text2);
  			append(form, div);
  			append(div, button0);
  			append(button0, text3);
  			append(div, text4);
  			append(div, button1);
  			append(button1, text5);
  			append(form, text6);
  			append(form, input0);
  			append(form, text7);
  			append(form, input1);
  			append(form, text8);
  			append(form, input2);
  			component.refs.form = form;
  		},

  		p(changed, ctx) {
  			if ((changed.$locale) && text0_value !== (text0_value = ctx.$locale.lessonExpireMessageStudent)) {
  				setData(text0, text0_value);
  			}

  			if ((changed.$locale) && text3_value !== (text3_value = ctx.$locale.lessonExpireRescheduleBtn)) {
  				setData(text3, text3_value);
  			}

  			if (changed.rescheduleUrl) {
  				setAttribute(button0, "href", ctx.rescheduleUrl);
  			}

  			if ((changed.$locale) && text5_value !== (text5_value = ctx.$locale.lessonExpireAppendBtn)) {
  				setData(text5, text5_value);
  			}

  			if (changed.appendUrl) {
  				setAttribute(button1, "href", ctx.appendUrl);
  			}

  			if (changed.formBuildId) {
  				input0.value = ctx.formBuildId;
  			}

  			if (changed.formToken) {
  				input1.value = ctx.formToken;
  			}

  			if (changed.formId) {
  				input2.value = ctx.formId;
  			}

  			if (changed.actionUrl) {
  				form.action = ctx.actionUrl;
  			}
  		},

  		d(detach) {
  			if (detach) {
  				detachNode(h1);
  				detachNode(text1);
  				detachNode(form);
  			}

  			removeListener(button0, "click", click_handler);
  			removeListener(button1, "click", click_handler_1);
  			removeListener(form, "submit", submit_handler);
  			if (component.refs.form === form) component.refs.form = null;
  		}
  	};
  }

  // (8:2) {#if role === 2}
  function create_if_block(component, ctx) {
  	var h1, text0_value = ctx.$locale.lessonExpireMessageProfessor, text0, text1, a, text2_value = ctx.$locale.lessonExpireBackHomeBtn, text2;

  	return {
  		c() {
  			h1 = createElement("h1");
  			text0 = createText(text0_value);
  			text1 = createText("\n    ");
  			a = createElement("a");
  			text2 = createText(text2_value);
  			h1.className = "title svelte-cnod2q";
  			a.className = "cws-button btn btn-xl btn-3d btn-round btn-default svelte-cnod2q";
  			a.href = ctx.backUrl;
  		},

  		m(target, anchor) {
  			insert(target, h1, anchor);
  			append(h1, text0);
  			insert(target, text1, anchor);
  			insert(target, a, anchor);
  			append(a, text2);
  		},

  		p(changed, ctx) {
  			if ((changed.$locale) && text0_value !== (text0_value = ctx.$locale.lessonExpireMessageProfessor)) {
  				setData(text0, text0_value);
  			}

  			if ((changed.$locale) && text2_value !== (text2_value = ctx.$locale.lessonExpireBackHomeBtn)) {
  				setData(text2, text2_value);
  			}

  			if (changed.backUrl) {
  				a.href = ctx.backUrl;
  			}
  		},

  		d(detach) {
  			if (detach) {
  				detachNode(h1);
  				detachNode(text1);
  				detachNode(a);
  			}
  		}
  	};
  }

  function LessonExpired$1(options) {
  	init(this, options);
  	this.refs = {};
  	this._state = assign(assign(this.store._init(["locale"]), data()), options.data);
  	this.store._add(this, ["locale"]);
  	this._intro = true;

  	this._handlers.destroy = [removeFromStore];

  	this._fragment = create_main_fragment(this, this._state);

  	if (options.target) {
  		this._fragment.c();
  		this._mount(options.target, options.anchor);
  	}
  }

  assign(LessonExpired$1.prototype, proto);
  assign(LessonExpired$1.prototype, methods);

  function Store(state, options) {
  	this._handlers = {};
  	this._dependents = [];

  	this._computed = blankObject();
  	this._sortedComputedProperties = [];

  	this._state = assign({}, state);
  	this._differs = options && options.immutable ? _differsImmutable : _differs;
  }

  assign(Store.prototype, {
  	_add(component, props) {
  		this._dependents.push({
  			component: component,
  			props: props
  		});
  	},

  	_init(props) {
  		const state = {};
  		for (let i = 0; i < props.length; i += 1) {
  			const prop = props[i];
  			state['$' + prop] = this._state[prop];
  		}
  		return state;
  	},

  	_remove(component) {
  		let i = this._dependents.length;
  		while (i--) {
  			if (this._dependents[i].component === component) {
  				this._dependents.splice(i, 1);
  				return;
  			}
  		}
  	},

  	_set(newState, changed) {
  		const previous = this._state;
  		this._state = assign(assign({}, previous), newState);

  		for (let i = 0; i < this._sortedComputedProperties.length; i += 1) {
  			this._sortedComputedProperties[i].update(this._state, changed);
  		}

  		this.fire('state', {
  			changed,
  			previous,
  			current: this._state
  		});

  		this._dependents
  			.filter(dependent => {
  				const componentState = {};
  				let dirty = false;

  				for (let j = 0; j < dependent.props.length; j += 1) {
  					const prop = dependent.props[j];
  					if (prop in changed) {
  						componentState['$' + prop] = this._state[prop];
  						dirty = true;
  					}
  				}

  				if (dirty) {
  					dependent.component._stage(componentState);
  					return true;
  				}
  			})
  			.forEach(dependent => {
  				dependent.component.set({});
  			});

  		this.fire('update', {
  			changed,
  			previous,
  			current: this._state
  		});
  	},

  	_sortComputedProperties() {
  		const computed = this._computed;
  		const sorted = this._sortedComputedProperties = [];
  		const visited = blankObject();
  		let currentKey;

  		function visit(key) {
  			const c = computed[key];

  			if (c) {
  				c.deps.forEach(dep => {
  					if (dep === currentKey) {
  						throw new Error(`Cyclical dependency detected between ${dep} <-> ${key}`);
  					}

  					visit(dep);
  				});

  				if (!visited[key]) {
  					visited[key] = true;
  					sorted.push(c);
  				}
  			}
  		}

  		for (const key in this._computed) {
  			visit(currentKey = key);
  		}
  	},

  	compute(key, deps, fn) {
  		let value;

  		const c = {
  			deps,
  			update: (state, changed, dirty) => {
  				const values = deps.map(dep => {
  					if (dep in changed) dirty = true;
  					return state[dep];
  				});

  				if (dirty) {
  					const newValue = fn.apply(null, values);
  					if (this._differs(newValue, value)) {
  						value = newValue;
  						changed[key] = true;
  						state[key] = value;
  					}
  				}
  			}
  		};

  		this._computed[key] = c;
  		this._sortComputedProperties();

  		const state = assign({}, this._state);
  		const changed = {};
  		c.update(state, changed, true);
  		this._set(state, changed);
  	},

  	fire,

  	get,

  	on,

  	set(newState) {
  		const oldState = this._state;
  		const changed = this._changed = {};
  		let dirty = false;

  		for (const key in newState) {
  			if (this._computed[key]) throw new Error(`'${key}' is a read-only computed property`);
  			if (this._differs(newState[key], oldState[key])) changed[key] = dirty = true;
  		}
  		if (!dirty) return;

  		this._set(newState, changed);
  	}
  });

  // Constants
  let tickDelay = 600;
  let beaconDelay = 1000 * 30; // 30 seconds
  let apiUrl;
  let locale;
  var Role;
  (function (Role) {
      Role[Role["Professor"] = 2] = "Professor";
      Role[Role["Student"] = 3] = "Student";
  })(Role || (Role = {}));
  class ClockDom {
      constructor(anchor, opposingRole) {
          this.anchor = anchor;
          this.opposingRole = opposingRole;
          this.state = {
              time: 0,
              startTime: 0,
              endTime: 0,
              duration: 0,
              cancelled: false,
              cancellable: false,
              partyOnline: false,
              finished: false,
              role: null,
          };
          this.root = anchor.parentNode;
          this.build();
      }
      build() {
          // Ihfath Lesson Details Clock UI
          const time = jSh$1.d(".ihfath-ldc-elapsed-time.ihfath-ldc-label");
          const timeInLesson = jSh$1.d(".ihfath-ldc-elapsed-time-label.ihfath-ldc-value", locale.inLesson);
          const timeCol = jSh$1.d(".ihfath-ldc-col.ihfath-ldc-col-time", null, [
              time,
              timeInLesson,
          ]);
          const opposingName = jSh$1.d(".ihfath-ldc-opposing-name.ihfath-ldc-label", this.opposingRole);
          const onlineStatus = jSh$1.d(".ihfath-ldc-opposing-online.ihfath-ldc-value", locale.online);
          const opposingCol = jSh$1.d(".ihfath-ldc-col.ihfath-ldc-col-opposing", null, [
              opposingName,
              onlineStatus,
          ]);
          const cancelCol = jSh$1.c("a", {
              sel: ".ihfath-ldc-col.ihfath-ldc-col-cancel",
              child: [
                  jSh$1.c("i", ".fa.fa-times"),
                  jSh$1.c("span", null, locale.actionCancel),
              ],
              prop: {
                  title: locale.actionCancelDesc,
              },
          });
          const finishedLabel = jSh$1.d(".ihfath-ldc-finished", locale.cancelled);
          // UI root node
          const main = jSh$1.d({
              sel: ".ihfath-ldc-clock",
              child: [
                  timeCol,
                  opposingCol,
                  cancelCol,
                  finishedLabel,
              ],
          });
          // Display in document
          this.root.insertBefore(main, this.anchor);
          this.root.removeChild(this.anchor);
          // Set update callback
          this._update = () => {
              const state = this.state;
              // FIXME: Intersecting states should be consolidated
              if (state.cancelled || state.cancellable || state.time > state.duration) {
                  if (cancelled || state.cancellable) {
                      main.classList.add("ihfath-ldc-state-cancelled");
                  }
                  else {
                      finishedLabel.textContent = locale.finished;
                  }
                  if (!main.classList.contains("ihfath-ldc-state-finished")) {
                      main.classList.add("ihfath-ldc-state-finished");
                      main.classList.remove("ihfath-ldc-state-cancellable");
                  }
              }
              else {
                  main.classList.remove("ihfath-ldc-state-finished");
                  if (state.partyOnline) {
                      onlineStatus.textContent = locale.online;
                  }
                  else {
                      onlineStatus.textContent = locale.offline;
                  }
                  if (state.role === Role.Student && state.cancellable) {
                      main.classList.add("ihfath-ldc-state-cancellable");
                  }
                  else {
                      main.classList.remove("ihfath-ldc-state-cancellable");
                  }
                  const relativeTime = state.time;
                  const absRelativeTime = Math.abs(state.time);
                  if (relativeTime < 0) {
                      timeInLesson.textContent = locale.beforeLesson;
                  }
                  else if (state.time < state.duration) {
                      timeInLesson.textContent = locale.inLesson;
                  }
                  else {
                      // Lesson's over
                      time.textContent = "N/A";
                      timeInLesson.textContent = locale.finished;
                      return;
                  }
                  const hourMs = 1000 * 60 * 60;
                  const minMs = 1000 * 60;
                  const secMs = 1000;
                  const hours = Math.floor(absRelativeTime / hourMs);
                  const mins = Math.floor((absRelativeTime - (hourMs * hours)) / minMs);
                  const secs = Math.floor((absRelativeTime - (hourMs * hours) - (minMs * mins)) / secMs);
                  const padd = (v, len = 2) => ("" + v).padStart(len, "0");
                  time.textContent = `${padd(hours)}:${padd(mins)}:${padd(secs)}`;
              }
          };
          this._update();
      }
      set(state) {
          this.state = Object.assign(this.state, state);
          this._update();
      }
  }
  // State
  let cancelled = false;
  let partyOnline = false;
  let cancellable = false;
  let cancelIntent = false;
  let lastBeacon = 0;
  let lessonId = 0;
  let duration = 0; // Hours
  let startTime = 0;
  let time = 0;
  let endTime = 0;
  let expiredLesson = null;
  let domInstances = [];
  let timeout = null;
  function clockSync({ apiUrl: apiUrlArg, locale: localeArg, lessonId: lessonIdArg, startDate: startDateArg, endDate: endDateArg, duration: durationArg, cancelled: cancelledArg, role: roleArg, dom: domArg, lessonExpireWrap, lessonExpireUrls, lessonExpireForm, }) {
      apiUrl = apiUrlArg;
      locale = localeArg;
      startTime = startDateArg;
      endTime = endDateArg;
      lessonId = lessonIdArg;
      duration = durationArg;
      cancelled = cancelledArg;
      timeout = setInterval(tick, tickDelay);
      const roles = {
          2: locale.prof,
          3: locale.student,
      };
      // Create DOM
      for (const domRoot of domArg) {
          const clockDom = new ClockDom(domRoot, roles[5 - roleArg]);
          clockDom.set({
              endTime: endDateArg,
              role: roleArg,
          });
          domInstances.push(clockDom);
      }
      // Create store
      const elStore = new Store({
          locale,
      });
      // Create LessonExpire
      expiredLesson = new LessonExpired$1({
          store: elStore,
          target: lessonExpireWrap,
          data: Object.assign({ active: false, role: roleArg, backUrl: lessonExpireUrls.back, rescheduleUrl: lessonExpireUrls.reschedule, appendUrl: lessonExpireUrls.append }, lessonExpireForm),
      });
      // Call first tick
      tick();
  }
  function tick() {
      if (cancelled) {
          for (const dom of domInstances) {
              dom.set({
                  cancelled,
              });
          }
          expiredLesson.set({
              active: true,
          });
          // Finish
          return clearInterval(timeout);
      }
      expiredLesson.set({
          active: cancellable,
      });
      // Update time
      const curTime = new Date().getTime();
      time = curTime - startTime;
      for (const dom of domInstances) {
          dom.set({
              time,
              partyOnline,
              cancellable,
              duration,
          });
      }
      if (curTime > startTime && curTime < endTime && (curTime - lastBeacon) > beaconDelay) {
          lastBeacon = curTime;
          message(lessonId);
      }
  }
  function message(lessonId) {
      fetch(apiUrl, {
          method: "POST",
          body: JSON.stringify({
              lessonId,
              cancel: cancelIntent,
          }),
      }).then(response => {
          if (response.ok) {
              response.text().then(data => {
                  const parsed = JSON.parse(data);
                  if (parsed.error) {
                      console.error("Failed to send beacon status code: ", parsed.cause);
                  }
                  else {
                      // Response is ok
                      const res = parsed.data;
                      if (res.cancelled) {
                          cancelled = true;
                      }
                      else {
                          partyOnline = res.partyOnline;
                          cancellable = res.cancellable;
                      }
                  }
              });
          }
          else {
              console.error("Failed to send beacon status code: ", response.status);
          }
      });
  }

  window.IhfathClockSync = clockSync;
  window.IhfathLessonNoteController = LessonNote;
  window.IhfathAttachments = Attachments;
  window.IhfathWhiteboardBooks = WhiteboardBooks;

}(jSh));
//# sourceMappingURL=lessondetails.js.map
