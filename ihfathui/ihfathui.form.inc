<?php

// Ihfath UI form callbacks
//
// Prefix (for autocomplete): ihfathui_

function _ihfathui_reportlessonissue_form($form, &$form_state, $is_prof, $lid) {
  $role = ihfathcore_get_role();
  $reasons = array_map(function ($reason) {
    return $reason[1];
  }, Ihfathmsg\Misc::getReportReasons($is_prof ? 2 : 3));

  $form_state["#ihfath"] = array(
    "lid" => $lid,
  );

  return array(
    "subject" => array(
      "#type"        => "select",
      "#title"       => t("Choose the nature of your issue"),
      "#options"     => $reasons,
      // "#description" => t("The underlying cause of your issue")
    ),

    "body" => array(
      "#type"       => "textarea",
      "#title"      => t("Describe your issue"),
      "#row"        => 3,
      "#attributes" => array(
        "class"       => array("ihfath-report-text"),
        "placeholder" => t("Issue description...")
      )
    ),

    "submit" => array(
      "#type" => "submit",
      "#value" => t("Report issue")
    )
  );
}

function _ihfathui_reportlessonissue_form_submit($form, &$form_state) {
  global $user;
  $role = ihfathcore_get_role();

  $reasons = Ihfathmsg\Misc::getReportReasons($role);
  if (is_numeric($form_state["values"]["subject"]) && isset($reasons[$form_state["values"]["subject"]])) {
    if (trim($form_state["values"]["body"]) < 2) {
      Ihfathmsg\Compose::report((int) $form_state["#ihfath"]["lid"], $user->uid, trim($form_state["values"]["body"]), (int) $form_state["values"]["subject"]);

      // Redirect to lesson page
      $form_state["redirect"] = "koa/lesson/" . $form_state["#ihfath"]["lid"];
    } else {
      form_set_error("body", t("Report too short"));
    }
  } else {
    form_set_error("subject", t("Invalid reason"));
  }
}

function _ihfathui_lessonexpired_student_form($form, &$form_state, $lesson) {
  $form["message"] = array(
    "#type" => "textfield",
    "#required" => TRUE,
  );

  $form["submit"] = array(
    "#type" => "submit",
  );

  $form_state["lesson"] = $lesson;

  return $form;
}

function _ihfathui_lessonexpired_student_form_submit($form, &$form_state) {
  $lesson = $form_state["lesson"];

  // TODO: Save message somewhere
  db_update("ihfath_lsn")
    ->fields(array(
      "notes_student" => $form_state["values"]["message"],
    ))
    ->condition("lid", $lesson->lid)
    ->execute();

  // Don't go anywhere
  $form_state["redirect"] = FALSE;
}
