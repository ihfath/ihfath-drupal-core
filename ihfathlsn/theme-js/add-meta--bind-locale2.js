window.addEventListener("DOMContentLoaded", function() {
  // Get DOM elements
  var select    = jSh("#edit-lang");
  var inputName = jSh("#edit-name");
  var inputDesc = jSh("#edit-desc");
  
  var hiddenInputs = jSh(".ihfath-meta-data-hidden");
  var langMap      = {};
  var selectLang   = select.value;
  
  // Map langs to inputs
  for (var i=0; i<hiddenInputs.length; i++) {
    var input     = hiddenInputs[i];
    var inputLang = input.dataset.lang;
    
    switch (input.dataset.meta) {
      case "name":
        (langMap[inputLang] || (langMap[inputLang] = {})).name = input;
        break;
      case "desc":
        (langMap[inputLang] || (langMap[inputLang] = {})).desc = input;
        break;
    }
  }
  
  // Add listeners
  var currentLang = select.value;
  
  select.addEventListener("change", function() {
    currentLang  = this.value;
    var nextLang = langMap[currentLang];
    
    inputName.value = nextLang.name.value;
    inputDesc.value = nextLang.desc.value;
  });
  
  inputName.addEventListener("input", function() {
    langMap[currentLang].name.value = this.value;
  });
  
  inputDesc.addEventListener("input", function() {
    langMap[currentLang].desc.value = this.value;
  });
});
