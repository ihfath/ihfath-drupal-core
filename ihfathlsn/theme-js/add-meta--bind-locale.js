window.addEventListener("DOMContentLoaded", function() {
  // Make classes
  var firstInstance = true;
  
  function IhfathLanguageBind(main, lang, selectLang, hiddenName, hiddenDesc) {
    lces.types.component.call(this);
    
    // Add states/properties
    this.setState("selected", false);
    this.setState("name", hiddenName.value);
    this.setState("desc", hiddenName.value);
    this.lang = lang;
    
    // Add listeners
    this.addStateListener("name", function(name) {
      hiddenName.value = name;
    });
    
    this.addStateListener("desc", function(desc) {
      hiddenDesc.value = desc;
    });
    
    // Done
    main.addMember(this);
    
    if (lang === selectLang) {
      main.lang     = lang;
      this.selected = true;
      
      firstInstance = false;
    }
  }
  
  jSh.inherit(IhfathLanguageBind, lces.types.component);
  
  // Get DOM elements
  var select    = jSh("#edit-lang");
  var inputName = jSh("#edit-name");
  var inputDesc = jSh("#edit-desc");
  
  var langModelMap = {};
  
  select.addEventListener("change", function() {
    console.time("switch-time");
    var lang = this.value;
    
    langModelMap[lang].selected = true;
    console.timeEnd("switch-time");
  });
  
  // Add and bind model
  var mainModel = lces.new("group");
  
  mainModel.lang = null;
  mainModel.setState("selected", false);
  mainModel.setExclusiveState("selected", true, 1);
  mainModel.addExclusiveListener("selected", function(trigger) {
    inputName.value = trigger.name;
    inputDesc.value = trigger.desc;
    
    mainModel.lang = trigger.lang;
  });
  
  // Add DOM element listeners
  inputName.addEventListener("input", function() {
    console.time("input-name-time");
    langModelMap[mainModel.lang].name = this.value;
    console.timeEnd("input-name-time");
  });
  
  inputDesc.addEventListener("input", function() {
    console.time("input-desc-time");
    langModelMap[mainModel.lang].desc = this.value;
    console.timeEnd("input-desc-time");
  });
  
  var hiddenInputs = jSh(".ihfath-meta-data-hidden");
  var tempMap      = {};
  var langs        = [];
  var selectLang   = select.value;
  
  // Map langs to inputs
  for (var i=0; i<hiddenInputs.length; i++) {
    var input     = hiddenInputs[i];
    var inputLang = input.dataset.lang;
    
    switch (input.dataset.meta) {
      case "name":
        (tempMap[inputLang] || (langs.push(inputLang), tempMap[inputLang] = {})).name = input;
        break;
      case "desc":
        (tempMap[inputLang] || (langs.push(inputLang), tempMap[inputLang] = {})).desc = input;
        break;
    }
  }
  
  // Add bindings
  for (var i=0; i<langs.length; i++) {
    var lang     = langs[i];
    var inputMap = tempMap[lang];
    
    langModelMap[lang] = new IhfathLanguageBind(mainModel, lang, selectLang, inputMap.name, inputMap.desc);
  }
});
