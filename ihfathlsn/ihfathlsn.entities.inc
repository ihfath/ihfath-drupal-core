<?php

// Ihfath lesson entity information
//
// Prefix (for autocomplete): ihfathlsn_

function ihfathlsn_entity_info() {
  $entities = array();

  // Course Entities

  $entities["course_category"] = array(
    "label" => t("Course Category"),
    "base table" => "ihfath_lsn_types",
    "load hook" => "course_category_load",
    "uri callback" => "ihfathlsn_course_category_uri",
    "creation callback" => "_ihfathlsn_create_course_category",
    "fieldable" => TRUE,
    "entity keys" => array(
      "id" => "tid",
    ),
    "translation" => array(
      "ihfathlsn" => TRUE,
    ),
    "bundles" => array(
      "course_category" => array(
        "label" => t("Course Category"),
      )
    ),
  );

  $entities["course"] = array(
    "label" => t("Course"),
    "base table" => "ihfath_lsn_programs",
    "load hook" => "course_load",
    "uri callback" => "ihfathlsn_course_category_uri",
    "creation callback" => "_ihfathlsn_create_course",
    "fieldable" => TRUE,
    "entity keys" => array(
      "id" => "pid",
    ),
    "translation" => array(
      "ihfathlsn" => TRUE,
    ),
    "bundles" => array(
      "course" => array(
        "label" => t("Course"),
      )
    ),
  );

  $entities["course_level"] = array(
    "label" => t("Course Level"),
    "base table" => "ihfath_lsn_meta_entities",
    "load hook" => "course_level_load",
    "uri callback" => "ihfathlsn_course_category_uri",
    "fieldable" => TRUE,
    "entity keys" => array(
      "id" => "id",
    ),
    "translation" => array(
      "ihfathlsn" => TRUE,
    ),
    "bundles" => array(
      "course_level" => array(
        "label" => t("Course Level"),
      )
    ),
  );

  $entities["course_faq"] = array(
    "label" => t("Course FAQ"),
    "base table" => "ihfath_lsn_meta_entities",
    "load hook" => "course_faq_load",
    "uri callback" => "ihfathlsn_course_category_uri",
    "fieldable" => TRUE,
    "entity keys" => array(
      "id" => "id",
    ),
    "translation" => array(
      "ihfathlsn" => TRUE,
    ),
    "bundles" => array(
      "course_faq" => array(
        "label" => t("Course FAQ"),
      )
    ),
  );

  // Book entity
  $entities["ihfath_book"] = array(
    "label" => t("Ihfath Book"),
    "base table" => "ihfath_lsn_books",
    "load hook" => "ihfath_book_load",
    "uri callback" => "ihfathlsn_course_category_uri",
    "fieldable" => TRUE,
    "entity keys" => array(
      "id" => "bid",
    ),
    "translation" => array(
      "ihfathlsn" => TRUE,
    ),
    "bundles" => array(
      "ihfath_book" => array(
        "label" => t("Ihfath Book"),
      )
    ),
  );

  // FAQ Entities

  $entities["general_faq"] = array(
    "label" => t("General FAQ"),
    "base table" => "ihfath_lsn_meta_entities",
    "load hook" => "general_faq_load",
    "uri callback" => "ihfathlsn_course_category_uri",
    "fieldable" => TRUE,
    "entity keys" => array(
      "id" => "id",
    ),
    "translation" => array(
      "ihfathlsn" => TRUE,
    ),
    "bundles" => array(
      "general_faq" => array(
        "label" => t("General FAQ"),
      )
    ),
  );

  return $entities;
}

function ihfathlsn_course_category_uri($ccat) {
  return array(
    "path" => "admin/ihfath/config/course",
  );
}

function _ihfathlsn_create_course_category() {
  $cat = (object) array(
    "available" => 0,
  );

  return $cat;
}

function _ihfathlsn_create_course($category = 0) {
  $cat = (object) array(
    "available" => 0,
    "type" => $category,
  );

  return $cat;
}

function _ihfathlsn_save_course_data($type = "level", $id = NULL, $fields = array()) {
  if (!isset($id)) {
    $id = db_insert("ihfath_lsn_meta_entities")
            ->fields(array(
              "id" => NULL,
            ))
            ->execute();
  }

  if ($fields) {
    field_attach_update($type === "level" ? "course_level" : "course_faq", (object) (array(
      "id" => $id,
    ) + $fields));
  }

  return $id;
}

// NOTE: Courses are also called Ihfath Lesson Programs for historical reasons
//       likewise, Course Categories are called Ihfath Lesson Types
class IhfathCourse {
  const CATEGORY_DATA = array(
    "data" => array(),
    "fields" => array(),
  );

  static function saveCategory($id, $name, $available, $data = NULL) {
    if (!isset($data)) {
      $data = (object) self::CATEGORY_DATA;
    }

    $type_id = $id;
    if ($id) {
      ihfathlsn_update_type($id, $name, $data->data, $available);
    } else {
      $type_id = ihfathlsn_add_type($name, $data->data, $available);
    }

    $course_cat_arr = array(
      "tid" => $type_id,
    );
    $course_cat_arr += $data->fields;
    field_attach_update("course_category", (object) $course_cat_arr);

    return $type_id;
  }

  static function saveCourse($id, $name, $category_id, $available, $data) {
    $program_id = $id;

    if ($id) {
      ihfathlsn_update_program($id, $name, $data->data, $available);
    } else {
      $program_id = ihfathlsn_add_program($name, $category_id, $data->data, $available);
    }

    $course_arr = array(
      "pid" => $program_id,
    );
    $course_arr += $data->fields;
    field_attach_update("course", (object) $course_arr);

    return $program_id;
  }

  static function saveLevel($id, $fields) {
    return _ihfathlsn_save_course_data("level", $id, $fields);
  }

  static function saveFaq($id, $fields) {
    return _ihfathlsn_save_course_data("faq", $id, $fields);
  }

  static function getLevelBooks($id) {
    $query = new EntityFieldQuery();
    $query
      ->entityCondition("entity_type", "course_level")
      ->propertyCondition("id", $id);
    $result = $query->execute();
    $book_data = array();

    // Load book list (if any)
    if (isset($result["course_level"])) {
      $fields = entity_load("course_level", array_keys($result["course_level"]))[$id];
      $books = json_decode($fields->field_ihfathlsn_c_lvl_books[LANGUAGE_NONE][0]["value"]);

      foreach ($books as $book_id) {
        $loaded = IhfathBook::loadBook($book_id);

        $book_data[] = array(
          "id" => $loaded->bhid,
          "title" => $loaded->field_book_title_field[LANGUAGE_NONE][0]["value"],
        );
      }
    }

    return $book_data;
  }

  static private function _loadCourseData($type = "level", $course_ids) {
    $bundle = $type === "level" ? "course_level" : "course_faq";
    $field = $type === "level" ? "field_ihfathlsn_c_lvl_c_ref" : "field_ihfathlsn_c_faq_c_ref";

    $query = new EntityFieldQuery();
    $query->entityCondition("bundle", $bundle)
          ->fieldCondition($field, "value", $course_ids, "IN");
    $result = $query->execute();

    if (!isset($result[$bundle])) {
      return array();
    }

    return entity_load($bundle, array_keys($result[$bundle]));
  }

  static function loadLevels($course_ids) {
    return self::_loadCourseData("level", $course_ids);
  }

  static function loadFaq($course_ids) {
    return self::_loadCourseData("faq", $course_ids);
  }

  static function loadCategory($tid) {
    $type = ihfathlsn_get_types(NULL, NULL, $tid);
    $type_fields = (object) array(
      "tid" => $tid,
    );

    field_attach_load("course_category", array(
      $tid => $type_fields,
    ));

    $category = (object) array_merge((array) $type, (array) $type_fields);
    return $category;
  }

  static function loadAllCourses($pid = NULL, $available = NULL) {
    $programs = ihfathlsn_get_programs(NULL, $available, $pid);

    if (!$programs) {
      return array();
    }

    if (!is_array($programs)) {
      $programs = array($programs->pid => $programs);
    }

    foreach ($programs as $program) {
      $program->levels = array();
      $program->faq = array();
    }

    field_attach_load("course", $programs);

    $levels = self::loadLevels(array_keys($programs));
    $faqs = self::loadFaq(array_keys($programs));

    foreach ($levels as $level) {
      $programs[$level->field_ihfathlsn_c_lvl_c_ref[LANGUAGE_NONE][0]["value"]]->levels[] = $level;
    }

    foreach ($faqs as $faq) {
      $programs[$faq->field_ihfathlsn_c_faq_c_ref[LANGUAGE_NONE][0]["value"]]->faq[] = $faq;
    }

    return $programs;
  }

  static function deleteCourse($id) {
    $course = self::loadCourse($id);

    if (!$course) {
      return NULL;
    }

    self::removeCourseData(array_keys($course->levels), "level");
    self::removeCourseData(array_keys($course->faq), "faq");

    field_attach_delete("course", $id);
    db_delete("ihfath_lsn_programs")
      ->condition("pid", $id)
      ->execute();

    return $id;
  }

  static function removeCourseData($ids, $type = "level") {
    if ($type !== "level" && $type !== "faq") {
      throw new ErrorException("IhfathCourse::removeCourseData: \$type should be either 'level' or 'faq'");
    }

    if (!count($ids)) {
      return;
    }

    $data = self::_loadCourseData($type, $ids);

    if (count($data)) {
      foreach ($data as $entry) {
        field_attach_delete("course_" . $type, $entry->id);
      }

      db_delete("ihfath_lsn_meta_entities")
        ->condition("id", $ids, "IN")
        ->execute();
    }
  }

  static function loadCourse($pid = NULL) {
    return array_values(self::loadAllCourses($pid))[0];
  }

  static function loadCourseByName($name) {
    $program = db_select("ihfath_lsn_programs", "ilp")
                 ->fields("ilp", array("pid", "name"))
                 ->condition("ilp.name", $name)
                 ->execute()->fetchObject();

    if ($program) {
      return self::loadCourse($program->pid);
    }

    // Program doesn't exist
    return NULL;
  }

  static function listAll($shallow = TRUE) {
    $types = ihfathlsn_get_types();
    $programs = ihfathlsn_get_programs();

    if (!$shallow) {
      field_attach_load("course_category", $types);
      field_attach_load("course", $programs);
    }

    $types_mapped = array();
    foreach ($types as $type) {
      $types_mapped[$type->tid] = $type;
      $type->programs = array();
    }

    foreach ($programs as $program) {
      $types_mapped[$program->type]->programs[] = $program;
    }

    return $types_mapped;
  }
}

class IhfathBook {
  const FILE_DIR = Ihfathmsg\Prepare::ATTACH_DIR;
  const BOOK_DIR = "public://ihfathbooks/";
  const BOOK_DIR_ABS = DRUPAL_ROOT . "/sites/default/files/ihfathbooks";
  const BOOK_BATCH_PAGES = 35;

  /**
   * Map of processed partial documents
   */
  static $processed = array();

  static function absPath($uriPath) {
    return file_stream_wrapper_get_instance_by_uri($uriPath)->realpath();
  }

  static function saveBook($id, $book_data) {
    $new_book = !isset($id);
    $book = (object) array(
      "bid" => $id,
      "processed" => FALSE,
      "documents" => array(),
      "pages" => 0,
    );
    $unprocessed = FALSE;

    // Create new book if it doesn't exist
    if (!isset($id)) {
      $id = db_insert("ihfath_lsn_books")
              ->fields(array(
                "bid" => NULL,
                "bhid" => "",
                "preview_page" => "",
                "direction" => $book_data->direction,
                "pages" => 0,
              ))
              ->execute();

      $book->bid = $id;
      $unprocessed = TRUE;
    } else {
      $book = self::loadBookMeta($id);

      // FIXME: Some form of error handling would be appropriate here
      if (!$book) {
        return NULL;
      }
    }

    $book_data->bid = $book->bid;
    field_attach_update("ihfath_book", $book_data);

    $errors = array();

    $cur_docs = array_map(function($doc) {
      return $doc->hash;
    }, $book->documents);
    $new_documents = array_diff($book_data->hashes, $cur_docs);
    $removed_documents = array_diff($cur_docs, $book_data->hashes);

    // Remove all deleted docs
    $remove_docs = array();

    foreach ($book->documents as $doc) {
      if (in_array($doc->hash, $removed_documents)) {
        $remove_docs[] = $doc;
        $book->pages -= $doc->pages;
      }
    }

    if ($remove_docs) {
      self::deleteBookDocuments($book->bid, $remove_docs);
    }

    $total_pages = $book->pages;

    // Process new documents in book
    if ($new_documents) {
      $fhashes = "";
      // FIXME: Order this correctly
      $weight = count($cur_docs) - count($remove_docs);

      // Process documents
      foreach ($new_documents as $fhash) {
        if ($fhash) {
          $doc_meta = self::processDocuments($book->bid, $fhash, $weight);

          if ($doc_meta) {
            $fhashes .= $fhash;
            $total_pages += $doc_meta->pages;
            $weight++;
          } else {
            // Document isn't a PDF
            $errors[] = $fhash;
          }
        }
      }

      // Generate new Book Hash ID if this is a new book
      if ($new_book) {
        $book->bhid = substr(sha1(microtime() . $total_pages . $book->bid . $fhashes), 0, 32);
      }
    }

    // Save the page count, bhid, and direction
    db_update("ihfath_lsn_books")
      ->fields(array(
        "bhid" => $book->bhid,
        "pages" => $total_pages,
        "direction" => $book_data->direction,
      ))
      ->condition("bid", $book->bid)
      ->execute();

    // Clear caches
    cache_set("ihfath_book_" . $book->bhid, NULL);

    return $book->bhid;
  }

  static function parseInfo($info) {
    $pmatch = array();
    $res = preg_match('/^Pages:\s+(\d+)$/m', $info, $pmatch);

    // Check that this PDF is valid
    if (!$pmatch) {
      return NULL;
    }

    $vmatch = array();
    preg_match('/^PDF\s+version:\s+([\d\.]+)$/m', $info, $vmatch);

    $cmatch = array();
    preg_match('/^CreationDate:\s+(?!\s)(.+)$/m', $info, $cmatch);

    $data = (object) array(
      "pages" => (int) $pmatch[1],
      "pdf_version" => (float) $vmatch[1],
      "creation" => $cmatch ? $cmatch[1] : "",
    );

    return $data;
  }

  // Extract PDF info from new documents
  static function processDocuments($bid, $hash, $weight) {
    $book_hash = substr(sha1($bid . microtime() . $hash), 0, 16);
    $dir_name = substr(sha1($hash . time()), 0, 3) . "-" . $book_hash . "-" . substr(sha1(microtime() . "-book"), 0, 6);
    $new_dir = self::absPath(self::BOOK_DIR . $dir_name);
    $pdf_path = self::absPath(self::FILE_DIR . $hash);

    // Get path
    $mod_base_path = drupal_get_path("module", "ihfathlsn");
    $base_path = DRUPAL_ROOT . "/" . implode("/", array_slice(explode("/", $mod_base_path), 0, -1));

    ob_start();
    // Confirm the document is a PDF and find its page count
    passthru("{$base_path}/bin/env {$base_path}/bin/pdfinfo $pdf_path");
    $pdf_info = ob_get_contents();
    ob_end_clean();

    $info = self::parseInfo($pdf_info);
    $doc_id = NULL;

    if ($info) {
      // Make new book dir
      mkdir($new_dir);

      $doc_id = db_insert("ihfath_lsn_book_files")
                  ->fields(array(
                    "bid" => $bid,
                    "hash" => $hash,
                    "dir_hash" => $dir_name,
                    "pages" => $info->pages,
                    "processed" => 0,
                    "weight" => $weight,
                  ))
                  ->execute();

      return (object) array(
        "id" => $doc_id,
        "pages" => $info->pages,
      );
    }

    // Invalid PDF
    return NULL;
  }

  static function deleteBook($bid) {
    $book = IhfathBook::loadBookMeta($bid);

    if (!$book) {
      return NULL;
    }

    self::deleteBookDocuments($bid, $book->documents);

    // TODO: Delete bookmarks...

    // Finally delete the book in question
    db_delete("ihfath_lsn_books")
      ->condition("bid", $bid)
      ->execute();

    return TRUE;
  }

  static function deleteBookDocuments($bid, $docs) {
    $doc_ids = array_keys($docs);

    // Delete all documents
    db_delete("ihfath_lsn_book_files")
      ->condition("pdid", $doc_ids, "IN")
      ->condition("bid", $bid)
      ->execute();

    // Delete all pages
    db_delete("ihfath_lsn_book_pages")
      ->condition("pdid", $doc_ids, "IN")
      ->execute();

    $page_dir = IhfathBook::absPath(IhfathBook::BOOK_DIR);

    // Delete all page files
    foreach ($docs as $doc) {
      // Don't waste time in PHP with its native functions
      @exec('rm -r "' . $page_dir . "/" . $doc->dir_hash . '" > /dev/null 2>&1 &');
    }

    // TODO: Delete the actual PDFs themselves to free up space
  }

  static function loadBook($bhid, $priveleged = FALSE) {
    $book_query = db_select("ihfath_lsn_books", "ilb");

    if ($priveleged) {
      // Load all fields including sensitive ones
      $book_query->fields("ilb");
    } else {
      $book_query->fields("ilb", array(
        "bid",
        "bhid",
        "preview_page",
        "direction",
        "pages",
      ));
    }

    $book_query->condition("ilb.bhid", $bhid);
    $book = $book_query->execute()->fetchObject();

    if (!$book) {
      return NULL;
    }

    field_attach_load("ihfath_book", array(
      $book->bid => $book,
    ));

    if (!$priveleged) {
      unset($book->bid);
    }

    return $book;
  }

  static function loadBookPage($bhid, $page) {
    $book_data = cache_get("ihfath_book_" . $bhid);

    if ($book_data->data ?? NULL) {
      $book_data = $book_data->data;
    } else {
      $book = db_select("ihfath_lsn_books", "ilb")
                ->fields("ilb", array("pages", "bid"))
                ->condition("ilb.bhid", $bhid)
                ->execute()->fetchObject();
      $docs = db_select("ihfath_lsn_book_files", "ilbf")
                ->fields("ilbf")
                ->condition("bid", $book->bid)
                ->orderBy("ilbf.weight", "ASC")
                ->execute()->fetchAllAssoc("pdid");
      $book_data = (object) array(
        "book" => $book,
        "docs" => $docs,
      );

      cache_set("ihfath_book_" . $bhid, $book_data);
    }

    $document = NULL;
    $rel_page = 0;
    $total_pages = 0;
    foreach ($book_data->docs as $cur_doc) {
      $old_total = $total_pages;
      $total_pages += $cur_doc->pages;

      if ($page <= $total_pages) {
        $rel_page = $page - $old_total;
        $document = $cur_doc;
        break;
      }
    }

    // _ihv_dump(array(
    //   $book_data,
    //   $document,
    //   $rel_page,
    //   $page,
    //   $page_id,
    // ), 1);

    // FIXME: This shouldn't fail
    if (!$document) {
      return NULL;
    }

    $page_id = db_select("ihfath_lsn_book_pages", "ilbp")
                 ->fields("ilbp")
                 ->condition("ilbp.pdid", $document->pdid)
                 ->condition("ilbp.page", $rel_page)
                 ->execute()->fetchObject();
    return $page_id->hash;
  }

  /**
   * Getter for internal use (book processing in particular)
   */
  static function loadBookMeta($bid) {
    $book = db_select("ihfath_lsn_books", "ilb")
              ->fields("ilb")
              ->condition("bid", $bid)
              ->execute()->fetchObject();

    if (!$book) {
      return NULL;
    }

    // Get documents
    $documents = db_select("ihfath_lsn_book_files", "ilbf")
                   ->fields("ilbf")
                   ->condition("bid", $bid)
                   ->orderBy("ilbf.weight", "ASC")
                   ->execute()->fetchAllAssoc("pdid");

    $processed = 0;
    foreach ($documents as $doc) {
      $processed += $doc->processed;
    }

    $book->processed = $processed;
    $book->documents = array_values($documents);
    return $book;
  }

  static function getBookList() {
    $books_q = db_query("SELECT ilb.*, ilbf.hashes, ilbf.processed FROM {ihfath_lsn_books} ilb
      LEFT JOIN (
        SELECT GROUP_CONCAT(hash SEPARATOR ',') hashes, SUM(processed) processed, bid FROM {ihfath_lsn_book_files}
        GROUP BY bid
      ) ilbf
      ON ilb.bid = ilbf.bid");
    $books = $books_q->fetchAllAssoc("bid");

    field_attach_load("ihfath_book", $books);

    foreach ($books as $book) {
      $book->hashes = explode(",", $book->hashes);
      $book->page_data = array();
    }

    return $books;
  }
}
