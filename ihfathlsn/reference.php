<?php

function formname_form($form, &$form_state) {
  $form = array();
  $form['#theme'] = 'formname_form';
  $form['#submit'][] = '_MODULE_submit';
  return $form;
}

function MODULE_theme($existing, $type, $theme, $path) {
  return array(
    'formname_form' => array(
      'render element' => 'form',
      'template' => 'theme/formname-form'
    )
  );
}

?>

<!-- Theme... -->
<div class="whatever"> <?php print drupal_render($form['some_element']); ?> </div> etc...

<!-- Don't forget... -->
<?php print drupal_render_children(); ?>

<!-- Reference two... -->
<?php
/**
 * @file MODULE.module
 */

/**
 * Implements hook_menu().
 */
function MODULE_menu() {
  $items = array();
  $items['some/path'] = array(
    'title' => 'Form Title',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('formname_form'),
    'access callback' => TRUE,
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );

  return $items;
}

function MODULE_theme2($existing, $type, $theme, $path) {
  echo '';
  return array(
    'formname_form' => array(
      'render element' => 'form',
      'template' => 'theme/formname-form',
    ),
  );
}

/**
 * Implements hook_form().
 */
function formname_form2($form, &$form_state) {
  $form = array();
  $form['#theme'] = 'formname_form';
  $form['#submit'][] = '_MODULE_submit';
  return $form;
}

/**
 * @param $form
 * @param $form_state
 * Implements hook_form_FORM_ID_alter().
 */
function MODULE_form_formname_form_alter($form, &$form_state) {
  echo '';
}

/**
 * @param $form
 * @param $form_state
 */
function _MODULE_submit($form, &$form_state) {

}
