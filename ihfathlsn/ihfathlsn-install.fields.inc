<?php

namespace IhfathLsn\Install;

// Ihfath Lesson's Fields (Un)Installation Functions
//
// Prefix (for autocomplete): ihfathlsn_

const PROF_SCHEDULE_FIELD = "field_ihfathlsn_prof_schedule";
const PROF_PROGRAMS_FIELD = "field_ihfathlsn_prof_programs";

function install_professor_fields() {
  $schedule_field_name = PROF_SCHEDULE_FIELD;
  $programs_field_name = PROF_PROGRAMS_FIELD;

  // Check for schedule field
  if (!field_info_field($schedule_field_name)) {
    // Create the field
    $field = array(
      "field_name"   => $schedule_field_name,
      "type"         => "text_long",
      "entity_types" => array("user"),
      "cardinality"  => 1
    );

    field_create_field($field);

    // Create the instance
    $instance = array(
      "field_name"  => $schedule_field_name,
      "bundle"      => "user",
      "entity_type" => "user",
      "label"       => "Ihfath Professor Schedule",
      "widget"      => array(
        "type" => "field_ihfathlsn_schedule"
      )
    );

    field_create_instance($instance);
    // SUCCESS
  }

  // Check for programs field
  if (!field_info_field($programs_field_name)) {
    // Create the field
    $field = array(
      "field_name"   => $programs_field_name,
      "type"         => "text_long",
      "entity_types" => array("user"),
      "cardinality"  => 1
    );

    field_create_field($field);

    // Create the instance
    $instance = array(
      "field_name"  => $programs_field_name,
      "bundle"      => "user",
      "entity_type" => "user",
      "label"       => "Ihfath Professor Programs",
      "widget"      => array(
        "type" => "field_ihfathlsn_programs"
      )
    );

    field_create_instance($instance);
    // SUCCESS
  }
}

function uninstall_professor_fields() {
  $schedule_field_name = PROF_SCHEDULE_FIELD;
  $programs_field_name = PROF_PROGRAMS_FIELD;

  // Check for schedule field
  if (field_info_field($schedule_field_name)) {
    // Delete the instance
    $instance = field_info_instance("user", $schedule_field_name, "user");
    field_delete_instance($instance);

    // Then delete the field
    field_delete_field($schedule_field_name);
  }

  // Check for programs field
  if (field_info_field($programs_field_name)) {
    // Delete the instance
    $instance = field_info_instance("user", $programs_field_name, "user");
    field_delete_instance($instance);

    // Then delete the field
    field_delete_field($programs_field_name);
  }
}

// User fields
const USER_FNAME_FIELD = "field_ihfath_firstname";

// User fields
const USER_LNAME_FIELD = "field_ihfath_lastname";

// User fields
const USER_PHONE_FIELD = "field_ihfath_phone";

// User fields
const USER_ADDR_FIELD = "field_ihfath_address";

// User fields
const USER_CV_FIELD = "field_ihfath_cv";

// User fields
const USER_VIDEODEMO_FIELD = "field_ihfath_videodemo";

// Book Title Field
const BOOK_TITLE_FIELD = "field_book_title_field";

// Book File Hash Field
const BOOK_FHASH_FIELD = "field_book_fhash_field";

// Book Page Count Field
const BOOK_PCOUNT_FIELD = "field_book_pcount_field";

// Book Processed Flag Field
const BOOK_PROC_FIELD = "field_book_proc_field";

// Course CoursePage Title Field
const COURSE_CPTITLE_FIELD = "field_ihfathlsn_c_cptitle";

// Course CoursePage Description Field
const COURSE_CPDESC_FIELD = "field_ihfathlsn_c_cpdesc";

// Course CoursePage Features Field
const COURSE_CPFEAT_FIELD = "field_ihfathlsn_c_cpfeat";
// JSON: [{ title, description }, ...]

// Course FrontPage Data Field
const COURSE_FPDATA_FIELD = "field_ihfathlsn_c_fpdata";

// Course Prices Field
const COURSE_PRICES_FIELD = "field_ihfathlsn_c_prices";

// Course Prices Field
const COURSE_F_PRICE_FIELD = "field_ihfathlsn_c_f_price";

// Course CoursePage Image Field
const COURSE_CPIMAGE_FIELD = "field_ihfathlsn_c_cpimage";

// Course CoursePage Level Description Field
const COURSE_CPLVLTITLE_FIELD = "field_ihfathlsn_c_cplvltitle";

// Course Level Title Field
const COURSE_LVL_TITLE_FIELD = "field_ihfathlsn_c_lvl_title";

// Course Level Description Field
const COURSE_LVL_DESC_FIELD = "field_ihfathlsn_c_lvl_desc";

// Course Level Books Field
const COURSE_LVL_BOOKS_FIELD = "field_ihfathlsn_c_lvl_books";

// Course Level Course Reference Field
const COURSE_LVL_COURSEREF_FIELD = "field_ihfathlsn_c_lvl_c_ref";

// Course FAQ Title Field
const COURSE_FAQ_TITLE_FIELD = "field_ihfathlsn_c_faq_title";

// Course FAQ Description Field
const COURSE_FAQ_DESC_FIELD = "field_ihfathlsn_c_faq_desc";

// Course FAQ Course Reference Field
const COURSE_FAQ_COURSEREF_FIELD = "field_ihfathlsn_c_faq_c_ref";

function install_course_fields() {
  $field_fname = USER_FNAME_FIELD;
  $field_lname = USER_LNAME_FIELD;
  $field_addr = USER_ADDR_FIELD;
  $field_phone = USER_PHONE_FIELD;
  $field_cv = USER_CV_FIELD;
  $field_videodemo = USER_VIDEODEMO_FIELD;

  $field_bktitle = BOOK_TITLE_FIELD;

  $field_cptitle = COURSE_CPTITLE_FIELD;
  $field_cpdesc = COURSE_CPDESC_FIELD;
  $field_cpfeat = COURSE_CPFEAT_FIELD;
  $field_cpprices = COURSE_PRICES_FIELD;
  $field_f_price = COURSE_F_PRICE_FIELD;
  $field_cpimage = COURSE_CPIMAGE_FIELD;
  $field_cplvltitle = COURSE_CPLVLTITLE_FIELD;
  $field_fpdata = COURSE_FPDATA_FIELD;

  $field_lvl_title = COURSE_LVL_TITLE_FIELD;
  $field_lvl_desc = COURSE_LVL_DESC_FIELD;
  $field_lvl_books = COURSE_LVL_BOOKS_FIELD;
  $field_lvl_ref = COURSE_LVL_COURSEREF_FIELD;

  $field_faq_title = COURSE_FAQ_TITLE_FIELD;
  $field_faq_desc = COURSE_FAQ_DESC_FIELD;
  $field_faq_ref = COURSE_FAQ_COURSEREF_FIELD;

  $index = array(
    "basic" => array("value"),
  );

  // FIXME: Why define fields twice?
  $field_types = array(
    $field_fname => array("text", "user", "First name", $index),
    $field_lname => array("text", "user", "Last name", $index),
    $field_addr => array("text", "user", "Address", $index),
    $field_phone => array("text", "user", "Phone", $index),
    $field_cv => array("text_long", "user", "CV"),
    $field_videodemo => array("text", "user", "Video demo", $index),

    $field_bktitle => array("text", "ihfath_book", "Ihfath Book Title"),
    $field_cptitle => array("text", "course", "Ihfath Course Page Title"),
    $field_cpdesc => array("text_long", "course", "Ihfath Course Page Description"),
    $field_cpfeat => array("text_long", "course", "Ihfath Course Page Features"),
    $field_cpprices => array("text", "course", "Ihfath Course Prices (IDs of Commerce Products)"),
    $field_f_price => array("number_integer", "course", "Ihfath Course Featured Price (ID of a Commerce Product)"),
    $field_cpimage => array("text", "course", "Ihfath Course Page Image"),
    $field_cplvltitle => array("text", "course", "Ihfath Course Page Levels' Title"),
    $field_fpdata => array("text_long", "course", "Ihfath Course FrontPage Data"),

    $field_lvl_title => array("text", "course_level", "Ihfath Course Level Title"),
    $field_lvl_desc => array("text_long", "course_level", "Ihfath Course Level Description"),
    $field_lvl_books => array("text_long", "course_level", "Ihfath Course Level Books"),
    $field_lvl_ref => array("number_integer", "course_level", "Ihfath Course Level Course Reference (ID)", $index),

    $field_faq_title => array("text", "course_faq", "Ihfath Course FAQ Title"),
    $field_faq_desc => array("text_long", "course_faq", "Ihfath Course FAQ Description"),
    $field_faq_ref => array("number_integer", "course_faq", "Ihfath Course FAQ Course Reference (ID)", $index),
  );

  foreach ($field_types as $field_name => $field) {
    list($type, $bundle, $label) = $field;
    $indexes = $field[3] ?? NULL;

    if (!field_info_field($field_name)) {
      $field_config = array(
        "field_name" => $field_name,
        "type" => $type,
        "entity_types" => array($bundle),
        "cardinality" => 1,
        "translatable" => TRUE,
      );

      if ($indexes) {
        $field_config["indexes"] = $indexes;
      }

      field_create_field($field_config);

      field_create_instance(array(
        "field_name" => $field_name,
        "bundle" => $bundle,
        "entity_type" => $bundle,
        "label" => $label,
      ));
    }
  }
}

function uninstall_course_fields() {
  $field_fname = USER_FNAME_FIELD;
  $field_lname = USER_LNAME_FIELD;
  $field_addr = USER_ADDR_FIELD;
  $field_phone = USER_PHONE_FIELD;
  $field_cv = USER_CV_FIELD;
  $field_videodemo = USER_VIDEODEMO_FIELD;

  $field_bktitle = BOOK_TITLE_FIELD;
  $field_bk_fhash = BOOK_FHASH_FIELD;
  $field_bk_pcount = BOOK_PCOUNT_FIELD;
  $field_bk_proc = BOOK_PROC_FIELD;

  $field_cptitle = COURSE_CPTITLE_FIELD;
  $field_cpdesc = COURSE_CPDESC_FIELD;
  $field_cpfeat = COURSE_CPFEAT_FIELD;
  $field_cpprices = COURSE_PRICES_FIELD;
  $field_f_price = COURSE_F_PRICE_FIELD;
  $field_cpimage = COURSE_CPIMAGE_FIELD;
  $field_cplvltitle = COURSE_CPLVLTITLE_FIELD;
  $field_fpdata = COURSE_FPDATA_FIELD;

  $field_lvl_title = COURSE_LVL_TITLE_FIELD;
  $field_lvl_desc = COURSE_LVL_DESC_FIELD;
  $field_lvl_books = COURSE_LVL_BOOKS_FIELD;
  $field_lvl_ref = COURSE_LVL_COURSEREF_FIELD;

  $field_faq_title = COURSE_FAQ_TITLE_FIELD;
  $field_faq_desc = COURSE_FAQ_DESC_FIELD;
  $field_faq_ref = COURSE_FAQ_COURSEREF_FIELD;

  $fields = array(
    $field_fname => "user",
    $field_lname => "user",
    $field_addr => "user",
    $field_phone => "user",
    $field_cv => "user",
    $field_videodemo => "user",
    $field_bktitle => "ihfath_book",
    $field_bk_fhash => "ihfath_book",
    $field_bk_pcount => "ihfath_book",
    $field_bk_proc => "ihfath_book",
    $field_cptitle => "course",
    $field_cpdesc => "course",
    $field_cpfeat => "course",
    $field_cpprices => "course",
    $field_f_price => "course",
    $field_cpimage => "course",
    $field_cplvltitle => "course",
    $field_fpdata => "course",
    $field_lvl_title => "course_level",
    $field_lvl_desc => "course_level",
    $field_lvl_books => "course_level",
    $field_lvl_ref => "course_level",
    $field_faq_title => "course_faq",
    $field_faq_desc => "course_faq",
    $field_faq_ref => "course_faq",
  );

  foreach ($fields as $field => $bundle) {
    if (field_info_field($field)) {
      // Delete the instance
      $instance = field_info_instance($bundle, $field, $bundle);
      field_delete_instance($instance);

      // Then delete the field
      field_delete_field($field);
    }
  }
}
