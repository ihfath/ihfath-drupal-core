<?php

class IhfathlsnFormAdminMeta {
  public $form = array(
    "#attached" => array(
      "js" => array()
    ),
    
    "type" => array( // Lesson program type
      "#type"    => "select",
      "#title"   => "",
      "#options" => array(),
      
      "#prefix" => '<div class="form-input"><div class="row"><div class="col-md-12">',
      "#suffix" => '</div></div></div>'
    ),
    
    "name" => array(
      "#type"     => "textfield",
      "#title"    => "",
      "#required" => TRUE,
      
      "#prefix" => '<div class="form-input"><div class="row"><div class="col-md-7">',
      "#suffix" => '</div>'
    ),
    
    "lang" => array(
      "#type" => "select",
      "#title" => "Language",
      "#options" => array(),
      
      "#prefix" => '<div class="col-md-5">',
      "#suffix" => '</div></div></div>'
    ),
    
    "desc" => array(
      "#type"  => "textarea",
      "#title" => "",
      
      "#prefix" => '<div class="form-input"><div class="row"><div class="col-md-12">',
      "#suffix" => '</div></div></div>'
    ),
    
    "avail" => array(
      "#type"  => "checkbox",
      "#title" => "",
      
      "#prefix" => '<div class="form-input"><div class="row"><div class="col-md-12">',
      "#suffix" => '</div></div></div>'
    ),
    
    "buttons" => array(
      "delete" => array(
        "#type"       => "submit",
        "#value"      => "",
        "#submit"     => array("ihfathlsn_admin_lesson_delete_meta_form_submit"),
        "#attributes" => array(
          "class" => array(
            "btn-juicy_pink ihfath-force-btn-jpink"
          )
        ),
        
        "#prefix" => '<span style="display: inline-block; float: right;">',
        "#suffix" => "<script>" .
                       "window.addEventListener('DOMContentLoaded', function() {" .
                         "var buttons = document.getElementsByClassName('ihfath-force-btn-jpink');" .
                         "Array.prototype.forEach.call(buttons, function(e) {" .
                           "e.classList.remove('main-bg');" .
                         "});" .
                        "});" .
                     "</script>" . '</span>'
      ),
      
      "submit" => array(
        "#type"  => "submit",
        "#value" => ""
      ),
      
      "go_back" => array(
        "#markup"     => "",
        "#attributes" => array(
          "class" => array(
            "btn-md"
          )
        )
      ),
      
      "#prefix" => '<div class="form-input"><div class="row"><div class="col-md-12">',
      "#suffix" => '</div></div></div>'
    )
  );
  
  function __construct($delta, $meta = NULL, $types = NULL, $base_url) {
    global $language;
    $def_lang = $language->language;
    
    $form = &$this->form;
    
    // Add binding JS
    $form["#attached"]["js"] = array(
      drupal_get_path("module", "ihfathlsn") . "/theme-js/add-meta--bind-locale2.js" => array(
        "type"   => "file",
        "weight" => 2
      )
    );
    
    // Translate the various form elements
    $form["name"]["#title"] = t(ucfirst($delta) . " name");
    $form["desc"]["#title"] = t("Description");
    $form["avail"]["#title"] = t("Available");
    $form["lang"]["#title"] = t("Language");
    $form["buttons"]["submit"]["#value"] = t("Submit");
    
    $form["buttons"]["go_back"]["#markup"] = (
      '<a href="' . $base_url . '/ihfath/admin/lesson-meta/' . '" class="cws-button btn btn-lg main-bg submit-btn shape form-submit new-angle">'
      . '<i class="fa fa-arrow-left"></i>'
      . 'Back to meta list' .
      '</a>'
    );
    
    if ($meta) {
      $form["name"]["#default_value"] = $meta->data["current"]->name;
      $form["desc"]["#default_value"] = $meta->data["current"]->description;
      $form["avail"]["#default_value"] = $meta->available;
      
      $form["buttons"]["delete"]["#value"] = t("Delete");
    } else {
      // New meta, remove the delete button
      unset($form["buttons"]["delete"]);
    }
    
    if ($delta === "type" || !$types) {
      // Type meta, remove the type field
      unset($form["type"]);
    } else {
      // Set field title
      $form["type"]["#title"] = t("Program type");
      
      // Add options
      $type_options = &$form["type"]["#options"];
      
      if ($meta) {
        $def_type = $meta->type;
        $form["type"]["#disabled"] = TRUE;
      } else {
        $def_type = 1;
      }
      
      $form["type"]["#default_value"] = $def_type;
      
      foreach ($types as $type) {
        $type_options[$type->tid] = $type->data["current"]->name;
      }
    }
    
    // Add languages
    $langs        = locale_language_list("name");
    $lang_options = &$form["lang"]["#options"];
    
    // Set to default language
    $form["lang"]["#default_value"] = $meta ? $meta->data["current"]->lang : $def_lang;
    
    // Add options to language <select>
    // and add hidden <input>'s
    foreach ($langs as $lang_code => $lang) {
      $lang_options[$lang_code] = $lang;
      
      if ($meta) {
        $def_name = $meta->data["current"]->name;
        $def_desc = $meta->data["current"]->description;
      } else {
        $def_name = "";
        $def_desc = "";
      }
      
      // Add individual hidden inputs
      $form["name-" . $lang_code] = array(
        "#type"          => "hidden",
        "#default_value" => $def_name,
        "#attributes"    => array(
          "class"     => array("ihfath-meta-data-hidden"),
          "data-meta" => "name",
          "data-lang" => $lang_code
        )
      );
      
      $form["desc-" . $lang_code] = array(
        "#type"          => "hidden",
        "#default_value" => $def_desc,
        "#attributes"    => array(
          "class"     => array("ihfath-meta-data-hidden"),
          "data-meta" => "desc",
          "data-lang" => $lang_code
        )
      );
    }
  }
}
