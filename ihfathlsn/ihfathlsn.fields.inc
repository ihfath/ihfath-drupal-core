<?php

// Ihfath Lesson's Fields Hook Implementations
//
// Prefix (for autocomplete): ihfathlsn_

define("IHFATHLSN_PROF_SCHEDULE_FIELD", "field_ihfathlsn_prof_schedule");

function ihfathlsn_field_widget_info() {
  return array(
    "field_ihfathlsn_schedule" => array(
      "label"       => t("Ihfath Professor Schedule"),
      "field types" => array("text_long")
    ),
    
    "field_ihfathlsn_programs" => array(
      "label"       => t("Ihfath Professor Programs"),
      "field types" => array("text_long")
    )
  );
}

function ihfathlsn_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  $value = isset($items[0]["value"]) ? $items[0]["value"] : "{}";
  $user  = $form_state["build_info"]["args"][0];
  
  $widget = $element;
  $widget["#delta"] = $delta;
  
  switch ($instance["widget"]["type"]) {
    case "field_ihfathlsn_schedule":
      // Get base paths
      $base_path        = drupal_get_path("module", "ihfathlsn") . "/";
      $ihfath_base_path = implode("/", array_slice(explode("/", $base_path), 0, -2)) . "/";
      
      $table_base = $value === "{}" ? NULL : unserialize($value);
      
      if ($table_base && $user->timezone) {
        $timezone_offset = _ihfathui_get_timezone_offset($user->timezone) / 60 / 60;
        $table_base["table"] = _ihfathui_prof_week_schedule_timezone_shift($table_base["table"], $timezone_offset);
      }
      
      $default_value = $table_base ? json_encode($table_base) : $value;
      
      $widget += array(
        "#theme" => "ihfathlsn_schedule_field",
        "value"  => array(
          "#type"          => "hidden",
          "#default_value" => $default_value,
          "#attributes"    => array(
            "id"    => "ihfathlsn-schedule-widget-input",
            "class" => array("ihfathlsn-invisible")
          ),
          
          "#element_validate" => array("ihfathlsn_schedule_field_form_validate")
        ),
        
        // Add JS
        "#attached" => array(
          "js" => array(
            $ihfath_base_path . "js-common/cellidi.js" => array(
              "type"   => "file",
              "weight" => 5
            ),
            $base_path . "theme/schedule-field/schedule.js" => array(
              "type"   => "file",
              "weight" => 5
            )
          )
        )
      );
      
      return $widget;
    
    case "field_ihfathlsn_programs":
      $widget += array(
        "#theme" => "ihfathlsn_programs_field",
        "value"  => array(
          "#type"          => "hidden",
          "#default_value" => $value === "{}" ? $value : json_encode(unserialize($value)),
          "#attributes"    => array(
            "id"    => "ihfathlsn-programs-widget-input",
            "class" => array("ihfathlsn-invisible")
          ),
          
          "#element_validate" => array("ihfathlsn_programs_field_form_validate")
        ),
        
        // Get all the types and programs
        "#ihfath" => array(
          "types"    => ihfathlsn_get_types(),
          "programs" => ihfathlsn_get_programs()
        ),
      );
      
      return $widget;
  }
}

function ihfathlsn_schedule_field_form_validate($element, &$form_state) {
  $parents = $element["#parents"];
  $value   = $form_state["values"][$parents[0]][$parents[1]][$parents[2]][$parents[3]];
  $user    = $form_state["build_info"]["args"][0];
  
  if (is_array($value)) {
    // This isn't user induced, exit
    return;
  }
  
  // Validate schedule
  $schedule       = json_decode($value, TRUE);
  $valid_schedule = TRUE;
  $error          = array();
  
  if (count($schedule) === 0 || _ihfathlsn_validate_weekly_schedule($schedule, $error)) {
    if ($user->timezone) {
      $timezone_offset = _ihfathui_get_timezone_offset($user->timezone) / 60 / 60;
      $schedule["table"] = _ihfathui_prof_week_schedule_timezone_shift($schedule["table"], -$timezone_offset);
    }
    
    form_set_value($element, serialize($schedule), $form_state);
  } else {
    form_error($element, t("Ihfath schedule data invalid: " . $error["reason"]));
  }
}

function ihfathlsn_programs_field_form_validate($element, &$form_state) {
  $parents = $element["#parents"];
  $value   = $form_state["values"][$parents[0]][$parents[1]][$parents[2]][$parents[3]];
  
  if (is_array($value)) {
    // This isn't user induced, exit
    return;
  }
  
  // User input programs
  $input_programs = json_decode($value, TRUE);
  
  // Get current programs
  $programs = ihfathlsn_get_programs();
  
  // Ensure valid programs
  foreach ($input_programs as $program) {
    if (!isset($programs[$program])) {
      // Non existant program, form error and exit
      form_error($element, t("Ihfath programs data invalid"));
      return;
    }
  }
  
  // Valid programs
  form_set_value($element, serialize($input_programs), $form_state);
}

function _ihfathlsn_validate_weekly_schedule($schedule, &$error) {
  // Preliminary structure check
  if (!is_array($schedule) ||
      !isset($schedule["bounds"], $schedule["table"]) ||
      !is_array($schedule["bounds"]) ||
      !is_array($schedule["table"]) ||
      count($schedule["bounds"]) !== 4 ||
      count($schedule["table"]) !== 7) {
    $error["reason"] = "Table or bounds dimensions invalid";
    // One of the checks went wrong
    return FALSE;
  }
  
  // Check bounds
  $bounds = $schedule["bounds"];
  
  foreach ($bounds as $index => $bound) {
    if ($index < 2) {
      // Validate day bounds
      if (!is_numeric($bound) || $bound > 7 || $bound < 1 || intval($bound) !== $bound) {
        $error["reason"] = "Invalid day bounds: " . $bound . " type: " . gettype($bound);
        // Invalid day
        return FALSE;
      }
    } else {
      // Validate hour bounds
      if (!is_numeric($bound) || $bound > 23 || $bound < 0 || intval($bound) !== $bound) {
        $error["reason"] = "Invalid hour bounds: " . $bound . " type: " . gettype($bound);
        // Invalid hour
        return FALSE;
      }
    }
  }
  
  $start_day  = $bounds[0];
  $end_day    = $bounds[1];
  $start_hour = $bounds[2];
  $end_hour   = $bounds[3];
  
  // Check table
  foreach ($schedule["table"] as $day_hours) {
    if (count($day_hours) !== 24) {
      // Invalid amount of hours
      return FALSE;
    }
    
    // Flag for weekend column/day
    $is_weekend = FALSE;
    
    foreach ($day_hours as $hour_index => $hour_state) {
      switch ($hour_state) {
        case 0:
        case 1:
          if ($is_weekend) {
            // Somehow, workday hours on a weekend...
            $error["reason"] = "Workhours on a weekend";
            return FALSE;
          }
          break;
        case 3:
          if (!$is_weekend && $hour_index !== 0) {
            // Somehow, weekend hours on a workday...
            $error["reason"] = "Weekend hours on a workday";
            return FALSE;
          }
          
          $is_weekend = true;
          break;
        default:
          // Invalid work hour state
          $error["reason"] = "Invalid state";
          return FALSE;
      }
    }
  }
  
  // TODO: Confirm $bounds's constraints correlate properly with $table
  
  // All test have been passed
  return TRUE;
}

function ihfathlsn_gen_empty_schedule($fill = NULL, $use_fill = FALSE) {
  $schedule = array(
    "bounds" => array(
      1, 7,
      0, 23,
    ),
    
    "table" => NULL
  );
  
  $table = array();
  
  for ($i=0; $i<7; $i++) {
    $day_column = array();
    
    for ($j=0; $j<24; $j++) {
      $day_column[] = $use_fill ? $fill : 1;
    }
    
    $table[] = $day_column;
  }
  
  $schedule["table"] = $table;
  return $schedule;
}
