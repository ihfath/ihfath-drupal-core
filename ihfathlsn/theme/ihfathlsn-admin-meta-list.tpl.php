<?php
global $language;

$base_url = base_path();
$edit_str = t("Edit");
$cur_lang = $language->language;
?>

<!-- NOTE: IT WORKS! <iframe src="https://app.conceptboard.com/board/q7fm-xy09-i5yd-97hc-pbxn" width="100%" height="800" style="height: 600px !important;"></iframe> -->

<!-- TYPES -->
<a href="<?php echo $base_url . "ihfath/admin/lesson-meta/type/add"; ?>" class="cws-button btn btn-md right-icon main-bg with-icon shape new-angle">
  <?php echo t("Add Lesson Type"); ?> <i class="fa fa-plus"></i>
</a>
<div class="heading">
  <h3 class="head-6">
    <!-- Nothing here -->
  </h3>
</div>

<div class="view-content ihfath-vc">
  <div class="portfolio p-1-col simple" id="container" style="position: relative;">
    <?php
    foreach ($types as $type) {
      $edit_url = $base_url . "ihfath/admin/lesson-meta/type/" . $type->tid; ?>
      
      <div class="views-row views-row-1 views-row-odd views-row-first portfolio-item term-15 term-14 ihfath-pi" style="position: absolute; left: 0px; top: 0px;">
        <div class="img-holder" style="float: right;">
          <a href="<?php echo $edit_url; ?>" class="cws-button btn btn-md main-bg">
            <?php echo $edit_str; ?> <span class="fa fa-pencil"></span>
          </a>
        </div>
        <div class="name-holder">
          <h4><a href="<?php echo $edit_url; ?>" class="main-color"><?php echo $type->data["current"]->name; ?></a></h4>
          <p><?php echo $type->data["current"]->description; ?></p>
        </div>
      </div>
    
    <?php } ?>
  </div>
</div>

<!-- PROGRAMS -->
<?php
if (count($types)) { ?>
  
  <a href="<?php echo $base_url . "ihfath/admin/lesson-meta/program/add"; ?>" class="cws-button btn btn-md right-icon btn-violet with-icon shape new-angle">
    <?php echo t("Add Lesson Program"); ?> <i class="fa fa-plus"></i>
  </a>
  <div class="heading">
    <h3 class="head-6">
      <!-- Nothing here -->
    </h3>
  </div>
  
<?php } ?>

<div class="view-content">
  <div class="portfolio p-1-col simple" id="container" style="position: relative;">
    <?php
    foreach ($programs as $program) {
      $edit_url  = $base_url . "ihfath/admin/lesson-meta/program/" . $program->pid;
      $type_name = $types[$program->type]->data["current"]->name; ?>
      
      <div class="views-row views-row-1 views-row-odd views-row-first portfolio-item term-15 term-14 ihfath-pi" style="">
        <div class="img-holder" style="float: right;">
          <a href="<?php echo $edit_url; ?>" class="cws-button btn btn-md btn-violet">
            <?php echo $edit_str; ?> <span class="fa fa-pencil"></span>
          </a>
        </div>
        <div class="name-holder">
          <h4><a href="<?php echo $edit_url; ?>" class="main-color ihfath-color-violet"><?php echo $program->data["current"]->name; ?> <i style="color: #dadada; font-weight: normal;">- <?php echo $type_name; ?></i></a></h4>
          <p><?php echo $program->data["current"]->description; ?></p>
        </div>
      </div>
    
    <?php } ?>
  </div>
</div>

<!-- STYLING -->
<style media="screen">
  *.ihfath-color-violet {
    color: #8d6dc4;
  }
  
  .ihfath-vc {
    margin-bottom: 50px;
  }
  
  .portfolio-item.ihfath-pi {
    padding-bottom: 0px;
    margin-bottom: 25px;
  }
</style>
