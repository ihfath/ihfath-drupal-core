<?php

// Get professor role
$roles     = user_roles();
$prof_role = NULL;

foreach ($roles as $rid => $role) {
  if ($role === "professor") {
    $prof_role = $rid;
  }
}

// Get current language
global $language;
$def_lang = $language->language;

// Group programs by type
$types        = $form["#ihfath"]["types"];
$program_list = array();

foreach ($form["#ihfath"]["programs"] as $program) {
  $types[$program->type]->programs[] = $program;

  $program_list[] = (int) $program->pid;
}
?>

<div class="ihfathlsn-programs-widget-wrapper">
  <div class="ihfathlsn-programs-widget-head ihfathlsn-programs-widget-head-enabled">
    <?php

    if (isset($prof_role)) {
      echo t("Ihfath Professor Lesson Types");
    } else {
      echo t("Professor role doesn't exist, please create it");
    }

     ?>
  </div>
  <div class="ihfathlsn-programs-widget-head ihfathlsn-programs-widget-head-disabled ihfathlsn-invisible">
    <span><?php echo t("Ihfath programs disabled for non-professors"); ?></span>
  </div>

  <?php
  if (isset($prof_role)) { ?>

    <div class="ihfathlsn-programs-widget-content">

      <?php
      foreach ($types as $type) { ?>

        <div class="ihfathlsn-programs-type-head">
          <div class="ihfathlsn-programs-type-head-caption">
            <!-- Type name here -->
            <?php
            $type_name = $type->data["current"]->name;
            echo $type_name; ?>
          </div>
          <div class="ihfathlsn-programs-select">
            <?php

            if (!isset($type->programs)) {
              continue;
            }

            foreach ($type->programs as $program) {
              $check_id = "ihfathlsn-programs-option-{$program->pid}"; ?>

              <div class="ihfathlsn-programs-option-wrap">
                <div class="ihfathlsn-programs-option-input">
                  <input type="checkbox" id="<?php echo $check_id; ?>">
                </div>
                <label for="<?php echo $check_id; ?>" class="ihfathlsn-programs-option-caption">
                  <?php
                  $program_name = $program->data["current"]->name;
                  echo $program_name; ?>
                </label>
              </div>

            <?php } ?>

          </div>
        </div>

    <?php } ?>

  </div>

  <?php
  }

  // Add hidden inputs
  echo drupal_render_children($form); ?>
</div>

<?php
if (isset($prof_role)) { ?>

  <script type="text/javascript">
    window.addEventListener("DOMContentLoaded", function() {
      // Only show UI when professor role is checked
      var rid                 = "<?php echo $prof_role; ?>";
      var state               = 1;
      var content             = jSh(".ihfathlsn-programs-widget-content")[0];
      var contentHead         = jSh(".ihfathlsn-programs-widget-head-enabled")[0];
      var contentHeadDisabled = jSh(".ihfathlsn-programs-widget-head-disabled")[0];
      var checkboxes          = jSh("#edit-roles input[type=\"checkbox\"]");
      var checkbox;

      // Find the professor checkbox
      for (var i=0; i<checkboxes.length; i++) {
        var cbox = checkboxes[i];

        if (cbox.value == rid) {
          checkbox = cbox;
        }
      }

      // Swap state when checkbox checked/unchecked
      checkbox.addEventListener("change", function() {
        swapState(Number(this.checked));
      });

      function swapState(newState) {
        console.log("SWAP STATE: " + newState);
        console.trace();

        if (newState !== state) {
          var displayStates         = ["none", "block"];
          var displayStatesDisabled = ["block", "none"];

          content.style.display = displayStates[newState];
          contentHead.style.display = displayStates[newState];
          contentHeadDisabled.style.display = displayStatesDisabled[newState];
        }

        state = newState;
        return newState;
      }

      // Link UI and state with hidden input
      var programInput    = jSh("#ihfathlsn-programs-widget-input");
      var programs        = <?php echo json_encode($program_list); ?>;
      var programMap      = {};
      var currentPrograms = [];

      for (var i=0; i<programs.length; i++) {
        var program = programs[i];

        // Get checkbox
        var programCheckbox = jSh("#ihfathlsn-programs-option-" + program);

        // Add to map
        programMap[program] = programCheckbox;

        // Add properties
        programCheckbox.ihfathProgram = program;

        programCheckbox.addEventListener("change", function() {
          if (this.checked) {
            // Add program
            syncInput(updateCurrentPrograms([this.ihfathProgram]));
          } else {
            // Remove program
            syncInput(updateCurrentPrograms(null, [this.ihfathProgram]));
          }
        });
      }

      // Apply initial visibility state
      swapState(Number(checkbox.checked));

      // Get initial state of data
      currentPrograms = syncInput();

      // Update checkboxes
      syncBoxes(currentPrograms, programs, programMap);

      function updateCurrentPrograms(addArg, removeArg) {
        var add    = addArg || [];
        var remove = removeArg || [];

        for (var i=0; i<add.length; i++) {
          var program = add[i];
          var index   = currentPrograms.indexOf(program);

          if (!~index) {
            currentPrograms.push(program);
          }
        }

        for (var i=0; i<remove.length; i++) {
          var index = currentPrograms.indexOf(remove[i]);

          if (~index) {
            currentPrograms.splice(index, 1);
          }
        }

        return currentPrograms;
      }

      function syncBoxes(state, fullState, boxes) {
        for (var i=0; i<fullState.length; i++) {
          var program    = fullState[i];
          var box        = boxes[program];
          var hasProgram = Boolean(~state.indexOf(program));

          // Set checked or unchecked
          box.checked = hasProgram;
        }
      }

      function syncInput(programs) {
        if (programs) {
          programInput.value = JSON.stringify(programs);

          return programs;
        } else {
          var json = jSh.parseJSON(programInput.value);

          // Check if valid data found
          if (type(json) === "array") {
            for (var i=0; i<json.length; i++) {
              var data = json[i];

              if (!programMap[data]) {
                // Invalid data, return empty array
                return [];
              }
            }
          } else {
            return [];
          }

          // JSON is valid
          return json;
        }
      }

      function type(value) {
        var base = typeof value;

        if (base === "object") {
          return value === null ? "null" : (Array.isArray(value) ? "array" : "object");
        } else {
          return base;
        }
      }
    });
  </script>

<?php
} ?>

<style media="screen">
  .ihfathlsn-invisible {
    display: none;
  }

  #field-ihfathlsn-prof-programs-add-more-wrapper {
    overflow: hidden;
  }

  .ihfathlsn-programs-widget-wrapper {
    margin-top: 10px;
    margin-bottom: 10px;
  }

  .ihfathlsn-programs-widget-head,
  .ihfathlsn-programs-type-head-caption {
    height: 40px;
    padding: 0px 10px;

    background: rgba(0, 0, 0, 0.05);
    color: #595959;
    font-size: 14px;
    line-height: 40px;
  }

  .ihfathlsn-programs-widget-head {
    border-top-right-radius: 3px;
    border-top-left-radius: 3px;
    border-bottom: 1px solid rgba(0, 0, 0, 0.1);
  }

  .ihfathlsn-programs-type-head-caption {
    background: rgba(0, 0, 0, 0.1);
    color: #1D5173;
  }

  .ihfathlsn-programs-widget-head-disabled span {
    font-style: italic;
  }

  .ihfathlsn-programs-widget-head {
    border-bottom: 1px solid rgba(0, 0, 0, 0.05);
  }

  .ihfathlsn-programs-select {
    padding: 10px 0px;

    font-size: 0px;
  }

  .ihfathlsn-programs-option-wrap {
    display: inline-block;
    margin-right: 10px;

    font-size: 0px;
    border: 1px solid rgba(0, 0, 0, 0.05);
    border-radius: 3px;
  }

  .ihfathlsn-programs-option-input {
    display: inline-block;
    vertical-align: top;
    width: 40px;
    height: 40px;

    font-size: 18px;
    line-height: 40px;
    text-align: center;
  }

  .ihfathlsn-programs-option-input input {
    display: inline-block;
  }

  .ihfathlsn-programs-option-caption {
    display: inline-block;
    vertical-align: top;
    height: 40px;
    padding: 0px 10px 0px 0px;

    line-height: 40px;
    font-size: 14px;
  }

  .ihfathlsn-programs-option-caption {
    font-weight: normal;
  }
</style>
