<?php
// Stuff
$roles     = user_roles();
$prof_role = NULL;

foreach ($roles as $rid => $role) {
  if ($role === "professor") {
    $prof_role = $rid;
  }
}
?>

<div class="ihfathlsn-schedule-widget" tabindex="0">
  <div class="ihfathlsn-disabled" id="ihfathlsn-schedule-widget-collapsed-caption">
    <?php
    
    if (isset($prof_role)) {
      echo t("Ihfath schedule disabled for non-professors");
    } else {
      echo t("Professor role doesn't exist, please create it");
    }
    
    ?>
  </div>
  <div class="ihfathlsn-enabled ihfathlsn-invisible" id="ihfathlsn-schedule-widget-expanded-caption">
    <div class="ihfathlsn-schedule-widget-caption-divider">
      Ihfath <?php echo t("Weekly Schedule"); ?> <span class="ihfath-header-action-caption" data-terms="<?php echo t("click to expand") . "|" . t("click to collapse"); ?>">(<?php echo t("click to expand"); ?>)</span>
    </div>
    <div class="ihfathlsn-schedule-widget-caption-divider ihfathlsn-schedule-workhours">
      <span>0</span> <?php echo t("work hours"); ?>
    </div>
    <div class="ihfathlsn-schedule-widget-caption-divider">
      <span>168</span> <?php echo t("total hours"); ?>
    </div>
    <div class="ihfathlsn-schedule-widget-caption-divider ihfathlsn-schedule-widget-caption-divider-neutral ihfathlsn-invisible">
      <?php echo t("Click") . " <span>" . t("save") . "</span> " . t("to apply your changes"); ?>
    </div>
  </div>
  <div class="ihfathlsn-schedule-widget-content ihfathlsn-invisible" id="ihfathlsn-schedule-widget-content">
    <!-- JS populates this -->
  </div>
  
  <?php
  // Add hidden inputs
  echo drupal_render_children($form); ?>
</div>

<?php
if (isset($prof_role)) { ?>

  <!-- Hover Modal -->
  <!-- <div class="ihfathlsn-schedule-widget-chooser">
    <div class="ihfathlsn-schedule-widget-chooser-row ihfathlsn-schedule-widget-chooser-name">
        
    </div>
    <div class="ihfathlsn-schedule-widget-chooser-row ihfathlsn-schedule-widget-chooser-row-option ihfathlsn-schedule-widget-chooser-checked">
      <div class="ihfathlsn-schedule-widget-chooser-checkbox">
        
      </div>
      <div class="ihfathlsn-schedule-widget-chooser-caption">
        
      </div>
    </div>
  </div> -->
  
  <!-- Logic -->
  <script type="text/javascript">
    window.addEventListener("load", function() {
      var rid = "<?php echo $prof_role; ?>";
      
      // Cell state mappings
      var mapStateNames = [
        "<?php echo t("Unavailable"); ?>",
        "<?php echo t("Vacant"); ?>",
        "<?php echo t("Booked"); ?>",
        "<?php echo t("Weekend"); ?>",
        "<?php echo t("Booked by user"); ?>",
      ];
      
      var mapStateColors = [
        "#CC2F1F",
        "#34B324",
        "#1D5FBF",
        "#B32493",
        "#000000", // This shouldn't ever be visualized in the table
      ];
      
      var possibleStates = [
        0, 1, 3
      ];
      
      var dayMap = [
        "<?php echo t("Monday"); ?>",
        "<?php echo t("Tuesday"); ?>",
        "<?php echo t("Wednesday"); ?>",
        "<?php echo t("Thursday"); ?>",
        "<?php echo t("Friday"); ?>",
        "<?php echo t("Saturday"); ?>",
        "<?php echo t("Sunday"); ?>"
      ];
      
      var close         = "<?php echo t("Close"); ?>";
      var closeSchedule = "<?php echo t("Close schedule"); ?>";
      
      // Create the schedule editor
      IhfathWeeklyScheduleEditor({
        rid: rid,
        dayMap: dayMap,
        mapStateNames: mapStateNames,
        mapStateColors: mapStateColors,
        possibleStates: possibleStates,
        closeSchedule: closeSchedule,
        close: close
      });
    });
  </script>
  
<?php
} ?>

<style media="screen">
  
  .ihfathlsn-schedule-widget div.ihfathlsn-invisible,
  .ihfathlsn-schedule-widget div.ihfathlsn-schedule-widget-caption-divider.ihfathlsn-invisible {
    display: none;
  }
  
  .ihfathlsn-schedule-widget {
    margin: 10px 0px;
    padding: 0px;
    overflow: hidden;
    
    background: rgba(0, 0, 0, 0.05);
    border-radius: 3px;
  }
  
  .ihfathlsn-disabled,
  .ihfathlsn-enabled .ihfathlsn-schedule-widget-caption-divider,
  .ihfathlsn-schedule-widget-content-header-day {
    padding: 10px;
  }
  
  .ihfathlsn-disabled,
  .ihfathlsn-enabled,
  .ihfathlsn-schedule-widget-content-header-day {
    font-size: 14px;
  }
  
  .ihfathlsn-disabled {
    position: relative;
    z-index: 10;
    
    box-shadow: 0px 1px 3px rgba(0, 0, 0, 0.25);
    font-style: italic;
  }
  
  .ihfathlsn-enabled {
    position: relative;
    z-index: 10;
    
    box-shadow: 0px 0px 3px rgba(0, 0, 0, 0.5);
  }
  
  .ihfathlsn-enabled,
  .ihfathlsn-schedule-widget-content-header {
    font-size: 0px;
    cursor: pointer;
  }
  
  .ihfathlsn-schedule-widget.ihfathlsn-expanded .ihfathlsn-enabled {
    border-bottom: 1px solid rgba(0, 0, 0, 0.1);
  }
  
  .ihfathlsn-enabled .ihfathlsn-schedule-widget-caption-divider.ihfathlsn-schedule-widget-caption-divider-neutral,
  .ihfathlsn-enabled .ihfathlsn-schedule-widget-caption-divider:first-child,
  .ihfathlsn-schedule-widget .ihfathlsn-schedule-widget-content-header-day {
    color: #595959;
  }
  
  .ihfathlsn-enabled .ihfathlsn-schedule-widget-caption-divider:first-child {
    box-sizing: border-box;
    width: <?php echo 100 * (2 / 7) ?>%;
    border-width: 0px;
  }
  
  .ihfathlsn-enabled .ihfathlsn-schedule-widget-caption-divider:last-child {
    width: auto !important;
  }
  
  .ihfathlsn-enabled .ihfathlsn-schedule-widget-caption-divider:first-child span {
    font-weight: normal;
    font-style: italic;
    opacity: 0.5;
  }
  
  .ihfathlsn-schedule-widget-caption-divider,
  .ihfathlsn-schedule-widget-content-header-day {
    box-sizing: border-box;
    display: inline-block;
    width: <?php echo 100 / 7; ?>%;
    
    color: #1D5173;
    border-left: 1px solid rgba(0, 0, 0, 0.05);
    font-size: 14px;
  }
  
  .ihfathlsn-schedule-widget-content-header-day {
    width: <?php echo 100 / 7; ?>%;
    text-align: center;
    background: rgba(0, 0, 0, 0.05);
  }
  
  .ihfathlsn-schedule-widget-content-header-day:first-child {
    border: 0px;
  }
  
  .ihfathlsn-schedule-widget-content-header .ihfathlsn-schedule-widget-content-header-day:first-child {
    border: 0px;
  }
  
  .ihfathlsn-schedule-widget-caption-divider span {
    font-weight: bold;
  }
  
  /* Cells */
  .ihfathlsn-schedule-cell {
    position: relative;
    display: inline-block;
    box-sizing: border-box;
    padding: 10px;
    width: <?php echo 100 / 7; ?>%;
    
    font-weight: bold;
    border-bottom: 1px solid rgba(255, 255, 255, 0.15);
    color: #fff;
    text-align: center;
    cursor: pointer;
  }
  
  .ihfathlsn-schedule-cell span {
    font-weight: normal;
    opacity: 0.75;
  }
  
  .ihfathlsn-schedule-cell .ihfathlsn-schedule-cell-sel-indicator {
    position: absolute;
    z-index: 10;
    left: 0px;
    top: 0px;
    right: 0px;
    bottom: 0px;
    overflow: hidden;
    
    pointer-events: none;
  }
  
  .ihfathlsn-schedule-cell:hover .ihfathlsn-schedule-cell-sel-indicator::after,
  .ihfathlsn-schedule-cell.ihfathlsn-schedule-cell-sel .ihfathlsn-schedule-cell-sel-indicator::after {
    content: "";
    position: absolute;
    width: 25px;
    height: 25px;
    
    background: #fff;
    opacity: 0.5;
    
    transform: rotate(45deg);
    
    /* Top left corner */
    left: -15px;
    top: -15px;
  }
  
  .ihfathlsn-schedule-cell.ihfathlsn-schedule-cell-sel .ihfathlsn-schedule-cell-sel-indicator::after {
    opacity: 1;
  }
  
  .ihfathlsn-schedule-cell.ihfathlsn-schedule-cell-sel .ihfathlsn-schedule-cell-sel-indicator {
    background: linear-gradient(to top, rgba(255, 255, 255, 0.05), rgba(255, 255, 255, 0.05) 40%, rgba(255, 255, 255, 0.0) 50%);
  }
  
  .ihfathlsn-schedule-cell.ihfathlsn-schedule-cell-sel {
    text-shadow: 0px 1px 2px rgba(0, 0, 0, 0.75);
  }
  
  .ihfathlsn-schedule-cell:hover::after {
    content: "";
    position: absolute;
    z-index: 10;
    left: 0px;
    right: 0px;
    top: 0px;
    bottom: 0px;
    
    background: rgba(0, 0, 0, 0.05);
  }
  
  .ihfathlsn-schedule-widget-content .ihfathlsn-schedule-row:last-child .ihfathlsn-schedule-cell {
    border: 0px;
  }
  
  /* State Modal */
  
  .ihfathlsn-schedule-widget-chooser {
    position: absolute;
    z-index: 100;
    width: 180px;
    overflow: hidden;
    
    border-radius: 3px;
    background: #F2F2F2;
    box-shadow: 0px 3px 6px rgba(0, 0, 0, 0.7);
  }
  
  .ihfathlsn-schedule-widget-chooser-row {
    padding: 0px 5px;
    height: 40px;
  }
  
  .ihfathlsn-schedule-widget-chooser-name,
  .ihfathlsn-schedule-widget-chooser-close-schedule {
    position: relative;
    text-align: center;
    line-height: 40px;
    font-size: 1.1em;
    padding: 0px 40px 0px 0px;
    
    color: #595959;
  }
  
  .ihfathlsn-schedule-widget-chooser-name-content > span {
    font-weight: bold;
  }
  
  .ihfathlsn-schedule-widget-chooser-name-content > span > span {
    font-weight: normal;
    opacity: 0.5;
  }
  
  .ihfathlsn-schedule-widget-chooser-name-close {
    position: absolute;
    right: 0px;
    top: 0px;
    width: 40px;
    height: 40px;
    
    border-left: 1px solid rgba(0, 0, 0, 0.05);
    font-size: 16px;
    color: #808080;
    cursor: pointer;
    text-align: center;
    line-height: 40px
  }
  
  .ihfathlsn-schedule-widget-chooser-name-close:hover {
    background: rgba(0, 0, 0, 0.05);
  }
  
  .ihfathlsn-schedule-widget-chooser-row-option {
    padding: 0px;
    cursor: pointer;
  }
  
  .ihfathlsn-schedule-widget-chooser-checkbox {
    position: relative;
    display: inline-block;
    vertical-align: top;
    height: 40px;
    width: 40px;
  }
  
  .ihfathlsn-schedule-widget-chooser-checked .ihfathlsn-schedule-widget-chooser-checkbox::after {
    content: "";
    position: absolute;
    left: 0px;
    top: 0px;
    right: 0px;
    bottom: 0px;
    margin: 12px;
    
    border-radius: 100%;
    background: rgba(255, 255, 255, 0.85);
  }
  
  .ihfathlsn-schedule-widget-chooser-caption {
    display: inline-block;
    vertical-align: top;
    height: 40px;
    
    line-height: 40px;
    color: #fff;
  }
  
  .ihfathlsn-schedule-widget-chooser-checked .ihfathlsn-schedule-widget-chooser-caption {
    font-weight: bold;
  }
  
  .ihfathlsn-schedule-widget-chooser-close-schedule {
    padding: 0px;
    border-top: rgba(0, 0, 0, 0.05);
    
    text-align: center;
    background: rgba(0, 0, 0, 0.05);
    cursor: pointer;
  }
  
  .ihfathlsn-schedule-widget-chooser-close-schedule:hover {
    background: rgba(0, 0, 0, 0.1);
  }
</style>
