<?php
// Stuff
$roles     = user_roles();
$prof_role = NULL;

foreach ($roles as $rid => $role) {
  if ($role === "professor") {
    $prof_role = $rid;
  }
}
?>

<div class="ihfathlsn-schedule-widget" tabindex="0">
  <div class="ihfathlsn-disabled" id="ihfathlsn-schedule-widget-collapsed-caption">
    <?php
    
    if (isset($prof_role)) {
      echo t("Ihfath schedule disabled for non-professors");
    } else {
      echo t("Professor role doesn't exist, please create it");
    }
    
    ?>
  </div>
  <div class="ihfathlsn-enabled ihfathlsn-invisible" id="ihfathlsn-schedule-widget-expanded-caption">
    <div class="ihfathlsn-schedule-widget-caption-divider">
      Ihfath <?php echo t("Weekly Schedule"); ?> <span>(<?php echo t("click to expand"); ?>)</span>
    </div>
    <div class="ihfathlsn-schedule-widget-caption-divider ihfathlsn-schedule-workhours">
      <span>0</span> <?php echo t("work hours"); ?>
    </div>
    <div class="ihfathlsn-schedule-widget-caption-divider">
      <span>168</span> <?php echo t("total hours"); ?>
    </div>
    <div class="ihfathlsn-schedule-widget-caption-divider ihfathlsn-schedule-widget-caption-divider-neutral ihfathlsn-invisible">
      <?php echo t("Click") . " <span>" . t("save") . "</span> " . t("to apply your changes"); ?>
    </div>
  </div>
  <div class="ihfathlsn-schedule-widget-content-header ihfathlsn-invisible">
    <!-- JS populates this -->
  </div>
  <div class="ihfathlsn-schedule-widget-content ihfathlsn-invisible" id="ihfathlsn-schedule-widget-content">
    <div class="ihfathlsn-schedule-row">
      <div class="ihfathlsn-schedule-cell ihfathlsn-schedule-cell-sel">
        0:48
      </div>
    </div>
  </div>
  
  <?php
  // Add hidden inputs
  echo drupal_render_children($form); ?>
</div>

<?php
if (isset($prof_role)) { ?>

  <!-- Hover Modal -->
  <!-- <div class="ihfathlsn-schedule-widget-chooser">
    <div class="ihfathlsn-schedule-widget-chooser-row ihfathlsn-schedule-widget-chooser-name">
        
    </div>
    <div class="ihfathlsn-schedule-widget-chooser-row ihfathlsn-schedule-widget-chooser-row-option ihfathlsn-schedule-widget-chooser-checked">
      <div class="ihfathlsn-schedule-widget-chooser-checkbox">
        
      </div>
      <div class="ihfathlsn-schedule-widget-chooser-caption">
        
      </div>
    </div>
  </div> -->
  
  <!-- Logic -->
  <script type="text/javascript">
    window.addEventListener("DOMContentLoaded", function() {
      var rid   = "<?php echo $prof_role; ?>";
      var state = null;
      
      var wrapper        = jSh(".ihfathlsn-schedule-widget")[0];
      var collapsed      = jSh("#ihfathlsn-schedule-widget-collapsed-caption");
      var expanded       = jSh("#ihfathlsn-schedule-widget-expanded-caption");
      var contentHead    = jSh(".ihfathlsn-schedule-widget-content-header")[0];
      var content        = jSh("#ihfathlsn-schedule-widget-content");
      var checkboxes     = jSh("#edit-roles input[type=\"checkbox\"]");
      var checkbox;
      
      // Find the professor checkbox
      for (var i=0; i<checkboxes.length; i++) {
        var cbox = checkboxes[i];
        
        if (cbox.value == rid) {
          checkbox = cbox;
        }
      }
      
      // Add event listeners
      checkbox.addEventListener("change", function() {
        // Remove if `professor` role is unchecked
        swapState(Number(this.checked));
      });
      
      content.addEventListener("mousedown", function(e) {
        // Prevent default text selection browser behaviour
        e.preventDefault();
      });
      
      expanded.addEventListener("mousedown", function(e) {
        // Prevent default text selection browser behaviour
        e.preventDefault();
      });
      
      expanded.addEventListener("click", function() {
        // Expand when the bar is clicked
        swapState(state === 1 ? 2 : 1);
      });
      
      wrapper.addEventListener("focus", function() {
        // Blur the wrapper if it gains focus while collapsed
        setTimeout(function() {
          if (state !== 2) {
            wrapper.blur();
          }
        }, 0);
      });
      
      wrapper.addEventListener("keydown", function(e) {
        if (e.ctrlKey) {
          switch (e.keyCode) {
            case 90: // Z key
              // Go back (or forward) in history
              table = applyCellStateHistory(cellStateHistory(null, e.shiftKey), cellMap);
              
              // Update modal
              updateModalFromCell(null, true);
              
              // Update input
              updateInput();
              
              e.preventDefault();
              break;
            case 65: // A key
              // Select all
              selTable = renderCellSelField(genTableCells(null, true, true), selTable, cellMap);
              
              e.preventDefault();
          }
        }
      });
      
      function swapState(newStateRaw, force) {
        var newState = isNaN(newStateRaw + "") ? state : newStateRaw;
        
        if (force || newState !== state) {
          var activeBar     = ["none", "block", "block"];
          var activeContent = ["none", "none", "block"];
          var inactive      = ["block", "none", "none"];
          
          var newContentState = activeContent[newState];
          var emptyElement    = {style: {}};
          
          collapsed.style.display = inactive[newState];
          expanded.style.display = activeBar[newState];
          content.style.display = newContentState;
          contentHead.style.display = newContentState;
          (statusModal || emptyElement).style.display = newContentState;
          
          if (newContentState === "block") {
            wrapper.classList.add("ihfathlsn-expanded");
            wrapper.focus();
          } else {
            wrapper.classList.remove("ihfathlsn-expanded");
            wrapper.blur();
          }
          
          state = newState;
        }
        
        return newState;
      }
      
      // Set initial state
      swapState(Number(checkbox.checked));
      
      // Clear cell wrapper
      content.innerHTML = "";
      
      // Get cell input field
      var cellInput = jSh("#ihfathlsn-schedule-widget-input");
      
      // Set initial cell boundaries
      var boundStartDay  = 1;
      var boundEndDay    = 7;
      var boundStartHour = 0;
      var boundEndHour   = 23;
      
      // Cell state mappings
      var mapStateNames = [
        "<?php echo t("Unavailable"); ?>",
        "<?php echo t("Vacant"); ?>",
        "<?php echo t("Booked"); ?>",
        "<?php echo t("Weekend"); ?>"
      ];
      
      var mapStateColors = [
        "#CC2F1F",
        "#34B324",
        "#1D5FBF",
        "#B32493"
      ];
      
      var possibleStates = [
        0, 1, 3
      ];
      
      function filterState(state) {
        switch (state) {
          case 0:
          case 1:
          case 3:
            return state;
          default:
            return 0;
        }
      }
      
      var dayMap = [
        "<?php echo t("Monday"); ?>",
        "<?php echo t("Tuesday"); ?>",
        "<?php echo t("Wednesday"); ?>",
        "<?php echo t("Thursday"); ?>",
        "<?php echo t("Friday"); ?>",
        "<?php echo t("Saturday"); ?>",
        "<?php echo t("Sunday"); ?>"
      ];
      
      // Day offset shift
      var dayShift = -1;
      
      function getDayShift(x) {
        // Get day offset
        var offset = x + dayShift;
        return offset < 0 ? 7 + offset : (offset > 6 ? offset - 7 : offset);
      }
      
      // Generate days
      var dayUIWrapper = jSh(".ihfathlsn-schedule-widget-content-header")[0];
      var frag         = jSh.docFrag();
      
      for (var x=0; x<7; x++) {
        // Get day offset
        var virtualX = getDayShift(x);
        
        // Add day cell
        frag.appendChild(jSh.d(".ihfathlsn-schedule-widget-content-header-day", dayMap[virtualX]));
      }
      
      // Append all days
      dayUIWrapper.innerHTML = "";
      dayUIWrapper.appendChild(frag);
      
      // Generate cells
      var initialCells = jSh.parseJSON(cellInput.value);
      var cellMap      = [
        [], [], [], [],
        [], [], []
      ];
      
      if (!(initialCells.table && initialCells.bounds)) {
        // Generate the cell JSON table since it isn't available
        initialCells = genTableCells();
        console.log(initialCells);
      } else {
        var bounds = initialCells.bounds;
        
        boundStartDay  = bounds[0];
        boundStartHour = bounds[1];
        boundEndDay    = bounds[2];
        boundEndHour   = bounds[3];
      }
      
      var table         = initialCells.table;
      var selTable      = genTableCells(null, true);
      var selCells      = [];
      var lastSelToggle = true;
      var frag          = jSh.docFrag();
      
      for (var y=0; y<24; y++) {
        var row = jSh.d(".ihfathlsn-schedule-row");
        
        for (var x=0; x<7; x++) {
          // Make cell time in 12hour format
          var yMap     = y === 0 ? 12 : y;
          var cellTime = y < 12 ? [yMap, "AM"] : [(y === 12 ? 12 : yMap - 12), "PM"];
          
          // Get day offset
          var virtualX = getDayShift(x);
          
          var cell = jSh.d(".ihfathlsn-schedule-cell", cellTime[0] + "", [
            jSh.c("span", null, cellTime[1]),
            
            // Cell selection indicator
            jSh.d(".ihfathlsn-schedule-cell-sel-indicator")
          ]);
          
          cell.ihfathVisualCoords = [x, y];
          cell.ihfathScheduleCoords = [virtualX, y];
          cell.ihfathScheduleState = filterState(table[virtualX][y]);
          cell.ihfathDay = dayMap[virtualX];
          cell.ihfathTime = cellTime;
          cell.style.background = mapStateColors[cell.ihfathScheduleState];
          
          cell.updateState = function(state) {
            var filteredState = filterState(state);
            var coords        = this.ihfathScheduleCoords;
            
            this.ihfathScheduleState = filteredState;
            this.style.background    = mapStateColors[filteredState];
          }
          
          // Append cell DOM to cell row and cellMap
          row.appendChild(cell);
          cellMap[virtualX][y] = cell;
        }
        
        frag.appendChild(row);
      }
      
      // Add cells to the cell wrapper
      content.appendChild(frag);
      
      // Setup modal
      var statusModal        = jSh.d(".ihfathlsn-schedule-widget-chooser"); //jSh(".ihfathlsn-schedule-widget-chooser")[0];
      var colorBarMap        = {};
      var statusModalVisible = false;
      var statusModalCRect;
      var currentCell;
      
      // Clear modal
      statusModal.innerHTML = "";
      document.body.appendChild(statusModal);
      
      // Create Modal DOM elements
      var statusModalName        = jSh.c("span", ".ihfathlsn-schedule-widget-chooser-name-content");
      var statusModalNameClose   = jSh.d(".ihfathlsn-schedule-widget-chooser-name-close", "×", null, {title: "<?php echo t("Close"); ?>"});
      var statusModalNameWrapper = jSh.d(".ihfathlsn-schedule-widget-chooser-row.ihfathlsn-schedule-widget-chooser-name", null, [
        statusModalName,
        statusModalNameClose
      ]);
      
      // Close weekly schedule
      var statusModalCloseSchedule = jSh.d({
        sel: ".ihfathlsn-schedule-widget-chooser-row.ihfathlsn-schedule-widget-chooser-close-schedule",
        text: "<?php echo t("Close schedule"); ?>",
        events: {
          click: function() {
            swapState(1);
          }
        }
      });
      
      statusModal.appendChild(statusModalNameWrapper);
      statusModalNameClose.addEventListener("click", hideModal);
      
      // Hide the statusModal initially
      swapState(null, true);
      
      for (var i=0; i<possibleStates.length; i++) {
        var curState = possibleStates[i];
        
        var colorRow     = jSh.d(".ihfathlsn-schedule-widget-chooser-row.ihfathlsn-schedule-widget-chooser-row-option");
        var colorCheck   = jSh.d(".ihfathlsn-schedule-widget-chooser-checkbox");
        var colorCaption = jSh.d(".ihfathlsn-schedule-widget-chooser-caption", mapStateNames[curState]);
        
        colorRow.appendChild(colorCheck, colorCaption);
        
        colorRow.ihfathScheduleState = curState;
        colorRow.style.background = mapStateColors[curState];
        
        colorRow.addEventListener("click", function() {
          updateModalState(this.ihfathScheduleState);
          updateCell(currentCell, this.ihfathScheduleState);
        });
        
        statusModal.appendChild(colorRow);
        colorBarMap[curState] = colorRow;
      }
      
      // Add schedule closing button
      statusModal.appendChild(statusModalCloseSchedule);
      
      // Cell selection state
      var cellExplicitSel = false;
      var cellSelecting   = false;
      var newCellSelState;
      var cellHover;
      var oldSelTable;
      
      // Add mouse events to cell wrapper
      content.addEventListener("mousemove", function(e) {
        var target = e.target;
        
        if (target.ihfathScheduleCoords) {
          if (!cellExplicitSel) {
            if (!newCellSelState && (target !== currentCell || !statusModalVisible)) {
              updateModalFromCell(target);
            }
          } else if (cellSelecting && cellHover !== target) {
            // Reset to old table selection state temporarily
            renderCellSelField(oldSelTable, selTable, cellMap);
            var curCellCoords = currentCell.ihfathScheduleCoords;
            
            if (currentCell === target) {
              // Just render this cell
              selTable = renderCellSelField(mutateCellSelections([[
                curCellCoords[0],
                curCellCoords[1],
                newCellSelState
              ]], oldSelTable), oldSelTable, cellMap);
            } else {
              var affectedCells = [];
              
              // Compute affectedCells
              var targetCellCoords = target.ihfathScheduleCoords;
              
              // Check if targetCell and currentCell in the same column
              if (targetCellCoords[0] === curCellCoords[0]) {
                var diffBase = targetCellCoords[1] - curCellCoords[1];
                var diff     = ((diffBase > 0) - (diffBase < 0)) || +diffBase;
                var bound    = targetCellCoords[1] + diff;
                
                for (var i=curCellCoords[1]; i!==bound; i+=diff) {
                  affectedCells.push([
                    curCellCoords[0],
                    i,
                    newCellSelState
                  ]);
                }
              } else {
                affectedCells.push(
                  [targetCellCoords[0], targetCellCoords[1], newCellSelState],
                  [curCellCoords[0], curCellCoords[1], newCellSelState]
                );
              }
              
              // Render new state
              selTable = renderCellSelField(mutateCellSelections(affectedCells, oldSelTable), oldSelTable, cellMap);
            }
            
            // Set cellHover to prevent redundant repeats of this if block
            cellHover = target;
          }
        }
      });
      
      content.addEventListener("mousedown", function(e) {
        var target = e.target;
        
        if (target.ihfathScheduleCoords) {
          // Prevent implicit cell selection on hovering
          cellExplicitSel = true;
          cellSelecting   = true;
          currentCell     = target;
          
          var coords = target.ihfathScheduleCoords;
          newCellSelState = !selTable[coords[0]][coords[1]];
          
          if (e.ctrlKey) {
            // Select the whole column
            var newSel = [];
            
            for (var y=0; y<24; y++) {
              newSel.push([
                coords[0],
                y,
                newCellSelState
              ]);
            }
          } else {
            // Select single cell
            var newSel = [[
              coords[0],
              coords[1],
              newCellSelState
            ]];
          }
          
          // "Unattach" modal if deselecting
          if (!newCellSelState && !e.shiftKey) {
            cellExplicitSel = false;
          }
          
          // Diff and render new field
          oldSelTable = selTable = renderCellSelField(mutateCellSelections(newSel, e.shiftKey ? selTable : genTableCells(null, true)), selTable, cellMap);
          
          // Update modal
          updateModalFromCell(target, true);
          
          // Add events to stop selecting
          function stopSelecting() {
            cellSelecting = false;
          }
          
          window.addEventListener("mouseup", stopSelecting);
        }
      });
      
      statusModal.addEventListener("click", function() {
        // Focus back on the main wrapper after clicking the modal
        setTimeout(function() {
          wrapper.focus();
        }, 0);
      });
      
      // Functions
      function hideModal() {
        if (statusModalVisible) {
          statusModal.style.display = "none";
          
          statusModalVisible = false;
        }
      }
      
      function updateModalFromCell(cell, force) {
        // Make modal visible
        if (!statusModalVisible) {
          statusModal.style.display = "block";
          
          statusModalVisible = true;
        }
        
        // Update modal cell state
        var sameCell = currentCell === cell;
        currentCell  = cell || currentCell;
        
        if (force || !sameCell) {
          // Set to cell name
          statusModalName.innerHTML = "";
          statusModalName.appendChild([
            jSh.t(currentCell.ihfathDay + " "),
            jSh.c("span", null, currentCell.ihfathTime[0], [
              jSh.c("span", null, currentCell.ihfathTime[1])
            ])
          ]);
          
          // Update modal position to left or right of cell
          var cellCRect   = currentCell.getBoundingClientRect();
          var modalCRect  = statusModalCRect || (statusModalCRect = statusModal.getBoundingClientRect());
          var cellHeight  = cellCRect.bottom - cellCRect.top;
          var modalHeight = modalCRect.bottom - modalCRect.top;
          
          // Compute the horizontal offset (left or right)
          var modalOffset = currentCell.ihfathVisualCoords[0] > 4
                            ? -((modalCRect.right - modalCRect.left))
                            : cellCRect.right - cellCRect.left; // offset - (modalWidth + cellWidth) : offset + cellWidth
          
          statusModal.style.left = (cellCRect.left + scrollX + modalOffset) + "px";
          statusModal.style.top  = (cellCRect.top + scrollY + (cellHeight / 2) - (modalHeight / 2)) + "px";
          
          // Update colors
          updateModalState(currentCell.ihfathScheduleState);
        }
      }
      
      function updateModalState(state) {
        for (var i=0; i<possibleStates.length; i++) {
          var curState = possibleStates[i];
          var curRow   = colorBarMap[curState];
          
          if (curState === state) {
            curRow.classList.add("ihfathlsn-schedule-widget-chooser-checked");
          } else {
            curRow.classList.remove("ihfathlsn-schedule-widget-chooser-checked");
          }
        }
      }
      
      function updateCell(cellTarget, state) {
        // Save history
        cellStateHistory(table);
        
        // Group cells
        var cells = selCells.slice();
        cells.push(cellTarget);
        
        if (state === 3) {
          // Fill whole column for Weekend state
          var columns   = {};
          var columnArr = [];
          var newCells  = [];
          
          for (var i=0; i<cells.length; i++) {
            var cellX = cells[i].ihfathScheduleCoords[0];
            
            if (!columns[cellX]) {
              columns[cellX] = true;
              columnArr.push(cellX);
            }
          }
          
          for (var i=0; i<columnArr.length; i++) {
            var column = columnArr[i];
            
            for (var y=0; y<24; y++) {
              newCells.push(cellMap[column][y]);
            }
          }
          
          // Override cells
          cells = newCells;
        } else {
          // Check if any cells have the Weekend state
          var weekendColumns   = {};
          var weekendColumnArr = [];
          var skipCellIndexes  = {};
          
          for (var i=0; i<cells.length; i++) {
            var cell  = cells[i];
            var cellX = cell.ihfathScheduleCoords[0];
            
            if (weekendColumns[cellX]) {
              skipCellIndexes[i] = true;
            } else if (cell.ihfathScheduleState === 3) {
              skipCellIndexes[i] = true;
              
              weekendColumns[cellX] = true;
              weekendColumnArr.push(cellX);
            }
          }
          
          if (weekendColumnArr.length) {
            var newCells = [];
            
            // Add full columns
            for (var i=0; i<weekendColumnArr.length; i++) {
              var column = weekendColumnArr[i];
              
              for (var y=0; y<24; y++) {
                newCells.push(cellMap[column][y]);
              }
            }
            
            // Add the other cells
            for (var i=0; i<cells.length; i++) {
              // Add if not already added via a Weekend column
              if (!skipCellIndexes[i]) {
                newCells.push(cells[i]);
              }
            }
            
            cells = newCells;
          }
        }
        
        for (var i=0; i<cells.length; i++) {
          var cell = cells[i];
          
          // Update cell visual state
          cell.ihfathScheduleState = state;
          cell.style.background = mapStateColors[state];
          
          // Update actual data
          var coords = cell.ihfathScheduleCoords;
          table[coords[0]][coords[1]] = state;
        }
        
        // Push update to hidden input
        updateInput();
      }
      
      var cellStateRedoHistoryStack = [];
      var cellStateHistoryStack     = [];
      var maxCellStateHistory       = 50;
      function cellStateHistory(history, redo) {
        if (history || redo) {
          // If redo is on, pop out latest redo from stack, and return it
          if (redo) {
            var lastRedo = cellStateRedoHistoryStack.pop();
            
            // Push redo (if any) onto history stack
            lastRedo && cellStateHistoryStack.push(lastRedo);
            
            // Return redo
            return lastRedo || table;
          }
          
          // Clear redos if not empty
          if (cellStateRedoHistoryStack.length > 0) {
            cellStateRedoHistoryStack = [];
          }
          
          // Copy the old history into a new array
          var historyCopy = [];
          
          for (var x=0; x<7; x++) {
            historyCopy.push(history[x].slice());
          }
          
          // Push to history stack
          cellStateHistoryStack.push(historyCopy);
          
          // Truncate stack if too large
          if (cellStateHistoryStack.length > maxCellStateHistory) {
            cellStateHistoryStack.shift();
          }
          
          return historyCopy;
        } else {
          // Pop the older history out
          var historyStack = cellStateHistoryStack.pop();
          
          // Save to redo stack
          historyStack && cellStateRedoHistoryStack.push(historyStack);
          
          return historyStack || table;
        }
      }
      
      function applyCellStateHistory(history, cellMap) {
        for (var x=0; x<7; x++) {
          for (var y=0; y<24; y++) {
            var cell     = cellMap[x][y];
            var newState = history[x][y];
            
            if (newState !== cell.ihfathScheduleState) {
              cell.ihfathScheduleState = newState;
              cell.style.background = mapStateColors[newState];
            }
          }
        }
        
        return history;
      }
      
      function mutateCellSelections(cells, oldField) {
        var newField = [];
        
        // Copy oldField to newField
        for (var i=0; i<7; i++) {
          newField.push(oldField[i].slice());
        }
        
        // Mutate cells
        for (var i=0; i<cells.length; i++) {
          var cellState = cells[i];
          
          newField[cellState[0]][cellState[1]] = cellState[2];
        }
        
        return newField;
      }
      
      function renderCellSelField(newField, oldField, cellMap) {
        // Reset selCells
        selCells = [];
        
        // Render a new cell selection field
        for (var x=0; x<7; x++) {
          for (var y=0; y<24; y++) {
            var cell     = cellMap[x][y];
            var newState = newField[x][y];
            
            // Add to selCells if selected
            if (newState) {
              selCells.push(cell);
            }
            
            if (newState !== oldField[x][y]) {
              if (newState) {
                cell.classList.add("ihfathlsn-schedule-cell-sel");
              } else {
                cell.classList.remove("ihfathlsn-schedule-cell-sel");
              }
            }
          }
        }
        
        return newField;
      }
      
      var workingHoursUI = jSh(".ihfathlsn-schedule-workhours")[0].jSh(0);
      function scanBounds(table) {
        // Calculate working hours and bounds
        var workingHours = 0;
        
        var startDay   = 1;
        var endDay     = 7;
        var startHour  = 0;
        var endHour    = 23;
        var foundStart = false;
        
        for (var x=0; x<7; x++) {
          for (var y=0; y<24; y++) {
            var cell  = cellMap[x][y];
            var state = cell.ihfathScheduleState;
            
            if (state === 1) {
              // Increment working hours
              workingHours++;
              
              if (foundStart) {
                // Set to the end
                endDay  = x + 1;
                endHour = Math.max(endHour, y);
              } else {
                // Set to the start
                startDay  = x + 1;
                startHour = y;
                
                // Set base endHour
                endHour = startHour;
                
                foundStart = true;
              }
            }
          }
        }
        
        // Update working hours UI
        workingHoursUI.textContent = workingHours;
        
        // Return bounds
        return [
          startDay,
          endDay,
          startHour,
          endHour
        ];
      }
      
      // Update working hours
      scanBounds(table);
      
      function updateInput() {
        var bounds = scanBounds(table);
        
        cellInput.value = JSON.stringify({
          bounds: bounds,
          table: table
        });
      }
      
      function genTableCells(map, selMap, newValue) {
        var useNewValue = newValue !== undefined;
        
        if (selMap) {
          var initialState = useNewValue ? newValue : false;
        } else {
          var initialState = useNewValue ? newValue : 0;
        }
        
        if (map) {
          // TODO: Check if this `if` and `map` are required
        } else {
          var table = [];
          
          for (var i=0; i<7; i++) {
            var day = [];
            
            for (var j=0; j<24; j++) {
              day.push(initialState);
            }
            
            table.push(day);
          }
          
          if (selMap) {
            return table;
          } else {
            return {
              bounds: [1, 7, 0, 23],
              table: table
            }
          }
        }
      }
    });
  </script>
  
<?php
} ?>

<style media="screen">
  
  .ihfathlsn-schedule-widget div.ihfathlsn-invisible,
  .ihfathlsn-schedule-widget div.ihfathlsn-schedule-widget-caption-divider.ihfathlsn-invisible {
    display: none;
  }
  
  .ihfathlsn-schedule-widget {
    margin: 10px 0px;
    padding: 0px;
    overflow: hidden;
    
    background: rgba(0, 0, 0, 0.05);
    border-radius: 3px;
  }
  
  .ihfathlsn-disabled,
  .ihfathlsn-enabled .ihfathlsn-schedule-widget-caption-divider,
  .ihfathlsn-schedule-widget-content-header-day {
    padding: 10px;
  }
  
  .ihfathlsn-disabled,
  .ihfathlsn-enabled,
  .ihfathlsn-schedule-widget-content-header-day {
    font-size: 14px;
  }
  
  .ihfathlsn-disabled {
    position: relative;
    z-index: 10;
    
    box-shadow: 0px 1px 3px rgba(0, 0, 0, 0.25);
    font-style: italic;
  }
  
  .ihfathlsn-enabled {
    position: relative;
    z-index: 10;
    
    box-shadow: 0px 0px 3px rgba(0, 0, 0, 0.5);
  }
  
  .ihfathlsn-enabled,
  .ihfathlsn-schedule-widget-content-header {
    font-size: 0px;
    cursor: pointer;
  }
  
  .ihfathlsn-schedule-widget.ihfathlsn-expanded .ihfathlsn-enabled {
    border-bottom: 1px solid rgba(0, 0, 0, 0.1);
  }
  
  .ihfathlsn-enabled .ihfathlsn-schedule-widget-caption-divider.ihfathlsn-schedule-widget-caption-divider-neutral,
  .ihfathlsn-enabled .ihfathlsn-schedule-widget-caption-divider:first-child,
  .ihfathlsn-schedule-widget .ihfathlsn-schedule-widget-content-header-day {
    color: #595959;
  }
  
  .ihfathlsn-enabled .ihfathlsn-schedule-widget-caption-divider:first-child {
    box-sizing: border-box;
    width: <?php echo 100 * (2 / 7) ?>%;
    border-width: 0px;
  }
  
  .ihfathlsn-enabled .ihfathlsn-schedule-widget-caption-divider:last-child {
    width: auto !important;
  }
  
  .ihfathlsn-enabled .ihfathlsn-schedule-widget-caption-divider:first-child span {
    font-weight: normal;
    font-style: italic;
    opacity: 0.5;
  }
  
  .ihfathlsn-schedule-widget-caption-divider,
  .ihfathlsn-schedule-widget-content-header-day {
    box-sizing: border-box;
    display: inline-block;
    width: <?php echo 100 / 7; ?>%;
    
    color: #1D5173;
    border-left: 1px solid rgba(0, 0, 0, 0.05);
    font-size: 14px;
  }
  
  .ihfathlsn-schedule-widget-content-header-day {
    width: <?php echo 100 / 7; ?>%;
    text-align: center;
    background: rgba(0, 0, 0, 0.05);
  }
  
  .ihfathlsn-schedule-widget-content-header-day:first-child {
    border: 0px;
  }
  
  .ihfathlsn-schedule-widget-content-header .ihfathlsn-schedule-widget-content-header-day:first-child {
    border: 0px;
  }
  
  .ihfathlsn-schedule-widget-caption-divider span {
    font-weight: bold;
  }
  
  /* Cells */
  .ihfathlsn-schedule-cell {
    position: relative;
    display: inline-block;
    box-sizing: border-box;
    padding: 10px;
    width: <?php echo 100 / 7; ?>%;
    
    font-weight: bold;
    border-bottom: 1px solid rgba(255, 255, 255, 0.15);
    color: #fff;
    text-align: center;
    cursor: pointer;
  }
  
  .ihfathlsn-schedule-cell span {
    font-weight: normal;
    opacity: 0.75;
  }
  
  .ihfathlsn-schedule-cell .ihfathlsn-schedule-cell-sel-indicator {
    position: absolute;
    z-index: 10;
    left: 0px;
    top: 0px;
    right: 0px;
    bottom: 0px;
    overflow: hidden;
    
    pointer-events: none;
  }
  
  .ihfathlsn-schedule-cell:hover .ihfathlsn-schedule-cell-sel-indicator::after,
  .ihfathlsn-schedule-cell.ihfathlsn-schedule-cell-sel .ihfathlsn-schedule-cell-sel-indicator::after {
    content: "";
    position: absolute;
    width: 25px;
    height: 25px;
    
    background: #fff;
    opacity: 0.5;
    
    transform: rotate(45deg);
    
    /* Top left corner */
    left: -15px;
    top: -15px;
  }
  
  .ihfathlsn-schedule-cell.ihfathlsn-schedule-cell-sel .ihfathlsn-schedule-cell-sel-indicator::after {
    opacity: 1;
  }
  
  .ihfathlsn-schedule-cell.ihfathlsn-schedule-cell-sel .ihfathlsn-schedule-cell-sel-indicator {
    background: linear-gradient(to top, rgba(255, 255, 255, 0.05), rgba(255, 255, 255, 0.05) 40%, rgba(255, 255, 255, 0.0) 50%);
  }
  
  .ihfathlsn-schedule-cell.ihfathlsn-schedule-cell-sel {
    text-shadow: 0px 1px 2px rgba(0, 0, 0, 0.75);
  }
  
  .ihfathlsn-schedule-cell:hover::after {
    content: "";
    position: absolute;
    z-index: 10;
    left: 0px;
    right: 0px;
    top: 0px;
    bottom: 0px;
    
    background: rgba(0, 0, 0, 0.05);
  }
  
  .ihfathlsn-schedule-widget-content .ihfathlsn-schedule-row:last-child .ihfathlsn-schedule-cell {
    border: 0px;
  }
  
  /* State Modal */
  
  .ihfathlsn-schedule-widget-chooser {
    position: absolute;
    z-index: 100;
    width: 180px;
    overflow: hidden;
    
    border-radius: 3px;
    background: #F2F2F2;
    box-shadow: 0px 3px 6px rgba(0, 0, 0, 0.7);
  }
  
  .ihfathlsn-schedule-widget-chooser-row {
    padding: 0px 5px;
    height: 40px;
  }
  
  .ihfathlsn-schedule-widget-chooser-name,
  .ihfathlsn-schedule-widget-chooser-close-schedule {
    position: relative;
    text-align: center;
    line-height: 40px;
    font-size: 1.1em;
    padding: 0px 40px 0px 0px;
    
    color: #595959;
  }
  
  .ihfathlsn-schedule-widget-chooser-name-content > span {
    font-weight: bold;
  }
  
  .ihfathlsn-schedule-widget-chooser-name-content > span > span {
    font-weight: normal;
    opacity: 0.5;
  }
  
  .ihfathlsn-schedule-widget-chooser-name-close {
    position: absolute;
    right: 0px;
    top: 0px;
    width: 40px;
    height: 40px;
    
    border-left: 1px solid rgba(0, 0, 0, 0.05);
    font-size: 16px;
    color: #808080;
    cursor: pointer;
    text-align: center;
    line-height: 40px
  }
  
  .ihfathlsn-schedule-widget-chooser-name-close:hover {
    background: rgba(0, 0, 0, 0.05);
  }
  
  .ihfathlsn-schedule-widget-chooser-row-option {
    padding: 0px;
    cursor: pointer;
  }
  
  .ihfathlsn-schedule-widget-chooser-checkbox {
    position: relative;
    display: inline-block;
    vertical-align: top;
    height: 40px;
    width: 40px;
  }
  
  .ihfathlsn-schedule-widget-chooser-checked .ihfathlsn-schedule-widget-chooser-checkbox::after {
    content: "";
    position: absolute;
    left: 0px;
    top: 0px;
    right: 0px;
    bottom: 0px;
    margin: 12px;
    
    border-radius: 100%;
    background: rgba(255, 255, 255, 0.85);
  }
  
  .ihfathlsn-schedule-widget-chooser-caption {
    display: inline-block;
    vertical-align: top;
    height: 40px;
    
    line-height: 40px;
    color: #fff;
  }
  
  .ihfathlsn-schedule-widget-chooser-checked .ihfathlsn-schedule-widget-chooser-caption {
    font-weight: bold;
  }
  
  .ihfathlsn-schedule-widget-chooser-close-schedule {
    padding: 0px;
    border-top: rgba(0, 0, 0, 0.05);
    
    text-align: center;
    background: rgba(0, 0, 0, 0.05);
    cursor: pointer;
  }
  
  .ihfathlsn-schedule-widget-chooser-close-schedule:hover {
    background: rgba(0, 0, 0, 0.1);
  }
</style>
