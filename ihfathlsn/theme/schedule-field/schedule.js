// Ihfath admin professor schedule editor JS source

function IhfathWeeklyScheduleEditor(uiModel) {
  var options = {
    element: jSh("#ihfathlsn-schedule-widget-content"),
    focus: jSh(".ihfathlsn-schedule-widget")[0],
    name: "ihfath-prof-schedule-editor",
    
    dimensions: [
      7, 24
    ],
    
    offsetX: 0,
    offsetY: 0,
    
    canSelect: true,
    history: true,
    clipboard: true,
    
    headers: {
      top: scheduleOnNewHeaderCell
    },
    
    newCell: scheduleOnNewCell,
    newRow: scheduleOnNewRow,
    onCellSelectChange: scheduleOnCellSelectChange,
    onCellStateChange: scheduleOnCellStateChange
  };
  
  // Visibility state
  var state = null;
  
  var wrapper      = options.focus;
  var collapsed    = jSh("#ihfathlsn-schedule-widget-collapsed-caption");
  var expanded     = jSh("#ihfathlsn-schedule-widget-expanded-caption");
  var expandedText = expanded.jSh(".ihfath-header-action-caption")[0];
  var content      = options.element;
  var checkboxes   = jSh("#edit-roles input[type=\"checkbox\"]");
  var checkbox;
  
  // Find the professor checkbox
  for (var i=0; i<checkboxes.length; i++) {
    var cbox = checkboxes[i];
    
    if (cbox.value == uiModel.rid) {
      checkbox = cbox;
    }
  }
  
  // Add event listeners
  checkbox.addEventListener("change", function() {
    // Remove if `professor` role is unchecked
    swapState(Number(this.checked));
  });
  
  content.addEventListener("mousedown", function(e) {
    // Prevent default text selection browser behaviour
    e.preventDefault();
  });
  
  expanded.addEventListener("mousedown", function(e) {
    // Prevent default text selection browser behaviour
    e.preventDefault();
  });
  
  expanded.addEventListener("click", function() {
    // Expand when the bar is clicked
    swapState(state === 1 ? 2 : 1);
  });
  
  wrapper.addEventListener("focus", function() {
    // Blur the wrapper if it gains focus while collapsed
    setTimeout(function() {
      if (state !== 2) {
        wrapper.blur();
      }
    }, 0);
  });
  
  // Expand/collapse captions
  var expandedTextTerms = expandedText.dataset.terms.split("|");
  
  function swapState(newStateRaw, force) {
    var newState = isNaN(newStateRaw + "") ? state : newStateRaw;
    
    if (force || newState !== state) {
      var activeBar      = ["none", "block", "block"];
      var activeContent  = ["none", "none", "block"];
      var inactive       = ["block", "none", "none"];
      var expandedPhrase = [0, 0, 1];
      
      var newContentState = activeContent[newState];
      var emptyElement    = {style: {}};
      
      expandedText.textContent = "(" + expandedTextTerms[expandedPhrase[newState]] + ")";
      collapsed.style.display = inactive[newState];
      expanded.style.display = activeBar[newState];
      content.style.display = newContentState;
      (statusModal || emptyElement).style.display = newContentState;
      
      if (newContentState === "block") {
        wrapper.classList.add("ihfathlsn-expanded");
        wrapper.focus();
      } else {
        wrapper.classList.remove("ihfathlsn-expanded");
        wrapper.blur();
      }
      
      state = newState;
    }
    
    return newState;
  }
  
  // Set initial state
  swapState(Number(checkbox.checked));
  
  // Clear cell wrapper
  content.innerHTML = "";
  
  // Get cell input field
  var cellInput = jSh("#ihfathlsn-schedule-widget-input");
  
  // Get uiModel
  var rid            = uiModel.rid;
  var dayMap         = uiModel.dayMap;
  var mapStateNames  = uiModel.mapStateNames;
  var mapStateColors = uiModel.mapStateColors;
  var possibleStates = uiModel.possibleStates;
  
  // Day offset shift
  var dayShift = -1;
  
  // filterState utility
  function filterState(state) {
    switch (state) {
      case 0:
      case 1:
      case 3:
        return state;
      default:
        return 0;
    }
  }
  
  // Create new schedule editor
  var schedule = Cellidi(options);
  
  // IhfathSchedule DOM processing functions
  function scheduleOnNewCell(x, y, virtualX, virtualY, data, cellDisplay) {
    y = virtualY;
    
    // Make cell time in 12hour format
    var yMap     = y === 0 ? 12 : y;
    var cellTime = y < 12 ? [yMap, "AM"] : [(y === 12 ? 12 : yMap - 12), "PM"];
    
    var cell = jSh.d(".ihfathlsn-schedule-cell", cellTime[0] + "", [
      jSh.c("span", null, cellTime[1]),
      
      // Cell selection indicator
      jSh.d(".ihfathlsn-schedule-cell-sel-indicator")
    ]);
    
    return {
      dom: cell,
      
      data: {
        ihfathTime: cellTime,
        ihfathDay: uiModel.dayMap[virtualX]
      }
    };
  }
  
  function scheduleOnNewRow(row) {
    row.classList.add("ihfathlsn-schedule-row");
  }
  
  function scheduleOnNewHeaderCell(x, virtualX, orientation, cd) {
    return jSh.d(".ihfathlsn-schedule-widget-content-header-day", uiModel.dayMap[virtualX]);
  }
  
  function scheduleOnCellSelectChange(selected, cellModels) {
    for (var i=0; i<selected.length; i++) {
      var toggled = selected[i];
      var cell    = cellModels[i];
      var dom     = cell.dom;
      
      if (toggled) {
        dom.classList.add("ihfathlsn-schedule-cell-sel");
      } else {
        dom.classList.remove("ihfathlsn-schedule-cell-sel");
      }
    }
  }
  
  function scheduleOnCellStateChange(state, cellModels) {
    for (var i=0; i<state.length; i++) {
      var newState = state[i];
      var cell     = cellModels[i];
      var dom      = cell.dom;
      
      dom.style.background = mapStateColors[newState];
    }
  }
  
  // Change the header's classname
  schedule.headers.domTop.classList.add("ihfathlsn-schedule-widget-content-header");
  
  // Create modal
  var statusModal        = jSh.d(".ihfathlsn-schedule-widget-chooser"); //jSh(".ihfathlsn-schedule-widget-chooser")[0];
  var colorBarMap        = {};
  var statusModalVisible = false;
  var statusModalCRect;
  var currentCell;
  
  // Clear modal
  statusModal.innerHTML = "";
  document.body.appendChild(statusModal);
  
  // Create Modal DOM elements
  var statusModalName        = jSh.c("span", ".ihfathlsn-schedule-widget-chooser-name-content");
  var statusModalNameClose   = jSh.d(".ihfathlsn-schedule-widget-chooser-name-close", "×", null, {title: uiModel.close});
  var statusModalNameWrapper = jSh.d(".ihfathlsn-schedule-widget-chooser-row.ihfathlsn-schedule-widget-chooser-name", null, [
    statusModalName,
    statusModalNameClose
  ]);
  
  // Close weekly schedule
  var statusModalCloseSchedule = jSh.d({
    sel: ".ihfathlsn-schedule-widget-chooser-row.ihfathlsn-schedule-widget-chooser-close-schedule",
    text: uiModel.closeSchedule,
    events: {
      click: function() {
        swapState(1);
      }
    }
  });
  
  statusModal.appendChild(statusModalNameWrapper);
  statusModalNameClose.addEventListener("click", hideModal);
  
  for (var i=0; i<possibleStates.length; i++) {
    var curState = possibleStates[i];
    
    var colorRow     = jSh.d(".ihfathlsn-schedule-widget-chooser-row.ihfathlsn-schedule-widget-chooser-row-option");
    var colorCheck   = jSh.d(".ihfathlsn-schedule-widget-chooser-checkbox");
    var colorCaption = jSh.d(".ihfathlsn-schedule-widget-chooser-caption", mapStateNames[curState]);
    
    colorRow.appendChild(colorCheck, colorCaption);
    
    colorRow.ihfathScheduleState = curState;
    colorRow.style.background = mapStateColors[curState];
    
    colorRow.addEventListener("click", function() {
      updateModalState(this.ihfathScheduleState);
      
      // Propagated to schedule
      // schedule.changeSelection(this.ihfathScheduleState);
      updateCellState(this.ihfathScheduleState);
    });
    
    statusModal.appendChild(colorRow);
    colorBarMap[curState] = colorRow;
  }
  
  // Add schedule closing button
  statusModal.appendChild(statusModalCloseSchedule);
  
  // Bind modal to selected cell
  schedule.addStateListener("selectedCell", function(cell) {
    updateModalFromCell(cell);
  });
  
  // Count work hours and save to cellInput
  schedule.on("newstate", function(e) {
    var state  = schedule.state;
    var bounds = scanBounds(state);
    
    cellInput.value = JSON.stringify({
      bounds: bounds,
      table: state
    });
    
    // Update modal
    updateModalFromCell();
  });
  
  statusModal.addEventListener("click", function() {
    // Focus back on the main wrapper after clicking the modal
    setTimeout(function() {
      wrapper.focus();
    }, 0);
  });
  
  // Functions
  function hideModal() {
    if (statusModalVisible) {
      statusModal.style.display = "none";
      
      statusModalVisible = false;
    }
  }
  
  function updateModalFromCell(cell, force) {
    // Make modal visible
    if (!statusModalVisible) {
      statusModal.style.display = "block";
      
      statusModalVisible = true;
    }
    
    // Update modal cell state
    var sameCell = currentCell === cell;
    currentCell  = cell || currentCell;
    
    if (force || !sameCell && cell) {
      // Set to cell name
      statusModalName.innerHTML = "";
      statusModalName.appendChild([
        jSh.t(currentCell.data.ihfathDay + " "),
        jSh.c("span", null, currentCell.data.ihfathTime[0], [
          jSh.c("span", null, currentCell.data.ihfathTime[1])
        ])
      ]);
      
      // Update modal position to left or right of cell
      var cellCRect   = currentCell.dom.getBoundingClientRect();
      var modalCRect  = statusModalCRect || (statusModalCRect = statusModal.getBoundingClientRect());
      var cellHeight  = cellCRect.bottom - cellCRect.top;
      var modalHeight = modalCRect.bottom - modalCRect.top;
      
      // Compute the horizontal offset (left or right)
      var modalOffset = currentCell.coords[0] > 4
                        ? -((modalCRect.right - modalCRect.left))
                        : cellCRect.right - cellCRect.left; // offset - (modalWidth + cellWidth) : offset + cellWidth
      
      statusModal.style.left = (cellCRect.left + scrollX + modalOffset) + "px";
      statusModal.style.top  = (cellCRect.top + scrollY + (cellHeight / 2) - (modalHeight / 2)) + "px";
    }
    
    // Update colors
    if (currentCell) {
      updateModalState(schedule.getCellState(currentCell));
    }
  }
  
  function updateModalState(state) {
    for (var i=0; i<possibleStates.length; i++) {
      var curState = possibleStates[i];
      var curRow   = colorBarMap[curState];
      
      if (curState === state) {
        curRow.classList.add("ihfathlsn-schedule-widget-chooser-checked");
      } else {
        curRow.classList.remove("ihfathlsn-schedule-widget-chooser-checked");
      }
    }
  }
  
  function updateCellState(state) {
    var selection = schedule.selection;
    var columns   = {};
    var height    = schedule.options.dimensions[1];
    
    if (state === 3) {
      // Change EVERYTHING (in this column) to purple, thank you
      schedule.mapEachCell(selection, function(selected, x, y) {
        if (selected) {
          columns[x] = x;
        }
      });
      
      // Redo selection
      var empty      = schedule.genStateTable(false);
      var filled     = schedule.genStateTable(false);
      var columnKeys = Object.getOwnPropertyNames(columns);
      
      for (var i=0; i<columnKeys.length; i++) {
        var column = columns[columnKeys[i]];
        
        for (var y=0; y<height; y++) {
          filled[column][y] = true;
        }
      }
      
      // Apply the changes
      schedule.changeSubset(schedule.diffStateTables(filled, empty).coords, state);
    } else {
      // Check for ANYTHING purple/weekend, and FILL their columns
      schedule.mapEachCell(selection, function(selection, x, y, refs) {
        if (selection && refs[0] === 3) {
          columns[x] = x;
        }
      }, [schedule._state]);
      
      // Redo selection
      var empty   = schedule.genStateTable(false);
      var filled  = schedule.genStateTable(selection);
      var colKeys = Object.getOwnPropertyNames(columns);
      
      for (var i=0; i<colKeys.length; i++) {
        var column = columns[colKeys[i]];
        
        for (var y=0; y<height; y++) {
          filled[column][y] = true;
        }
      }
      
      // Apply the changes
      schedule.changeSubset(schedule.diffStateTables(filled, empty).coords, state);
    }
  }
  
  var workingHoursUI = jSh(".ihfathlsn-schedule-workhours")[0].jSh(0);
  function scanBounds(table) {
    // Calculate working hours and bounds
    var workingHours = 0;
    
    var startDay   = 1;
    var endDay     = 7;
    var startHour  = 0;
    var endHour    = 23;
    var foundStart = false;
    
    for (var x=0; x<7; x++) {
      for (var y=0; y<24; y++) {
        var state = table[x][y];
        
        if (state === 1) {
          // Increment working hours
          workingHours++;
          
          if (foundStart) {
            // Set to the end
            endDay  = x + 1;
            endHour = Math.max(endHour, y);
            startHour = Math.min(startHour, y);
          } else {
            // Set to the start
            startDay  = x + 1;
            startHour = y;
            
            // Set base endHour
            endHour = startHour;
            
            foundStart = true;
          }
        }
      }
    }
    
    // Update working hours UI
    workingHoursUI.textContent = workingHours;
    
    // Return bounds
    return [
      startDay,
      endDay,
      startHour,
      endHour
    ];
  }
  
  // Try to update from the value of `cellInput`
  var currentStateValue = jSh.parseJSON(cellInput.value);
  
  if (currentStateValue.error || !currentStateValue.bounds) {
    // Just fill it with red
    schedule.state = schedule.genStateTable(0);
  } else {
    // Valid data, render it
    schedule.state = currentStateValue.table;
  }
  
  // Hide the statusModal initially
  swapState(null, true);
}
