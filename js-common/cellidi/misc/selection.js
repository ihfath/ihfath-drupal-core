// Cellidi CellDisplay selection functionality

function addSelection(cd) {
  var oldSelection   = cd.genStateTable(false);
  var selection      = cd.genStateTable(false);
  var options        = cd.options;
  var focus          = options.focus || cd.wrapper;
  var emptyFunc      = options.emptyState || undefined;
  var linkedCells    = cd._linkedCellsList.length && cd._linkedCellsList;
  var selectedCell   = null;
  var selectingState = true;
  
  // Add selecting state to CellDisplay
  cd.setState("selecting", false);
  
  // Expose `selection` to other components
  cd.setState("selection", cd.genStateTable(false));
  
  // Add newselection event to CellDisplay
  cd.addEvent("newselection");
  
  // Update selection
  cd.on("newselection", function() {
    cd.states.selection.stateStatus = cd.genStateTable(selection);
  });
  
  // CellDisplay listeners
  
  // Receive new selection other component
  cd.addStateListener("selection", function(newTable) {
    // Render new selection
    selection = renderSelDiff(cd, newTable, selection, linkedCells, emptyFunc);
    
    // Trigger event
    cd.triggerEvent("newselection", {
      selection: cd.genStateTable(selection)
    });
    
    // Replace older selection table
    oldSelection = selection;
  });
  
  // Shortcut key select all (Ctrl + A) (TODO: Have to rework this to work with `rebuild` event)
  focus.addEventListener("keydown", function(e) {
    if (e.ctrlKey && e.keyCode === 65 /* A Key */) {
      var fullSelection = cd.genStateTable(true);
      
      selection = renderSelDiff(cd, fullSelection, selection, linkedCells, emptyFunc);
      cd.triggerEvent("newselection", {
        selection: cd.genStateTable(selection)
      });
      
      e.preventDefault();
    }
  });
  
  // Mouse selection
  cd.on("activeinput", function(e) {
    var cell = e.cell;
    
    if (cell && !e.header) {
      cd.selecting = true;
      
      if (cell === selectedCell) {
        cd.selectedCell = selectedCell = null;
      } else {
        cd.selectedCell = selectedCell = cell;
      }
      
      var tmpTable;
      var newTable;
      
      if (e.event.shiftKey) {
        // Just change the current cells
        tmpTable = cd.genStateTable(selection);
        var cellCoords = selectedCell.coords;
        
        selectingState = !tmpTable[cellCoords[0]][cellCoords[1]];
        
        if (e.event.ctrlKey) {
          var x      = cellCoords[0];
          var height = cd.options.dimensions[1];
          
          for (var y=0; y<height; y++) {
            tmpTable[x][y] = selectingState;
          }
        } else {
          tmpTable[cellCoords[0]][cellCoords[1]] = selectingState;
        }
        
        newTable = renderSelDiff(cd, tmpTable, selection, linkedCells, emptyFunc);
      } else {
        // Make all cells unselected then select this one
        tmpTable = cd.genStateTable(false);
        var coords = cell.coords;
        
        if (e.event.ctrlKey) {
          var curX      = coords[0];
          var curHeight = cd.options.dimensions[1];
          
          for (var y=0; y<curHeight; y++) {
            tmpTable[curX][y] = true;
          }
        } else {
          tmpTable[coords[0]][coords[1]] = true;
        }
        
        newTable = renderSelDiff(cd, tmpTable, selection, linkedCells, emptyFunc);
      }
      
      oldSelection = newTable;
      selection    = newTable;
      cd.triggerEvent("newselection", {
        selection: cd.genStateTable(newTable)
      });
    }
  });
  
  cd.on("blurinput", function() {
    cd.selecting = false;
  });
  
  cd.addStateListener("hoveredCell", function(cell) {
    if (cd.selecting && selectedCell) {
      if (cell === selectedCell) {
        // Just go back to the old selection table with the current cell (un)selected
        selection = renderSelDiff(cd, oldSelection, selection, linkedCells, emptyFunc);
      } else {
        //
        var newTable      = cd.genStateTable(oldSelection);
        var cellCoords    = cell.coords;
        var selCellCoords = selectedCell.coords;
        
        // Select a partial column if both cells are on the same column
        if (cellCoords[0] === selCellCoords[0]) {
          var startCoords = Math.min(cellCoords[1], selCellCoords[1]);
          var endCoords   = Math.max(cellCoords[1], selCellCoords[1]) + 1;
          
          var xCoord = cellCoords[0];
          
          for (var y=startCoords; y<endCoords; y++) {
            newTable[xCoord][y] = selectingState;
          }
        } else {
          newTable[cellCoords[0]][cellCoords[1]] = selectingState;
        }
        
        // Render (un)selected cells
        selection = renderSelDiff(cd, newTable, selection, linkedCells, emptyFunc);
        
        // Trigger newselection event
        cd.triggerEvent("newselection", {
          selection: cd.genStateTable(selection)
        });
      }
    }
  });
  
  // Keep track of selection count
  cd.on("newselection", function(e) {
    // TODO: Implement this...
  });
  
  // Reset when cellDisplay is rebuilt
  cd.on("rebuild", function(e) {
    linkedCells    = cd._linkedCellsList.length && cd._linkedCellsList;
    selectedCell   = null;
    selectingState = true;
    
    // FIXME: Finish this...
  });
}

function renderSelDiff(cd, newSelTable, oldSelTable, linkedCells, emptyFunc) {
  if (linkedCells) {
    // Clear any linked cells (cells in the space of cellspans)
    var emptyState = false;
    
    for (var i=0; i<linkedCells.length; i++) {
      var linkedCoord = linkedCells[i];
      
      oldSelTable[linkedCoord[0]][linkedCoord[1]] = emptyState;
      newSelTable[linkedCoord[0]][linkedCoord[1]] = emptyState;
    }
  }
  
  var diff    = cd.diffStateTables(oldSelTable, newSelTable, [newSelTable, cd._cellMap]);
  var diffMap = diff.coords;
  
  // Call selector callback
  var cbDiff   = cd.options.onCellSelectChange(diff.ref[0], diff.ref[1]);
  var newTable = cd.genStateTable(newSelTable);
  
  if (cbDiff) {
    for (var i=0; i<cbDiff.length; i++) {
      var coord = diffMap[i];
      newTable[coord[0]][coord[1]] = cbDiff[i];
    }
  }
  
  return newTable;
}

module.exports = addSelection;
