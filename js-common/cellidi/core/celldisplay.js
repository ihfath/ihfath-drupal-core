var domCells = require("./dom-cells");
var domWrapper = require("./dom-wrapper");

// CellDisplay class file

var displayCount = 0;

// CellDisplay constructor
function CellDisplay(options) {
  // Get LCES component
  lces.types.component.call(this);
  var that = this;
  
  // Set the CellDisplay's Cellidi id
  jSh.constProp(this, "cid", ++displayCount);
  
  // Extend base options
  var baseOptions = jSh.extendObj({}, this.options);
  this.options = jSh.extendObj(baseOptions, options);
  
  // Visual (non-virtual) Cell Mapping
  this._cellMap = [];
  
  // Virtual (non-visual) cell mapping
  this._cellVirtualMap = [];
  
  // Linked cells (for cellspans)
  this._linkedCells = [];
  
  // Wrapper Residue (for cleanup while rerendering)
  this._wrapperResidue = [];
  
  // Header map
  this.headers = {
    top: null,
    bottom: null,
    right: null,
    left: null,
    corner: null,
    
    // DOM element wrappers for top and bottom headers
    domTop: null,
    domBottom: null,
  };
  
  // Current state (overall data state)
  this._state       = this.genStateTable(this.options.emptyState || null);
  this._presetState = false;
  
  // Add model states
  if (options.model) {
    var model       = options.model;
    var modelStates = Object.getOwnPropertyNames(model);
    
    for (var i=0; i<modelStates.length; i++) {
      var modelStateName = modelStates[i];
      
      this.setState(modelStateName, model[modelStateName]);
    }
  }
  
  // Add events
  
  // 'newstate' event
  //
  // This event is triggered after a new state has completely propogated through
  // all the necessary protocols and is evidently different from the previous
  // state, a user can use this to maybe store the new state in some place, etc
  this.addEvent("newstate");
  
  // 'render' event
  //
  // This event is normally triggered by user code to indicate if and when the
  // CellDisplay should trigger a rerender (normally to update some visual
  // element's positions, or something of similar nature)
  this.addEvent("render");
  
  // 'activeinput' event
  this.addEvent("activeinput");
  
  // 'blurinput' event
  this.addEvent("blurinput");
  
  // 'rebuild' event
  //
  // This is triggered when `cb.rebuild(newStateTable)` is called and the cells
  // are reconstructed
  this.addEvent("rebuild");
  
  // Rebuild callback, called with the data to map cell-by-cell
  this.rebuildCb = null;
  
  // Add states
  
  // Auto adjusted state (for the user)
  this.setState("state", this._state);
  this.addStateListener("state", function(newState) {
    var offsetX = that.options.offsetX;
    var offsetY = that.options.offsetY;
    
    var newTable = that._shiftStateTable(newState, offsetX, offsetY);
    
    // Apply changes
    that.change(newTable);
  });
  
  // Change `state`'s get function
  var tmpStateTable = null;
  
  this.states.state.get = function() {
    // If tmp table is stale update it
    if (!tmpStateTable) {
      tmpStateTable = that._shiftStateTable(that._state, that.options.offsetX * -1, that.options.offsetY * -1);
    }
    
    return tmpStateTable;
  };
  
  // Update the `state` LCES state when `newstate` event is fired
  this.on("newstate", function() {
    tmpStateTable = null;
  });
  
  // Current hovered cell (model)
  this.setState("hoveredCell", null);
  
  // Current hovered header cell
  this.setState("hoveredHeaderCell", null);
  
  // Current (main) selected cell model
  this.setState("selectedCell", null);
}

jSh.inherit(CellDisplay, lces.types.component);

CellDisplay.prototype.rebuild = function(newStateTable) {
  if (!this.options.emptyState) {
    return false;
  }
  
  var oldWrapper = this.wrapper;
  
  // Cleanup
  this._wrapperCleanUp();
  
  // Set new state
  if (Array.isArray(newStateTable)) {
    this._state       = this.genStateTable(newStateTable, this.options.emptyState);
    this._presetState = true;
  } else {
    this._presetState = false;
  }
  
  // Rebuild wrapper
  this.wrapper = domWrapper(this, true);
  
  // Rebuild cells
  domCells(this);
  
  // Remove the old wrapper and add the new one
  this.options.element.removeChild(oldWrapper);
  this.options.element.appendChild(this.wrapper);
  
  this.triggerEvent("rebuild", {});
};

CellDisplay.prototype._wrapperCleanUp = function() {
  var residue = this._wrapperResidue;
  
  for (var i=0; i<residue.length; i++) {
    var curResidue = residue[i];
    
    switch (curResidue[0]) {
      case "dom":
        curResidue[1].removeEventListener(curResidue[2], curResidue[3]);
        break;
    }
  }
  
  this._wrapperResidue = [];
};

/**
 * _residueDom
 *
 * Internal mechanism to keep track of event listener functions that should
 * be removed on a rerender
 */
CellDisplay.prototype._residueDom = function(element, event, callback) {
  this._wrapperResidue.push([
    "dom", element, event, callback
  ]);
  
  element.addEventListener(event, callback);
};

CellDisplay.prototype._transformCoords = function(x, y, direction) {
  var dimensions = this.options.dimensions;
  var newX       = x + (this.options.offsetX * direction);
  var newY       = y + (this.options.offsetY * direction);
  
  var width  = dimensions[0];
  var height = dimensions[1];
  
  if (newX >= width) {
    newX = newX % width;
  } else if (newX < 0) {
    newX += width;
  }
  
  if (newY >= height) {
    newY = newY % height;
  } else if (newY < 0) {
    newY += height;
  }
  
  return [
    newX,
    newY
  ];
};

CellDisplay.prototype.fromVirtualCoords = function(x, y) {
  return this._transformCoords(x, y, -1);
};

CellDisplay.prototype.toVirtualCoords = function(x, y) {
  return this._transformCoords(x, y, 1);
};

CellDisplay.prototype._shiftStateTable = function(table, offsetX, offsetY) {
  var dimensions = this.options.dimensions;
  var width      = dimensions[0];
  var height     = dimensions[1];
  var newTable   = [];
  var column;
  
  for (var x=0; x<width; x++) {
    var newX = x + offsetX;
    
    if (newX >= width) {
      newX = newX % width;
    } else if (newX < 0) {
      newX += width;
    }
    
    if (offsetY === 0) {
      newTable.push(table[newX].slice());
    } else if (offsetY < 0) {
      column = table[newX];
      
      newTable.push(column.slice(height + offsetY).concat(column.slice(0, height + offsetY)));
    } else {
      column = table[newX];
      
      newTable.push(column.slice(offsetY).concat(column.slice(0, offsetY)));
    }
  }
  
  return newTable;
};

CellDisplay.prototype.getCellModelFromCoords = function(x, y) {
  var coords = [
    x, y
  ];
  
  return this._cellMap[coords[0]][coords[1]] || null;
};

CellDisplay.prototype.getCellModelFromDOM = function(dom) {
  var coords;
  
  // Return null if not a valid cell DOM
  if (!(coords = dom.cellidiCoords)) {
    return null;
  }
  
  return this._cellMap[coords[0]][coords[1]] || null;
};

CellDisplay.prototype.getCellState = function(cellModel) {
  var coords = cellModel.coords;
  
  return this._state[coords[0]][coords[1]];
};

CellDisplay.prototype.genStateTable = function(newValue, copyCallback) {
  // Just copy a current table
  if (Array.isArray(newValue)) {
    var copy = [];
    
    if (typeof copyCallback === "function") {
      for (var j=0; j<newValue.length; j++) {
        var curCol = newValue[j];
        var newCol = [];
        
        for (var k=0; k<curCol.length; k++) {
          newCol.push(copyCallback(true, curCol[k]));
        }
        
        copy.push(newCol);
      }
      
    } else {
      for (var i=0; i<newValue.length; i++) {
        copy.push(newValue[i].slice());
      }
    }
    
    return copy;
  }
  
  // Generate a generic table
  var dimensions = this.options.dimensions;
  var width      = dimensions[0];
  var height     = dimensions[1];
  var callback   = null;
  var initialState;
  
  if (typeof newValue === "function") {
    callback = newValue;
  } else {
    // Use newValue if defined
    if (newValue === undefined) {
      initialState = 0;
    } else {
      initialState = newValue;
    }
  }
  
  var table = [];
  
  for (var x=0; x<width; x++) {
    var column = [];
    
    for (var y=0; y<height; y++) {
      if (callback) {
        // Use value returned from callback instead of initialState
        column.push(callback(x, y));
      } else {
        column.push(initialState);
      }
    }
    
    table.push(column);
  }
  
  return table;
};

// Static method
CellDisplay.genStateTable = function(newValue, width, height, copyCallback) {
  return CellDisplay.prototype.genStateTable.call({
    options: {
      dimensions: [width, height]
    }
  }, newValue, copyCallback);
};

CellDisplay.prototype.diffStateTables = function(table1, table2, tableRefs, diffFunc) {
  var dimensions = this.options.dimensions;
  var width      = dimensions[0];
  var height     = dimensions[1];
  
  var diffCoordArray = [];
  var diffRefArray   = [];
  
  if (tableRefs) {
    for (var i=0; i<tableRefs.length; i++) {
      diffRefArray.push([]);
    }
  }
  
  for (var x=0; x<width; x++) {
    for (var y=0; y<height; y++) {
      var cell1  = table1[x][y];
      var cell2  = table2[x][y];
      var append = false;
      
      if (diffFunc) {
        append = diffFunc(cell1, cell2);
      } else {
        if (cell1 !== cell2) {
          append = true;
        }
      }
      
      if (append) {
        diffCoordArray.push([x, y]);
        
        if (tableRefs) {
          for (var i=0; i<tableRefs.length; i++) {
            diffRefArray[i].push(tableRefs[i][x][y]);
          }
        }
      }
    }
  }
  
  var returnObj = {
    coords: diffCoordArray
  };
  
  if (tableRefs) {
    returnObj.ref = diffRefArray;
  }
  
  // Return the arrays
  return returnObj;
};

CellDisplay.prototype.mapEachCell = CellDisplay.mapEachCell =
function mapEachCell(table, callback, tableRefs) {
  var width;
  var height;
  
  if (this !== CellDisplay) {
    var dimensions = this.options.dimensions;
    
    width  = dimensions[0];
    height = dimensions[1];
  } else {
    width  = table.length;
    height = table[0].length;
  }
  
  var newTable = [];
  
  for (var x=0; x<width; x++) {
    var column = [];
    
    for (var y=0; y<height; y++) {
      if (tableRefs) {
        var currentRefs = [];
        
        for (var i=0; i<tableRefs.length; i++) {
          currentRefs.push(tableRefs[i][x][y]);
        }
        
        column.push(callback(table[x][y], x, y, currentRefs));
      } else {
        // Call without table refs
        column.push(callback(table[x][y], x, y));
      }
    }
    
    newTable.push(column);
  }
  
  return newTable;
};

CellDisplay.prototype.change = function(newStateTable, noDiff) {
  var dimensions = this.options.dimensions;
  var width      = dimensions[0];
  var height     = dimensions[1];
  var newTable   = [];
  var cells      = this._cellMap;
  
  var tmpCoordArray = [];
  var tmpStateArray = [];
  var tmpCellArray  = [];
  
  if (noDiff) {
    // Render the whole thing
    for (var x=0; x<width; x++) {
      for (var y=0; y<height; y++) {
        tmpCoordArray.push([x, y]);
        tmpStateArray.push(newStateTable[x][y]);
        tmpCellArray.push(cells[x][y]);
      }
    }
  } else {
    // Diff out the changed parts to avoid unnecessary rendering
    var diffState = this.diffStateTables(newStateTable, this._state, [newStateTable, this._cellMap], this.options.diffStates);
    
    tmpCoordArray = diffState.coords;
    tmpStateArray = diffState.ref[0];
    tmpCellArray  = diffState.ref[1];
  }
  
  // Now render by calling user callback
  var cbDiff = this.options.onCellStateChange(tmpStateArray, tmpCellArray);
  
  // Apply diff if any
  if (cbDiff) {
    for (var i=0; i<cbDiff.length; i++) {
      var coord = tmpCoordArray[i];
      newStateTable[coord[0]][coord[1]] = cbDiff[i];
    }
  }
  
  // Set new state
  this._state = this.genStateTable(newStateTable, this.options.emptyState);
  
  // Trigger newstate event
  this.triggerEvent("newstate", {
    state: this.genStateTable(newStateTable, this.options.emptyState)
  });
  
  // Return stateTable
  return newStateTable;
};

CellDisplay.prototype.changeSubset = function(coords, newState, noDiff) {
  var newStateTable = this.genStateTable(this._state, this.options.emptyState);
  var oldState      = this._state;
  var callback;
  
  if (typeof newState === "function") {
    callback = newState;
  }
  
  // Apply changes
  for (var i=0; i<coords.length; i++) {
    var coord = coords[i];
    
    if (callback) {
      newStateTable[coord[0]][coord[1]] = callback(coord[0], coord[1], oldState[coord[0]][coord[1]]);
    } else {
      newStateTable[coord[0]][coord[1]] = newState;
    }
  }
  
  // Render complete table with changes
  this.change(newStateTable, noDiff);
  
  // Return table
  return newStateTable;
};

CellDisplay.prototype.changeSelection = function(newState) {
  if (this.options.canSelect) {
    var emptySelection = this.genStateTable(false);
    var diff           = this.diffStateTables(this.selection, emptySelection, [this._cellMap]);
    
    return this.changeSubset(diff.coords, newState);
  } else {
    return null;
  }
};

// Base options to be extended by user options
CellDisplay.prototype.options = {
  element: null,
  focus: null,
  name: null,
  
  dimensions: [0, 0],
  
  offsetX: 0,
  offsetY: 0,
  
  canSelect: false,
  maxSelect: 0,
  
  history: false,
  maxHistory: 60,
  
  clipboard: false,
  
  baseCellHeight: 40, // TODO: Use this throughout the codebase
  setCellWidth: true,
  
  headers: {},
  
  model: {},
  
  onCellSelectChange: null,
  onCellStateChange: null,
  newCell: null,
  diffStates: null,
  emptyState: null
};

// Set exports
module.exports = CellDisplay;
