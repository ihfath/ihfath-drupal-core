function IhfathMasterSlaveInstance(options) {
  if (jSh.type(options) !== "object")
    throw new Error("MasterSlaveInstance requires an options object as first arg");
  
  lces.types.component.call(this);
  
  // Settings
  this.masterDelay = 1000 * 60 * 0.35;
  this.updateDelay = 1000 * 60 * 0.1;
  this.delay       = this.masterDelay + 2000;
  this.gracePeriod = 2500;
  this.updateKey   = "lces-ihfath-master-slave";
  this.field       = "-" + options.field;
  this.masterCB    = options.poll;
  this.updateCB    = options.updateData;
  
  // Check if in dev environment
  this.isDev = location.host === "b-fusefactory.org";
  
  // states/properties
  this.setState("master", false);
  this.setState("id", "ihf" + (new Date().getTime()) + "-" + Math.random());
  
  this.pendingMaster = false;
  this.latestMaster  = null;
  this.timer         = null;
  
  // Init
  this.checkMaster();
  
  // Run update poll
  setInterval(() => {
    var data = this.getCurStateValue();
    
    if (data !== null) {
      this.updateCB.call(this, data);
    }
  }, this.updateDelay);
}

jSh.inherit(IhfathMasterSlaveInstance, lces.types.component);
jSh.extendObj(IhfathMasterSlaveInstance.prototype, {
  SYNC_NO_MASTER: 0,
  SYNC_HAS_MASTER: 1,
  SYNC_GAIN_MASTER: 2,
  SYNC_LOST_MASTER: 3,
  SYNC_CUR_MASTER: 4,
  
  setMaster(force, onChangeMaster) {
    if (force && !this.pendingMaster) {
      clearTimeout(this.timer);
      
      this.timer = setTimeout(() => {
        this.checkMaster();
      }, this.gracePeriod);
    }
    
    this.onChangeMaster = onChangeMaster;
    this.pendingMaster  = true;
    this.setCurState(true);
  },
  
  checkMaster() {
    var state = this.getCurState();
    
    if (this.isDev)
      jSh("title")[0].textContent = state.state + " " + this.id;
    
    switch (state.state) {
      case this.SYNC_NO_MASTER: {
        // Attempt to take over
        this.setMaster();
        
        this.timer = setTimeout(() => {
          this.checkMaster();
        }, this.gracePeriod / 2);
        break;
      }
      
      case this.SYNC_HAS_MASTER: {
        if (typeof this.onChangeMaster === "function") {
          this.onChangeMaster.call(this);
          this.onChangeMaster = null;
        }
        
        this.pendingMaster = false;
        this.timer = setTimeout(() => {
          this.checkMaster();
        }, this.delay);
        break;
      }
      
      case this.SYNC_GAIN_MASTER: {
        this.master = true;
        this.pendingMaster = false;
        
        if (typeof this.onChangeMaster === "function") {
          this.onChangeMaster.call(this);
          this.onChangeMaster = null;
        }
        
        // Fall-through
      }
      
      case this.SYNC_CUR_MASTER: {
        this.setCurState(false);
        
        // Run callback
        this.masterCB();
        
        this.timer = setTimeout(() => {
          this.checkMaster();
        }, this.masterDelay);
        break;
      }
      
      case this.SYNC_LOST_MASTER: {
        this.pendingMaster = false;
        this.master = false;
        
        this.timer = setTimeout(() => {
          this.checkMaster();
        }, this.delay);
        break;
      }
    }
  },
  
  getCurState() {
    var time     = new Date().getTime();
    var state    = localStorage.getItem(this.updateKey + this.field);
    var noMaster = false;
    var stateJSON;
    
    // stateJSON = {
    //   time: TIME,
    //   id: ID,
    //   request: 0 | 1
    // }
    
    // There is no master
    if (typeof state !== "string" || !state.trim()) {
      noMaster = true;
    } else {
      stateJSON = jSh.parseJSON(state);
    }
    
    // Error in JSON, or master is late, no master
    if (!noMaster) {
      if (stateJSON.error || !stateJSON.id) {
        noMaster = true;
      } else {
        // stateJSON is intact and valid
        this.latestMaster = stateJSON;
        
        if (time - stateJSON.time > this.delay + this.gracePeriod) {
          noMaster = true;
        } else {
          if (stateJSON.id === this.id) {
            if (stateJSON.request) {
              return {state: this.SYNC_GAIN_MASTER};
            } else {
              return {state: this.SYNC_CUR_MASTER};
            }
          } else if (this.master) {
            return {state: this.SYNC_LOST_MASTER};
          }
        }
      }
    }
    
    if (noMaster) {
      return {state: this.SYNC_NO_MASTER};
    } else {
      return {state: this.SYNC_HAS_MASTER};
    }
  },
  
  setCurState(request) {
    var time = new Date().getTime();
    
    localStorage.setItem(this.updateKey + this.field, JSON.stringify({
      time: time,
      id: this.id,
      request: Number(request)
    }));
  },
  
  setCurStateValue(value) {
    if (this.master) {
      localStorage.setItem(this.updateKey + this.field + "-value", JSON.stringify(value));
    }
  },
  
  getCurStateValue() {
    var curValue = localStorage.getItem(this.updateKey + this.field + "-value");
    var curValueJSON;
    
    // Value unset or corrupt
    if (!curValue) {
      return null;
    }
    
    curValueJSON = jSh.parseJSON(curValue);
    
    // Value invalid JSON
    if (curValueJSON.error) {
      return null;
    } else {
      return curValueJSON;
    }
  }
});
