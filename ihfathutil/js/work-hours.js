// Contained in this nice wrapper function to avoid external conflicts
window.addEventListener("DOMContentLoaded", function() {
  var isDev = location.host === "b-fusefactory.org";
  var initHalted = localStorage.getItem("ihfathwo-halted") == 1;
  
  var devTickDelay  = 5000;
  var prodTickDelay = 15000;
  
  // Utility functions
  function getTime() {
    return Math.floor(new Date().getTime() / 1000);
  }
  
  // Constructors
  function WorkHourUI() {
    // Init LCES component
    lces.types.component.call(this);
    var that = this;
    
    // Make DOM elements
    var mainHours   = jSh.d(".ihfathwo-hours.ihfathwo-visible", null, [jSh.c("span", ".ihfathwo-val"), jSh.c("span", ".ihfathwo-unit", "hr")]);
    var mainMins    = jSh.d(".ihfathwo-minutes.ihfathwo-visible", null, [jSh.c("span", ".ihfathwo-val"), jSh.c("span", ".ihfathwo-unit", "min")]);
    var mainButton  = jSh.d(".ihfathwo-toggle-btn.ihfathwo-recording", null, null, {title: "Stop recording work hours"});
    var recIndicate = jSh.d(".ihfathwo-rec-indicator");
    var mainBody    = this.mainBody = jSh.d(".ihfath-wo-main.ihfath-wo-hours.ihfath-wo-mins", null, [
      jSh.d(".ihfathwo-col", null, mainButton),
      jSh.d(".ihfathwo-col.ihfathwo-time", null, [
        mainHours,
        jSh.t(" "),
        mainMins
      ]),
      recIndicate
    ]);
    
    // Make button elements
    mainButton.appendChild(jSh.svg(null, 40, 40, [
      jSh.path(".ihfathwo-start-rec", "M8.59 35V5l25.98 15z"),
      jSh.path(".ihfathwo-stop-rec", "M35 35V5H5v30z")
    ]));
    
    // Add loading spinner
    mainButton.appendChild(jSh.svg(".ihfathwo-load-spinner", 40, 40, [
      jSh.path(null, "M27.494 7.02l-2.498 4.328c3.093 1.784 5 5.08 5.004 8.652-.005 3.57-1.913 6.866-5.006 8.648l2.496 4.327C32.13 30.3 34.992 25.355 35 20c-.005-5.356-2.866-10.303-7.506-12.98z")
    ]));
    
    // ==== Properties ====
    
    // Timer for "ticks"
    this.timer          = null;
    this.incrementTimer = null;
    this.lastSetDur     = 0;
    this.lastTime       = getTime();
    this.first          = true;
    this.canUpdate      = true;
    
    this.lastUpdateTimestamp = 0;
    
    // LCES states
    this.setState("expanded", false);
    this.setState("counting", false);
    this.setState("loading", false);
    this.setState("hoursToday", 0);
    
    // ==== Listeners ====
    
    // lces-state: expanded
    this.addStateListener("expanded", function(expanded) {
      if (expanded) {
        mainBody.classList.add("ihfathwo-visible");
      } else {
        mainBody.classList.remove("ihfathwo-visible");
      }
    });
    
    // lces-state: counting
    this.addStateListener("counting", (counting) => {
      if (counting) {
        mainButton.classList.add("ihfathwo-recording");
        recIndicate.classList.add("ihfathwo-recording");
        mainButton.title = "Stop recording work hours";
        this.incrementTick();
      } else {
        mainButton.classList.remove("ihfathwo-recording");
        recIndicate.classList.remove("ihfathwo-recording");
        mainButton.title = "Start recording work hours";
        clearTimeout(this.incrementTimer);
      }
    });
    
    this.addStateListener("loading", function(loading) {
      if (loading) {
        mainButton.classList.add("ihfathwo-loading");
      } else {
        mainButton.classList.remove("ihfathwo-loading");
      }
    });
    
    // lces-state: hoursToday
    this.addStateListener("hoursToday", function(hoursToday) {
      var hour = 60 * 60; // Hour in seconds
      var min  = 60; // Min in seconds
      
      if (hoursToday > hour) {
        var hours = Math.floor(hoursToday / hour);
        
        // Show hours thingy
        mainHours.classList.add("ihfathwo-visible");
        mainHours.getChild(0).textContent = hours;
        
        // Shave off hours
        hoursToday -= hour * hours;
      } else {
        // Hide hours thingy
        mainHours.classList.remove("ihfathwo-visible");
      }
      
      // Check for mins
      if (hours === undefined || hoursToday > min) {
        var mins = jSh.numOp(Math.floor(hoursToday / min), 0);
        
        mainMins.classList.add("ihfathwo-visible");
        mainMins.getChild(0).textContent = mins;
      } else {
        // Hide hours thingy
        mainMins.classList.remove("ihfathwo-visible");
      }
    });
    
    // Click event
    mainButton.addEventListener("click", () => {
      clearTimeout(this.timer);
      
      if (!this.loading) {
        if (!this.MSInstance.master) {
          this.MSInstance.setMaster(true, function() {
            if (this.master)
              that.send("trigger", null, function() {
                that.canUpdate = true;
              });
          });
        } else {
          that.send("trigger", null, function() {
            that.canUpdate = true;
          });
        }
        
        this.canUpdate = false;
        this.loading   = true;
      }
    });
    
    // Start ticking
    // if (!initHalted)
    //   this.tick();
    
    // Master/Slave ticking start
    this.MSInstance = new IhfathMasterSlaveInstance({
      field: "work-hours",
      
      updateData(data) {
        if (that.canUpdate && !this.pendingMaster) {
          if (!this.master && data.timestamp <= that.lastUpdateTimestamp) {
            return false;
          } else {
            that.lastUpdateTimestamp = data.timestamp;
          }
          
          var action = data.action;
          var first  = data.first;
          var res    = data.data;
          
          if (action === "trigger" || !res.state)
            that.hoursToday = res.duration;
          
          if (first && res.state) {
            that.hoursToday = res.duration + getTime() - res.last;
          }
          
          that.lastTime = res.last;
          that.lastSetDur = res.duration;
          that.counting = res.state;
          that.expanded = true;
          that.loading = false;
        }
      },
      
      poll() {
        that.send("state", that.first);
      }
    });
    
    // Add to page
    document.body.appendChild(mainBody);
  }
  
  jSh.inherit(WorkHourUI, lces.types.component);
  
  jSh.extendObj(WorkHourUI.prototype, {
    // halt() {
    //   clearTimeout(this.timer);
    //   clearTimeout(this.incrementTimer);
    //
    //   localStorage.setItem("ihfathwo-halted", 1);
    // },
    //
    // resume() {
    //   this.tick();
    //
    //   if (this.counting)
    //     this.incrementTick();
    //
    //   localStorage.setItem("ihfathwo-halted", 0);
    // },
    
    // tick() {
    //   this.send("state", this.first);
    //
    //   // this.timer = setTimeout(() => this.tick(), 15000);
    //   this.timer = setTimeout(() => this.tick(), isDev ? devTickDelay : prodTickDelay);
    //   this.first = false;
    // },
    
    incrementTick() {
      this.hoursToday = this.lastSetDur + getTime() - this.lastTime;
      
      this.incrementTimer = setTimeout(() => this.incrementTick(), 2000);
    },
    
    send(action, first, cb) {
      var date    = new Date();
      var curNode = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();
      var cur     = date.toJSON();
      var that    = this;
      
      var req = new lcRequest({
        uri: `${ isDev ? "/drupal" : "" }/ihfath/ajax/staffupdate/${ action }/${ cur }/${ curNode }`,
        success() {
          if (that.loading && action === "state")
            return false; // Ehh, what?
          
          var res = action !== "debug" ? jSh.parseJSON(this.responseText) : this.responseText;
          
          if (!res.error && action !== "debug") {
            that.lastUpdateTimestamp = new Date().getTime();
            
            that.MSInstance.setCurStateValue({
              timestamp: that.lastUpdateTimestamp,
              action: action,
              first: first,
              data: res
            });
          }
          
          if (action === "debug") {
            console.warn("WORKHOURSUI-DEBUG-DUMP");
            console.log(res);
          }
          
          // Resume ticking
          // if (action === "trigger") {
          //   that.tick();
          // }
          
          // Call callback now
          if (typeof cb === "function")
            cb();
        }
      });
      
      req.send();
    }
  });
  
  window.WorkHourUI = WorkHourUI;
  window.WHUITEST = new WorkHourUI();
});
