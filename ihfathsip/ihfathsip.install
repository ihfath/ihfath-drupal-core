<?php

function ihfathsip_enable() {
  // Create the ihfathsip_default_org variable
  if (variable_get("ihfathsip_default_org", NULL) === NULL) {
    variable_set("ihfathsip_default_org", "");
  }
}

function ihfathsip_uninstall() {
  // Cleanup
  variable_del("ihfathsip_default_org");
}

function ihfathsip_schema() {
  $schema = array();
  
  $schema["ihfath_sip_org"] = array(
    "dscription" => "{ihfath_sip_org} holds Ihfath OnSIP organization information",
    "fields"     => array(
      "oid" => array(
        "description" => "Organization ID",
        "type"        => "serial",
        "not null"    => TRUE,
        "unsigned"    => TRUE
      ),
      "name" => array(
        "description" => "Name of organization",
        "type"        => "varchar",
        "length"      => 255,
        "not null"    => TRUE
      ),
      "domain" => array(
        "description" => "SIP domain of organization",
        "type"        => "varchar",
        "length"      => 512,
        "not null"    => TRUE
      ),
      "org_id" => array(
        "description" => "OnSIP organization ID",
        "type"        => "int",
        "not null"    => TRUE,
        "unsigned"    => TRUE
      ),
      "username" => array(
        "description" => "Username for the admin of organization",
        "type"        => "varchar",
        "length"      => 255,
        "not null"    => TRUE
      ),
      "password" => array(
        "description" => "Password for the admin of the organization",
        "type"        => "varchar",
        "length"      => 255,
        "not null"    => TRUE
      ),
      "onsip_session" => array(
        "description" => "OnSIP Session ID",
        "type"        => "varchar",
        "length"      => 512,
        "not null"    => TRUE,
        "default"     => ""
      ),
      "onsip_session_time" => array(
        "description" => "Last Session Registered",
        "type"        => "int",
        "not null"    => TRUE,
        "unsigned"    => TRUE,
        "default"     => 0
      ),
    ),
    "primary key" => array("oid"),
  );
  
  $schema["ihfath_sip_user"] = array(
    "description" => "{ihfath_sip_user} holds OnSIP <-> Ihfath user bindings",
    "fields"      => array(
      "uid" => array(
        "description" => "User UID",
        "type"        => "int",
        "not null"    => TRUE,
        "unsigned"    => TRUE
      ),
      "oid" => array(
        "description" => "Organization ID",
        "type"        => "int",
        "not null"    => TRUE,
        "unsigned"    => TRUE
      ),
      "sip_uid" => array(
        "description" => "OnSIP UID",
        "type"        => "int",
        "not null"    => TRUE,
        "unsigned"    => TRUE
      ),
      "sip_auth_user" => array(
        "description" => "User OnSIP authorization username",
        "type"        => "varchar",
        "length"      => 255,
        "not null"    => TRUE
      ),
      "sip_name" => array(
        "description" => "User OnSIP username",
        "type"        => "varchar",
        "length"      => 255,
        "not null"    => TRUE
      ),
      "sip_pass" => array(
        "description" => "User SIP password",
        "type"        => "varchar",
        "length"      => 255,
        "not null"    => TRUE
      ),
      "onsip_pass" => array(
        "description" => "Password for OnSIP's UI",
        "type"        => "varchar",
        "length"      => 255,
        "not null"    => TRUE
      ),
    ),
    "primary key" => array("uid"),
    "indexes"     => array(
      "org_id" => array("oid")
    )
  );
  
  return $schema;
}
