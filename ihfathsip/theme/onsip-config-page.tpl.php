<?php
global $base_url;

$orgs = IhfathSip\Base::loadOrgs();
$default_org = IhfathSip\Base::getDefaultOrg();

$new_org_form = &$form["new_org"];
$checker = 0;
?>
<div class="config">
  <h2>OnSIP Organizations</h2>
  <table>
    <tbody>
      <tr>
        <th><?php echo t("Name"); ?></th>
        <th><?php echo t("Domain"); ?></th>
        <th style="width: 10%;"><?php echo t("Org ID"); ?></th>
        <th><?php echo t("Admin Username"); ?></th>
        <th><?php echo t("Admin Password"); ?></th>
        <th style="width: 5%;"><?php echo t("Default"); ?></th>
        <th><?php echo t("Users"); ?></th>
        <th><?php echo t("Actions"); ?></th>
      </tr>
      
      <?php
      if (count($orgs)) {
        foreach ($orgs as $org) {
          $checker++; ?>
          
          <tr class="<?php echo ($checker % 2) ? "even" : "odd"; ?>">
            <td><?php echo $org->name; ?></td>
            <td><?php echo $org->domain; ?></td>
            <td><?php echo $org->org_id; ?></td>
            <td><?php echo $org->username; ?></td>
            <td>***</td>
            <td><?php echo $org->oid == $default_org ? t("yes") : t("no"); ?></td>
            <td><?php echo IhfathSip\Base::getOrgUserCount($org->oid); ?></td>
            <td style="text-align: right;">
              <a href="<?php echo $base_url; ?>/admin/ihfath/config/onsip/default/<?php echo $org->oid; ?>"><?php echo t("set default"); ?></a> |
              <a href="<?php echo $base_url; ?>/admin/ihfath/config/onsip/delete/<?php echo $org->oid; ?>"><?php echo t("delete"); ?></a>
            </td>
          </tr>
          
        <?php
        }
      } else {
        $checker++; ?>
        
        <tr class="<?php echo ($checker % 2) ? "even" : "odd"; ?>">
          <td colspan="8"><center style="margin: 20px 0px;"><?php echo t("No organizations available"); ?></center></td>
        </tr>
        
      <?php
      }
      
      $checker++; ?>
      
      <tr class="<?php echo ($checker % 2) ? "even" : "odd"; ?>">
        <td colspan="8" class="ihfath-new-org-form">
          <h3><?php echo t("Create an OnSIP organization"); ?></h3>
          <hr>
          
          <?php echo render($new_org_form); ?>
          
        </td>
      </tr>
    </tbody>
  </table>
  
  <div class="ihfath-hidden-stuff">
    <?php echo drupal_render_children($form); ?>
  </div>
</div>
<style media="screen">
  .ihfath-hidden-stuff {
    display: none;
  }
  
  .ihfath-new-org-form {
    position: relative;
  }
  
  .form-item {
    margin: 10px 0px !important;
  }
  
  .form-item.form-type-textfield {
    display: inline-block;
    width: 45%;
  }
</style>
